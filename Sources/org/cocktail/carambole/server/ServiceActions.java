/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import java.net.URL;

import org.cocktail.carambole.server.components.BonDeCommande2;
import org.cocktail.carambole.server.components.DetailCommande;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.xml.EOXMLCommande;
import org.cocktail.fwkcktldepense.server.process.ProcessCommande;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver.xml.WOXMLDecoder;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

import er.extensions.eof.ERXEC;

public class ServiceActions extends DirectAction {

	public ServiceActions(WORequest aRequest) {
		super(aRequest);
	}

	public WOActionResults defaultAction() {
		return pageWithName("Main");
	}

	public WOActionResults creerCommandeAction() {

		Session session = (Session) context().session();
		WOXMLDecoder decoderCommande;
		EOXMLCommande laCommandeACreer = null;
		URL url = cktlApp.resourceManager().pathURLForResourceNamed("mapping/Commande.xml", "app", null);
		byte[] datas = cktlApp.resourceManager().bytesForResourceNamed("mapping/CommandeDataTest.xml", "app", null);
		WOResponse aRep = new WOResponse();

		aRep.setHeader("text/xml", "content-type");
		aRep.setStatus(200);

		try {

			decoderCommande = WOXMLDecoder.decoderWithMapping(url.toString());
			// Lecture du flux dans le content de la requete.
			if (request().content() != null && request().content().length() > 0) {
				laCommandeACreer = (EOXMLCommande) decoderCommande.decodeRootObject(request().content());
			}
			else {
				// Pour le developpement : lecture du flux XML sur systeme de fichiers
				laCommandeACreer = (EOXMLCommande) decoderCommande.decodeRootObject(new NSData(datas));
			}

			// Identification de l'utilisateur 
			EOUtilisateur utilisateur = null;
			boolean erreurLogin = false;
			boolean erreurBD = false;
			boolean erreurPassword = false;
			CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
			loggedUserInfo.compteForPersId(laCommandeACreer.utilisateur().persId(), true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				erreurLogin = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_COMPTE);
				erreurPassword = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_PASSWORD);
				erreurBD = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_SOURCE);
				if (loggedUserInfo.errorMessage() != null)
					CktlLog.rawLog(">> Erreur | " + loggedUserInfo.errorMessage());
			}

			if (erreurLogin || erreurBD || erreurPassword) {
				throw new Exception("Identification de l'utilisateur impossible");
			}
			else {
				session.setConnectedUserInfo(loggedUserInfo);
				String erreur = session.setConnectedUser(loggedUserInfo.login());
				if (erreur != null) {
					CktlLog.log("CONNEXION IMPOSSIBLE : " + erreur);
					throw new Exception(erreur);
				}
				else {
					//					NSArray utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), "Utilisateur", "persId", session.connectedUserInfo().persId());
					@SuppressWarnings("unchecked")
					NSArray<EOUtilisateur> utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", session.connectedUserInfo().persId());
					if (utilisateurs != null && utilisateurs.count() == 1) {
						utilisateur = (EOUtilisateur) utilisateurs.lastObject();
						if (utilisateur.isValide()) {
							CktlLog.log("user login : " + loggedUserInfo.login() + " - OK");
							session.setUtilisateur(utilisateur);
						}
						else {
							throw new Exception("Vous n'&ecirc;tes plus autoris&eacute; &agrave; utiliser cette application.");
						}
					}
					else {
						throw new Exception("Vous n'&ecirc;tes pas autoris&eacute; &agrave; utiliser cette application.");
					}
				}

			}
			// Creation de la commande a partir du xml
			FactoryCommande fc = new FactoryCommande();
			EOEditingContext edc = session.defaultEditingContext();
			EOCommande laNouvelleCommande = fc.creer(edc, laCommandeACreer, utilisateur);
			ProcessCommande.enregistrer(session.dataBus(), edc, laNouvelleCommande, utilisateur);
			aRep.appendContentString("<commande>");
			aRep.appendContentString("<numero>" + laNouvelleCommande.commNumero() + "</numero>");
			aRep.appendContentString("<exercice>" + laNouvelleCommande.exercice().exeExercice() + "</exercice>");
			aRep.appendContentString("</commande>");

		} catch (Exception e) {
			aRep.appendContentString("<commande>");
			aRep.appendContentString("<numero>-1</numero>");
			aRep.appendContentString("<erreur>" + e.getMessage() + "</erreur>");
			aRep.appendContentString("</commande>");
		}

		session.terminate();

		return aRep;
	}

	public WOActionResults modifierCommandeAction() {
		WOXMLDecoder decoderCommande;
		EOXMLCommande laCommandeAModifier = null;
		URL url = cktlApp.resourceManager().pathURLForResourceNamed("Commande.xml", "app", null);
		byte[] datas = cktlApp.resourceManager().bytesForResourceNamed("CommandeData.xml", "app", null);
		WOResponse aRep = new WOResponse();
		aRep.setStatus(200);

		try {

			decoderCommande = WOXMLDecoder.decoderWithMapping(url.toString());
			// Lecture du flux dans le content de la requete.
			if (request().content() != null && request().content().length() > 0) {
				laCommandeAModifier = (EOXMLCommande) decoderCommande.decodeRootObject(request().content());
			}
			else {
				// Pour le developpement : lecture du flux XML sur systeme de fichiers
				laCommandeAModifier = (EOXMLCommande) decoderCommande.decodeRootObject(new NSData(datas));
			}

			// Identification de l'utilisateur 
			EOUtilisateur utilisateur = null;
			boolean erreurLogin = false;
			boolean erreurBD = false;
			boolean erreurPassword = false;
			Session session = (Session) context().session();
			CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
			loggedUserInfo.compteForPersId(laCommandeAModifier.utilisateur().persId(), true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				erreurLogin = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_COMPTE);
				erreurPassword = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_PASSWORD);
				erreurBD = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_SOURCE);
				if (loggedUserInfo.errorMessage() != null)
					CktlLog.rawLog(">> Erreur | " + loggedUserInfo.errorMessage());
			}

			if (erreurLogin || erreurBD || erreurPassword) {
				throw new Exception("Identification de l'utilisateur impossible");
			}
			else {
				session.setConnectedUserInfo(loggedUserInfo);
				String erreur = session.setConnectedUser(loggedUserInfo.login());
				if (erreur != null) {
					CktlLog.log("CONNEXION IMPOSSIBLE : " + erreur);
					throw new Exception(erreur);
				}
				else {
					@SuppressWarnings("unchecked")
					NSArray<EOUtilisateur> utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", session.connectedUserInfo().persId());
					if (utilisateurs != null && utilisateurs.count() == 1) {
						utilisateur = (EOUtilisateur) utilisateurs.lastObject();
						if (utilisateur.isValide()) {
							CktlLog.log("user login : " + loggedUserInfo.login() + " - OK");
							session.setUtilisateur(utilisateur);
						}
						else {
							throw new Exception("Vous n'&ecirc;tes plus autoris&eacute; &agrave; utiliser cette application.");
						}
					}
					else {
						throw new Exception("Vous n'&ecirc;tes pas autoris&eacute; &agrave; utiliser cette application.");
					}
				}

			}
			// Creation de la commande a partir du xml
			EOEditingContext edc = ERXEC.newEditingContext();
			session.setNestedEdc(edc);
			EOExercice exercice = (EOExercice) EOUtilities.objectWithPrimaryKeyValue(edc, EOExercice.ENTITY_NAME, laCommandeAModifier.exercice());
			Number numero = laCommandeAModifier.numero();
			EOCommande commande = FinderCommande.getCommande(edc, exercice, numero);
			if (exercice != null && numero != null && commande != null) {
				session.setLaCommandeEnCours(commande);
				if (commande.isModifiable(edc, utilisateur, null)) {
					commande.reconstruireTableaux();
					if (laCommandeAModifier.prestationId() != null) {
						commande.setPrestationId(laCommandeAModifier.prestationId());
					}
					BonDeCommande2 nextPage = NavigationCtrl.goToBonDeCommande(context(), commande, null, "Informations");
					aRep = nextPage.generateResponse();
				}
				else if (commande.isConsultable(edc, utilisateur)) {
					DetailCommande nextPage = (DetailCommande) pageWithName("DetailCommande");
					nextPage.setLaCommande(commande);
					aRep = nextPage.generateResponse();
				}
				else {
					throw new Exception("Vous n'avez pas les droits suffisants pour visualiser ou modifier cette commande");
				}
			}
			else {
				throw new Exception("Aucune commande sur l'exercice " + exercice.exeExercice() + " avec le numero " + numero);
			}
		} catch (Exception e) {
			aRep.setHeader("text/xml", "content-type");
			aRep.appendContentString("<commande>");
			aRep.appendContentString("<numero>" + laCommandeAModifier.numero() + "</numero>");
			aRep.appendContentString("<erreur>" + e.getMessage() + "</erreur>");
			aRep.appendContentString("</commande>");
		}

		return aRep;
	}

	public WOActionResults detaillerCommandeAction() {
		WOXMLDecoder decoderCommande;
		EOXMLCommande laCommandeADetailler = null;
		URL url = cktlApp.resourceManager().pathURLForResourceNamed("Commande.xml", "app", null);
		byte[] datas = cktlApp.resourceManager().bytesForResourceNamed("CommandeData.xml", "app", null);
		WOResponse aRep = new WOResponse();
		aRep.setStatus(200);

		try {

			decoderCommande = WOXMLDecoder.decoderWithMapping(url.toString());
			// Lecture du flux dans le content de la requete.
			if (request().content() != null && request().content().length() > 0) {
				laCommandeADetailler = (EOXMLCommande) decoderCommande.decodeRootObject(request().content());
			}
			else {
				// Pour le developpement : lecture du flux XML sur systeme de fichiers
				laCommandeADetailler = (EOXMLCommande) decoderCommande.decodeRootObject(new NSData(datas));
			}

			// Identification de l'utilisateur 
			EOUtilisateur utilisateur = null;
			boolean erreurLogin = false;
			boolean erreurBD = false;
			boolean erreurPassword = false;
			Session session = (Session) context().session();
			CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
			loggedUserInfo.compteForPersId(laCommandeADetailler.utilisateur().persId(), true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				erreurLogin = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_COMPTE);
				erreurPassword = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_PASSWORD);
				erreurBD = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_SOURCE);
				if (loggedUserInfo.errorMessage() != null)
					CktlLog.rawLog(">> Erreur | " + loggedUserInfo.errorMessage());
			}

			if (erreurLogin || erreurBD || erreurPassword) {
				throw new Exception("Identification de l'utilisateur impossible");
			}
			else {
				session.setConnectedUserInfo(loggedUserInfo);
				String erreur = session.setConnectedUser(loggedUserInfo.login());
				if (erreur != null) {
					CktlLog.log("CONNEXION IMPOSSIBLE : " + erreur);
					throw new Exception(erreur);
				}
				else {
					@SuppressWarnings("unchecked")
					NSArray<EOUtilisateur> utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", session.connectedUserInfo().persId());
					if (utilisateurs != null && utilisateurs.count() == 1) {
						utilisateur = (EOUtilisateur) utilisateurs.lastObject();
						if (utilisateur.isValide()) {
							CktlLog.log("user login : " + loggedUserInfo.login() + " - OK");
							session.setUtilisateur(utilisateur);
						}
						else {
							throw new Exception("Vous n'&ecirc;tes plus autoris&eacute; &agrave; utiliser cette application.");
						}
					}
					else {
						throw new Exception("Vous n'&ecirc;tes pas autoris&eacute; &agrave; utiliser cette application.");
					}
				}

			}
			// Recherche de la commande a partir du xml
			EOEditingContext edc = ERXEC.newEditingContext();
			session.setNestedEdc(edc);
			EOExercice exercice = (EOExercice) EOUtilities.objectWithPrimaryKeyValue(edc, EOExercice.ENTITY_NAME, laCommandeADetailler.exercice());
			Number numero = laCommandeADetailler.numero();
			EOCommande commande = FinderCommande.getCommande(edc, exercice, numero);
			if (exercice != null && numero != null && commande != null) {
				session.setLaCommandeEnCours(commande);
				if (commande.isConsultable(edc, utilisateur)) {
					DetailCommande nextPage = (DetailCommande) pageWithName("DetailCommande");
					nextPage.setLaCommande(commande);
					aRep = nextPage.generateResponse();
				}
				else {
					throw new Exception("Vous n'etes pas autorise a visualiser cette commande");
				}
			}
			else {
				throw new Exception("Aucune commande sur l'exercice " + exercice.exeExercice() + " avec le numero " + numero);
			}
		} catch (Exception e) {
			aRep.appendContentString("<commande>");
			aRep.appendContentString("<numero>" + laCommandeADetailler.numero() + "</numero>");
			aRep.appendContentString("<erreur>" + e.getMessage() + "</erreur>");
			aRep.appendContentString("</commande>");
		}

		return aRep;
	}

}
