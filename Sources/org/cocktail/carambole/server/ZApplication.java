/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktldepense.server.metier.EOParametre;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrintConst;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.jdbcadaptor.JDBCContext;

import er.extensions.foundation.ERXProperties;

public abstract class ZApplication extends CocktailAjaxApplication {

	/**
	 * @return boolèen bloquant: si false, le dèmarrage de l'application s'interrompt !
	 */
	protected abstract boolean initApplicationSpecial();

	//protected abstract String[] requiredParams();

	protected abstract String[] maquettesSix();

	protected abstract String txtAppliVersion();

	protected abstract String parametresTableName();

	protected abstract String appliId();

	protected SAUTClient sautClient;
	NSMutableDictionary dicoBdConnexionServerName;
	NSMutableDictionary dicoBdConnexionServerId;
	//on n'initialise pas le sharedEditingContext pour tenter de resoudre des pbs de lock sur certains objets (EOCompte)
	//protected EOSharedEditingContext mySharedEditingContext = EOSharedEditingContext.defaultSharedEditingContext();
	private NSMutableDictionary appParametres;

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#initApplication()
	 */
	public final void initApplication() {
		System.out.println("Lancement de l'application serveur " + this.appliId() + " !");

		super.initApplication();

		boolean ok1 = init();
		boolean ok2 = checkUp();

		if (!ok2) {
			CktlLog.rawLog("");
			CktlLog.rawLog("Il y a eu des ERREURS CRITIQUES D'INITIALISATION, impossible de démarrer l'application (voir ci-dessus pour les détails) !!!");
			CktlLog.rawLog("");
			terminate();
			System.exit(-1);
		}

		boolean ok3 = initApplicationSpecial();
		if (!ok3) {
			CktlLog.rawLog("");
			CktlLog.rawLog("Il y a eu des ERREURS CRITIQUES D'INITIALISATION, impossible de démarrer l'application (voir ci-dessus pour les détails) !!!");
			CktlLog.rawLog("");
			terminate();
			System.exit(-1);
		}

		if (!ok1) {
			CktlLog.rawLog("");
			CktlLog.rawLog("Il y a eu des ERREURS D'INITIALISATION !!!");
			CktlLog.rawLog("");
		}

		CktlLog.rawLog("INITIALISATION TERMINEE");
	}

	private boolean init() {
		boolean ok = true;

		// full logs?
		if (getParamBoolean("SHOWSQLLOGS")) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupSQLGeneration);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupDatabaseAccess);
		}

		// init SAUT
		if (getParam("SAUT_URL") != null) {
			initSAUTClient(getParam("SAUT_URL"));
		}

		return ok;
	}

	/**
	 * @return true si tout est ok, false si il y a des warnings
	 */
	private boolean checkUp() {
		// Versions...
		CktlLog.log("Versions");
		//CktlLog.log("WebObjects: " + getWOVersion());
		CktlLog.log("Java Runtime: " + System.getProperty("java.version"));
		CktlLog.log(name() + ": " + txtAppliVersion());

		// Propriètès système...
		if ("1".equals(getParam("DISPLAY_SYSTEM_PROPERTIES"))) {
			CktlLog.log("Proprietes systeme\n" + System.getProperties());
		}

		// Base de donnèes...
		boolean ok1 = checkBdConnection();
		if (ok1) {
			// Vèrif des modèles...
			rawLogModelInfos();
			ok1 = checkModel();
			if (ok1) {

				// Verification de la version de la base...
				//ok1 = checkDbVersion();
				//Controle de version effectué en même temps que controle des frmks
				// Verification du serveur SIX et de la presence des maquettes SIX s'il y en a...
				if (ERXProperties.booleanForKey("org.cocktail.carambole.bondecommande.impression.six")) {
					checkSix();
				}
				else {
					CktlLog.log("org.cocktail.carambole.bondecommande.impression.six à false, le contrôle d'accès au service SIX n'est pas vérifié.");
				}
			}
		}

		return ok1;
	}

	/**
	 * Compare 2 numèros de version donnès sous la forme de tableaux de int. Chaque numèro de version peut contenir autant d'èlèments que l'on veut.<br>
	 * 2 numèros null ou vides sont considèrès ègaux.<br>
	 * 1 numèro null est considèrè infèrieur è l'autre non null.<br>
	 * 1 numèro vide est considèrè infèrieur è l'autre non vide.<br>
	 * <b>ATTENTION:<b> 1.2.0 est considèrè ègal è 1.2 !!!
	 * 
	 * @param version
	 * @param anotherVersion
	 * @return Un entier nègatif, zèro, ou un entier positif si le premier argument est infèrieur, ègal è, ou supèrieur au second.
	 */
	public static int compare(final int[] version, final int[] anotherVersion) {
		if (version == null && anotherVersion == null) {
			return 0;
		}
		if (version == null) {
			return -1;
		}
		if (anotherVersion == null) {
			return 1;
		}
		for (int i = 0; i < version.length && i < anotherVersion.length; i++) {
			if (version[i] < anotherVersion[i]) {
				return -1;
			}
			if (version[i] > anotherVersion[i]) {
				return 1;
			}
		}
		if (version.length < anotherVersion.length) {
			for (int i = version.length; i < anotherVersion.length; i++) {
				if (anotherVersion[i] > 0) {
					return -1;
				}
			}
		}
		if (version.length > anotherVersion.length) {
			for (int i = anotherVersion.length; i < version.length; i++) {
				if (version[i] > 0) {
					return 1;
				}
			}
		}
		return 0;
	}

	public void compareFwkVersion(String fwkName, int[] fwkVersion, int[] minVersion) throws Exception {
		CktlLog.log(fwkName, toString(fwkVersion) + " (minimum requis: " + toString(minVersion) + ")");
		if (compare(fwkVersion, minVersion) < 0) {
			throw new Exception("Version de framework " + fwkName + " incompatible (version de fwk utilisèe: " + toString(fwkVersion)
					+ ", version minimum requise par l'application: " + toString(minVersion) + ")");
		}
	}

	private static String toString(int[] array) {
		if (array == null) {
			return "";
		}
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < array.length; i++) {
			sb = sb.append(array[i]);
			sb = sb.append('.');
		}
		if (sb.length() > 0) {
			sb = sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	/**
	 * Récupère une valeur dans la table parametresTableName(). Si elle n'existe pas dans la table, va la chercher dans le configFileName(), et si
	 * n'existe pas va dans la table configTableName()
	 * 
	 * @param paramKey La clé à rechercher
	 * @return La première valeur associée à la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public String getParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}

	/**
	 * Récupère x valeur(s) dans la table parametresTableName(). Si elle n'existe pas dans la table, va la chercher dans le configFileName(), et si
	 * n'existe pas va dans la table configTableName()
	 * 
	 * @param paramKey La clé à rechercher
	 * @return La(les) valeur(s) associée(s) à la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public NSArray getParams(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			a = config().valuesForKey(paramKey);
		}
		if (a == null) {
			a = new NSArray();
		}
		return a;
	}

	/**
	 * Récupère une valeur dans la table parametresTableName(). A n'utiliser que pour récupérer des paramètres spécifiques à l'application, et si on
	 * ne veut pas aller chercher le paramètre ailleurs que dans la table de paramétrage de l'application.
	 * 
	 * @param paramKey La clé à rechercher
	 * @return La première valeur associée à la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public String getAppParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a != null && a.count() > 0) {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}

	/**
	 * Récupère x valeur(s) dans la table parametresTableName(). A n'utiliser que pour récupérer des paramètres spécifiques à l'application, et si on
	 * ne veut pas aller chercher les paramètres ailleurs que dans la table de paramétrage de l'application.
	 * 
	 * @param paramKey La clé à rechercher
	 * @return La(les) valeur(s) associée(s) à la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public NSArray getAppParams(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		if (a == null) {
			a = new NSArray();
		}
		return a;
	}

	public boolean getParamBoolean(String paramKey) {
		return getParamBoolean(paramKey, false);
	}

	public boolean getParamBoolean(String paramKey, boolean defaultValueWhenNotFound) {
		String s = getParam(paramKey);
		if (s == null) {
			return defaultValueWhenNotFound;
		}
		if (s.equals("1") || s.equalsIgnoreCase("O") || s.equalsIgnoreCase("OUI") || s.equalsIgnoreCase("Y") || s.equalsIgnoreCase("YES")
				|| s.equalsIgnoreCase("TRUE")) {
			return true;
		}
		return false;
	}

	public String hrefContactMail() {
		return "mailto:" + contactMail();
	}

	public String appHtmlCssStyles() {
		return getParam("APP_HTML_CSS_STYLES");
	}

	public boolean appUseCas() {
		return getParamBoolean("APP_USE_CAS");
	}

	public boolean allowWebAccess() {
		return getParamBoolean("ALLOW_WEB_ACCESS", true);
	}

	public void resetAppParametres() {
		appParametres = null;
	}

	private void initSAUTClient(String url) {
		try {
			sautClient = new SAUTClient(url);
		} catch (Exception e) {
			CktlLog.log("L'initialisation du SAUTClient a echouee. Pas de SAUT trouve a l'URL " + url);
			sautClient = null;
		}
	}

	protected SAUTClient sautClient() {
		return sautClient;
	}

	protected Hashtable getUserAuthentication(String usrLogin, String usrPass) {
		// if (sautClient() != null && usrPass != null) {
		// String userAuthentication = sautClient().requestDecryptedUserInfo(usrLogin, usrPass, getParam("APP_ALIAS"));
		// if (userAuthentication == null) {
		// Hashtable h = new Hashtable();
		// h.put("err", "Erreur lors de la tentative d'identification, le serveur SAUT a renvoye null");
		// return h;
		// }
		// Properties userId = SAUTClient.toProperties(userAuthentication);
		// if (!userId.get("errno").equals("0")) {
		// Hashtable h = new Hashtable();
		// h.put("err", "Erreur, identification incorrecte : (" + userId.get("errno").toString() + ") " + userId.get("msg"));
		// return h;
		// }
		// return userId;
		// }
		// else {
		CktlUserInfoDB info = new CktlUserInfoDB(dataBus());
		if (usrPass == null) {
			System.out.println("!!! Connexion de " + usrLogin + " sans mot de passe !!!");
			info.setAcceptEmptyPass(true);
		}
		info.compteForLogin(usrLogin, usrPass, true);
		Hashtable h = new Hashtable();
		switch (info.errorCode()) {
		case CktlUserInfoDB.ERROR_COMPTE:
			h.put("err", "Login inconnu !");
			return h;
		case CktlUserInfoDB.ERROR_PASSWORD:
			h.put("err", "Mot de passe incorrect !");
			return h;
		case CktlUserInfoDB.ERROR_INDIVIDU:
			h.put("err", "Pas d'individu associè a ce login ! " + info.errorMessage());
			return h;
		case CktlUserInfoDB.ERROR_SOURCE:
			h.put("err", "Erreur lors de la tentative d'authentification, peut-ètre un pb de connection è la base de donnèes ! "
					+ info.errorMessage());
			return h;
		case CktlUserInfoDB.ERROR_NONE:
			return info.toHashtable();
		default:
			h.put("err", "Erreur lors de l'authentification (erreur non rècupèrèe) ! " + info.errorMessage());
			return h;
		}
		// }
	}

	public boolean sendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, String filename, NSData filedata) {
		if (getParamBoolean("TEST_MODE") && getParam("TEST_EMAIL") != null && getParam("TEST_EMAIL").length() > 0) {
			String preamble = "Vous recevez ce message car l'adresse \"" + getParam("TEST_EMAIL")
					+ "\" est l'adresse de test spècifièe dans le fichier de configuration de l'application " + this.appliId() + ".\n\n";
			preamble = preamble + "Normalement le message devrait ètre adressè è :\n";
			preamble = preamble + "To : " + mailTo + "\n";
			preamble = preamble + "Cc : " + mailCC + "\n\n";
			preamble = preamble + "------- Message original -------\n";
			mailBody = preamble + mailBody;

			mailTo = getParam("TEST_EMAIL");
			mailCC = null;
		}
		mailBody = mailBody + "\n\n" + "---------------------------------------------------\n"
				+ "Ce message a ètè gènèrè automatiquement par l'application " + this.appliId();
		return mailBus().sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata);
	}

	/**
	 * @return Le nè de version de WebObjects
	 */
	private String getWOVersion() {
		try {
			final InputStream is = resourceManager().inputStreamForResourceNamed("version.plist", "JavaWebObjects", null);
			final NSData data = new NSData(is, 1024);
			final Object plist = NSPropertyListSerialization.propertyListFromData(data, null);
			final String list = NSPropertyListSerialization.stringFromPropertyList(plist);
			final NSDictionary dico = NSPropertyListSerialization.dictionaryForString(list);
			return (String) dico.valueForKey("CFBundleShortVersionString") + "." + (String) dico.valueForKey("BuildVersion");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//	private boolean checkModels() {
	//		try {
	//			// On rècupère tous les modèles utilisès
	//			dicoBdConnexionServerName = new NSMutableDictionary();
	//			dicoBdConnexionServerId = new NSMutableDictionary();
	//			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
	//			for (int i = 0; i < vModelGroup.models().count(); i++) {
	//				EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
	//				dicoBdConnexionServerName.takeValueForKey(bdConnexionServerName(tmpEOModel), tmpEOModel.name());
	//				dicoBdConnexionServerId.takeValueForKey(bdConnexionServerId(tmpEOModel), tmpEOModel.name());
	//				CktlLog.log("");
	//				CktlLog.log("Modele " + tmpEOModel.name() + " : ");
	//				CktlLog.log("  * Connexion base de donnees: " + tmpEOModel.connectionDictionary().valueForKey("URL"));
	//				CktlLog.log("  * Instance: " + bdConnexionServerId(tmpEOModel));
	//				CktlLog.log("  * User base de donnees: " + tmpEOModel.connectionDictionary().valueForKey("username"));
	//			}
	//
	//			// Vèrifier que tous les modèles sont sur le mème serverId
	//			String sid = null;
	//			boolean erreurSid = false;
	//			for (int i = 0; i < vModelGroup.models().count(); i++) {
	//				EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
	//				if (sid == null) {
	//					sid = bdConnexionServerId(tmpEOModel);
	//				}
	//				else if (bdConnexionServerId(tmpEOModel) != null && !sid.toUpperCase().equals(bdConnexionServerId(tmpEOModel).toUpperCase())) {
	//					erreurSid = true;
	//				}
	//			}
	//			if (erreurSid) {
	//				CktlLog.log("Les modeles pointent vers differentes instances de base de donnees (cf. ci-dessus), ceci peut causer des problemes d'incoherence lors de l'execution.");
	//			}
	//			return !erreurSid;
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//			return false;
	//		}
	//	}

	private boolean checkBdConnection() {
		CktlLog.log("Base de donnees");
		if (CktlDataBus.isDatabaseConnected()) {
			CktlLog.log("La connexion a la base de donnees est active");
			return true;
		}
		else {
			CktlLog.log("La connexion a la base de donnees n'est pas active!!");
			return false;
		}
	}

	public String bdConnexionInfo() {
		String info = (String) dicoBdConnexionServerName.valueForKey(mainModelName());
		if (info != null && info.length() > 0) {
			info = "@" + info;
		}
		info = (String) dicoBdConnexionServerId.valueForKey(mainModelName()) + info;
		return info;
	}

	/**
	 * Renvoie le serverid de la base de donnèes (par exemple gest).
	 */
	private String bdConnexionServerId(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverBdName = null;
		if (parts != null && parts.length > 1) {
			serverBdName = parts[parts.length - 1];
		}
		return serverBdName;
	}

	/**
	 * Renvoie le serverName de la base de donnèes (par exemple jane).
	 */
	private String bdConnexionServerName(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverName = null;
		if (parts != null && parts.length > 0) {
			serverName = parts[0];
		}
		return serverName;
	}

	private String[] bdSecondPartUrl(EOModel model) {
		String url = (String) model.connectionDictionary().valueForKey("URL");
		if (url == null || url.length() == 0) {
			return new String[0];
		}
		String[] res;
		// L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
		// On sèpare la partie jdbc de la partie server
		res = url.split("@");
		if (res.length > 1) {
			String serverUrl = res[1];
			res = serverUrl.split(":");
			if (res.length > 0) {
				return res;
			}
		}
		return new String[0];
	}

	//	/**
	//	 * Verifie la version de la base de donnee par rapport a la version minimum requise pour cette version de l'application
	//	 */
	//	private boolean checkDbVersion() {
	//		if (org.cocktail.depense.server.Version.minAppliBdVersion() == null) {
	//			return true;
	//		}
	//		CktlLog.log("Verification de la version de la base...");
	//		CktlLog.log("Version minimum requise: " + org.cocktail.depense.server.Version.minAppliBdVersion());
	//		String ver = getAppliBdVersion();
	//		if (ver == null) {
	//			CktlLog.log("Version utilisee", "N/A");
	//			CktlLog.log("Impossible de dèterminer la version de la base utilisèe (table DB_VERSION vide). Cela risque de poser des problèmes! Mettre la base è jour.");
	//			CktlLog.log("On ne lance pas l'application tant que ce problème n'est pas rèsolu !!! Dèsolè !");
	//			terminate();
	//			return false;
	//		}
	//		else {
	//			CktlLog.log("Version utilisee: " + ver);
	//			if (ver.compareTo(org.cocktail.depense.server.Version.minAppliBdVersion()) >= 0) {
	//				CktlLog.log("Test de version OK.");
	//				return true;
	//			}
	//			else {
	//				CktlLog.log("Version de base incompatible. Cela risque de poser des problemes! Mettre la base a jour (version minimum: "
	//						+ org.cocktail.depense.server.Version.minAppliBdVersion() + ") pour utiliser cette version de l'application.");
	//				return false;
	//			}
	//		}
	//	}

	/**
	 * Rècupère la version actuelle de la base de donnees avec un rawRowsForSQL
	 */
	//	public String getAppliBdVersion() {
	//		String a = org.cocktail.depense.server.Version.getBdVersion(null);
	//		//		try {
	//		//			a = EOUtilities.rawRowsForSQL(mySharedEditingContext, mainModelName(),
	//		//					"select db_version from db_version where db_date = (select max(db_date) from db_version)");
	//		//			return (String) ((NSDictionary) a.lastObject()).valueForKey("DB_VERSION");
	//		//		}
	//		//		catch (Exception e) {
	//		//			return null;
	//		//		}
	//
	//		return a;
	//	}

	//	/**
	//	 * Vèrifie si tous les paramètres nècessaires è l'application sont bien prèsents et initialisès.
	//	 */
	//	private boolean checkRequiredParams() {
	//		boolean ok = true;
	//		if (requiredParams() == null || requiredParams().length == 0) {
	//			return ok;
	//		}
	//		CktlLog.log("Verification des parametres");
	//		for (int i = 0; i < requiredParams().length; i++) {
	//			String p = getParam(requiredParams()[i]);
	//			if (p == null || p.trim().length() == 0) {
	//				CktlLog.log("Le parametre " + requiredParams()[i] + " n'est pas renseigne dans la configuration. ");
	//				CktlLog.log("Veuillez l'ajouter soit:");
	//				CktlLog.log("		Dans le fichier de configuration: " + configFileName());
	//				CktlLog.log("		Dans la table des parametres de jefy_depense: " + parametresTableName());
	//				CktlLog.log("		Dans la table des parametres des applications: " + configTableName());
	//
	//				ok = false;
	//			}
	//			else {
	//				// On affiche la paire clè/valeur dans le log
	//				CktlLog.log(requiredParams()[i] + ": " + p);
	//			}
	//		}
	//		CktlLog.log("");
	//		return ok;
	//	}

	private boolean checkSix() {
		boolean ok = true;

		if (maquettesSix() != null && maquettesSix().length > 0) {
			try {
				CktlLog.log("Verification du dialogue avec le service d'impression");
				CktlPrinter printer = checkIfPrintServiceAvailable(config());
				String sixServiceHost = getParam("SIX_SERVICE_HOST");
				String sixServicePort = getParam("SIX_SERVICE_PORT");
				for (int i = 0; i < maquettesSix().length; i++) {
					try {
						String maquetteSix = getParam(maquettesSix()[i]);
						checkIfReportsExists(sixServiceHost, sixServicePort, printer, maquettesSix()[i], maquetteSix);
						CktlLog.log("Le modele d'impression reference par le parametre " + maquettesSix()[i] + " (=" + maquetteSix
								+ ") est accessible");
					} catch (Exception e) {
						ok = false;
						CktlLog.log(e.getMessage());
					}
				}
			} catch (Exception e) {
				ok = false;
				CktlLog.log(e.getMessage());
			}
		}

		return ok;
	}

	/**
	 * Vèrifie la disponibilitè du service d'impression.
	 */
	private CktlPrinter checkIfPrintServiceAvailable(CktlConfig config) throws Exception {
		CktlPrinter printer = null;
		try {
			printer = CktlPrinter.newDefaultInstance(config);
			CktlLog.log("Le moteur d'impression est accessible");
		} catch (ClassNotFoundException e) {
			throw new Exception(
					"La classe necessaire a l'appel au service d'impression n'est pas accessible (parametre XML_PRINTER_DRIVER). Les frameworks ne doivent pas etre a jour.");
		} catch (Exception e1) {
			throw new Exception(
					"Impossible d'initialiser le client du service d'impression. Certains parametres peuvent contenir des valeurs erronees (parametres commencant par SIX_).");
		}

		if (printer != null) {
			Dictionary dicoprint = printer.checkService();
			if (dicoprint == null) {
				throw new Exception("Le service d'impression est indisponible.");
			}
			CktlLog.log("Le service d'impression est disponible.");
			CktlLog.log("SERVICE_NAME: " + dicoprint.get(CktlPrintConst.SERVICE_NAME_KEY));
			CktlLog.log("SERVICE_VERSION_KEY: " + dicoprint.get(CktlPrintConst.SERVICE_VERSION_KEY));
			CktlLog.log("SERVICE_DESCRIPTION_KEY: " + dicoprint.get(CktlPrintConst.SERVICE_DESCRIPTION_KEY));
			CktlLog.log("");
			return printer;
		}
		return null;
	}

	/**
	 * Vèrifie si le modèle d'impression est bien disponible.
	 */
	private void checkIfReportsExists(String sixServiceHost, String sixServicePort, CktlPrinter printer, String maquetteParam, String maquetteID)
			throws Exception {
		if (!printer.checkTemplate(maquetteID)) {
			switch (printer.getErrorCode()) {
			case CktlPrintConst.ERR_CONNECT:
				throw new Exception("Le service SIX " + sixServiceHost + " : " + sixServicePort + " n'est pas disponible.");

			case CktlPrintConst.ERR_NO_TEMPLATE:
				throw new Exception("Le modele d'impression " + maquetteID + " (reference par le parametre " + maquetteParam
						+ ") n'est pas reference dans la base de donnees du service d'impression.");

			case CktlPrintConst.ERR_OK:
				break;

			default:
				throw new Exception("Une erreur est survenue lors d'une operation SIX. Code erreur : " + printer.getErrorCode());
			}
		}
	}

	/**
	 * Rècupère le contenu du fichier filePath
	 */
	public String fileContent(String filePath) {
		try {
			File f = new File(filePath);
			int size = (int) f.length();
			FileInputStream in = new FileInputStream(f);
			byte[] data = new byte[size];
			in.read(data);
			in.close();
			return new String(data);
		} catch (FileNotFoundException fnfe) {
			System.err.println("Fichier " + filePath + " introuvable !");
			return "";
		} catch (IOException ioe) {
			System.err.println("Erreur lors de la lecture du fchier " + filePath + " : " + ioe);
			return "";
		}
	}

	public String getReportsLocation(String fileName) {
		String param = getParam("REPORTS_LOCATION");
		if (param != null) {
			return param + "/" + fileName;
		}
		else {

			WOResourceManager rm = application().resourceManager();
			URL url = rm.pathURLForResourceNamed("Properties", null, null);
			if (url == null) {
				return null;
			}
			String resourcesRoot = new File(url.getPath()).getParent();
			return resourcesRoot.concat("/").concat("Reports/").concat(fileName);
		}
	}

	/**
	 * Fournit la connection JDBC utilisee par un model pour acceder a la base de donnees.
	 * 
	 * @param modelName Nom du model a partir duquel on veut recuperer la connection JDBC (ex: "Pie" pour Pie.eomodeld). Passer <code>null</code> pour
	 *            obtenir la connexion par defaut.
	 * @return La connexion JDBC associee au model specifie, ou la connexion par defaut.
	 */
	public Connection getJDBCConnection(final String modelName) {

		Connection conn = null;
		if (modelName != null) {
			conn = ((JDBCContext) EODatabaseContext.registeredDatabaseContextForModel(EOModelGroup.defaultGroup().modelNamed(modelName),
					new EOEditingContext()).availableChannel().adaptorChannel().adaptorContext()).connection();
			// System.out.println("connection : "+conn);
		}
		else
			conn = getJDBCConnection();

		return conn;
	}

	public Connection getJDBCConnection() {
		return ((JDBCContext) getAdaptorContext()).connection();
	}

	public EOAdaptorContext getAdaptorContext() {
		return CktlDataBus.databaseContext().availableChannel().adaptorChannel().adaptorContext();
	}

	public WOComponent getSessionTimeoutPage(WOContext context) {
		return null;
	}

	/**
	 * @return Les parametres de l'application stockés dans la table parametresTableName()
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			//			if (mySharedEditingContext == null || parametresTableName() == null) {
			//				return new NSMutableDictionary();
			//			}
			if (parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			try {
				NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
				String previousParamKey = null;
				NSMutableArray a = null;
				java.util.Enumeration enumerator = vParam.objectEnumerator();
				while (enumerator.hasMoreElements()) {
					EOGenericRecord vTmpRec = (EOGenericRecord) enumerator.nextElement();
					if (vTmpRec.valueForKey(EOParametre.PAR_KEY_KEY) == null
							|| ((String) vTmpRec.valueForKey(EOParametre.PAR_KEY_KEY)).equals("")
							|| vTmpRec.valueForKey(EOParametre.PAR_VALUE_KEY) == null) {
						continue;
					}
					if (!((String) vTmpRec.valueForKey(EOParametre.PAR_KEY_KEY)).equalsIgnoreCase(previousParamKey)) {
						if (a != null && a.count() > 0) {
							appParametres.setObjectForKey(a, previousParamKey);
						}
						previousParamKey = (String) vTmpRec.valueForKey(EOParametre.PAR_KEY_KEY);
						a = new NSMutableArray();
					}
					if (vTmpRec.valueForKey(EOParametre.PAR_VALUE_KEY) != null) {
						a.addObject((String) vTmpRec.valueForKey(EOParametre.PAR_VALUE_KEY));
					}
				}
				if (a != null && a.count() > 0) {
					appParametres.setObjectForKey(a, previousParamKey);
				}
				//mySharedEditingContext.invalidateAllObjects();
			} catch (Exception e) {
				appParametres = new NSMutableDictionary();
			}
		}
		return appParametres;
	}

}