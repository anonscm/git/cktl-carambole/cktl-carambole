/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.filtre;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cocktail.carambole.server.controleurs.LiquidationEngagementsCtrl.FiltresEngagements;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreBean;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreConverter;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 *
 * Convertisseur de filtres lies aux engagements.
 * Le resultat de la conversion est une collection de EOQUalifier.
 *
 * @author flagouey
 * $LastChangedBy$
 * $Date$
 */
public class EngagementFiltreConverter implements CktlDepenseFiltreConverter<Collection<EOQualifier>> {

	/**
	 * Constructeur par défaut.
	 */
	public EngagementFiltreConverter() {
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<EOQualifier> convert(List<CktlDepenseFiltreBean> filtres) {
		List<EOQualifier> qualsFiltres = new ArrayList<EOQualifier>();
		for (CktlDepenseFiltreBean currentFiltre : filtres) {
			if (!currentFiltre.isEmpty()) {
				EOQualifier filtreQualBuilt = buildFiltre(currentFiltre);
				if (filtreQualBuilt != null) {
					qualsFiltres.add(filtreQualBuilt);
				}
			}
		}
		return qualsFiltres;
	}

	private EOQualifier buildFiltre(CktlDepenseFiltreBean filtre) {
		EOQualifier filtreQual = null;
		FiltresEngagements currentFiltreKey = FiltresEngagements.valueOf(filtre.getKey());
		String filtreValue = filtre.getValue();
		switch (currentFiltreKey) {
			case UB :
				filtreQual = buildFiltreUb(filtreValue);
				break;
			case PCO :
				filtreQual = buildFiltrePco(filtreValue);
				break;
			case COMMANDE :
				filtreQual = buildFiltreCommande(filtreValue);
				break;
			default:
		}
		return filtreQual;
	}

	private EOQualifier buildFiltreUb(String ub) {
		return ERXQ.equals(EOEngagementBudget.ORGAN.append(EOOrgan.ORG_UB).key(), ub);
	}

	private EOQualifier buildFiltrePco(String pcoNum) {
		return ERXQ.equals(
				EOEngagementBudget.ENGAGEMENT_CONTROLE_PLAN_COMPTABLES.append(EOEngagementControlePlanComptable.PLAN_COMPTABLE).append(EOPlanComptable.PCO_NUM).key(),
				pcoNum);
	}

	private EOQualifier buildFiltreCommande(String numCommande) {
		return ERXQ.equals(
				EOEngagementBudget.COMMANDE_ENGAGEMENTS.append(EOCommandeEngagement.COMMANDE).append(EOCommande.COMM_NUMERO).key(),
				numCommande);
	}

}
