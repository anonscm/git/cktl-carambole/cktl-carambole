/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import org.cocktail.carambole.server.components.DetailFacture;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

/**
 * DirectAction poiur accéder directement à une facture.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class DAConsultFact extends DirectAction {
	public final static String DPP_ID_KEY = "dppId";
	private NSDictionary actionParams;

	public DAConsultFact(WORequest request) {
		super(request);
	}

	public WOActionResults defaultAction() {
		try {
			try {
				int dppId = Integer.parseInt(request().stringFormValueForKey(DPP_ID_KEY));
				setLoginComment("Accès à la facture ID : " + dppId);
				actionParams = new NSDictionary(new Integer(dppId), DPP_ID_KEY);
				if (useCasService())
					return pageForURL(getLoginActionURL(context(), false, DAConsultFact.class.getCanonicalName(), true, actionParams));
				//return pageForURL(getLoginActionURL(context(), false, "org.cocktail.carambole.serveur.DAConsultFact", true, actionParams));
				else
					return loginNoCasPage(actionParams);
			} catch (NumberFormatException e) {
				throw new Exception("Impossible de récupérer le n° de la facture : " + request().stringFormValueForKey(DPP_ID_KEY));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorPage(e.getMessage());
		}

	}

	/*
	 * @see DirectAction#casLoginLink()
	 */
	public String casLoginLink() {
		return getLoginActionURL(context(), false, "org.cocktail.carambole.serveur.DAConsultFact", true, actionParams);
	}

	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		WOActionResults nextPage = super.loginCasSuccessPage(netid, actionParams);
		CktlLog.trace(null);
		String errorMsg = jefySession().setConnectedUser(netid);
		if (!(nextPage instanceof CktlAlertPage)) {
			//CktlLog.log("login : " + netid + ", type : consultation - OK");
			nextPage = getDestPage(jefySession(), actionParams);
		}
		return nextPage;
	}

	public WOComponent getDestPage(Session session, NSDictionary actionParams) {
		try {
			CktlLog.log("login : " + session.connectedUserInfo().login() + ", type : DAConsultFact - OK");
			String strIntOrdre = actionParams.valueForKey(DPP_ID_KEY).toString();
			int dppId = Integer.parseInt(strIntOrdre);
			EOEditingContext edc = session.defaultEditingContext();
			EODepensePapier dpp = EODepensePapier.fetchByKeyValue(edc, EODepensePapier.DPP_ID_KEY, dppId);
			if (dpp == null) {
				throw new Exception("Impossible de trouver la facture (dpp_id = " + dppId + ")");
			}
			if (!dpp.commandeassociee().isLiquidable(edc, session.getUtilisateur(), null)) {
				throw new Exception("Vous n'avez pas le droit de liquider.");
			}

			DetailFacture nextPage = (DetailFacture) cktlApp.pageWithName(DetailFacture.class.getName(), session.context());
			nextPage.setLaFacture(dpp);
			session.setLaDepensePapierEnCours(dpp);
			session.setLaCommandeEnCours(dpp.commandeassociee());
			return nextPage;
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorPage(e.getMessage());
		}

	}

	public CktlLoginResponder getNewLoginResponder(NSDictionary actionParams) {
		return new DefaultLoginResponder(actionParams);
	}

	public static String buildLink(Session session, Integer dppId) {
		return ((Application) session.application()).getApplicationURL(session.context()) + "/wa/DAConsultFact?commId=" + dppId;
	}
}
