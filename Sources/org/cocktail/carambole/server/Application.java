/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.components.ErrorPage;
import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOGrhumParametres;
import org.cocktail.fwkcktldepense.server.metier.EOIndividu;
import org.cocktail.fwkcktldepense.server.metier.EOParametre;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.service.ConfigurationExtourneService;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.MyCRIMailBus;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EODatabaseChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOCooperatingObjectStore;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.woinject.WOInject;

import er.extensions.appserver.ERXMessageEncoding;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

public class Application extends ZApplication {
	//Active ou non le service facturier pour les liquidations sur extourne.
	//16/10/2012 : a priori la fonctionalité ne sera pas nécessaire.
	public static final Boolean ACTIVER_SF_SUR_EXTOURNE = Boolean.FALSE;

	public static final String APPLICATIONINTERNALNAME = VersionMe.APPLICATIONINTERNALNAME;

	public static final String APP2DECIMALES_FORMATTER_KEY = "application.app2DecimalesFormatter";
	public static final String APP5DECIMALES_FORMATTER_KEY = "application.app5DecimalesFormatter";
	public static final String APP_DATE_FORMATTER_KEY = "application.appDateFormatter";

	public static NSTimeZone ntz = null;

	public static Logger log = Logger.getLogger(Application.class.getName());

	private static final String SHOW_GESTION_FOURNISSEUR_MENU = "CARAMBOLE_SHOW_GESTION_FOURNISSEUR_MENU";
	private static final String SHOW_GESTION_CODE_ANALYTIQUE_MENU = "CARAMBOLE_SHOW_GESTION_CODE_ANALYTIQUE_MENU";
	private static final String PRESELECTION_TAUX_PRORATA = "CARAMBOLE_PRESELECTION_TAUX_PRORATA";
	public static final String XML_PRINTER_DRIVER = "XML_PRINTER_DRIVER";
	public static final String SIX_SERVICE_HOST = "SIX_SERVICE_HOST";
	public static final String SIX_SERVICE_PORT = "SIX_SERVICE_PORT";
	public static final String SIX_USE_COMPRESSION = "SIX_USE_COMPRESSION";
	public static final String AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY = "org.cocktail.gfc.depense.autoriser_commande_hors_marche_sur_codes_marche";

	private static final String[] REQUIRED_PARAMS_AVEC_SIX = {
			"APP_ID", "APP_USE_CAS", SHOW_GESTION_FOURNISSEUR_MENU, SHOW_GESTION_CODE_ANALYTIQUE_MENU, XML_PRINTER_DRIVER, SIX_SERVICE_HOST, SIX_SERVICE_PORT, SIX_USE_COMPRESSION
	};
	private static final String[] REQUIRED_PARAMS = {
			"APP_ID", "APP_USE_CAS", SHOW_GESTION_FOURNISSEUR_MENU, SHOW_GESTION_CODE_ANALYTIQUE_MENU,
	};
	private static final String[] MAQUETTES_SIX = {
			"SIXID_BONCOMMANDE"
	};
	private static final String PARAMETRES_TABLE_NAME = EOParametre.ENTITY_NAME;

	private final String urlDatabaseConnection = null;
	private String appAdminMail = null;
	/**
	 * Formatteur e deux decimales e utiliser pour les données numériques non monétaires.
	 */
	public NSNumberFormatter app2DecimalesFormatter;
	/**
	 * Formatteur é 5 decimales é utiliser pour les pourcentages dans la repartition.
	 */
	public NSNumberFormatter app5DecimalesFormatter;

	/**
	 * Formatteur de dates.
	 */
	public NSTimestampFormatter appDateFormatter;

	//public static boolean serviceAchat = true;
	/**
	 * Liste des emails des utilisateurs connectés
	 */
	private static NSMutableArray<String> utilisateurs;

	private static boolean test = false;

	public static String bdServerId;
	public static CaramboleParamManager caramboleParamManager = new CaramboleParamManager();
	private String mailServiceFait = null;

	public static void main(String argv[]) {
		//Fix pb lenteur au démarrage
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.carambole.server.Application", argv);
	}

	private Version _appVersion;

	public Application() {
		super();
		setAllowsConcurrentRequestHandling(false);
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		setPageRefreshOnBacktrackEnabled(true);
		WOMessage.setDefaultEncoding("UTF-8");
		WOMessage.setDefaultURLEncoding("UTF-8");
		ERXMessageEncoding.setDefaultEncoding("UTF8");
		ERXMessageEncoding.setDefaultEncodingForAllLanguages("UTF8");

		WOResponse.setDefaultEncoding("UTF-8");
		utilisateurs = new NSMutableArray<String>();
		setupDatabaseChannelCloserTimer();
		ERXEC.setDefaultFetchTimestampLag(2000);

		/* Remplacement du requestHandler pour les ressources statiques */
		//registerRequestHandler(new FwkCktlResourcesInJarRequestHandler(), "_wr_");
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");
	}

	/**
	 * Initialise l'application.
	 * 
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#initApplication()
	 */
	@Override
	protected boolean initApplicationSpecial() {
		CktlLog.rawLog("Demarrage de Carambole le " + DateCtrl.currentDateTimeString());

		initParametres();
		test = config().booleanForKey("TEST");
		if (test) {
			MyCRIMailBus.setFORWARD_ALL_TO(appAdminMail());
		}
		CktlLog.rawLog("TEST: " + test);

		if ("DEBUG".equals(ERXProperties.stringForKey("log4j.logger.org.cocktail"))) {
			CktlLog.setLevel(CktlLog.LEVEL_DEBUG);
		}

		//testCxml();
		return true;
	}

	/**
	 * Initialisation de parametres.
	 */
	private void initParametres() {

		caramboleParamManager.checkAndInitParamsWithDefault();

		CktlParamManager.copyParamValue(CaramboleParamManager.CARAMBOLE_CHECK_COHERENCE_INSEE_DISABLED, FwkCktlPersonneParamManager.INDIVIDU_CHECK_COHERENCE_INSEE_DISABLED);
		//		FwkCktlPersonneParamManager.setParamValue(FwkCktlPersonneParamManager.INDIVIDU_CHECK_COHERENCE_INSEE_DISABLED, "OUI");
		//CktlParamManager.copyParamValue(AgrhumParamManager.AGRHUM_ADRESSE_PERSO_DESACTIVE, FwkCktlPersonneGuiAjaxParamManager.PERSONNE_ADRESSE_PERSO_DESACTIVE);

		EOEditingContext edc = ERXEC.newEditingContext();
		Object sa = config().valueForKey("SERVICE_ACHAT");
		NSArray<EOExercice> exercices = EOExercice.fetchAll(edc, new EOKeyValueQualifier(EOExercice.EXE_STAT_KEY, EOQualifier.QualifierOperatorNotEqual, EOExercice.ETAT_CLOS));

		String sql = "insert into jefy_depense.parametre (exe_ordre,par_key,par_value,par_description) values (_exeordre_,'" + AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY + "', '_res_','OUI/NON');";
		String sqlres = "";
		for (int i = 0; i < exercices.count(); i++) {
			EOExercice exercice = exercices.objectAtIndex(i);
			EOParametre param = FinderParametre.getParametre(edc, AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY, exercice);
			if (param == null) {
				String res = "OUI";
				if (sa != null) {
					Boolean bool = config().booleanForKey("SERVICE_ACHAT");
					if (!bool) {
						res = "NON";
					}
				}
				sqlres = sqlres.concat(sql.replaceAll("_exeordre_", "" + exercice.exeExercice().intValue()).replaceAll("_res_", res));
			}
		}
		try {
			if (sqlres.length() > 0) {
				sqlres = "begin " + sqlres + " commit; end;";
				EOUtilities.rawRowsForSQL(edc, mainModelName(), sqlres, null);
				log.debug("Initialisation des parametres dans la base " + AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Impossible d'enregistrer les parametres dans la base : " + AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY);
		}

		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_ENVELOPPE_NIVEAU_SELECTION_KEY, "UB", "Niveau de selection des enveloppes d'extourne (UB/CR)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_ENVELOPPE_AUTORISEE_TCD1_KEY, "N", "Autoriser les liquidations sur enveloppe d'extourne pour les dépenses sur type de crédits de la section 1 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_ENVELOPPE_AUTORISEE_TCD2_KEY, "N", "Autoriser les liquidations sur enveloppe d'extourne pour les dépenses sur type de crédits de la section 2 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_ENVELOPPE_AUTORISEE_TCD3_KEY, "O", "Autoriser les liquidations sur enveloppe d'extourne pour les dépenses sur type de crédits de la section 3 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_FLECHEE_AUTORISEE_TCD1_KEY, "O", "Autoriser les liquidations sur liquidation d'extourne flêchées pour les dépenses sur type de crédits de la section 1 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_FLECHEE_AUTORISEE_TCD2_KEY, "N", "Autoriser les liquidations sur liquidation d'extourne flêchées pour les dépenses sur type de crédits de la section 2 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, ConfigurationExtourneService.EXTOURNE_FLECHEE_AUTORISEE_TCD3_KEY, "N", "Autoriser les liquidations sur liquidation d'extourne flêchées pour les dépenses sur type de crédits de la section 3 (O/N)");
		creerParametreDansBaseSiInexistant(edc, exercices, "org.cocktail.gfc.depense.bondecommande.messageinformationhautpage", "", "Message qui figure sur tous les bons de commande fournisseurs, au dessus de la zone contenant le détail des articles commandés");
		creerParametreDansBaseSiInexistant(edc, exercices, "org.cocktail.gfc.depense.bondecommande.messageinformationbaspage", "", "Message qui figure sur tous les bons de commande fournisseurs, en bas de page (remplace le paramètre MESSAGE_IMPRESSION)");
		creerParametreDansBaseSiInexistant(edc, exercices, "org.cocktail.gfc.depense.bondecommande.messageobservation", "", "Message qui figure sur tous les bons de commande fournisseurs, dans la zone Observations");

	}

	private void creerParametreDansBaseSiInexistant(EOEditingContext edc, NSArray<EOExercice> exercices, String paramKey, String paramValue, String commentaire) {
		String sql = "insert into jefy_depense.parametre (exe_ordre,par_key,par_value,par_description) values (_exeordre_,'" + paramKey + "', '" + paramValue.replaceAll("'", "''") + "','" + commentaire.replaceAll("'", "''") + "');";
		String sqlres = "";
		for (int i = 0; i < exercices.count(); i++) {
			EOExercice exercice = exercices.objectAtIndex(i);
			EOParametre param = FinderParametre.getParametre(edc, paramKey, exercice);
			if (param == null) {
				sqlres = sqlres.concat(sql.replaceAll("_exeordre_", "" + exercice.exeExercice().intValue()));
			}
		}
		try {
			if (sqlres.length() > 0) {
				sqlres = "begin " + sqlres + " commit; end;";
				EOUtilities.rawRowsForSQL(edc, mainModelName(), sqlres, null);
				log.debug("Initialisation des parametres dans la base " + paramKey);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Impossible d'enregistrer les parametres dans la base : " + paramKey);
		}

	}

	/**
	 * Execute les operations au demarrage de l'application, juste apres l'initialisation standard de l'application.
	 */
	@Override
	public void startRunning() {
		EOModelGroup modelGroup = EOModelGroup.defaultGroup();
		CktlLog.rawLog("Modeles:" + modelGroup.models().valueForKeyPath("path"));
		EOModel modeleCarambole = modelGroup.modelNamed(mainModelName());
		if (modeleCarambole != null) {
			Application.bdServerId = bdConnexionServerId(modeleCarambole);
		}
		initFormatters();
		initTimeZones();
		this.appDateFormatter = new NSTimestampFormatter();
		this.appDateFormatter.setPattern("%d/%m/%Y");

	}

	@Override
	public String configFileName() {
		return "Carambole.config";
	}

	/**
	 * Retourne le nom de <em>EOModel</em> de l'application. Le nom ne doit pas avoir d'extention <em>.emodeld</em>.
	 */
	@Override
	public String mainModelName() {
		return "carambole";
	}

	@Override
	public String configTableName() {
		return EOGrhumParametres.ENTITY_NAME;
	}

	/**
	 * Retourne le mot de passe du super-administrateur. Il permet de se connecter a l'application avec le nom d'un autre utilisateur
	 * (l'authentification local et non celle CAS doit etre activee dans ce cas).
	 */
	@Override
	public String getRootPassword() {
		return "O57m1mnXWRFIE";
	}

	@Override
	public WOResponse handleException(Exception anException, WOContext aContext) {
		if (aContext != null && aContext.hasSession()) {
			Session session = (Session) aContext.session();
			try {
				@SuppressWarnings("unchecked")
				NSDictionary<String, Object> extraInfo = extraInformationForExceptionInContext(anException, aContext);
				CktlMailBus cmb = session.mailBus();
				String smtpServeur = config().stringForKey("GRHUM_HOST_MAIL");
				String destinataires = config().stringForKey("APP_ADMIN_MAIL");

				if (cmb != null &&
						smtpServeur != null && smtpServeur.equals("") == false &&
						destinataires != null && destinataires.equals("") == false) {
					String objet = "[CARAMBOLE]:Exception:[";
					objet += VersionMe.txtAppliVersion() + "]";
					String contenu = "Date : " + new NSTimestamp().toLocaleString() + "\n";
					contenu += "Utilisateur : " + session.getUtilisateur().nomPrenom() + "\n\n";
					contenu += "Identifiant : " + session.connectedUserInfo().login() + "\n\n";
					contenu += "OS: " + System.getProperty("os.name") + "\n";
					contenu += "Java vm version: " + System.getProperty("java.vm.version") + "\n";
					contenu += "WO version: " + System.getProperty("com.webobjects.version") + "\n\n";
					contenu += "allowsConcurrentRequestHandlingEnabled: " + isConcurrentRequestHandlingEnabled() + "\n";
					contenu += "directConnectEnabled: " + isDirectConnectEnabled() + "\n\n";
					EOCommande cmde = session.getLaCommandeEnCours();
					if (cmde != null && cmde.editingContext() != null) {
						Number numero = cmde.commNumero();
						if (numero != null) {
							contenu += "N° Cmde en cours de traitement: " + numero + "\n";
						}
						else {
							contenu += "Creation de commande en cours." + "\n";
						}
						if (cmde.utilisateur() != null) {
							contenu += "Proprietaire: " + cmde.utilisateur().nomPrenom() + "\n";
						}
						else {
							contenu += "Proprietaire: Indetermine" + "\n";
						}
					}
					EODepensePapier facture = session.getLaDepensePapierEnCours();
					if (facture != null && facture.editingContext() != null) {
						if (facture.dppNumeroFacture() != null) {
							contenu += "N° Facture en cours de traitement: " + facture.dppNumeroFacture() + "\n";
						}
						else {
							contenu += "N° Facture en cours de traitement: Inconnu" + "\n";
						}
					}
					contenu += "\n";

					// contenu += "Machine : " + context().request().headerForKey("x-webobjects-remote-addr")+"\n\n";
					// contenu += "Commentaires de l'utilisateur : "+"\n";
					// contenu += getObservations()+"\n\n";
					contenu += "Exception : " + "\n";
					contenu += getMessage(anException, extraInfo) + "\n";
					contenu += "\n\n";
					//cmb.sendMail(getEmailFrom(session),destinataires,null,objet,contenu);

				}
				else {
					CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
					CktlLog.log("Veuillez verifier que les parametres GRHUM_HOST_MAIL et APP_ADMIN_MAIL sont bien renseignes");
					CktlLog.log("GRHUM_HOST_MAIL = " + smtpServeur);
					CktlLog.log("APP_ADMIN_MAIL = " + destinataires);
					CktlLog.log("cmb = " + cmb);
					CktlLog.log("\n\n\n");
				}
				ErrorPage errorPage = (ErrorPage) pageWithName("ErrorPage", aContext);
				errorPage.setMessage(getMessage(anException, extraInfo));

				WOResponse errorResponse = errorPage.generateResponse();

				return errorResponse;
			} catch (Exception e) {
				// session.setNestedEdc(null);
				CktlLog.log("\n\n\n");
				CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! Exception durant le traitement d'une autre exception !!!!!!!!!!!!!!!!");
				CktlLog.log("Message Exception dans exception: " + e.getMessage());
				CktlLog.log("Stack Exception dans exception: " + e.getStackTrace());
				super.handleException(e, aContext);
				CktlLog.log("\n");
				CktlLog.log("Message Exception originale: " + anException.getMessage());
				CktlLog.log("Stack Exception dans exception: " + anException.getStackTrace());
				return super.handleException(anException, aContext);
			}
		}
		else {
			return super.handleException(anException, aContext);
			/*
			 * ((Session)aContext.session()).setAlertMessage("Vous ne pouvez pas revenir en arrière !" ); return
			 * pageWithName("Accueil",aContext).generateResponse();
			 */}
	}

	protected String getMessage(Throwable e, NSDictionary<String, Object> extraInfo) {
		String message = "";
		if (e != null) {
			message = stackTraceToString(e, false) + "\n\n";
			message += "Info extra :\n";
			if (extraInfo != null) {
				message += NSPropertyListSerialization.stringFromPropertyList(extraInfo) + "\n\n";
			}
		}
		return message;
	}

	/**
	 * permet de recuperer la trace d'une exception au format string message + trace
	 * 
	 * @param e
	 * @return
	 */
	public static String stackTraceToString(Throwable e, boolean useHtml) {
		String tagCR = "\n";
		if (useHtml) {
			tagCR = "<br>";
		}
		String stackStr = e + tagCR + tagCR;
		StackTraceElement[] stack = e.getStackTrace();
		for (int i = 0; i < stack.length; i++) {
			stackStr += (stack[i]).toString() + tagCR;
		}
		return stackStr;
	}

	protected String getEmailFrom(Session session) {
		CktlUserInfoDB utilisateurInfos = new CktlUserInfoDB(session.dataBus());
		EOUtilisateur utilisateur = session.getUtilisateur();
		EOIndividu individu = utilisateur.individu();
		Integer noIndividu = (Integer) EOUtilities.primaryKeyForObject(individu.editingContext(), individu).allValues().lastObject();
		utilisateurInfos.individuForNoIndividu(noIndividu, true);
		return utilisateurInfos.email();
	}

	/**
	 * Initialise le TimeZone é utiliser pour l'application.
	 */
	protected void initTimeZones() {
		//appLog().trace(new NSArray(TimeZone.getAvailableIDs()));
		CktlLog.log("Initialisation du NSTimeZone");
		// myLog.appendKeyValue("NSTimeZone par defaut recupere sur le systeme (avant initialisation)", NSTimeZone.defaultTimeZone(),75  );
		String tz = config().valueForKey("DEFAULT_NS_TIMEZONE").toString();
		if (tz == null || tz.equals("")) {
			CktlLog.log("Le parametre DEFAULT_NS_TIMEZONE n'est pas defini dans le fichier .config.");
			TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
		}
		else {
			ntz = NSTimeZone.timeZoneWithName(tz, false);
			if (ntz == null) {
				CktlLog.log("Le parametre DEFAULT_NS_TIMEZONE defini dans le fichier .config n'est pas valide (" + tz + ")");
				TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
				NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
			}
			else {
				//CktlLog.log("TimeZone : "+ntz);
				TimeZone.setDefault(TimeZone.getTimeZone(tz));
				NSTimeZone.setDefaultTimeZone(ntz);
			}
		}
		ntz = NSTimeZone.defaultTimeZone();
		CktlLog.log("NSTimeZone par defaut utilise dans l'application:" + NSTimeZone.defaultTimeZone());
		//		NSTimestampFormatter ntf = new NSTimestampFormatter("%d/%m/%Y %H:%M:%S %Z");
		//		CktlLog.log("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone: " + ntf.defaultParseTimeZone());
		//		CktlLog.log("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone: " + ntf.defaultFormatTimeZone());
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
		CktlLog.log("Les DateFormat afficheront les dates avec le TimeZone: " + df.getTimeZone());
		//CktlLog.log("new NSTimestamp() : " + new NSTimestamp() + " >>>>>>> " + ntf.format(new NSTimestamp()));
		CktlLog.log("new Date() : " + new Date() + " >>>> " + df.format(new Date()));
	}

	/**
	 * Initialise les formatters é utiliser.
	 * 
	 * @see appMonnaieFormatter
	 * @see app2DecimalesFormatter
	 */
	public void initFormatters() {
		/*
		 * //Créer le numberFormatter this.appMonnaieFormatter = new NSNumberFormatter(); this.appMonnaieFormatter.setDecimalSeparator(","); //
		 * this.appMonnaieFormatter.setThousandSeparator("\u0020"); this.appMonnaieFormatter.setThousandSeparator(" ");
		 * 
		 * this.appMonnaieFormatter.setHasThousandSeparators(true); //this.appMonnaieFormatter.setPattern("#,##0.00;0,00;-#,##0.00"); EOEditingContext
		 * ed = ERXEC.newEditingContext(); EOExercice exercice = FinderExercice.getExercicePourDate(ed, new NSTimestamp()); EODevise devise =
		 * FinderDevise.getDeviseEnCours(ed, exercice); int nbDecimales = EODevise.defaultNbDecimales; if (devise!=null) nbDecimales =
		 * devise.devNbDecimales().intValue(); if (nbDecimales == 0) { this.appMonnaieFormatter.setPattern("# ##0;0;-# ##0"); } else { String str =
		 * "# ##0."; String str1 = ";0,"; String str2 = ";-# ##0.";
		 * 
		 * for (int i = 0; i < nbDecimales; i++) { str += "0"; str1 += "0"; str2 += "0"; }
		 * 
		 * this.appMonnaieFormatter.setPattern(str+str1+str2); }
		 */this.app2DecimalesFormatter = new NSNumberFormatter();
		this.app2DecimalesFormatter.setDecimalSeparator(",");
		// this.appMonnaieFormatter.setThousandSeparator("\u0020");
		this.app2DecimalesFormatter.setThousandSeparator(" ");
		this.app2DecimalesFormatter.setHasThousandSeparators(true);
		this.app2DecimalesFormatter.setPattern("#,##0.00;0,00;-#,##0.00");

		this.app5DecimalesFormatter = new NSNumberFormatter();
		this.app5DecimalesFormatter.setDecimalSeparator(",");
		// this.appMonnaieFormatter.setThousandSeparator("\u0020");
		this.app5DecimalesFormatter.setThousandSeparator(" ");

		this.app5DecimalesFormatter.setHasThousandSeparators(true);
		this.app5DecimalesFormatter.setPattern("##0.00000;0,00000;-##0.00000");

	}

	public NSNumberFormatter app2DecimalesFormatter() {
		return this.app2DecimalesFormatter;
	}

	public NSNumberFormatter getApp5DecimalesFormatter() {
		return this.app5DecimalesFormatter;
	}

	public String urlDatabaseConnection() {
		if (urlDatabaseConnection == null) {
			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
			for (int i = 0; i < vModelGroup.models().count(); i++) {
				// EOModel tmpEOModel= (EOModel)vModelGroup.models().objectAtIndex(i);
				// dicoBdConnexionServerName.takeValueForKey( bdConnexionServerName(tmpEOModel) , tmpEOModel.name());
				// dicoBdConnexionServerId.takeValueForKey( bdConnexionServerId(tmpEOModel) , tmpEOModel.name());
			}
		}
		return urlDatabaseConnection;
	}

	/**
	 * Renvoie l'url de connexion é la base de données.
	 * 
	 * @return
	 */
	public String bdConnexionName(EOModel model) {
		return bdConnexionUrl(model);
	}

	/**
	 * Renvoie l'url de connexion é la base de données pour le modele spécifié en parametre.
	 * 
	 * @param model
	 * @return
	 */
	public String bdConnexionUrl(EOModel model) {
		NSDictionary<String, Object> vDico = model.connectionDictionary();
		return (String) vDico.valueForKey("URL");
	}

	/**
	 * Renvoie le user base de données
	 * 
	 * @return
	 */
	public String bdConnexionUser(EOModel model) {
		NSDictionary<String, Object> vDico = model.connectionDictionary();
		return (String) vDico.valueForKey("username");
	}

	/**
	 * Renvoie le serverid de la base de données (par exemple gest).
	 * 
	 * @return
	 */
	public String bdConnexionServerId(EOModel model) {
		String url = bdConnexionUrl(model);
		String serverUrl = null;
		// String serverName=null;
		String serverBdName = null;
		//		appLog.trace("URL", url);
		//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
		//On sépare la partie jdbc de la partie server
		String[] parts = url.split("@");
		if (parts.length > 1) {
			serverUrl = parts[1];
			parts = serverUrl.split(":");
			if (parts.length > 0) {
				// serverName = parts[0];
				if (parts.length > 1) {
					serverBdName = parts[parts.length - 1];
				}
			}
		}
		return serverBdName;
	}

	/**
	 * Renvoie le serverid de la base de données (par exemple jane).
	 * 
	 * @return
	 */
	public String bdConnexionServerName(EOModel model) {
		String url = bdConnexionUrl(model);
		String serverUrl = null;
		String serverName = null;
		// String serverBdName=null;
		//		appLog.trace("URL", url);
		//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
		//On sépare la partie jdbc de la partie server
		String[] parts = url.split("@");
		if (parts.length > 1) {
			serverUrl = parts[1];
			parts = serverUrl.split(":");
			if (parts.length > 0) {
				serverName = parts[0];
				/*
				 * if (parts.length>1) { serverBdName = parts[parts.length - 1]; }
				 */}
		}
		return serverName;
	}

	public String appAdminMail() {
		if (appAdminMail == null) {
			appAdminMail = config().stringForKey("APP_ADMIN_MAIL");
		}
		return appAdminMail;
	}

	public NSMutableArray<String> utilisateurs() {
		return utilisateurs;
	}

	public void setupDatabaseChannelCloserTimer() {
		Timer timer = new Timer(true);
		//Close open database connections every four hours.
		timer.scheduleAtFixedRate(new DBChannelCloserTask(), new Date(), 14400000);
	}

	class DBChannelCloserTask extends TimerTask {
		public DBChannelCloserTask() {
			super();
		}

		@Override
		public void run() {
			closeDatabaseChannels();
			NSLog.out.appendln("running timer");
		}

		public void closeDatabaseChannels() {
			int i, contextCount, j, channelCount;
			NSArray<EOCooperatingObjectStore> databaseContexts;
			EOObjectStoreCoordinator coordinator;
			coordinator = EOObjectStoreCoordinator.defaultCoordinator();
			databaseContexts = coordinator.cooperatingObjectStores();
			contextCount = databaseContexts.count();
			//Iterate through all an app’s cooperating object stores (database contexts).
			for (i = 0; i < contextCount; i++) {
				NSArray channels = ((EODatabaseContext) databaseContexts.objectAtIndex(i)).registeredChannels();
				channelCount = channels.count();
				for (j = 0; j < channelCount; j++) {
					//Make sure the channel you're trying to close isn't performing a transaction.
					if (!((EODatabaseChannel) channels.objectAtIndex(j)).adaptorChannel().adaptorContext().hasOpenTransaction()) {
						((EODatabaseChannel) channels.objectAtIndex(j)).adaptorChannel().closeChannel();
					}
				}
			}
		}
	}

	/**
	 * @return true si l'application est lancée en mode test.
	 */
	public boolean test() {
		//    	return config().booleanForKey("TEST");
		return test;
	}

	@Override
	protected String appliId() {
		return VersionMe.APPLICATION_STRID;
	}

	@Override
	protected String parametresTableName() {
		return PARAMETRES_TABLE_NAME;
	}

	@Override
	protected String txtAppliVersion() {
		return VersionMe.txtAppliVersion();
	}

	@Override
	public String[] configMandatoryKeys() {
		if (ERXProperties.booleanForKey("org.cocktail.carambole.bondecommande.impression.six")) {
			return REQUIRED_PARAMS_AVEC_SIX;
		}
		return REQUIRED_PARAMS;
	}

	@Override
	protected String[] maquettesSix() {
		return MAQUETTES_SIX;
	}

	/**
	 * Check for Windows XP
	 * 
	 * @return true if runs on XP
	 */
	public boolean _isAdditionalForeignSupportedDevelopmentPlatform() {
		String s = System.getProperty("os.name");
		return (s != null && s.equals("Windows XP"));
	}

	/**
	 * Calls _isAdditionalForeignSupportedDevelopmentPlatform
	 * 
	 * @see com.webobjects.appserver.WOApplication#_isForeignSupportedDevelopmentPlatform()
	 */
	@Override
	public boolean _isForeignSupportedDevelopmentPlatform() {
		return (super._isForeignSupportedDevelopmentPlatform() || _isAdditionalForeignSupportedDevelopmentPlatform());
	}

	public Version appVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}
		return _appVersion;
	}

	@Override
	public A_CktlVersion appCktlVersion() {
		return appVersion();
	}

	public Boolean showGestionFournisseursMenu() {
		return Boolean.valueOf("OUI".equals(getParam(SHOW_GESTION_FOURNISSEUR_MENU)));
	}

	public Boolean showGestionCodeAnalytiqueMenu() {
		return Boolean.valueOf("OUI".equals(getParam(SHOW_GESTION_CODE_ANALYTIQUE_MENU)));
	}

	public Boolean preselectionTauxProrata() {
		return Boolean.valueOf(!"NON".equals(getParam(PRESELECTION_TAUX_PRORATA)));
	}

	public String mailCentralisationCertification() {
		if (mailServiceFait == null) {
			mailServiceFait = getParam(CaramboleParamManager.DEPENSE_CENTRALISATION_MAIL_SERVICE_FACTURIER);
		}
		return mailServiceFait;
	}

}