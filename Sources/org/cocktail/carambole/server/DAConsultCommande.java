/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import org.cocktail.carambole.server.components.DetailCommande;
import org.cocktail.carambole.server.components.Main;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

/**
 * DirectAction poiur accéder directement à une commande.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class DAConsultCommande extends DirectAction {
	public final static String COMM_ID_KEY = "commId";
	private NSDictionary<String, Object> actionParams;

	public DAConsultCommande(WORequest request) {
		super(request);
	}

	public WOActionResults defaultAction() {
		String res = request().stringFormValueForKey(COMM_ID_KEY);
		if (res != null) {
			res = res.trim();
			res = StringCtrl.keepDigits(res);
			if (res.length() > 0) {
				int commande = Integer.parseInt(res);
				setLoginComment("Accès à la commande ID : " + commande);
				actionParams = new NSDictionary<String, Object>(new Integer(commande), COMM_ID_KEY);
				if (useCasService())
					return pageForURL(getLoginActionURL(context(), false, DAConsultCommande.class.getCanonicalName(), true, actionParams));
				else
					return loginNoCasPage(actionParams);
			}
		}
		return CktlAlertPage.newAlertPageWithCaller(pageWithName(Main.class.getName()), "Erreur", "ID Commande erronné (" + res + ")", " OK ", CktlAlertPage.ERROR);
	}

	public String casLoginLink() {
		return getLoginActionURL(context(), false, DAConsultCommande.class.getCanonicalName(), true, actionParams);
	}

	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		WOActionResults nextPage = super.loginCasSuccessPage(netid, actionParams);
		CktlLog.trace(null);
		//	String errorMsg = jefySession().setConnectedUser(netid);
		if (!(nextPage instanceof CktlAlertPage)) {
			//CktlLog.log("login : " + netid + ", type : consultation - OK");
			nextPage = getDestPage(jefySession(), actionParams);
		}
		return nextPage;
	}

	public WOComponent getDestPage(Session session, NSDictionary actionParams) {
		try {
			CktlLog.log("login : " + session.connectedUserInfo().login() + ", type : DAConsultCommande - OK");
			String strIntOrdre = actionParams.valueForKey(COMM_ID_KEY).toString();
			int comId = Integer.parseInt(strIntOrdre);
			EOEditingContext edc = session.defaultEditingContext();
			EOCommande comm = EOCommande.fetchByKeyValue(edc, EOCommande.COMM_ID_KEY, comId);
			if (comm == null) {
				throw new Exception("Impossible de trouver la commande (comm_id = " + comId + ")");
			}
			if (!comm.isConsultable(edc, session.getUtilisateur(), null)) {
				throw new Exception("Vous n'avez pas le droit de consulter cette commande.");
			}
			DetailCommande nextPage = (DetailCommande) cktlApp.pageWithName(DetailCommande.class.getName(), session.context());
			nextPage.setLaCommande(comm);
			session.setLaCommandeEnCours(comm);
			return nextPage;
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorPage(e.getMessage());
		}

	}

	public CktlLoginResponder getNewLoginResponder(NSDictionary actionParams) {
		return new DefaultLoginResponder(actionParams);
	}

	public static String buildLink(Session session, Integer commId) {
		return ((Application) session.application()).getApplicationURL(session.context()) + "/wa/DAConsultCommande?commId=" + commId;
	}
}
