/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

public class MyComponent extends WOComponent {

	private static final long serialVersionUID = 1L;
	public final Session session = (Session) session();

	public MyComponent(WOContext context) {
		super(context);
	}

	public Application myApp() {
		return (Application) application();
	}

	public Session mySession() {
		return (Session) session();
	}

	public EOUtilisateur utilisateur() {
		return session.getUtilisateur();
	}

	public EOUtilisateur utilisateurInContext(EOEditingContext ed) {
		EOUtilisateur user = session.getUtilisateur();
		if (user.editingContext().equals(ed) == false) {
			user = (EOUtilisateur) ed.faultForGlobalID(edc().globalIDForObject(user), ed);
		}
		return user;
	}

	public EOEditingContext edc() {
		return session.defaultEditingContext();
	}

	public EOEditingContext nestedEdc() {
		return session.nestedEdc();
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public EOExercice exerciceDuJour() {
		return session.accueilControleur().getExerciceDuJour();
	}

	public EOExercice exerciceCourant() {
		return session.accueilControleur().getExerciceCourant();
	}

	public EOCommande laCommandeEnCours() {
		return session.getLaCommandeEnCours();
	}

	public void setLaCommandeEnCours(EOCommande laCommandeEnCours) {
		session.setLaCommandeEnCours(laCommandeEnCours);
	}

	public EOEngagementBudget getLEngagementBudgetEnCours() {
		return session.getLEngagementBudgetEnCours();
	}

	public void setLEngagementBudgetEnCours(EOEngagementBudget lEngagementBudgetEnCours) {
		session.setLEngagementBudgetEnCours(lEngagementBudgetEnCours);
	}

	public WODisplayGroup dgCommande() {
		return session.accueilControleur().getDgCommandes();
	}

	public WODisplayGroup dgEngagement() {
		return session.accueilControleur().getDgEngagements();
	}

	public WODisplayGroup dgPreCommande() {
		return session.accueilControleur().getDgPreCommandes();
	}

	public WODisplayGroup dgPreLiquidation() {
		return session.accueilControleur().getDgPreLiquidations();
	}

	public WODisplayGroup dgArticles() {
		return session.getDgArticles();
	}

	public WODisplayGroup dgFacture() {
		return session.accueilControleur().getDgFactures();
	}

	public EODepensePapier laDepensePapierEnCours() {
		return session.getLaDepensePapierEnCours();
	}

	public void setLaDepensePapierEnCours(EODepensePapier laDepensePapierEnCours) {
		session.setLaDepensePapierEnCours(laDepensePapierEnCours);
	}

	public WODisplayGroup getDgActif() {
		return session.dgActif();
	}

	public void setDgActif(WODisplayGroup dgActif) {
		session.setDgActif(dgActif);
	}

	public EODepenseBudget getLiquidationExtourneEnCours() {
		return session.getLiquidationExtourneEnCours();
	}

	public WOActionResults doNothing() {
		return null;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);

	}

}
