/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.utilitaires;

import java.util.Iterator;
import java.util.Map;

public class UtilString {

	/**
	 * Remplace une sous-chaine par une autre. C'est un proxy pour la methode replace de r.univlr.cri.util.StringCtrl.
	 * 
	 * @param string Chaine entiere.
	 * @param string2 Sous-chaine e remplacer.
	 * @param string3 Sous-chaine de remplacement.
	 * @return La chaine modifiee.
	 */
	public static String replaceStringByAnother(String s, String what, String byWhat) {
		StringBuffer sb;
		int i;

		//		  if ((s == null) || (what == null)) return s;
		sb = new StringBuffer();
		if (byWhat == null)
			byWhat = "";
		do {
			i = s.indexOf(what);
			if (i >= 0) {
				sb.append(s.substring(0, i));
				sb.append(byWhat);
				s = s.substring(i + what.length());
			}
		} while (i != -1);
		sb.append(s);
		return sb.toString();
		//return StringCtrl.replace(str, replaceWhat, byWhat);
	}

	/**
	 * Parcourt une chaine et remplace les identifiants (encadres par des caracteres %) contenus dans la chaine par les valeurs correspondantes de la
	 * map.<br>
	 * Exemple:<br>
	 * <code>
	 * Hashtable table = new Hashtable();<br>
	 * table.put("nom", "XIV");<br>
	 * table.put("prenom", "Louis");<br>
	 * String res = replaceWithValuesFromMap("Bonjour %nom% %prenom%!", table);<br>
	 * </code>
	 */
	public static String replaceWithValuesFromMap(String str, Map<String, Object> map) {
		String res = str;
		for (Iterator<String> iter = map.keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			res = UtilString.replaceStringByAnother(res, "%" + key + "%", map.get(key).toString());
		}
		return res;
	}

}
