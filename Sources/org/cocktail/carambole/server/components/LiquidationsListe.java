/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Liste de liquidations ou preliquidations (suivant binding).
 * 
 * @author rprin
 * @binding isSelectionPossible Boolean
 * @binding lesDepensesPapier NSArray Les depensePapiers à afficher.
 * @binding onSelectDepensePapierCallback Nom de l'action à appeler lors de la sélection d'une facture.
 * @binding selectedDepensePapier Variable qui peut recevoir l'objet selectionné.
 * @binding showLiquidations Boolean Indique si le composant doit afficher les liquidations
 * @binding showPreLiquidations Boolean Indique si le composant doit afficher les pre-liquidations
 * @binding showRepartCN Boolean Indique si le composant doit afficher la repartition des CN
 * @binding showReimputations Boolean Indique si le composant doit afficher les réimputations effectuées
 * @binding showReimputer Boolean Indique si le composant doit afficher le lien pour creer une reimputation
 * @binding showReverser Boolean Indique si le composant doit afficher le lien pour creer un ordre de reversement
 * @binding showSupprimerDepenseBudget Boolean Indique si le composant doit afficher le lien pour la suppression des depenseBudgets
 */
public class LiquidationsListe extends MyAjaxComponent {
	private static final long serialVersionUID = 1L;

	public static final String BINDING_lesDepensesPapier = "lesDepensesPapier";
	public static final String BINDING_onSelectDepensePapierCallback = "onSelectDepensePapierCallback";
	public static final String BINDING_selectedDepensePapier = "selectedDepensePapier";
	public static final String BINDING_showLiquidations = "showLiquidations";
	public static final String BINDING_showPreLiquidations = "showPreLiquidations";
	public static final String BINDING_showRepartCN = "showRepartCN";
	public static final String BINDING_isSelectionPossible = "isSelectionPossible";

	private EODepensePapier uneDepensePapier = null;

	public LiquidationsListe(WOContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public NSArray<EODepensePapier> lesDepensesPapier() {
		return (NSArray<EODepensePapier>) valueForBinding(BINDING_lesDepensesPapier);
	}

	public WOActionResults onSelectDepensePapier() {
		EODepensePapier dp = getUneDepensePapier();
		setValueForBinding(dp, BINDING_selectedDepensePapier);

		if (hasBinding(BINDING_onSelectDepensePapierCallback)) {
			return performParentAction((String) valueForBinding(BINDING_onSelectDepensePapierCallback));
		}
		else {
			return null;
		}

	}

	public EODepensePapier getUneDepensePapier() {
		return uneDepensePapier;
	}

	public void setUneDepensePapier(EODepensePapier uneDepensePapier) {
		this.uneDepensePapier = uneDepensePapier;
	}

}