/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;

public class BordereauListe extends MyAjaxComponent {

	private static final long serialVersionUID = -5281765295986767442L;
	public static final String BINDING_actionFiltrerName = "actionFiltrerName";
	public static final String BINDING_dg = "dg";
	//public static final String BINDING_selectedObject = "selectedObject";
	//public static final String BINDING_actionFiltrerName = "actionFiltrerName";
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	public static final String BINDING_callbackOnSelectionner = "callbackOnSelectionner";
	public static final String BINDING_showFiltres = "showFiltres";
	public static final String BINDING_decimalFormatter = "decimalFormatter";

	public static final String ROW_KEY = "unObjet.";

	public static final String COL_BOR_NUM_KEY = EOBordereau.BOR_NUM_KEY;
	public static final String COL_GES_CODE_KEY = EOBordereau.TO_GESTION_KEY + "." + EOGestion.GES_CODE_KEY;
	public static final String COL_BOR_ETAT_KEY = EOBordereau.BOR_ETAT_KEY;
	public static final String COL_DATE_CREATION_KEY = EOBordereau.BOR_DATE_CREATION_KEY;
	public static final String COL_DATE_VISA_KEY = EOBordereau.BOR_DATE_VISA_KEY;
	public static final String COL_UTILISATEUR_KEY = EOBordereau.TO_UTILISATEUR_KEY + "." + EOUtilisateur.UTL_PRENOM_NOM_KEY;
	public static final String COL_NB_KEY = EOBordereau.BOR_NB_KEY;
	public static final String COL_MONTANT_KEY = EOBordereau.BOR_MONTANT_KEY;

	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

	public EOEnterpriseObject unObjet;
	private NSArray<CktlAjaxTableViewColumn> colonnes;

	private static NSArray<String> ETATSFORBORD = new NSArray<String>(EOBordereau.BOR_ETAT_VALIDE, EOBordereau.BOR_ETAT_VISE);
	public String unEtatForBord;
	private String selectedEtatForBord;

	/** Tableau contenant les clés identifiant les colonnes à afficher par défaut. */
	public static NSArray<String> DEFAULT_COLONNES_KEYS = new NSArray<String>(new String[] {
			COL_GES_CODE_KEY, COL_BOR_NUM_KEY, COL_NB_KEY, COL_MONTANT_KEY, COL_DATE_CREATION_KEY, COL_UTILISATEUR_KEY, COL_BOR_ETAT_KEY
	});

	public BordereauListe(WOContext context) {
		super(context);
	}

	public String bordereauxTbViewId() {
		return getComponentId() + "_bordereauxTbView";
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {

			CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
			col2.setLibelle("Numéro");
			col2.setOrderKeyPath(COL_BOR_NUM_KEY);
			//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
			CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_BOR_NUM_KEY, "emptyValue");
			col2.setAssociations(ass2);
			col2.setRowCssStyle("white-space: normal;");
			col2.setRowCssClass("alignToCenter");
			_colonnesMap.takeValueForKey(col2, COL_BOR_NUM_KEY);

			CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
			col4.setLibelle("Date création");
			CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DATE_CREATION_KEY, null);
			col4.setAssociations(ass4);
			col4.setRowCssClass("alignToCenter");
			ass4.setDateformat("dd/MM/yyyy");
			_colonnesMap.takeValueForKey(col4, COL_DATE_CREATION_KEY);

			CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
			col5.setLibelle("UB");
			col5.setOrderKeyPath(COL_GES_CODE_KEY);
			col5.setRowCssClass("alignToCenter");
			CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_GES_CODE_KEY, "emptyValue");
			col5.setAssociations(ass5);
			_colonnesMap.takeValueForKey(col5, COL_GES_CODE_KEY);

			//		CktlAjaxTableViewColumn ttc = new CktlAjaxTableViewColumn();
			//		ttc.setLibelle("TTC");
			//		ttc.setOrderKeyPath(COL_DPCO_TTC_SAISIE_KEY);
			//		ttc.setRowCssClass("alignToRight");
			//		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DPCO_TTC_SAISIE_KEY, "emptyValue");
			//		ttc.setAssociations(ass3);
			//		_colonnesMap.takeValueForKey(ttc, COL_DPCO_TTC_SAISIE_KEY);

			CktlAjaxTableViewColumn utilisateur = new CktlAjaxTableViewColumn();
			utilisateur.setLibelle("Créé par");
			utilisateur.setOrderKeyPath(COL_UTILISATEUR_KEY);
			CktlAjaxTableViewColumnAssociation assliquideur = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_UTILISATEUR_KEY, "emptyValue");
			utilisateur.setAssociations(assliquideur);
			utilisateur.setRowCssClass("alignToCenter");
			_colonnesMap.takeValueForKey(utilisateur, COL_UTILISATEUR_KEY);

			//		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
			//		col5.setLibelle("Test");
			//		col5.setOrderKeyPath(COL_DATE_KEY);
			//		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(IND_KEY + COL_DATE_KEY, "emptyValue");
			//		ass5.setDateFormat("%d/%m/%Y");
			//		//ass5.setFormatter("dateFormatter");
			//		col5.setAssociations(ass5);
			//		_colonnesMap.takeValueForKey(col5, COL_DATE_KEY);

			CktlAjaxTableViewColumn borEtat = new CktlAjaxTableViewColumn();
			borEtat.setLibelle("Etat");
			borEtat.setOrderKeyPath(COL_BOR_ETAT_KEY);
			CktlAjaxTableViewColumnAssociation assEtat = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_BOR_ETAT_KEY, "emptyValue");
			borEtat.setAssociations(assEtat);
			utilisateur.setRowCssClass("alignToCenter");
			_colonnesMap.takeValueForKey(borEtat, COL_BOR_ETAT_KEY);

			CktlAjaxTableViewColumn colDateVisa = new CktlAjaxTableViewColumn();
			colDateVisa.setLibelle("Date visa");
			CktlAjaxTableViewColumnAssociation assDateVisa = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DATE_VISA_KEY, null);
			colDateVisa.setAssociations(assDateVisa);
			assDateVisa.setDateformat("dd/MM/yyyy");
			borEtat.setRowCssClass("alignToCenter");
			_colonnesMap.takeValueForKey(colDateVisa, COL_DATE_VISA_KEY);

			CktlAjaxTableViewColumn nbObjets = new CktlAjaxTableViewColumn();
			nbObjets.setLibelle("Nombre");
			CktlAjaxTableViewColumnAssociation assObjets = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_NB_KEY, null);
			nbObjets.setAssociations(assObjets);
			assObjets.setNumberformat("0");
			nbObjets.setRowCssClass("alignToCenter");
			_colonnesMap.takeValueForKey(nbObjets, COL_NB_KEY);

			CktlAjaxTableViewColumn montant = new CktlAjaxTableViewColumn();
			montant.setLibelle("Montant");
			CktlAjaxTableViewColumnAssociation assMontant = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_MONTANT_KEY, null);
			montant.setAssociations(assMontant);
			assMontant.setFormatter(Application.APP2DECIMALES_FORMATTER_KEY);
			montant.setRowCssClass("alignToRight");
			_colonnesMap.takeValueForKey(montant, COL_MONTANT_KEY);

			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.objectForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}

		return keys;
	}

	public String emptyValue() {
		return "";
	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(BINDING_dg);
	}

	public WOActionResults filtrer() {
		dg().setSelectedObjects(NSArray.EmptyArray);
		return (WOActionResults) performParentAction((String) valueForBinding(BINDING_actionFiltrerName));
	}

	public Boolean showFiltres() {
		return booleanValueForBinding(BINDING_showFiltres, Boolean.TRUE);
	}

	public String listeContainerId() {
		return getComponentId() + "_listeContainer";
	}

	public WOActionResults callbackOnSelectionner() {
		if (hasBinding(BINDING_callbackOnSelectionner)) {
			return performParentAction((String) valueForBinding(BINDING_callbackOnSelectionner));
		}
		return null;
	}

	public NSArray<String> getEtatsForBord() {
		return ETATSFORBORD;
	}

	public String getSelectedEtatForBord() {
		return selectedEtatForBord;
	}

	public void setSelectedEtatForBord(String selectedEtatForBord) {
		this.selectedEtatForBord = selectedEtatForBord;
	}

	public String onFiltreComplete() {
		if (valueForBinding(BINDING_updateContainerID) != null) {
			return "function(oc){" + containerFiltreId() + "Update();" + valueForBinding(BINDING_updateContainerID) + "Update();}";
		}
		return "";
	}

	public String dateCreationMinId() {
		return getComponentId() + "_dateCreationMin";
	}

	public String dateCreationMaxId() {
		return getComponentId() + "_dateCreationMax";
	}

	public String containerFiltreId() {
		return getComponentId() + "_containerFiltre";
	}

	public NSNumberFormatter decimalFormatter() {
		return (NSNumberFormatter) valueForBinding(BINDING_decimalFormatter);
	}

}