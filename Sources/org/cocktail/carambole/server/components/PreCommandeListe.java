/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;

public class PreCommandeListe extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	public WODisplayGroup dg = null;
	private NSArray<NSDictionary<String, Object>> lesPreCommandes = null;
	private NSArray<EOExercice> lesExercices = null;
	private NSArray<EOUtilisateur> lesUtilisateurs = null;
	private EOUtilisateur unUtilisateur = null;
	private EOExercice unExercice = null;
	private NSDictionary<String, Object> laCommande = null;

	public PreCommandeListe(WOContext context) {
		super(context);
		setDgActif(dgPreCommande());
	}

	public boolean isUtilisateurSansDroit() {
		return !session.getUtilisateurAvecDroit().booleanValue();
	}

	public WOComponent precedentes() {
		dg.displayNextBatch();
		dg.setSelectedObject(null);
		return null;
	}

	public boolean isPrecedentesDisabled() {
		boolean isPrecedentesDisabled = false;
		int batchCount = dg.batchCount();
		int currentBatchCount = dg.currentBatchIndex();

		if (currentBatchCount == batchCount) {
			isPrecedentesDisabled = true;
		}
		return isPrecedentesDisabled;
	}

	public WOComponent suivantes() {
		dg.displayPreviousBatch();
		dg.setSelectedObject(null);
		return null;
	}

	public boolean isSuivantesDisabled() {
		boolean isSuivantesDisabled = false;
		int currentBatchCount = dg.currentBatchIndex();

		if (currentBatchCount == 1) {
			isSuivantesDisabled = true;
		}
		return isSuivantesDisabled;
	}

	@SuppressWarnings("unchecked")
	public WOComponent filtrer() {
		NSMutableDictionary<String, Object> bdgs = dg.queryBindings();
		EOUtilisateur createur = (EOUtilisateur) bdgs.objectForKey("createur");

		lesPreCommandes = dg.allObjects();
		if (createur != null) {
			bdgs.setObjectForKey(createur, "createur");
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("NOMPRENOM=%@", new NSArray<String>(createur.nomPrenom()));
			lesPreCommandes = EOQualifier.filteredArrayWithQualifier(lesPreCommandes, qual);
		}
		return null;
	}

	public WOComponent detailler() {
		DetailCommande nextPage = null;
		NSDictionary<String, Object> commande = getLaCommande();
		if (commande != null) {
			nextPage = (DetailCommande) pageWithName("DetailCommande");
			EOEditingContext edc = edc();
			EOEditingContext newEdc = ERXEC.newEditingContext();
			EOExercice exercice = (EOExercice) getDg().queryBindings().objectForKey("exercice");
			exercice = (EOExercice) newEdc.faultForGlobalID(edc.globalIDForObject(exercice), newEdc);
			Number numero = (Number) commande.objectForKey("NUMERO");
			session.setLaCommandeEnCours(null);
			session.setNestedEdc(newEdc);
			EOCommande commandeADetailler = FinderCommande.getCommande(newEdc, exercice, numero);
			session.setLaCommandeEnCours(commandeADetailler);
			nextPage.setLaCommande(commandeADetailler);
		}
		return nextPage;
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"ActualiserLesPreCommandes",
				"Accueil"
		});

		return items;
	}

	public NSDictionary<String, Object> getLaCommande() {
		return laCommande;
	}

	public void setLaCommande(NSDictionary<String, Object> laCommande) {
		this.laCommande = laCommande;
	}

	public EOExercice getUnExercice() {
		return unExercice;
	}

	public void setUnExercice(EOExercice unExercice) {
		this.unExercice = unExercice;
	}

	public NSArray<EOUtilisateur> getLesUtilisateurs() {
		if (lesUtilisateurs == null) {
			lesUtilisateurs = FinderUtilisateur.getUtilisateurs(edc(), utilisateur(), null, getSelectedExercice());
		}
		return lesUtilisateurs;
	}

	public void setLesUtilisateurs(NSArray<EOUtilisateur> lesUtilisateurs) {
		this.lesUtilisateurs = lesUtilisateurs;
	}

	public EOUtilisateur getUnUtilisateur() {
		return unUtilisateur;
	}

	public void setUnUtilisateur(EOUtilisateur unUtilisateur) {
		this.unUtilisateur = unUtilisateur;
	}

	public WODisplayGroup getDg() {
		if (dg == null) {
			dg = (WODisplayGroup) valueForBinding("dg");
		}
		return dg;
	}

	public void setDg(WODisplayGroup dg) {
		this.dg = dg;
	}

	public NSArray<EOExercice> getLesExercices() {
		if (lesExercices == null) {
			lesExercices = FinderExercice.getExercices(edc());
		}
		return lesExercices;
	}

	//	public void setLesExercices(NSArray<EOExercice> lesExercices) {
	//		this.lesExercices = lesExercices;
	//	}

	@SuppressWarnings("unchecked")
	public NSArray<NSDictionary<String, Object>> getLesPreCommandes() {
		if (lesPreCommandes == null) {
			lesPreCommandes = getDg().allObjects();
		}
		return lesPreCommandes;
	}

	//	public void setLesPreCommandes(NSArray lesPreCommandes) {
	//		this.lesPreCommandes = lesPreCommandes;
	//	}

	public EOExercice getSelectedExercice() {
		return (EOExercice) dg.queryBindings().valueForKey(EOCommande.EXERCICE_KEY);

	}

	public void setSelectedExercice(EOExercice selectedExercice) {
		dg.queryBindings().takeValueForKey(selectedExercice, EOCommande.EXERCICE_KEY);
		setLesUtilisateurs(null);
	}

}