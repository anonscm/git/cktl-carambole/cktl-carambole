/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.fwkcktldepense.server.finder.FinderArticle;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogue;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

public class Articles extends CommandeComponent {

	private static final long serialVersionUID = 1L;
	public WODisplayGroup dg = null;
	public EOCatalogue unArticle = null;
	public boolean isAfficherListeArticles = false;
	public boolean isAfficherCreerArticle = false;
	public EOCodeExer unCodeExer = null;
	public EOCodeExer unCodeExerSelectionne = null;

	public Articles(WOContext context) {
		super(context);
		initDG();
	}

	private void initDG() {
		dg = new WODisplayGroup();
		dg.setNumberOfObjectsPerBatch(20);
	}

	public void awake() {
		super.awake();
		initialiserBindings();
	}

	@SuppressWarnings("unchecked")
	public void initialiserBindings() {
		EOCommande laCommande = laCommandeEnCours();
		EOArticle leDernierArticle = null;
		NSArray<EOArticle> articles = laCommande.articles();
		if (articles != null && articles.count() > 0) {
			leDernierArticle = (EOArticle) articles.lastObject();
		}
		dg.queryBindings().removeAllObjects();
		dg.queryBindings().setObjectForKey(utilisateur(), "utilisateur");
		dg.queryBindings().setObjectForKey(laCommande.exercice(), "exercice");
		if (leDernierArticle != null) {
			dg.queryBindings().setObjectForKey(laCommande.fournisseur(), "fournisseur");
			dg.queryBindings().setObjectForKey(laCommande.fournisseur().fouCode(), "fouCode");
			dg.queryBindings().setObjectForKey(laCommande.fournisseur().fouNom(), "fouNom");
			dg.queryBindings().setObjectForKey(laCommande.typeEtat(), "typeEtat");
		}
	}

	public String idName() {
		String idName = "articles";
		String actif = session.onLoadBdCShowTab();
		if (actif.equals("2")) {
			idName = "actif";
		}
		return idName;
	}

	//    public NSArray getCodesExer(EOEditingContext ed) {
	//    	NSArray codes = FinderCodeExer.getCodeExerValidesPourExercice(ed, getExerciceCourant(ed));
	//    	return codes;
	//    }
	public void creerArticle() {
		setAfficherCreerArticle(true);
		setAfficherListeArticles(false);
	}

	public void modifierLaRecherche() {
		setAfficherCreerArticle(false);
		setAfficherListeArticles(false);
	}

	public void rechercher() {
		setAfficherCreerArticle(false);
		setAfficherListeArticles(true);

		@SuppressWarnings("unchecked")
		NSArray<EOCatalogueArticle> articles = FinderArticle.getArticles(laCommandeEnCours().editingContext(), dg.queryBindings());
		dg.setObjectArray(articles);
	}

	public void ajouterNouvelArticle() {
	}

	public WODisplayGroup getDg() {
		return dg;
	}

	public void setDg(WODisplayGroup dg) {
		this.dg = dg;
	}

	public EOCatalogue getUnArticle() {
		return unArticle;
	}

	public void setUnArticle(EOCatalogue unArticle) {
		this.unArticle = unArticle;
	}

	public boolean isAfficherCreerArticle() {
		return isAfficherCreerArticle;
	}

	public void setAfficherCreerArticle(boolean isAfficherCreerArticle) {
		this.isAfficherCreerArticle = isAfficherCreerArticle;
	}

	public boolean isAfficherListeArticles() {
		return isAfficherListeArticles;
	}

	public void setAfficherListeArticles(boolean isAfficherListeArticles) {
		this.isAfficherListeArticles = isAfficherListeArticles;
	}

	public boolean isAfficherRechercherArticle() {
		boolean isAfficherRechercherArticle = true;
		if (isAfficherCreerArticle() || isAfficherListeArticles()) {
			isAfficherRechercherArticle = false;
		}
		return isAfficherRechercherArticle;
	}

	public EOCodeExer getUnCodeExer() {
		return unCodeExer;
	}

	public void setUnCodeExer(EOCodeExer unCodeExer) {
		this.unCodeExer = unCodeExer;
	}

	public EOCodeExer getUnCodeExerSelectionne() {
		return unCodeExerSelectionne;
	}

	public void setUnCodeExerSelectionne(EOCodeExer unCodeExerSelectionne) {
		this.unCodeExerSelectionne = unCodeExerSelectionne;
	}

}