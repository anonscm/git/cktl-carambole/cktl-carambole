/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.NavigationCtrl;
import org.cocktail.fwkcktldepense.client.metier.EOPreDepenseControlePlanComptableInventaire;
import org.cocktail.fwkcktldepense.server.ConvertHelper;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryReversementBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryReversementPapier;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOMandat;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.process.ProcessDepense;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;

/**
 * Affiche le detail d'une depensePapier (avec les liquidations ou pre-liquidations associées).
 * 
 * @binding isSelectionPossible Boolean indique si l'utilisateur peut effectuer une selection sur le n° de facture.
 * @binding laDepensePapier EODepensePapier Obligatoire
 * @binding showLiquidations Boolean Indique si le composant doit afficher les liquidations
 * @binding showPreLiquidations Boolean Indique si le composant doit afficher les pre-liquidations
 * @binding showRepartCN Boolean Indique si le composant doit afficher la repartition des CN
 * @binding showReimputations Boolean Indique si le composant doit afficher les réimputations effectuées
 * @binding showReimputer Boolean Indique si le composant doit afficher le lien pour creer une reimputation
 * @binding showReverser Boolean Indique si le composant doit afficher le lien pour creer un ordre de reversement
 * @binding showSupprimerDepenseBudget Boolean Indique si le composant doit afficher le lien pour la suppression des depenseBudgets
 * @binding onSelectDepensePapierCallback String Nom de l'action à appeler lors de la selection
 * @author rprin
 */
public class LiquidationDetail extends MyAjaxComponent {

	private static final long serialVersionUID = 1L;

	public static final String BINDING_laDepensePapier = "laDepensePapier";
	public static final String BINDING_onSelectDepensePapierCallback = "onSelectDepensePapierCallback";
	//public static final String BINDING_selectedDepensePapier = "selectedDepensePapier";
	public static final String BINDING_showLiquidations = "showLiquidations";
	public static final String BINDING_showPreLiquidations = "showPreLiquidations";
	public static final String BINDING_showRepartCN = "showRepartCN";
	public static final String BINDING_isSelectionPossible = "isSelectionPossible";
	public static final String BINDING_showReverser = "showReverser";
	public static final String BINDING_showSupprimerDepenseBudget = "showSupprimerDepenseBudget";
	public static final String BINDING_showReimputations = "showReimputations";
	public static final String BINDING_showReimputer = "showReimputer";

	//private EODepensePapier uneDepensePapier = null;
	private _IDepenseBudget uneDepenseBudget = null;

	private _IDepenseControleHorsMarche uneDepenseControleHorsMarche = null;
	private _IDepenseControleAction uneDepenseControleAction = null;
	private _IDepenseControlePlanComptable uneDepenseControlePlanCo = null;
	public EOInventaire uneDepenseControlePlanCoInventaire = null;
	private _IDepenseControleConvention uneDepenseControleConvention = null;
	private _IDepenseControleAnalytique uneDepenseControleAnalytique = null;

	public LiquidationDetail(WOContext context) {
		super(context);
	}

	public EODepensePapier getLaDepensePapier() {
		return (EODepensePapier) valueForBinding(BINDING_laDepensePapier);
	}

	public WOActionResults onSelectDepensePapier() {

		if (hasBinding(BINDING_onSelectDepensePapierCallback)) {
			return performParentAction((String) valueForBinding(BINDING_onSelectDepensePapierCallback));
		}
		else {
			return null;
		}

	}

	public NSArray<? extends _IDepenseBudget> lesDepenseBudgets() {
		if (showAll()) {
			ArrayList<NSArray<? extends _IDepenseBudget>> tabs = new ArrayList<NSArray<? extends _IDepenseBudget>>();
			tabs.add(getLaDepensePapier().depenseBudgets());
			tabs.add(getLaDepensePapier().preDepenseBudgets());
			return NSArrayCtrl.unionOfNSArrays(tabs);
		}

		if (showLiquidations().booleanValue()) {
			return getLaDepensePapier().depenseBudgets();
		}
		if (showPreLiquidations().booleanValue()) {
			return getLaDepensePapier().preDepenseBudgets();
		}
		return NSArray.emptyArray();
	}

	public Boolean showDepensePapier() {
		if (getLaDepensePapier().isPreLiquidation()) {
			return showPreLiquidations();
		}
		else {
			return showLiquidations();
		}
	}

	public Boolean showAll() {
		return Boolean.valueOf(showLiquidations().booleanValue() && showPreLiquidations().booleanValue());
	}

	public Boolean showLiquidations() {
		return booleanValueForBinding(BINDING_showLiquidations, Boolean.TRUE);
	}

	public Boolean showPreLiquidations() {
		return booleanValueForBinding(BINDING_showPreLiquidations, Boolean.TRUE);
	}

	public _IDepenseBudget getUneDepenseBudget() {
		return uneDepenseBudget;
	}

	public void setUneDepenseBudget(_IDepenseBudget uneDepenseBudget) {
		this.uneDepenseBudget = uneDepenseBudget;
	}

	public Boolean isAfficherRepartCN() {
		return booleanValueForBinding(BINDING_showRepartCN, Boolean.TRUE);
	}

	public _IDepenseControleHorsMarche getUneDepenseControleHorsMarche() {
		return uneDepenseControleHorsMarche;
	}

	public void setUneDepenseControleHorsMarche(
			_IDepenseControleHorsMarche uneDepenseControleHorsMarche) {
		this.uneDepenseControleHorsMarche = uneDepenseControleHorsMarche;
	}

	public _IDepenseControleAction getUneDepenseControleAction() {
		return uneDepenseControleAction;
	}

	public void setUneDepenseControleAction(
			_IDepenseControleAction uneDepenseControleAction) {
		this.uneDepenseControleAction = uneDepenseControleAction;
	}

	public _IDepenseControleAnalytique getUneDepenseControleAnalytique() {
		return uneDepenseControleAnalytique;
	}

	public void setUneDepenseControleAnalytique(
			_IDepenseControleAnalytique uneDepenseControleAnalytique) {
		this.uneDepenseControleAnalytique = uneDepenseControleAnalytique;
	}

	public _IDepenseControleConvention getUneDepenseControleConvention() {
		return uneDepenseControleConvention;
	}

	public void setUneDepenseControleConvention(
			_IDepenseControleConvention uneDepenseControleConvention) {
		this.uneDepenseControleConvention = uneDepenseControleConvention;
	}

	public _IDepenseControlePlanComptable getUneDepenseControlePlanCo() {
		return uneDepenseControlePlanCo;
	}

	public void setUneDepenseControlePlanCo(
			_IDepenseControlePlanComptable uneDepenseControlePlanCo) {
		this.uneDepenseControlePlanCo = uneDepenseControlePlanCo;
	}

	public Boolean isSelectionPossible() {
		return booleanValueForBinding(BINDING_isSelectionPossible, Boolean.FALSE);
	}

	public EOMandat unMandat() {
		EOMandat unMandat = null;
		_IDepenseBudget depenseBudget = getUneDepenseBudget();
		if (depenseBudget != null) {
			NSArray<? extends _IDepenseControlePlanComptable> depenseControlePlanComptables = depenseBudget.depenseControlePlanComptables();
			if (depenseControlePlanComptables != null && depenseControlePlanComptables.count() > 0) {
				_IDepenseControlePlanComptable depenseControlePlanComptable = (_IDepenseControlePlanComptable) depenseControlePlanComptables.lastObject();
				if (depenseControlePlanComptable instanceof EODepenseControlePlanComptable) {
					unMandat = ((EODepenseControlePlanComptable) depenseControlePlanComptable).mandat();
				}
			}
		}
		return unMandat;
	}

	public WOComponent supprimerLaDepenseBudget() {
		WOComponent nextPage = null;
		EODepenseBudget laDepenseBudgetASupprimer = (EODepenseBudget) getUneDepenseBudget();
		EODepensePapier dp = laDepenseBudgetASupprimer.depensePapier();
		EOTypeApplication tyap = laDepenseBudgetASupprimer.engagementBudget().typeApplication();
		if (EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK.equals(tyap.tyapLibelle())) {
			EOCommande commande = laCommandeEnCours();
			EOEngagementBudget engageEnCours = session.getLEngagementBudgetEnCours();
			Integer commNumero = (commande != null ? commande.commNumero() : null);
			Integer engNumero = (engageEnCours != null ? engageEnCours.engNumero() : null);

			try {
				EOEditingContext edc = laDepenseBudgetASupprimer.editingContext();
				ProcessDepense.supprimer(session.dataBus(), edc, laDepenseBudgetASupprimer, utilisateurInContext(edc));

				if (dp.depenseBudgets() == null || dp.depenseBudgets().count() == 0) {
					EOEditingContext newEdc = ERXEC.newEditingContext();
					EOExercice exercice = (EOExercice) newEdc.faultForGlobalID(edc.globalIDForObject(dp.exercice()), newEdc);
					session.setLaCommandeEnCours(null);
					session.setLaDepensePapierEnCours(null);
					session.setLEngagementBudgetEnCours(null);
					session.setAlertMessage(null);
					session.setNestedEdc(newEdc);

					if (commande != null) {
						commande = FinderCommande.getCommande(newEdc, exercice, commNumero);
						session.setLaCommandeEnCours(commande);
						nextPage = (DetailCommande) session.getSavedPageWithName("DetailCommande");
						((DetailCommande) nextPage).setLaCommande(commande);
					}
					else {
						//on recupere l'engagement car celui-ci a peut-etre été supprimé avec la liquidation
						engageEnCours = FinderEngagementBudget.getEngagementBudget(newEdc, exercice, engNumero);
						if (engageEnCours != null) {
							session.setLEngagementBudgetEnCours(engageEnCours);
							nextPage = (DetailEngagementBudget) session.getSavedPageWithName(DetailEngagementBudget.class.getName());
						}
						else {
							nextPage = NavigationCtrl.goToAccueil(context());
						}
					}

				}

			} catch (FactoryException e) {
				String alertMessage = e.getMessageFormatte();
				if (e.isBloquant()) {
					nextPage = null;
					if (e.isInformatif()) {
						// Exception contenant un message d'information pour l'utilisateur
						session.setAlertMessage(alertMessage);
					}
					else {
						e.printStackTrace();
						throw e;
					}
				}
				else {
					session.setAlertMessage(alertMessage);
					if (commande != null) {
						session.setLaCommandeEnCours(commande);
						nextPage = (DetailCommande) session.getSavedPageWithName("DetailCommande");
						((DetailCommande) nextPage).setLaCommande(commande);
					}
					else if (engageEnCours != null) {
						nextPage = (DetailEngagementBudget) session.getSavedPageWithName(DetailEngagementBudget.class.getName());
					}
					else {
						nextPage = NavigationCtrl.goToAccueil(context());
					}
				}
			}

		}
		else {
			session.setAlertMessage("Il n'est pas possible de supprimer une liquidation générée dans une autre applications que Carambole.");
		}
		return nextPage;
	}

	public WOComponent reverserLaDepenseBudget() {
		WOComponent nextPage = null;
		EODepenseBudget laDepenseBudget = (EODepenseBudget) getUneDepenseBudget();
		EOTypeApplication tyap = laDepenseBudget.engagementBudget().typeApplication();
		if (EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK.equals(tyap.tyapLibelle())) {
			if (laDepenseBudget.depensePapier().isAExtourner()) {
				session.setAlertMessage("Les ORV ne sont pas autorisés pour des liquidations de charges à payer à extourner.");
			}
			else {
				nextPage = (Reversement) pageWithName(Reversement.class.getName());
				EOEditingContext edc = laDepenseBudget.editingContext();
				FactoryReversementPapier frp = new FactoryReversementPapier();
				EODepensePapier depensePapier = frp.creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), "", laDepenseBudget.exercice(), utilisateurInContext(edc), laDepenseBudget);
				FactoryReversementBudget frb = new FactoryReversementBudget();
				EODepenseBudget laDepenseBudgetAReverser = frb.creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), depensePapier, laDepenseBudget, laDepenseBudget.exercice(), utilisateurInContext(edc));
				session.setLaDepenseBudgetReversement(laDepenseBudgetAReverser);
				((Reversement) nextPage).setLaDepenseBudget(laDepenseBudgetAReverser);
			}
		}
		else {
			session.setAlertMessage("Il n'est pas possible de créer un ordre de reversement à partir d'une liquidation générée dans une autre applications que Carambole.");
		}
		return nextPage;
	}

	public WOComponent reimputerLaDepenseBudget() {
		WOComponent nextPage = null;
		EODepenseBudget laDepenseBudget = (EODepenseBudget) getUneDepenseBudget();
		if (laDepenseBudget.engagementBudget().isReimputable(getUneDepenseBudget().editingContext(), session.getUtilisateur(), null)) {
			//			nextPage = (Reimputation) pageWithName(Reimputation.class.getName());
			nextPage = (Reimputation2) pageWithName(Reimputation2.class.getName());
			//EOEditingContext edc = laDepenseBudget.editingContext();
			session.setLaDepenseBudgetReimputation(laDepenseBudget);
			((Reimputation2) nextPage).setLaDepenseBudget(laDepenseBudget);
			((Reimputation2) nextPage).initialisation();
			return nextPage;
		}
		else {
			session.setAlertMessage("La réimputation n'est pas possible pour les liquidations générées dans d'autres applications que Carambole.");
		}
		return nextPage;
	}

	public Boolean isDepenseBudgetReimputable() {
		return Boolean.valueOf(showReimputer().booleanValue() && getUneDepenseBudget().isReimputable(getUneDepenseBudget().editingContext(), session.getUtilisateur(), null));
	}

	public Boolean isDepensebudgetSupprimable() {
		return Boolean.valueOf(showSupprimerDepenseBudget().booleanValue() && (getUneDepenseBudget() instanceof EODepenseBudget) && ((EODepenseBudget) getUneDepenseBudget()).isSupprimable());
	}

	public Boolean isDepenseBudgetReversable() {
		return Boolean.valueOf(showReverser().booleanValue() && (getUneDepenseBudget() instanceof EODepenseBudget) && ((EODepenseBudget) getUneDepenseBudget()).isReversable());
	}

	public Boolean showReverser() {
		return booleanValueForBinding(BINDING_showReverser, Boolean.FALSE);
	}

	public Boolean showReimputer() {
		return booleanValueForBinding(BINDING_showReimputer, Boolean.FALSE);
	}

	public Boolean showSupprimerDepenseBudget() {
		return booleanValueForBinding(BINDING_showSupprimerDepenseBudget, Boolean.FALSE);
	}

	public Boolean showReimputations() {
		return Boolean.valueOf(booleanValueForBinding(BINDING_showReimputations, Boolean.FALSE).booleanValue() && (getUneDepenseBudget() instanceof EODepenseBudget));
	}

	public BigDecimal getMontantBudInventaire() {
		//uneDepenseControlePlanCoInventaire.lidMontant
		if (getUneDepenseBudget() instanceof EOPreDepenseBudget) {
			NSArray<org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptableInventaire> res = ((EOPreDepenseControlePlanComptable) getUneDepenseControlePlanCo()).preDepenseControlePlanComptableInventaires(new EOKeyValueQualifier(
					EOPreDepenseControlePlanComptableInventaire.INVENTAIRE_KEY, EOQualifier.QualifierOperatorEqual, uneDepenseControlePlanCoInventaire));
			if (res.count() > 0) {
				return res.objectAtIndex(0).pdpinMontantBudgetaire();
			}
			//return  ( (EOPreDepenseControlePlanComptable)getUneDepenseControlePlanCo()).preDepenseControlePlanComptableInventaires(new EOKeyValueQualifier(EOPreDepenseControlePlanComptableInventaire.INVENTAIRE_KEY, EOQualifier.QualifierOperatorEqual, uneDepenseControlePlanCoInventaire)).p   uneDepenseControlePlanCoInventaire.
		}
		else {
			return uneDepenseControlePlanCoInventaire.lidMontant();
		}
		return null;
	}

	public Boolean isPreDepense() {
		return getUneDepenseBudget() instanceof EOPreDepenseBudget;
	}

	public EORib fwkPersonneRib() {
		EORib res = null;
		if (getLaDepensePapier() != null && getLaDepensePapier().ribFournisseur() != null) {
			res = ConvertHelper.convertDepenseRibToPersonneRib(edc(), getLaDepensePapier().ribFournisseur());
		}
		return res;
	}

	public String getApplicationOrigine() {
		NSArray<? extends _IDepenseBudget> res = lesDepenseBudgets();
		if (res.count() > 0 && ((_IDepenseBudget) res.objectAtIndex(0)).engagementBudget() != null && ((_IDepenseBudget) res.objectAtIndex(0)).engagementBudget().typeApplication() != null) {
			return ((_IDepenseBudget) res.objectAtIndex(0)).engagementBudget().typeApplication().tyapLibelle();
		}
		return "nc";
	}

	public String getStyleForSourceType() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudget());
	}

	/**
	 * @return l'objet depenseControleHorsMarche s'il est unique
	 */
	public _IDepenseControleMarche getDepenseCtrlMarcheUnique() {
		EODepensePapier depensePapierEnCours = getLaDepensePapier();
		if (depensePapierEnCours.currentDepenseBudgets().count() == 1
				&& depensePapierEnCours.currentDepenseBudgets().objectAtIndex(0).depenseControleMarches() != null
				&& depensePapierEnCours.currentDepenseBudgets().objectAtIndex(0).depenseControleMarches().count() == 1) {
			return (depensePapierEnCours.currentDepenseBudgets().objectAtIndex(0)).depenseControleMarches().objectAtIndex(0);
		}
		return null;
	}
}
