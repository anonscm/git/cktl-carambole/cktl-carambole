/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

// Generated by the WOLips Templateengine Plug-in at 11 fevr. 2007 17:42:01

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.process.ProcessCommande;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;

public class CommandeAttestationMAPA extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	public EOCtrlSeuilMAPA unSeuil = null;
	public NSArray<EOCtrlSeuilMAPA> lesSeuils = null;

	public CommandeAttestationMAPA(WOContext context) {
		super(context);
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		//		String message = session.getAlertMessage();
		//		if (message != null && message.equals("") == false) {
		//			String str = "<script language=\"javascript\" type=\"text/javascript\">\n";
		//			str += "alert(\"" + message + "\");\n";
		//			str += "</script>";
		//			res.appendContentString(str);
		//			session.setAlertMessage(null);
		//		}
	}

	public NSArray<EOCtrlSeuilMAPA> getLesSeuils() {
		if (lesSeuils == null && laCommandeEnCours() != null) {
			lesSeuils = laCommandeEnCours().seuilsMAPADepasses();
		}
		return lesSeuils;
	}

	public void setLesSeuils(NSArray<EOCtrlSeuilMAPA> lesSeuils) {
		this.lesSeuils = lesSeuils;
	}

	public WOComponent annuler() {
		return null;
	}

	public WOComponent engager() {
		WOComponent nextPage = null;

		try {
			EOCommande commande = laCommandeEnCours();
			EOEditingContext edc = commande.editingContext();
			ProcessCommande.enregistrer(session.dataBus(), edc, commande, utilisateurInContext(edc));
			session.setAlertMessage(null);
			// On refetche la commande pour la rafraichir
			EOExercice exercice = commande.exercice();
			Number numero = commande.commNumero();
			commande = FinderCommande.getCommande(ERXEC.newEditingContext(), exercice, numero);
			session.setLaCommandeEnCours(commande);
			nextPage = pageWithName("CommandeApresEnregistrement");
		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				nextPage = null;
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					session.setAlertMessage(alertMessage);
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				session.setAlertMessage(alertMessage);
				nextPage = pageWithName("CommandeApresEnregistrement");
			}
		} catch (RuntimeException e1) {
			e1.printStackTrace();
			throw e1;
		}

		return nextPage;
	}

}