/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.controleurs.LiquidationExtourneListeCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LiquidationExtourneListe extends MyAjaxComponent {

	public static final String PAGE_NAME = LiquidationExtourneListe.class.getName();

	private static final long serialVersionUID = 1L;
	private static final NSMutableArray<String> ITEMS_MENU = new NSMutableArray<String>();
	private static final NSMutableArray<String> UPDATE_CONTAINER_IDS = new NSMutableArray<String>();

	static {
		ITEMS_MENU.add(Menu.LIQUIDER_SUR_EXTOURNE_MANDAT);
		ITEMS_MENU.add(Menu.SOLDER_EXTOURNE_MANDAT);
		ITEMS_MENU.add(Menu.ACCUEIL_KEY);

		UPDATE_CONTAINER_IDS.add(Menu.MENU_CONTAINER_ID);
	}

	private LiquidationExtourneListeCtrl ctrl;

	public LiquidationExtourneListe(WOContext context) {
		super(context);
		this.ctrl = new LiquidationExtourneListeCtrl(this);
	}

	public NSArray<String> itemsMenu() {
		return ITEMS_MENU;
	}

	public void initPage() {
		this.ctrl.initListeLiquidationsExtourne();
		session.setLiquidationExtourneEnCours(null);
	}

	// ------------------------------------------
	// actions
	// ------------------------------------------
	public WOActionResults appliquerFiltres() {
		ctrl.appliquerFiltres();
		majSession();
		return null;
	}

	public WOActionResults onSelect() {
		ctrl.onSelect();
		majSession();
		return null;
	}

	private void majSession() {
		session.setLiquidationExtourneEnCours(ctrl.getDepenseBudgetSelectionnee());
	}

	// ------------------------------------------
	// proprietes calculees
	// ------------------------------------------
	public NSArray<String> containersToUpdateList() {
		return UPDATE_CONTAINER_IDS;
	}

	// ------------------------------------------
	// bindings
	// ------------------------------------------

	// ------------------------------------------
	// getter - setter
	// ------------------------------------------
	public LiquidationExtourneListeCtrl getCtrl() {
		return ctrl;
	}
}