/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.HashMap;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderProrata;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EORibFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Saisie des reimputation ordonnateurs.
 * 
 * @author rprin
 */
public class Reimputation2 extends MyAjaxPage {
	private static final String PAGE_TITLE = "Réimputation";
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private boolean isModificationEnCours = false;

	private NSMutableDictionary<String, NSArray<EOTypeAction>> dicoLesActions = null;
	private NSMutableDictionary<String, NSArray<EOPlanComptable>> dicoLesPlansComptable = null;
	private NSMutableDictionary<String, NSArray<EOConvention>> dicoLesConventions = null;
	private NSMutableDictionary<String, NSArray<EOCodeAnalytique>> dicoLesCodesAnalytique = null;

	public boolean isRibDisabled = true;
	public boolean isFactureDisabled = true;
	public boolean isEngagementDisabled = true;
	public boolean isCocheSoldeeDisabled = true;

	//	private String filterTextModeDePaiement = null;
	//	private NSArray<EOModePaiement> modesDePaiement = null;
	//	private EOModePaiement unModeDePaiement = null;
	//	private EOModePaiement unModeDePaiementSelectionne;
	//	private NSArray<EORibFournisseur> ribs = null;
	public EORibFournisseur unRib = null;
	//	private EORibFournisseur unRibSelectionne = null;
	public EODepensePapier laDepensePapier = null;
	private EODepenseBudget laDepenseBudget = null;
	private NSArray<EOTauxProrata> lesTauxDeProratas = null;
	public EOTauxProrata unTauxDeProrata = null;
	//private EOTauxProrata unTauxDeProrataSelectionne = null;
	//public BigDecimal depHtSaisie = null, depTtcSaisie = null;
	public boolean depIsSoldee = false;

	public int indexDepenseBudgetsForMarche;
	private EODepenseControleMarche uneDepenseCtrlMarche = null;
	public int indexDepenseControleMarche;

	// Repartition par code de nomenclature
	private NSArray<EOCodeExer> lesCodesNomenclature = null;
	private EODepenseControlePlanComptable uneDepenseCtrlImputation = null;
	private EOInventaire unInventaire = null;
	// Repartition par action
	private NSArray<EOTypeAction> lesActions = null;
	private NSArray<EOPlanComptable> lesImputations = null;
	public int indexInventaire;
	private NSArray<EOConvention> lesConventions = null;
	private NSArray<EOCodeAnalytique> lesCodeAnalytiques = null;
	private NSArray<EODepenseBudget> lesDepenseBudgets = null;

	public FactoryDepenseControleHorsMarche fdchm = new FactoryDepenseControleHorsMarche();
	public FactoryDepenseControleAction fdca = new FactoryDepenseControleAction();
	public FactoryDepenseControlePlanComptable fdcpc = new FactoryDepenseControlePlanComptable();
	public FactoryDepenseControleConvention fdcc = new FactoryDepenseControleConvention();
	public FactoryDepenseControleAnalytique fdcca = new FactoryDepenseControleAnalytique();

	public Reimputation2(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);

	}

	public void initialisation() {
		EOCommande commande = laCommandeEnCours();
		EOEditingContext edc = getLaDepensePapier().editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);
		//		lesCodesNomenclature = FinderCodeExer.getCodeExerPourCommande(edc, commande);

		dicoLesActions = new NSMutableDictionary<String, NSArray<EOTypeAction>>();
		dicoLesPlansComptable = new NSMutableDictionary<String, NSArray<EOPlanComptable>>();
		dicoLesConventions = new NSMutableDictionary<String, NSArray<EOConvention>>();
		dicoLesCodesAnalytique = new NSMutableDictionary<String, NSArray<EOCodeAnalytique>>();
		//NSArray depensesBudget = laDepensePapier.depenseBudgets();
		//Enumeration enumDepensesBudget = depensesBudget.objectEnumerator();
		//while (enumDepensesBudget.hasMoreElements()) {
		//EODepenseBudget uneDepenseBudget = (EODepenseBudget) getLaDepenseBudget();
		//		lesCodesNomenclature = uneDepenseBudget.getCodeExersPossiblesForReimputation(edc, utilisateur, commande);
		//		NSArray actions = uneDepenseBudget.getTypeActionsPossiblesForReimputation(edc, utilisateur);
		//		dicoLesActions.setObjectForKey(actions, uneDepenseBudget.source().libelleCourt());
		//		NSArray plansComptable = uneDepenseBudget.getPlanComptablesPossiblesForReimputation(edc, utilisateur);
		//		dicoLesPlansComptable.setObjectForKey(plansComptable, uneDepenseBudget.source().libelleCourt());
		//		NSArray conventions = uneDepenseBudget.getConventionsPossiblesForReimputation(edc, utilisateur);
		//		dicoLesConventions.setObjectForKey(conventions, uneDepenseBudget.source().libelleCourt());
		//		NSArray codesAnalytique = uneDepenseBudget.getCodeAnalytiquesPossiblesForReimputation(edc, utilisateur);
		//		dicoLesCodesAnalytique.setObjectForKey(codesAnalytique, uneDepenseBudget.source().libelleCourt());
		//}
		lesCodesNomenclature = getLaDepenseBudget().getCodeExersPossiblesForReimputation(edc, utilisateur, commande);
		initialiseObjectsForRepartition(getLaDepenseBudget());
		//lesTauxDeProratas = null;
		getLaDepenseBudget().prepareForReimputation();
		initialisePourcentages();

	}

	public void initialiseObjectsForRepartition(EODepenseBudget uneDepenseBudget) {
		//EOCommande commande = laCommandeEnCours();
		EOEditingContext edc = getLaDepensePapier().editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);

		//EODepenseBudget uneDepenseBudget = (EODepenseBudget) getLaDepenseBudget();
		//lesCodesNomenclature = uneDepenseBudget.getCodeExersPossiblesForReimputation(edc, utilisateur, commande);
		NSArray<EOTypeAction> actions = uneDepenseBudget.getTypeActionsPossiblesForReimputation(edc, utilisateur);
		dicoLesActions.setObjectForKey(actions, uneDepenseBudget.getSource().libelleCourt());
		NSArray<EOPlanComptable> plansComptable = uneDepenseBudget.getPlanComptablesPossiblesForReimputation(edc, utilisateur);
		dicoLesPlansComptable.setObjectForKey(plansComptable, uneDepenseBudget.getSource().libelleCourt());
		NSArray<EOConvention> conventions = uneDepenseBudget.getConventionsPossiblesForReimputation(edc, utilisateur);
		dicoLesConventions.setObjectForKey(conventions, uneDepenseBudget.getSource().libelleCourt());
		NSArray<EOCodeAnalytique> codesAnalytique = uneDepenseBudget.getCodeAnalytiquesPossiblesForReimputation(edc, utilisateur);
		dicoLesCodesAnalytique.setObjectForKey(codesAnalytique, uneDepenseBudget.getSource().libelleCourt());
	}

	private void initialisePourcentages() {
		NSArray<EODepenseControleAction> lesDepenseControleActions = getLaDepenseBudget().depenseControleActions();

		HashMap<Object, BigDecimal> dico = new HashMap<Object, BigDecimal>();

		for (int i = 0; i < lesDepenseControleActions.count(); i++) {
			EODepenseControleAction array_element = (EODepenseControleAction) lesDepenseControleActions.objectAtIndex(i);
			dico.put(array_element, array_element.dactTtcSaisie());
		}
		for (int i = 0; i < lesDepenseControleActions.count(); i++) {
			EODepenseControleAction array_element = (EODepenseControleAction) lesDepenseControleActions.objectAtIndex(i);
			array_element.setMontantTtc((BigDecimal) dico.get(array_element));
		}

		NSArray<EODepenseControleAnalytique> lesDepenseControleAnalytiques = getUneDepenseBudgetForAnalytique().depenseControleAnalytiques();
		//Initialiser les pourcentages
		for (int i = 0; i < lesDepenseControleAnalytiques.count(); i++) {
			EODepenseControleAnalytique array_element = (EODepenseControleAnalytique) lesDepenseControleAnalytiques.objectAtIndex(i);
			dico.put(array_element, array_element.danaTtcSaisie());
		}
		for (int i = 0; i < lesDepenseControleAnalytiques.count(); i++) {
			EODepenseControleAnalytique array_element = (EODepenseControleAnalytique) lesDepenseControleAnalytiques.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.danaTtcSaisie());
			array_element.setMontantTtc((BigDecimal) dico.get(array_element));
		}

		NSArray<EODepenseControleConvention> lesDepenseControleConventions = getUneDepenseBudgetForConvention().depenseControleConventions();
		//Initialiser les pourcentages
		for (int i = 0; i < lesDepenseControleConventions.count(); i++) {
			EODepenseControleConvention array_element = (EODepenseControleConvention) lesDepenseControleConventions.objectAtIndex(i);
			dico.put(array_element, array_element.dconTtcSaisie());
		}
		for (int i = 0; i < lesDepenseControleConventions.count(); i++) {
			EODepenseControleConvention array_element = (EODepenseControleConvention) lesDepenseControleConventions.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.dconTtcSaisie());
			array_element.setMontantTtc((BigDecimal) dico.get(array_element));
		}

		NSArray<EODepenseControleHorsMarche> lesDepenseControleHorsMarches = getLaDepenseBudget().depenseControleHorsMarches();
		//Initialiser les pourcentages
		for (int i = 0; i < lesDepenseControleHorsMarches.count(); i++) {
			EODepenseControleHorsMarche array_element = (EODepenseControleHorsMarche) lesDepenseControleHorsMarches.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.dhomTtcSaisie());
			dico.put(array_element, array_element.dhomTtcSaisie());
		}
		for (int i = 0; i < lesDepenseControleHorsMarches.count(); i++) {
			EODepenseControleHorsMarche array_element = (EODepenseControleHorsMarche) lesDepenseControleHorsMarches.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.dhomTtcSaisie());
			array_element.setMontantTtc((BigDecimal) dico.get(array_element));
		}

		NSArray<EODepenseControlePlanComptable> lesDepenseControleImputations = getUneDepenseBudgetForImputation().depenseControlePlanComptables();
		//Initialiser les pourcentages
		for (int i = 0; i < lesDepenseControleImputations.count(); i++) {
			EODepenseControlePlanComptable array_element = (EODepenseControlePlanComptable) lesDepenseControleImputations.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.dpcoTtcSaisie());
			dico.put(array_element, array_element.dpcoTtcSaisie());
		}
		for (int i = 0; i < lesDepenseControleImputations.count(); i++) {
			EODepenseControlePlanComptable array_element = (EODepenseControlePlanComptable) lesDepenseControleImputations.objectAtIndex(i);
			//array_element.setMontantTtc(array_element.dpcoTtcSaisie());
			array_element.setMontantTtc((BigDecimal) dico.get(array_element));
		}

		//initialisation intégrée
		getLesDepenseControleMarches();
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UNE_REIMPUTATION_KEY,
				Menu.ANNULER_UNE_REIMPUTATION_KEY
		});

		return items;
	}

	public WOComponent submitFacture() {
		isModificationEnCours = false;
		return null;
	}

	public WOComponent submitEngagement() {
		isModificationEnCours = false;
		return null;
	}

	public WOComponent neFaitRien() {
		isModificationEnCours = false;
		return null;
	}

	public EODepensePapier getLaDepensePapier() {
		return laDepensePapier;
	}

	public void setLaDepensePapier(EODepensePapier laDepensePapier) {
		this.laDepensePapier = laDepensePapier;
	}

	public NSTimestamp getDateFacture() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateFacture();
		}
		return date;
	}

	public NSTimestamp getDateReception() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateReception();
		}
		return date;
	}

	public NSTimestamp getDateServiceFait() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateServiceFait();
		}
		return date;
	}

	public boolean isAfficherEngagements() {
		boolean isAfficherEngagements = false;

		if (laDepensePapier != null) {
			isAfficherEngagements = true;
		}
		return isAfficherEngagements;
	}

	public boolean isProrataDisabled() {
		boolean isProrataDisabled = true;
		EOCommande commande = laCommandeEnCours();

		if (getLaDepenseBudget().isReversement()) {
			return true;
		}

		if (commande == null || commande.isTauxProrataModifiableLiquidation(laDepensePapier.editingContext())) {
			isProrataDisabled = false;
		}
		return isProrataDisabled;
	}

	public NSArray<EOTauxProrata> getLesTauxDeProratas() {
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(laCommandeEnCours().exercice(), "exercice");
		if (getLaDepenseBudget().getSource() == null) {
			bindings.setObjectForKey(getLaDepenseBudget().engagementBudget().organ(), "organ");
		}
		else {
			bindings.setObjectForKey(getLaDepenseBudget().getSource().organ(), "organ");
		}
		//		bindings.setObjectForKey(getUneDepenseBudget().engagementBudget().organ(), "organ");
		lesTauxDeProratas = FinderProrata.getTauxProratas(laDepensePapier.editingContext(), bindings);
		return lesTauxDeProratas;
	}

	public void setLesTauxDeProratas(NSArray<EOTauxProrata> lesTauxDeProratas) {
		this.lesTauxDeProratas = lesTauxDeProratas;
	}

	//
	public EOTauxProrata getTauxDeProrataSelectionne() {
		return getLaDepenseBudget().tauxProrata();
	}

	public void setTauxDeProrataSelectionne(EOTauxProrata unTauxDeProrataSelectionne) {
		getLaDepenseBudget().setTauxProrata(unTauxDeProrataSelectionne);
		initialiseObjectsForRepartition(getLaDepenseBudget());
	}

	public boolean getDepIsSoldee() {
		//		return getUneDepenseBudget().solde();
		return getLaDepenseBudget().solde();
	}

	public void setDepIsSoldee(boolean depIsSoldee) {
		boolean oldValue = this.laDepenseBudget.solde();
		if (oldValue != depIsSoldee) {
			getLaDepenseBudget().setSolde(depIsSoldee);
		}
		//		boolean oldValue = this.uneDepenseBudget.solde();
		//		if (oldValue != depIsSoldee) {
		//			getUneDepenseBudget().setSolde(depIsSoldee);
		//		}
	}

	public BigDecimal engTotalHT() {
		BigDecimal engTotalHT = BigDecimal.valueOf(0);

		if (laDepensePapier != null && laDepensePapier.depenseBudgets() != null) {
			engTotalHT = (BigDecimal) laDepensePapier.depenseBudgets().valueForKeyPath("@sum.depHtSaisie");
		}
		return engTotalHT;
	}

	public BigDecimal engTotalTTC() {
		BigDecimal engTotalTTC = BigDecimal.valueOf(0);

		if (laDepensePapier != null && laDepensePapier.depenseBudgets() != null) {
			engTotalTTC = (BigDecimal) laDepensePapier.depenseBudgets().valueForKeyPath("@sum.depTtcSaisie");
			;
		}

		return engTotalTTC;
	}

	// Repartitions
	public boolean isAfficherRepartitions() {
		boolean isAfficherRepartitions = false;
		EODepensePapier dp = getLaDepensePapier();
		if (dp != null && dp.isComplete()) {
			BigDecimal montantFactureHT = getLaDepensePapier().dppHtInitial();
			BigDecimal totalHT = engTotalHT();
			BigDecimal montantFactureTTC = getLaDepensePapier().dppTtcInitial();
			BigDecimal totalTTC = engTotalTTC();

			if (montantFactureHT != null && /* montantFactureHT.floatValue() >= 0 && */totalHT != null &&
					montantFactureTTC != null && /* montantFactureTTC.floatValue() > 0 && */totalTTC != null &&
					montantFactureHT.floatValue() == totalHT.floatValue() &&
					montantFactureTTC.floatValue() == totalTTC.floatValue()) {
				isAfficherRepartitions = true;
			}
		}
		return isAfficherRepartitions;
	}

	public NSArray<EOCodeExer> getLesCodesNomenclature() {
		return lesCodesNomenclature;
	}

	public void setLesCodesNomenclature(NSArray<EOCodeExer> lesCodesNomenclature) {
		this.lesCodesNomenclature = lesCodesNomenclature;
	}

	public NSArray<EODepenseControleMarche> getLesDepenseControleMarches() {
		NSArray<EODepenseControleMarche> res = getLaDepenseBudget().depenseControleMarches();
		if (res != null && res.count() > 0) {
			((EODepenseControleMarche) res.objectAtIndex(0)).setPourcentage(BigDecimal.valueOf(100));
		}
		return res;
	}

	//	private BigDecimal getUneDepenseCtrlMarchePourcentage() {
	//		return uneDepenseCtrlMarche.pourcentage();
	//	}

	public BigDecimal getUneDepenseCtrlMarcheMontant() {
		return uneDepenseCtrlMarche.dmarTtcSaisie();
	}

	public String repartMarchePourcentageId() {
		return "RepartMarchePourcentage_" + indexDepenseBudgetsForMarche + "_" + indexDepenseControleMarche;
	}

	public String repartMarcheMontantId() {
		return "RepartMarcheMontant_" + indexDepenseBudgetsForMarche + "_" + indexDepenseControleMarche;
	}

	public NSArray<EOTypeAction> getLesActions() {
		EODepenseBudget db = getLaDepenseBudget();
		lesActions = (NSArray<EOTypeAction>) dicoLesActions.objectForKey(db.getSource().libelleCourt());
		return lesActions;
	}

	//	// Repartition par imputations
	public EODepenseBudget getUneDepenseBudgetForImputation() {
		return getLaDepenseBudget();
	}

	//
	public EODepenseControlePlanComptable getUneDepenseCtrlImputation() {
		return uneDepenseCtrlImputation;
	}

	public void setUneDepenseCtrlImputation(EODepenseControlePlanComptable uneDepenseCtrlImputation) {
		this.uneDepenseCtrlImputation = uneDepenseCtrlImputation;
	}

	public NSArray<EOPlanComptable> getLesImputations() {
		EODepenseBudget db = getUneDepenseBudgetForImputation();
		lesImputations = (NSArray<EOPlanComptable>) dicoLesPlansComptable.objectForKey(db.getSource().libelleCourt());
		return lesImputations;
	}

	public EOInventaire getUnInventaire() {
		return unInventaire;
	}

	public void setUnInventaire(EOInventaire unInventaire) {
		this.unInventaire = unInventaire;
	}

	public WOComponent ajouterInventaires() {
		EODepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		InventaireListeSelection nextPage = (InventaireListeSelection) pageWithName("InventaireListeSelection");
		nextPage.setDepense(dcpc);
		return nextPage;
	}

	public WOComponent supprimerInventaire() {
		EODepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		EOInventaire inventaire = getUnInventaire();
		if (dcpc != null && inventaire != null) {
			FactoryDepenseControlePlanComptable fdcpc = new FactoryDepenseControlePlanComptable();
			fdcpc.supprimerInventaire(dcpc, inventaire);
		}
		return null;
	}

	public EODepenseBudget getUneDepenseBudgetForConvention() {
		return getLaDepenseBudget();
	}

	public NSArray<EOConvention> getLesConventions() {
		EODepenseBudget db = getUneDepenseBudgetForConvention();
		lesConventions = (NSArray<EOConvention>) dicoLesConventions.objectForKey(db.getSource().libelleCourt());
		return lesConventions;
	}

	//	// Repartition par Codes analytiques
	public EODepenseBudget getUneDepenseBudgetForAnalytique() {
		return getLaDepenseBudget();
	}

	public NSArray<EOCodeAnalytique> getLesCodeAnalytiques() {
		EODepenseBudget db = getUneDepenseBudgetForAnalytique();
		lesCodeAnalytiques = (NSArray<EOCodeAnalytique>) dicoLesCodesAnalytique.objectForKey(db.getSource().libelleCourt());
		return lesCodeAnalytiques;
	}

	public void setLesCodeAnalytiques(NSArray<EOCodeAnalytique> lesCodeAnalytiques) {
		this.lesCodeAnalytiques = lesCodeAnalytiques;
	}

	public BigDecimal getMontantHTFacture() {
		BigDecimal montant = null;
		if (laDepensePapier != null) {
			montant = laDepensePapier.dppHtInitial();
		}
		return montant;
		// return getLaDepensePapier().dppHtInitial();
	}

	public BigDecimal getMontantTTCFacture() {
		BigDecimal montant = null;
		if (laDepensePapier != null) {
			montant = laDepensePapier.dppTtcInitial();
		}
		return montant;
	}

	public boolean isAjouterInventaireDisabled() {
		boolean isAjouterInventaireDisabled = true;
		EODepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		if (dcpc != null && dcpc.isInventairesPossibles()) {
			isAjouterInventaireDisabled = false;
		}
		return isAjouterInventaireDisabled;
	}

	public EODepenseBudget getLaDepenseBudget() {
		return laDepenseBudget;
	}

	public NSArray<EODepenseBudget> getLesDepenseBudgets() {
		return lesDepenseBudgets;
	}

	public void setLaDepenseBudget(EODepenseBudget laDepenseBudget) {
		setLaDepensePapier(null);
		this.laDepenseBudget = laDepenseBudget;
		setLaDepensePapier(laDepenseBudget.depensePapier());
		this.lesDepenseBudgets = new NSArray<EODepenseBudget>(this.laDepenseBudget);
		//a appeler suivant le contexte
		initialisation();

	}

	public WOComponent modifierLaSource() {
		SourceListeSelectionForReimputation nextPage = (SourceListeSelectionForReimputation) pageWithName(SourceListeSelectionForReimputation.class.getName());
		EODepenseBudget laDepenseBudget = getLaDepenseBudget();
		nextPage.setDepenseBudget(laDepenseBudget);
		return nextPage;
	}

	public String getReimpLibelle() {
		return session.getReimpLibelle();
	}

	public void setReimpLibelle(String reimpLibelle) {
		session.setReimpLibelle(reimpLibelle);
	}

	public Boolean isObjetReimputationGood() {
		return Boolean.valueOf(!StringCtrl.isEmpty(session.getReimpLibelle()));
	}

	public Boolean isReversement() {
		return getLaDepenseBudget().isReversement();
	}

	public String pageTitle() {
		return PAGE_TITLE;
	}
	
	public NSArray<EOCodeExer> lesCodesExerDeLaDepense() {
		NSMutableArray<EOCodeExer> lesCodesExer = new NSMutableArray<EOCodeExer>();	
		for (EODepenseBudget dpb : getLaDepensePapier().depenseBudgets()) {
			for (EODepenseControleHorsMarche dpchm : dpb.depenseControleHorsMarches()) {
				if (dpchm.codeExer() != null) {
					lesCodesExer.add(dpchm.codeExer());
				}
			}
		}	
		return lesCodesExer;
	}

}