/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.controleurs.SourceListSelectionForReimputationCtrl;
import org.cocktail.fwkcktldepense.server.finder.FinderBudget;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlSourceSelectionCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class SourceListeSelectionForReimputation extends SourceListeSelection {

	private static final long serialVersionUID = 1L;

	private EODepenseBudget depenseBudget;

	public SourceListeSelectionForReimputation(WOContext context) {
		super(context);
	}

	//	private SourceListSelectionForReimputationCtrl myCtrl() {
	//		return (SourceListSelectionForReimputationCtrl) getSourceSelectionCtrl();
	//	}

	public EODepenseBudget getDepenseBudget() {
		return depenseBudget;
		//		if (myCtrl().getDepenseBudget() == null) {
		//			setDepenseBudget(session.getLaDepenseBudgetReimputation());
		//		}
		//		return myCtrl().getDepenseBudget();
	}

	public void setDepenseBudget(EODepenseBudget depenseBudget) {
		this.depenseBudget = depenseBudget;
		//myCtrl().setDepenseBudget(depenseBudget);
	}

	@Override
	protected EOExercice getExercice() {
		return getDepenseBudget().exercice();
	}

	@Override
	public EOEditingContext getEditingContext() {
		return getDepenseBudget().editingContext();
	}

	/*
	 * protected NSArray<NSDictionary<String, Object>> getRawRowBudgets(EOEditingContext ed, NSMutableDictionary<String, Object> bindings) { // FIXME
	 * n'afficher que les sources compatibles avec la depenseBudget // changement de la ligne budgétaire // // dépense sans inventaire : si la dépense
	 * est mandatée, on choisit une ligne budgétaire de la même UB, sinon toutes les lignes, dans la limite que le disponible le permette. // dépense
	 * avec inventaire : suivant un paramètre a mettre en place, même limitation que la dépense sans inventaire ou restriction au niveau du CR
	 * (puisque le code inventaire prend en compte le CR) // le changement de ligne peut impliquer la modification des conventions et des codes
	 * analytiques éventuellement associés à cette dépense cf. changement convention // changement de ligne peut impliquer une création d'un nouvel
	 * engagement, si l'engagement concerne d'autres liquidations (hors ORVs liés a celle qu'on ré-impute) par exemple // si la dépense ré-imputée
	 * possède un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..
	 * 
	 * //si la dépense est mandatée, on choisit une ligne budgétaire de la même UB if (getDepenseBudget().isMandate()) {
	 * bindings.setObjectForKey(getDepenseBudget().getSource().organ().orgUb(), "organUb"); }
	 * 
	 * // ré-imputation possible par un type de crédit de la même section que le précédent, //sous réserve que le disponible budgétaire soit suffisant
	 * et que l'imputation comptable //utilisée par cette dépense soit affectée au type de crédit dans Maracuja.
	 * 
	 * @SuppressWarnings("unused") //rod 24/04/2012 a priori ca sert à rien EOTypeCredit tc = getDepenseBudget().getSource().typeCredit();
	 * bindings.setObjectForKey(EOTypeCredit.TCD_SECT_KEY, getDepenseBudget().getSource().typeCredit().tcdSect());
	 * 
	 * //FIXME ajouter filtre sur taux de prorata si depense mandatee pour recherche des sources ?
	 * 
	 * return super.getRawRowBudgets(ed, bindings); }
	 */
	@Override
	public WOComponent ajouterLesSourcesSelectionnees() {
		NSDictionary<String, Object> source = getLaSourceUtilisateurSelectionnee();
		if (source != null) {
			NSArray<EOBudgetExecCredit> credits = FinderBudget.getBudgets(getEditingContext(), new NSArray<NSDictionary<String, Object>>(source));
			if (credits != null && credits.count() > 0) {
				EOBudgetExecCredit credit = (EOBudgetExecCredit) credits.objectAtIndex(0);
				NSArray<EOTauxProrata> lesTaux = credit.getTauxProratas(getEditingContext());
				if (lesTaux != null && lesTaux.count() > 0) {
					EOSource newSource = new EOSource(credit.organ(), credit.typeCredit(), null, credit.exercice());

					if (lesTaux.indexOfObject(getDepenseBudget().tauxProrata()) == NSArray.NotFound) {
						EOTauxProrata unTaux = (EOTauxProrata) lesTaux.objectAtIndex(0);
						newSource.setTauxProrata(unTaux);
						getDepenseBudget().setTauxProrata(unTaux);
					}

					getDepenseBudget().setSource(newSource);

				}
			}
		}

		Reimputation2 nextPage = (Reimputation2) session.getSavedPageWithName(Reimputation2.class.getName());
		nextPage.setLaDepenseBudget(getDepenseBudget());
		nextPage.initialisation();
		return nextPage;
	}

	public NSDictionary<String, Object> getLaSourceUtilisateurSelectionnee() {
		NSMutableArray<NSDictionary<String, Object>> res = getLesSourcesUtilisateurSelectionnees();
		if (res == null || res.count() == 0) {
			return null;
		}
		return res.objectAtIndex(0);
	}

	//	public void setLaSourceUtilisateurSelectionnee(NSDictionary<String, Object> source) {
	//		setLesSourcesUtilisateurSelectionnees(new NSMutableArray<NSDictionary<String, Object>>(source));
	//	}

	@Override
	public CktlSourceSelectionCtrl getSourceSelectionCtrl() {
		if (sourceSelectionCtrl == null) {
			sourceSelectionCtrl = new SourceListSelectionForReimputationCtrl(mySession(), getEditingContext(), getExercice(), getDepenseBudget());
		}
		return sourceSelectionCtrl;
	}
}
