/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.reports.PrintFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereau;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;

public class LiquidationsDossierSuivi extends MyAjaxComponent {

	private static final long serialVersionUID = -6346309532396224570L;
	private WODisplayGroup dgForBordereaux = null;
	private WODisplayGroup dgForDepenses = null;
	private Boolean showFiltresForBordereaux = Boolean.TRUE;
	private String infoMsg = null;

	public String getInfoMsg() {
		return infoMsg;
	}

	public void setInfoMsg(String infoMsg) {
		this.infoMsg = infoMsg;
	}

	public LiquidationsDossierSuivi(WOContext context) {
		super(context);
	}

	public WODisplayGroup dgForBordereaux() {
		if (dgForBordereaux == null) {
			dgForBordereaux = new WODisplayGroup();
			//dgForBordereaux.setDelegate(new DgForBordereauxDelegate());
		}
		return dgForBordereaux;
	}

	public WODisplayGroup dgForDepenses() {
		if (dgForDepenses == null) {
			dgForDepenses = new WODisplayGroup();
		}
		return dgForDepenses;
	}

	public WOActionResults filtrer() {
		refreshData();
		return null;
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"Accueil"
		});
		return items;
	}

	public Boolean getShowFiltresForBordereaux() {
		return showFiltresForBordereaux;
	}

	public void setShowFiltresForBordereaux(Boolean showFiltres) {
		this.showFiltresForBordereaux = showFiltres;
	}

	public void refreshData() {
		@SuppressWarnings("unchecked")
		NSDictionary<String, Object> bdgs = dgForBordereaux().queryBindings();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (bdgs.valueForKey(EOBordereau.GES_CODE_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.TO_GESTION_KEY + "." + EOGestion.GES_CODE_KEY, EOQualifier.QualifierOperatorEqual, bdgs.valueForKey(EOBordereau.GES_CODE_KEY)));
		}
		if (bdgs.valueForKey(EOBordereau.BOR_ETAT_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.BOR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, bdgs.valueForKey(EOBordereau.BOR_ETAT_KEY)));
		}
		if (bdgs.valueForKey(EOBordereau.BOR_NUM_KEY) != null) {
			String nums = (String) bdgs.valueForKey(EOBordereau.BOR_NUM_KEY);
			nums = nums.trim();
			NSArray<String> num = NSArray.componentsSeparatedByString(nums, ",");
			NSMutableArray<EOQualifier> quals2 = new NSMutableArray<EOQualifier>();
			String res = "";
			for (int i = 0; i < num.count(); i++) {
				String unNum = num.objectAtIndex(i);
				unNum = unNum.trim();
				try {
					Integer unNum2 = new Integer(unNum);
					res = res + (res.length() > 0 ? "," : "") + unNum2.toString();
					quals2.addObject(new EOKeyValueQualifier(EOBordereau.BOR_NUM_KEY, EOQualifier.QualifierOperatorEqual, unNum2));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			bdgs.takeValueForKey(res, EOBordereau.BOR_NUM_KEY);

			if (quals2.count() > 0) {
				quals.addObject(new EOOrQualifier(quals2));
			}
		}
		if (bdgs.valueForKey("borDateCreationMin") != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.BOR_DATE_CREATION_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, bdgs.valueForKey("borDateCreationMin")));
		}
		if (bdgs.valueForKey("borDateCreationMax") != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.BOR_DATE_CREATION_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, bdgs.valueForKey("borDateCreationMax")));
		}

		NSArray<EOBordereau> lesBords = EOBordereau.fetchAllDossiersLiquidation(edc(), new EOAndQualifier(quals), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOBordereau.SORT_BOR_DATE_CREATION_DESC
		}));
		if (lesBords.count() > 100) {
			lesBords = lesBords.subarrayWithRange(new NSRange(0, 100));
			session.addSimpleInfoMessage("Information", "Seuls les 100 derniers dossiers trouvés sont affichés. Affinez vos critères de recherche si vous voulez trouver un dossier particulier.");
		}

		dgForBordereaux().setObjectArray(lesBords);
		refreshDepenses();
	}

	//	public final class DgForBordereauxDelegate {
	//		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
	//			//On met à jour les depenses en lien
	//			if (group.displayedObjects().count() == 0 || group.selectedObjects().count() != 1) {
	//				dgForDepenses().setObjectArray(NSArray.emptyArray());
	//			}
	//			if (group.selectedObjects().count() == 1) {
	//				EOBordereau bordereau = (EOBordereau) group.selectedObject();
	//				dgForDepenses().setObjectArray(bordereau.toDepenses());
	//			}
	//		}
	//	}

	public String containerDepensesId() {
		return getComponentId() + "_containerDepenses";
	}

	public void refreshDepenses() {
		//On met à jour les depenses en lien
		if (dgForBordereaux.displayedObjects().count() == 0 || dgForBordereaux.selectedObjects().count() != 1) {
			dgForDepenses().setObjectArray(NSArray.emptyArray());
		}
		if (dgForBordereaux.selectedObjects().count() == 1) {
			EOBordereau bordereau = (EOBordereau) dgForBordereaux.selectedObject();
			dgForDepenses().setObjectArray(bordereau.toDepenses());
		}
	}

	public boolean isImprimerUnDossierLiquidationDisabled() {
		EOBordereau bord = (EOBordereau) dgForBordereaux.selectedObject();
		boolean res = true;
		if (bord != null) {
			res = false;
		}
		return res;
	}

	public WOActionResults creerDossier() {
		LiquidationsDossierCreation nextPage = (LiquidationsDossierCreation) pageWithName(LiquidationsDossierCreation.class.getName());
		nextPage.refreshData();
		return nextPage;
	}

	public WOActionResults imprimerDocuments() {
		return imprimerUnDossierLiquidation();
	}

	public WOActionResults imprimerUnDossierLiquidation() {
		@SuppressWarnings("unchecked")
		NSArray<EOBordereau> bords = dgForBordereaux.selectedObjects();
		EOBordereau bord = null;

		try {
			if (bords.count() == 0) {
				throw new Exception("Veuillez sélectionner au moins un dossier à imprimer.");
			}

			String ins = "";
			for (int i = 0; i < bords.count(); i++) {
				EOBordereau aBord = (EOBordereau) bords.objectAtIndex(i);
				Integer borId = (Integer) EOUtilities.primaryKeyForObject(aBord.editingContext(), aBord).allValues().lastObject();
				ins += borId.intValue() + ",";
			}
			ins += "-1";

			NSData data = PrintFactory.printDossierLiquidation(session, ins);
			String fileName = null;
			bord = (EOBordereau) dgForBordereaux.selectedObjects().lastObject();

			if (bords.count() == 1) {
				fileName = "dossierLiquidation_" + bord.toExercice().exeExercice() + "_" + bord.toGestion().gesCode() + "_" + bord.borNum() + ".pdf";
			}
			else {
				fileName = "dossierLiquidation_" + bord.toExercice().exeExercice() + "_multiples" + ".pdf";
			}

			if (data == null) {
				throw new Exception("Impossible d'imprimer le dossier de liquidation " + fileName);
			}
			WOActionResults res = PrintFactory.afficherPdf(data, fileName);
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			session.setAlertMessage(e.getMessage() + " \\n" + e.toString());
			return null;
		}
	}

	/**
	 * Permet de concatener les valeurs associées aux clés à partir des éléments du tableau.
	 * 
	 * @param array
	 * @param key
	 * @param joinString
	 * @param lastValue
	 * @return
	 */
	public String joinedValues(NSArray<NSKeyValueCoding> array, String key, String joinString, String lastValue) {
		String res = "";
		for (int i = 0; i < array.count(); i++) {
			NSKeyValueCoding object = array.objectAtIndex(i);
			res += object.valueForKey(key) + joinString;
		}
		if (lastValue != null) {
			res += lastValue;
		}
		else if (res.length() > 0) {
			res = res.substring(0, res.length() - 1);
		}
		return res;
	}

	public EOBordereau selectedBordereau() {
		return (EOBordereau) dgForBordereaux().selectedObject();
	}

}