/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

// Generated by the WOLips Templateengine Plug-in at 2 avr. 2007 12:46:05

import java.math.BigDecimal;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOMandat;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

public class Reversement extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	public EODepenseBudget laDepenseBudget = null;
	private boolean isModificationEnCours = false;

	// Reversement
	public String unReversementLibelle = null;
	public Integer unReversementNbrePJ = null;
	public BigDecimal unReversementMontantHT = null;
	public BigDecimal unReversementMontantTTC = null;

	// Repartition par code de nomenclature
	EODepenseControleHorsMarche uneDepenseCtrlHorsMarche = null;
	public int indexDepenseControleHorsMarche;
	BigDecimal uneDepenseCtrlHorsMarcheMontant = null;

	// Repartition par action
	EODepenseControleAction uneDepenseCtrlAction = null;
	public int indexDepenseControleAction;
	BigDecimal uneDepenseCtrlActionMontant = null;

	// Repartition par imputation (plan comptable)
	EODepenseControlePlanComptable uneDepenseCtrlImputation = null;
	public int indexDepenseControleImputation;
	BigDecimal uneDepenseCtrlImputationMontant = null;
	EOInventaire unInventaire = null;
	BigDecimal unInventaireMontant = null;

	// Repartition par Convention
	EODepenseControleConvention uneDepenseCtrlConvention = null;
	public int indexDepenseControleConvention;
	BigDecimal uneDepenseCtrlConventionMontant = null;

	// Repartition par Code analytiques
	EODepenseControleAnalytique uneDepenseCtrlAnalytique = null;
	public int indexDepenseControleAnalytique;
	BigDecimal uneDepenseCtrlAnalytiqueMontant = null;

	public Reversement(WOContext context) {
		super(context);
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UN_REVERSEMENT_KEY, Menu.ANNULER_UN_REVERSEMENT_KEY
		});

		return items;
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		//		String message = session.getAlertMessage();
		//		if (message != null && message.equals("")==false) {
		//			String str = "<script language=\"javascript\" type=\"text/javascript\">\n";
		//			str += "alert(\""+message+"\");\n";
		//			str += "</script>";
		//			res.appendContentString(str);
		//			session.setAlertMessage(null);
		//		}
	}

	public WOComponent submitReversement() {
		isModificationEnCours = false;
		return null;
	}

	public WOComponent neFaitRien() {
		isModificationEnCours = false;
		return null;
	}

	public boolean isMontantDisabled() {
		boolean isMontantDisabled = true;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null &&
				getUnReversementLibelle() != null && getUnReversementLibelle().equals("") == false &&
				getUnReversementMontantHT() != null &&
				getUnReversementMontantTTC() != null && getUnReversementMontantTTC().doubleValue() > 0) {
			isMontantDisabled = false;
		}
		return isMontantDisabled;
	}

	public boolean isMontantReversementDisabled() {
		boolean isMontantReversementDisabled = true;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null &&
				getUnReversementLibelle() != null && getUnReversementLibelle().equals("") == false) {
			isMontantReversementDisabled = false;
		}
		return isMontantReversementDisabled;
	}

	public EODepenseBudget getLaDepenseBudget() {
		return laDepenseBudget;
	}

	public void setLaDepenseBudget(EODepenseBudget laDepenseBudget) {
		this.laDepenseBudget = laDepenseBudget;
	}

	public EOMandat unMandat() {
		EOMandat unMandat = null;
		EODepenseBudget depenseBudgetReversement = getLaDepenseBudget();
		if (depenseBudgetReversement != null) {
			EODepenseBudget depenseBudget = depenseBudgetReversement.depenseBudgetReversement();
			if (depenseBudget != null) {
				NSArray<EODepenseControlePlanComptable> depenseControlePlanComptables = depenseBudget.depenseControlePlanComptables();
				if (depenseControlePlanComptables != null && depenseControlePlanComptables.count() > 0) {
					EODepenseControlePlanComptable depenseControlePlanComptable = (EODepenseControlePlanComptable) depenseControlePlanComptables.lastObject();
					unMandat = depenseControlePlanComptable.mandat();
				}
			}
		}
		return unMandat;
	}

	// Reversement
	public String getUnReversementLibelle() {
		String libelle = null;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null) {
			EODepensePapier dp = db.depensePapier();
			libelle = dp.dppNumeroFacture();
		}
		return libelle;
	}

	public void setUnReversementLibelle(String unReversementLibelle) {
		if (laDepenseBudget != null) {
			String oldValue = this.laDepenseBudget.depensePapier().dppNumeroFacture();
			if (oldValue != null && oldValue.equals(unReversementLibelle) == false ||
					oldValue == null && unReversementLibelle != null) {
				isModificationEnCours = true;
				this.unReversementLibelle = unReversementLibelle;
				if (unReversementLibelle != null) {
					laDepenseBudget.depensePapier().setDppNumeroFacture(unReversementLibelle);
				}
			}

		}
	}

	public BigDecimal getUnReversementMontantHT() {
		BigDecimal unMontant = null;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null) {
			EODepensePapier dp = db.depensePapier();
			unMontant = dp.dppHtInitial().abs();
		}
		return unMontant;
	}

	public void setUnReversementMontantHT(BigDecimal unReversementMontantHT) {
		if (laDepenseBudget != null && isModificationEnCours == false) {
			BigDecimal oldValue = laDepenseBudget.depHtSaisie();
			if (oldValue != null && unReversementMontantHT != null && oldValue.abs().floatValue() != unReversementMontantHT.abs().floatValue() ||
					oldValue == null && unReversementMontantHT != null) {
				isModificationEnCours = true;
				this.unReversementMontantHT = unReversementMontantHT;
				if (unReversementMontantHT != null) {
					laDepenseBudget.depensePapier().setDppHtInitial(unReversementMontantHT.multiply(BigDecimal.valueOf(-1)));
					laDepenseBudget.setDepHtSaisie(unReversementMontantHT.multiply(BigDecimal.valueOf(-1)));
				}
			}
		}
	}

	public BigDecimal getUnReversementMontantTTC() {
		BigDecimal unMontant = null;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null) {
			EODepensePapier dp = db.depensePapier();
			unMontant = dp.dppTtcInitial().abs();
		}
		return unMontant;
	}

	public void setUnReversementMontantTTC(BigDecimal unReversementMontantTTC) {
		if (laDepenseBudget != null && isModificationEnCours == false) {
			BigDecimal oldValue = laDepenseBudget.depTtcSaisie();
			if (oldValue != null && unReversementMontantTTC != null && oldValue.abs().floatValue() != unReversementMontantTTC.abs().floatValue() ||
					oldValue == null && unReversementMontantTTC != null) {
				isModificationEnCours = true;
				this.unReversementMontantTTC = unReversementMontantTTC;
				if (unReversementMontantTTC != null) {
					laDepenseBudget.depensePapier().setDppTtcInitial(unReversementMontantTTC.multiply(BigDecimal.valueOf(-1)));
					laDepenseBudget.setDepTtcSaisie(unReversementMontantTTC.multiply(BigDecimal.valueOf(-1)));
				}
			}
		}
	}

	public Number getUnReversementNbrePJ() {
		Number unNombre = null;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null) {
			EODepensePapier dp = db.depensePapier();
			unNombre = dp.dppNbPiece();
		}
		return unNombre;
	}

	public void setUnReversementNbrePJ(Integer unReversementNbrePJ) {
		if (laDepenseBudget != null) {
			Number oldValue = laDepenseBudget.depensePapier().dppNbPiece();
			if (oldValue != null && unReversementNbrePJ != null && oldValue.intValue() != unReversementNbrePJ.intValue() ||
					oldValue == null && unReversementNbrePJ != null) {
				isModificationEnCours = true;
				this.unReversementNbrePJ = unReversementNbrePJ;
				if (unReversementNbrePJ != null) {
					laDepenseBudget.depensePapier().setDppNbPiece(unReversementNbrePJ);
				}
			}
		}
	}

	// Repartitions
	public boolean isAfficherRepartitions() {
		boolean isAfficherRepartitions = true;
		EODepenseBudget db = getLaDepenseBudget();
		if (db != null) {
			EODepensePapier dpp = db.depensePapier();
			BigDecimal montantReversementHT = dpp.dppHtInitial();
			BigDecimal montantReversementTTC = dpp.dppTtcInitial();

			if (montantReversementHT != null && montantReversementHT.floatValue() >= 0 &&
					montantReversementTTC != null && montantReversementTTC.floatValue() > 0) {
				isAfficherRepartitions = true;
			}
		}
		return isAfficherRepartitions;
	}

	// Repartition par Code de nomenclature
	public EODepenseControleHorsMarche getUneDepenseCtrlHorsMarche() {
		return uneDepenseCtrlHorsMarche;
	}

	public void setUneDepenseCtrlHorsMarche(EODepenseControleHorsMarche uneDepenseCtrlHorsMarche) {
		this.uneDepenseCtrlHorsMarche = uneDepenseCtrlHorsMarche;
	}

	public BigDecimal getUneDepenseCtrlHorsMarcheMontant() {
		return getUneDepenseCtrlHorsMarche().dhomTtcSaisie().abs();
	}

	public void setUneDepenseCtrlHorsMarcheMontant(BigDecimal uneDepenseCtrlHorsMarcheMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = this.uneDepenseCtrlHorsMarche.dhomTtcSaisie();
			if (oldValue != null && uneDepenseCtrlHorsMarcheMontant != null && oldValue.floatValue() != uneDepenseCtrlHorsMarcheMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlHorsMarcheMontant != null) {
				getUneDepenseCtrlHorsMarche().setMontantTtc(uneDepenseCtrlHorsMarcheMontant.abs().negate());
				isModificationEnCours = true;
			}
		}
	}

	public boolean afficheRepartHorsMarcheSourceLibelle() {
		boolean afficheRepartHorsMarcheSourceLibelle = false;

		if (indexDepenseControleHorsMarche == 0) {
			afficheRepartHorsMarcheSourceLibelle = true;
		}

		return afficheRepartHorsMarcheSourceLibelle;
	}

	public int nbreDepenseControleHorsMarche() {
		return getLaDepenseBudget().depenseControleHorsMarches().count();
	}

	// Repartition par actions
	public EODepenseControleAction getUneDepenseCtrlAction() {
		return uneDepenseCtrlAction;
	}

	public void setUneDepenseCtrlAction(EODepenseControleAction uneDepenseCtrlAction) {
		this.uneDepenseCtrlAction = uneDepenseCtrlAction;
	}

	public BigDecimal getUneDepenseCtrlActionMontant() {
		return getUneDepenseCtrlAction().dactTtcSaisie().abs();
	}

	public void setUneDepenseCtrlActionMontant(BigDecimal uneDepenseCtrlActionMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = this.uneDepenseCtrlAction.dactTtcSaisie();
			if (oldValue != null && uneDepenseCtrlActionMontant != null && oldValue.floatValue() != uneDepenseCtrlActionMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlActionMontant != null) {
				getUneDepenseCtrlAction().setMontantTtc(uneDepenseCtrlActionMontant.abs().negate());
				isModificationEnCours = true;
			}
		}
	}

	public boolean afficheRepartActionSourceLibelle() {
		boolean afficheRepartActionSourceLibelle = false;

		if (indexDepenseControleAction == 0) {
			afficheRepartActionSourceLibelle = true;
		}

		return afficheRepartActionSourceLibelle;
	}

	public int nbreDepenseControleAction() {
		return getLaDepenseBudget().depenseControleActions().count();
	}

	// Repartition par imputations
	public EODepenseControlePlanComptable getUneDepenseCtrlImputation() {
		return uneDepenseCtrlImputation;
	}

	public void setUneDepenseCtrlImputation(EODepenseControlePlanComptable uneDepenseCtrlImputation) {
		this.uneDepenseCtrlImputation = uneDepenseCtrlImputation;
	}

	public BigDecimal getUneDepenseCtrlImputationMontant() {
		return getUneDepenseCtrlImputation().dpcoTtcSaisie().abs();
	}

	public void setUneDepenseCtrlImputationMontant(BigDecimal uneDepenseCtrlImputationMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = this.uneDepenseCtrlImputation.dpcoTtcSaisie();
			if (oldValue != null && uneDepenseCtrlImputationMontant != null && oldValue.floatValue() != uneDepenseCtrlImputationMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlImputationMontant != null) {
				getUneDepenseCtrlImputation().setMontantTtc(uneDepenseCtrlImputationMontant.abs().negate());
				isModificationEnCours = true;
			}
		}
	}

	public boolean afficheRepartImputationSourceLibelle() {
		boolean afficheRepartImputationSourceLibelle = false;

		if (indexDepenseControleImputation == 0) {
			afficheRepartImputationSourceLibelle = true;
		}

		return afficheRepartImputationSourceLibelle;
	}

	public EOInventaire getUnInventaire() {
		return unInventaire;
	}

	public void setUnInventaire(EOInventaire unInventaire) {
		this.unInventaire = unInventaire;
	}

	public BigDecimal getUnInventaireMontant() {
		return getUnInventaire().lidMontant().abs();
	}

	public void setUnInventaireMontant(BigDecimal unMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = getUnInventaire().lidMontant();
			if (oldValue != null && unMontant != null && oldValue.floatValue() != unMontant.floatValue() ||
					oldValue == null && unMontant != null) {
				getUnInventaire().setLidMontant(unMontant.multiply(BigDecimal.valueOf(-1)));
				isModificationEnCours = true;
			}
		}
	}

	public int nbreDepenseControleImputation() {
		EODepenseBudget depenseBudget = getLaDepenseBudget();
		NSArray<EODepenseControlePlanComptable> ctrlPlanComptables = depenseBudget.depenseControlePlanComptables();
		int nbreImputations = ctrlPlanComptables.count();
		for (int i = 0; i < ctrlPlanComptables.count(); i++) {
			EODepenseControlePlanComptable dcpc = (EODepenseControlePlanComptable) ctrlPlanComptables.objectAtIndex(i);
			if (dcpc.isInventaireAffichable()) {
				nbreImputations += 1; // Pour le titre des colonnes
				nbreImputations += dcpc.inventaires().count(); // Pour les inventaires deja saisis
			}
		}
		return nbreImputations;
	}

	// Repartition par Conventions
	public EODepenseControleConvention getUneDepenseCtrlConvention() {
		return uneDepenseCtrlConvention;
	}

	public void setUneDepenseCtrlConvention(EODepenseControleConvention uneDepenseCtrlConvention) {
		this.uneDepenseCtrlConvention = uneDepenseCtrlConvention;
	}

	public BigDecimal getUneDepenseCtrlConventionMontant() {
		return getUneDepenseCtrlConvention().dconTtcSaisie().abs();
	}

	public void setUneDepenseCtrlConventionMontant(BigDecimal uneDepenseCtrlConventionMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = this.uneDepenseCtrlConvention.dconTtcSaisie();
			if (oldValue != null && uneDepenseCtrlConventionMontant != null && oldValue.floatValue() != uneDepenseCtrlConventionMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlConventionMontant != null) {
				getUneDepenseCtrlConvention().setMontantTtc(uneDepenseCtrlConventionMontant.abs().negate());
				isModificationEnCours = true;
			}
		}
	}

	public boolean afficheRepartConventionSourceLibelle() {
		boolean afficheRepartConventionSourceLibelle = false;

		if (indexDepenseControleConvention == 0) {
			afficheRepartConventionSourceLibelle = true;
		}

		return afficheRepartConventionSourceLibelle;
	}

	public int nbreDepenseControleConvention() {
		return getLaDepenseBudget().depenseControleConventions().count();
	}

	// Repartition par Codes analytiques
	public EODepenseControleAnalytique getUneDepenseCtrlAnalytique() {
		return uneDepenseCtrlAnalytique;
	}

	public void setUneDepenseCtrlAnalytique(EODepenseControleAnalytique uneDepenseCtrlAnalytique) {
		this.uneDepenseCtrlAnalytique = uneDepenseCtrlAnalytique;
	}

	public BigDecimal getUneDepenseCtrlAnalytiqueMontant() {
		return getUneDepenseCtrlAnalytique().danaTtcSaisie().abs();
	}

	public void setUneDepenseCtrlAnalytiqueMontant(BigDecimal uneDepenseCtrlAnalytiqueMontant) {
		if (isModificationEnCours == false) {
			BigDecimal oldValue = this.uneDepenseCtrlAnalytique.danaTtcSaisie();
			if (oldValue != null && uneDepenseCtrlAnalytiqueMontant != null && oldValue.floatValue() != uneDepenseCtrlAnalytiqueMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlAnalytiqueMontant != null) {
				getUneDepenseCtrlAnalytique().setMontantTtc(uneDepenseCtrlAnalytiqueMontant.abs().negate());
				isModificationEnCours = true;
			}
		}
	}

	public boolean afficheRepartAnalytiqueSourceLibelle() {
		boolean afficheRepartAnalytiqueSourceLibelle = false;

		if (indexDepenseControleAnalytique == 0) {
			afficheRepartAnalytiqueSourceLibelle = true;
		}

		return afficheRepartAnalytiqueSourceLibelle;
	}

	public int nbreDepenseControleAnalytique() {
		return getLaDepenseBudget().depenseControleAnalytiques().count();
	}

}