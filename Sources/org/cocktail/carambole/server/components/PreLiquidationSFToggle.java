/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.Session;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.process.ProcessPreDepense;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;

/**
 * Composant permettant d'afficher et/ou d'activer le service fait.
 * 
 * @author rprin
 * @binding depensePapier La depensePapier concernée
 * @binding utilisateurPersId PersId de l'utilisateur
 */
public class PreLiquidationSFToggle extends MyAjaxComponent {
	private static final long serialVersionUID = -8584798852860117478L;

	private static final String BINDING_utilisateurPersId = "utilisateurPersId";
	private static final String BINDING_depensePapier = "depensePapier";

	private ApplicationUser appUser;

	public PreLiquidationSFToggle(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public EODepensePapier depensePapier() {
		if (!hasBinding(BINDING_depensePapier)) {
			throw new RuntimeException("Le binding " + BINDING_depensePapier + " est nul.");
		}
		return (EODepensePapier) valueForBinding(BINDING_depensePapier);
	}

	public Boolean ifServiceFait() {
		//return Boolean.FALSE;
		return Boolean.valueOf(depensePapier().dppDateServiceFait() != null);
	}

	public Boolean ifQui() {
		return Boolean.valueOf(((Session) mySession()).isServiceFacturierDisponible() && depensePapier().sfPersonne() != null);
	}

	public String quiSf() {
		return depensePapier().sfPersonne().getNomAndPrenom();
	}

	public Boolean ifHasDroitSF() {
		//gerer pb des droits
		// = droit d'engager sur les lignes de la commande = droit de voir (ou modifier) de la commande
		return (((Session) mySession()).isServiceFacturierDisponible() && depensePapier().commandeAssociee().isModifiable(depensePapier().editingContext(), mySession().getUtilisateur(), null));
	}

	public WOActionResults attesterSF() {
		return null;
	}

	public Integer utilisateurPersId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	public ApplicationUser applicationUser() {
		if (appUser == null) {
			appUser = new ApplicationUser(depensePapier().editingContext(), utilisateurPersId());
		}
		return appUser;
	}

	public WOActionResults onConfirmer() {

		WOComponent nextPage = null;
		//			EOCommande commande = laCommandeEnCours();
		EODepensePapier depensePapier = depensePapier();
		try {
			EOEditingContext edc = depensePapier.editingContext();
			ProcessPreDepense.serviceFaitEtLiquide(session.dataBus(), edc, depensePapier, depensePapier.dppDateServiceFait(), utilisateurInContext(edc));
			session.setAlertMessage(null);
			edc.invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(new EOGlobalID[] {
					edc.globalIDForObject(depensePapier)
			}));
			session.setLaDepensePapierEnCours(depensePapier);
			//CktlAjaxWindow.close(context(), sFAttestationDialogID());
			nextPage = pageWithName(LiquidationApresEnregistrement.class.getName());
		} catch (FactoryException e) {
			depensePapier.setDppDateServiceFait(null);
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					session.setAlertMessage(alertMessage);
					setOnLoadJS("parent.MenuUpdate();");
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				nextPage = pageWithName(LiquidationApresEnregistrement.class.getName());
			}
		} catch (RuntimeException e1) {
			session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}

		return nextPage;

	}

	public WOActionResults onAnnuler() {
		depensePapier().setDppDateServiceFait(null);
		//CktlAjaxWindow.close(context(), sFAttestationDialogID());
		return null;
	}

	public String sFAttestationDialogID() {
		return getComponentId() + "_sFAttestationDialog";
	}

	public String onComplete() {
		return "function(){CAW.close('" + sFAttestationDialogID() + "_win');}";
		//	return "function(){parent.Windows.closeAll();}";
	}

	public String containerSFAttestationID() {
		return getComponentId() + "_containerSFAttestation";
	}

	public String onConfirmerComplete() {
		return "function oc(){" + containerSFAttestationID() + "Update();}";
	}

	//	public String jsOpenSfAttestationDialog() {
	//		return "function(v) {openCAMD_" + sFAttestationDialogID() + "('Attestation de service fait'); return false;}";
	//	}

}