/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Liste de liquidation avec recherche pour sélection.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class LiquidationListeSelection extends MyAjaxComponent {
	private static final long serialVersionUID = 1L;

	public static final String BINDING_dg = "dg";
	public static final String BINDING_selectedDico = "selectedDico";
	public static final String BINDING_actionFiltrerName = "actionFiltrerName";
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	private Boolean checkedAll = Boolean.FALSE;

	public static final String COL_ORGAN_KEY = "lBud";
	public static final String COL_DPP_NUMERO_FACTURE_KEY = "DPP_NUMERO_FACTURE";
	public static final String COL_FOURNISSEUR_KEY = "fournisseur";
	public static final String COL_PCO_KEY = "pco";
	public static final String COL_DPP_DATE_FACTURE_KEY = "DPP_DATE_FACTURE";
	public static final String COL_DPCO_TTC_SAISIE_KEY = "DPCO_TTC_SAISIE";
	public static final String COL_LIQUIDEUR_KEY = "liquideur";
	public static final String COL_MODE_PAIEMENT_KEY = "modePaiement";

	public static final String ROW_KEY = "unDepDico.";

	public NSDictionary<String, Object> unDepDico = null;

	private NSArray<CktlAjaxTableViewColumn> colonnes;

	public final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

	/** Tableau contenant les clés identiofiant les colonnes à afficher par défaut. */
	public static NSArray<String> DEFAULT_COLONNES_KEYS = new NSArray<String>(new String[] {
			COL_ORGAN_KEY, COL_DPP_NUMERO_FACTURE_KEY, COL_DPP_DATE_FACTURE_KEY, COL_MODE_PAIEMENT_KEY, COL_PCO_KEY, COL_DPCO_TTC_SAISIE_KEY, COL_FOURNISSEUR_KEY, COL_LIQUIDEUR_KEY
	});

	public String unLiquideur;
	public NSMutableDictionary unFourniseur;
    private NSMutableDictionary fourniseurSelection;

	public LiquidationListeSelection(WOContext context) {
		super(context);
	}

	//	public NSArray lesDepenseCtrlPlanco() {
	//		return dg().displayedObjects();
	//	}

	public WOActionResults filtrer() {
		dg().setSelectedObjects(NSArray.EmptyArray);
		return (WOActionResults) performParentAction((String) valueForBinding(BINDING_actionFiltrerName));
	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(BINDING_dg);
	}

	public Boolean getLigneSelected() {
		return Boolean.valueOf(dg().selectedObjects().contains(unDepDico));
	}

	@SuppressWarnings("unchecked")
	public void setLigneSelected(Boolean ligneSelected) {
		if (ligneSelected.booleanValue()) {
			dg().selectedObjects().add(unDepDico);
		}
		else {
			dg().selectedObjects().remove(unDepDico);
		}

	}

	public String lBud() {
		if (unDepDico != null) {
			return unDepDico.valueForKey("ORG_UB") +
					(unDepDico.valueForKey("ORG_CR") != null && !NSKeyValueCoding.NullValue.equals(unDepDico.valueForKey("ORG_CR")) ? "/" + unDepDico.valueForKey("ORG_CR") : "") +
					(unDepDico.valueForKey("ORG_SOUSCR") != null && !NSKeyValueCoding.NullValue.equals(unDepDico.valueForKey("ORG_SOUSCR")) ? "/" + unDepDico.valueForKey("ORG_SOUSCR") : "");
		}
		return null;
	}

	public String pco() {
		if (unDepDico != null) {
			return (String) unDepDico.valueForKey("PCO_NUM");
		}
		return null;
	}

	public String fournisseur() {
		if (unDepDico != null) {
			return unDepDico.valueForKey("ADR_NOM") + prenomFournisseur();
		}
		return null;
	}

	public String liquideur() {
		if (unDepDico != null) {
			return (String) unDepDico.valueForKey("UTLNOMPRENOM");

		}
		return null;
	}

	public String modePaiement() {
		if (unDepDico != null) {
			return (String) unDepDico.valueForKey("MOD_CODE") + " - " + (String) unDepDico.valueForKey("MOD_LIBELLE");

		}
		return null;
	}

	public String prenomFournisseur() {
		if (unDepDico != null) {
			return (String) (!NSKeyValueCoding.NullValue.equals(unDepDico.valueForKey("ADR_PRENOM")) ? " " + unDepDico.valueForKey("ADR_PRENOM") : "");
		}
		return null;
	}

	public Boolean isCheckedAll() {
		return checkedAll;
	}

	public void setIsCheckedAll(Boolean checked) {
		checkedAll = Boolean.valueOf(!checkedAll.booleanValue());
		checkAll(checkedAll);
	}

	public void checkAll(Boolean b) {
		if (b.booleanValue()) {
			dg().setSelectedObjects(dg().displayedObjects());
		}
		else {
			dg().setSelectedObjects(NSArray.EmptyArray);
		}

	}

	public String listeDepenseContainerId() {
		return getComponentId() + "_listeDepenseContainerId";
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {

			CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
			col1.setLibelle("Budget");
			col1.setOrderKeyPath(COL_ORGAN_KEY);
			CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(COL_ORGAN_KEY, "emptyValue");
			col1.setAssociations(ass1);
			_colonnesMap.takeValueForKey(col1, COL_ORGAN_KEY);

			CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
			col2.setLibelle("Numéro");
			col2.setOrderKeyPath(COL_DPP_NUMERO_FACTURE_KEY);
			//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
			CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DPP_NUMERO_FACTURE_KEY, "emptyValue");
			col2.setAssociations(ass2);
			col2.setRowCssStyle("white-space: normal;");
			_colonnesMap.takeValueForKey(col2, COL_DPP_NUMERO_FACTURE_KEY);

			CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
			col4.setLibelle("Date facture");
			//col4.setComponent(FouValideColonne.class.getName());
			//col4.setOrderKeyPath(COL_DPP_DATE_FACTURE_KEY);
			CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DPP_DATE_FACTURE_KEY, null);
			col4.setAssociations(ass4);
			ass4.setDateformat("dd/MM/yyyy");
			//ass4.setFormatter("application.appDateFormatter");
			_colonnesMap.takeValueForKey(col4, COL_DPP_DATE_FACTURE_KEY);

			CktlAjaxTableViewColumn modePaiement = new CktlAjaxTableViewColumn();
			modePaiement.setLibelle("Mode de paiement");
			modePaiement.setOrderKeyPath(COL_MODE_PAIEMENT_KEY);
			CktlAjaxTableViewColumnAssociation assModePaiement = new CktlAjaxTableViewColumnAssociation(COL_MODE_PAIEMENT_KEY, "emptyValue");
			modePaiement.setAssociations(assModePaiement);
			_colonnesMap.takeValueForKey(modePaiement, COL_MODE_PAIEMENT_KEY);

			CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
			col5.setLibelle("Compte");
			col5.setOrderKeyPath(COL_PCO_KEY);
			CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(COL_PCO_KEY, "emptyValue");
			col5.setAssociations(ass5);
			_colonnesMap.takeValueForKey(col5, COL_PCO_KEY);

			CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
			col6.setLibelle("Fournisseur");
			//	col6.setComponent(CktlAjaxTVCheckBoxCell.class.getName());
			col6.setOrderKeyPath(COL_FOURNISSEUR_KEY);
			col6.setRowCssClass("");
			col6.setRowCssStyle("white-space: normal;");
			CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(COL_FOURNISSEUR_KEY, "emptyValue");
			col6.setAssociations(ass6);
			_colonnesMap.takeValueForKey(col6, COL_FOURNISSEUR_KEY);

			CktlAjaxTableViewColumn ttc = new CktlAjaxTableViewColumn();
			ttc.setLibelle("TTC");
			ttc.setOrderKeyPath(COL_DPCO_TTC_SAISIE_KEY);
			ttc.setRowCssClass("alignToRight");
			CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DPCO_TTC_SAISIE_KEY, "emptyValue");
			ass3.setFormatter(Application.APP2DECIMALES_FORMATTER_KEY);
			ttc.setAssociations(ass3);
			_colonnesMap.takeValueForKey(ttc, COL_DPCO_TTC_SAISIE_KEY);

			CktlAjaxTableViewColumn liquideur = new CktlAjaxTableViewColumn();
			liquideur.setLibelle("Liquidé par");
			liquideur.setOrderKeyPath(COL_LIQUIDEUR_KEY);
			CktlAjaxTableViewColumnAssociation assliquideur = new CktlAjaxTableViewColumnAssociation(COL_LIQUIDEUR_KEY, "emptyValue");
			liquideur.setAssociations(assliquideur);
			_colonnesMap.takeValueForKey(liquideur, COL_LIQUIDEUR_KEY);

			//			CktlAjaxTableViewColumn vide = new CktlAjaxTableViewColumn();
			//			vide.setLibelle("Vide");
			//			//liquideur.setOrderKeyPath(COL_LIQUIDEUR_KEY);
			//			CktlAjaxTableViewColumnAssociation assVide = new CktlAjaxTableViewColumnAssociation(ROW_KEY + "vide", "emptyValue");
			//			vide.setAssociations(assVide);
			//			assVide.setValueWhenEmptyDynamique("emptyValue");
			//			_colonnesMap.takeValueForKey(vide, "vide");

			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.objectForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}

		return keys;
	}

	public String depenseTbViewId() {
		return getComponentId() + "_depenseTbView";
	}

	public String emptyValue() {
		return "";
	}

	public String dateFormatter() {
		return ("%d/%m/%Y");
	}
	
	public String fournisseurAffichage() {
	    String affichage = "";
	    if (unFourniseur != null) {
	        affichage = (String) unFourniseur.valueForKey("FOURNISSEUR");
	    }
	    return affichage;
	}
	
	
	public NSMutableDictionary fourniseurSelection() {
	    return fourniseurSelection;
	}
	
	public void setFourniseurSelection(NSMutableDictionary fourniseurSelection) {
	    if (fourniseurSelection != null) {
	        Integer maSelection = ((Number) fourniseurSelection.valueForKey("ADR_NUMERO")).intValue();
	        dg().queryBindings().put("ADR_NUMERO", maSelection);
	    } else {
	        dg().queryBindings().remove("ADR_NUMERO");
	    }
	    this.fourniseurSelection = fourniseurSelection;
	}
	
}