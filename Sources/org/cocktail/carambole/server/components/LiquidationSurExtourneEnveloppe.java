/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementBudget;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata.ETypeMontant;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit.ESection;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseExtourneCredits;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepense.server.service.ConfigurationExtourneService;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEnveloppeExtourneModalPanelCtrl;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlEnveloppeExtourneSelectionCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSelector;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;
import er.extensions.qualifiers.ERXOrQualifier;

public class LiquidationSurExtourneEnveloppe extends LiquidationSansEngagement {
	private static final long serialVersionUID = 1L;

	private MyCktlEnveloppeExtourneModalPanelCtrl cktlEnveloppeExtourneModalPanelCtrl;

	public LiquidationSurExtourneEnveloppe(WOContext context) {
		super(context);
	}

	public String getPageTitle() {
		String prefix = "Création d'une pré-liquidation";
		if (!isPreLiquidation()) {
			prefix = "Création d'une liquidation";
		}
		return prefix.concat(" sur enveloppe d'extourne");
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UNE_LIQUIDATION_SUR_EXTOURNE_ENVELOPPE_KEY, Menu.ANNULER_UNE_LIQUIDATION_KEY
		});
		return items;
	}

	public CktlEnveloppeExtourneModalPanelCtrl getCktlEnveloppeExtourneSelectionCtrl() {
		if (cktlEnveloppeExtourneModalPanelCtrl == null) {
			cktlEnveloppeExtourneModalPanelCtrl = this.new MyCktlEnveloppeExtourneModalPanelCtrl(session, edc(), exerciceCourant());
		}
		return cktlEnveloppeExtourneModalPanelCtrl;
	}

	@Override
	protected EOTauxProrata getTauxProrataDefautForCreditBudgetaire(NSArray<EOTauxProrata> creditTauxProrataList) {
		//on récupère le taux de prorata affecté à la partie liquidée sur crédits d'extourne 
		//et on vérifie si ce taux de prorata fait partie de la liste passée en parametre.
		_IDepenseBudget dbSurExtourne = getLaDepensePapier().currentDepenseBudgets().objectAtIndex(0);
		if (dbSurExtourne != null && dbSurExtourne.tauxProrata() != null) {
			if (creditTauxProrataList.indexOfObject(dbSurExtourne.tauxProrata()) != NSArray.NotFound) {
				return dbSurExtourne.tauxProrata();
			}
		}
		return super.getTauxProrataDefautForCreditBudgetaire(creditTauxProrataList);
	}

	/**
	 * Ce qui se passe quand l'utilisateur sélectionne une source de crédits d'extourne.
	 * 
	 * @return
	 */
	public WOActionResults ajouterCreditsExtourneCallback() {
		try {
			_IDepenseExtourneCredits creditSelectionne = getCktlEnveloppeExtourneSelectionCtrl().getEnveloppeExtourneSelectionCtrl().getEnveloppeSelectionnee();
			if (creditSelectionne.getDisponible().compareTo(BigDecimal.ZERO) <= 0) {
				throw new Exception("Le disponible sur " + creditSelectionne.libelleCourt() + " est égal à 0.");
			}
			NSArray<EOTauxProrata> tps = creditSelectionne.tauxProrataDisponibles();
			if (tps != null && tps.count() > 0) {
				creditSelectionne.setTauxProrata(tps.objectAtIndex(0));
			}
			getUneDepenseBudget().setSource(creditSelectionne);
			getUneDepenseBudget().updateFromSource();
			getUneDepenseBudget().engagementBudget().setSource(creditSelectionne);
			updateEngagementBudget();

		} catch (Exception e) {
			addSimpleErrorMessage("Erreur", e);
		} finally {
			AjaxUpdateContainer.updateContainerWithID("ContainerEngagement", context());
			AjaxUpdateContainer.updateContainerWithID("ContainerRepartitions", context());
		}
		return null;
	}

	public WOActionResults annulerCreditsExtourneCallback() {
		return null;
	}

	public String getStyleForSourceType() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudget());
	}

	public String getStyleForSourceTypeForHorsMarche() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForHorsMarche());
	}

	public String getStyleForSourceTypeForMarche() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForMarche());
	}

	public String getStyleForSourceTypeForAction() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForAction());
	}

	public String getStyleForSourceTypeForAnalytique() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForAnalytique());
	}

	public String getStyleForSourceTypeForConvention() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForConvention());
	}

	public String getStyleForSourceTypeForImputation() {
		return SourcesComponentsHelper.getStyleForSourceType(getUneDepenseBudgetForImputation());
	}

	public Boolean isDepenseBudgetSurExtourne() {
		return isDepenseBudgetSurExtourne(getUneDepenseBudget());
	}

	public Boolean isDepenseBudgetSurExtourne(_IDepenseBudget db) {
		return db != null && db.isSurExtourne();
	}

	/**
	 * En fontion des montants saisis, dispo, créer ou supprimer un depenseBudget sur budget exercice.
	 * 
	 * @throws Exception
	 */
	protected void updateEngagementBudget() throws Exception {
		EODepensePapier dpp = getLaDepensePapier();
		_IDepenseBudget dbSurExtourne = dpp.currentDepenseBudgets().objectAtIndex(0);
		_IDepenseBudget dbSurBudget;
		if (!ESourceCreditType.EXTOURNE.equals(dbSurExtourne.getSourceTypeCredit())) {
			throw new Exception("Erreur : le type de la source de credit devait etre EXTOURNE");
		}
		if (dbSurExtourne.getSource().tauxProrata() == null) {
			return;
		}
		_IDepenseExtourneCredits src = (_IDepenseExtourneCredits) dbSurExtourne.getSource();
		//recalculer le disponible sur la source en fonction du taux de prorata sélectionné et du TTC + HT de la source
		EOTauxProrata tp = src.tauxProrata();
		BigDecimal srcDispo = src.vecMontantBudDispoReel().abs();
		BigDecimal dppMontantTtc = laDepensePapierEnCours().dppTtcInitial();
		BigDecimal dppMontantHt = laDepensePapierEnCours().dppHtInitial();
		BigDecimal dppMontantTva = dppMontantTtc.subtract(dppMontantHt);
		//montant budgetaire calculé dans l'hypothese ou la depense ne se fait que sur le source extournée
		BigDecimal dppMontantBud = tp.montantBudgetaire(dppMontantHt, dppMontantTva);

		if (dppMontantBud.compareTo(srcDispo) > 0) {
			//on a un depassement, on cree/met à jour un depenseBudget sur exercice en cours
			dbSurExtourne.updateFromSource();
			EOEngagementBudget ebSurExtourne = dbSurExtourne.engagementBudget();
			NSDictionary<ETypeMontant, BigDecimal> montants = dbSurExtourne.tauxProrata().htEtTtcAPartirMontantBudgetaireDispo(dppMontantHt, dppMontantTtc, srcDispo);
			ebSurExtourne.setFournisseurRelationship(getFournisseur());
			ebSurExtourne.setEngLibelle(getLaDepensePapier().dppNumeroFacture());
			ebSurExtourne.setEngHtSaisie(montants.get(ETypeMontant.HT));
			ebSurExtourne.setEngTtcSaisie(montants.get(ETypeMontant.TTC));
			ebSurExtourne.setEngMontantBudgetaire(srcDispo);
			ebSurExtourne.setEngMontantBudgetaireReste(srcDispo);
			updateDepenseBudgetFromEngagementBudget(dbSurExtourne);

			if (dpp.currentDepenseBudgets().count() > 1) {
				dbSurBudget = dpp.currentDepenseBudgets().objectAtIndex(1);
			}
			else {
				FactoryEngagementBudget feb = new FactoryEngagementBudget();
				EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(edc(), EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
				EOEngagementBudget ebSurBudget = feb.creer(edc());
				ebSurBudget.setFournisseurRelationship(getFournisseur());
				ebSurBudget.setEngLibelle(getLaDepensePapier().dppNumeroFacture());
				ebSurBudget.setEngHtSaisie(BigDecimal.ZERO);
				ebSurBudget.setEngTtcSaisie(BigDecimal.ZERO);
				ebSurBudget.setEngMontantBudgetaire(BigDecimal.ZERO);
				ebSurBudget.setEngMontantBudgetaireReste(BigDecimal.ZERO);
				ebSurBudget.setTypeApplicationRelationship(typeApplication);
				ebSurBudget.setExerciceRelationship(exerciceCourant());
				ebSurBudget.setUtilisateurRelationship(utilisateur());
				dbSurBudget = getCtrl().getFdb().creer(edc(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, dpp,
						ebSurBudget, ebSurBudget.tauxProrata(), ebSurBudget.exercice(), utilisateur(), ESourceCreditType.BUDGET, ebSurBudget.source());
			}

			BigDecimal dbbMontantTtc = laDepensePapierEnCours().dppTtcInitial().subtract(dbSurExtourne.depTtcSaisie());
			BigDecimal dbbMontantHt = laDepensePapierEnCours().dppHtInitial().subtract(dbSurExtourne.depHtSaisie());
			BigDecimal dbbMontantTva = dbbMontantTtc.subtract(dbbMontantHt);
			if (dbSurBudget.getSource().tauxProrata() != null) {
				dbSurBudget.updateFromSource();
				BigDecimal dbbMontantBud = dbSurBudget.getSource().tauxProrata().montantBudgetaire(dbbMontantHt, dbbMontantTva);
				EOEngagementBudget ebSurBudget = dbSurBudget.engagementBudget();
				ebSurBudget.setEngHtSaisie(dbbMontantHt);
				ebSurBudget.setEngTtcSaisie(dbbMontantTtc);
				ebSurBudget.setEngMontantBudgetaire(dbbMontantBud);
				ebSurBudget.setEngMontantBudgetaireReste(dbbMontantBud);
				updateDepenseBudgetFromEngagementBudget(dbSurBudget);
			}

		}
		else {
			//pas de depassement, on supprime  les depenseBudgets et engagementBudget eventuels sur budget exercice
			_IFactoryDepenseBudget fdb = getCtrl().getFdb();
			FactoryEngagementBudget feb = new FactoryEngagementBudget();
			for (int i = dpp.currentDepenseBudgets().count() - 1; i > 0; i--) {
				feb.supprimer(edc(), dpp.currentDepenseBudgets().objectAtIndex(i).engagementBudget());
				fdb.supprimer(edc(), dpp.currentDepenseBudgets().objectAtIndex(i));
			}
			dbSurExtourne.updateFromSource();
			dbSurExtourne.engagementBudget().setFournisseurRelationship(getFournisseur());
			dbSurExtourne.engagementBudget().setEngLibelle(getLaDepensePapier().dppNumeroFacture());
			dbSurExtourne.engagementBudget().setEngHtSaisie(dppMontantHt);
			dbSurExtourne.engagementBudget().setEngTtcSaisie(dppMontantTtc);
			dbSurExtourne.engagementBudget().setEngMontantBudgetaire(dppMontantBud);
			dbSurExtourne.engagementBudget().setEngMontantBudgetaireReste(dppMontantBud);
			updateDepenseBudgetFromEngagementBudget(dbSurExtourne);
		}
		updateDepenseCtrlMarcheWithAttribution();
		updDicos();
	}

	public Boolean sourceExtourneSelectDisabled() {
		return !(getLaDepensePapier().dppTtcInitial() != null && getLaDepensePapier().dppTtcInitial().floatValue() != 0);
	}

	/**
	 * @return true seulement si on a un dépassement budget
	 */
	public Boolean showRepartAction() {
		return (getLaDepensePapier().depenseBudgets().count() > 1);
	}

	/**
	 * @return true seulement si on a un dépassement budget
	 */
	public Boolean showRepartNomenclature() {
		return (getLaDepensePapier().depenseBudgets().count() > 1);
	}

	private class MyCktlEnveloppeExtourneModalPanelCtrl extends CktlEnveloppeExtourneModalPanelCtrl {

		public MyCktlEnveloppeExtourneModalPanelCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
			super(session, edc, exercice);
			setEnveloppeExtourneSelectionCtrl(new MyCktlEnveloppeExtourneSelectionCtrl(session, edc, exercice));
		}
	}

	private class MyCktlEnveloppeExtourneSelectionCtrl extends CktlEnveloppeExtourneSelectionCtrl {

		public MyCktlEnveloppeExtourneSelectionCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
			super(session, edc, exercice);

			// surcharge initialisation
			ConfigurationExtourneService confExtourneService = ConfigurationExtourneService.instance();
			setShowNiveauOrganSelection(false);
			setNiveauOrganSelected(confExtourneService.extourneEnveloppeNiveauSelection(edc, exerciceCourant()));
			selectNiveauOrgan();
		}

		@Override
		public EOQualifier enrichirQualifiers(EOQualifier qualifier) {
			EOQualifier qualifierBase = super.enrichirQualifiers(qualifier);
			EOQualifier qualTypeCreditAutorise = null;
			EOQualifier qualTypeCreditNonAutorise = null;

			NSMutableArray<EOQualifier> qualTypeCreditAutoriseList = new NSMutableArray<EOQualifier>();
			NSMutableArray<EOQualifier> qualTypeCreditNonAutoriseList = new NSMutableArray<EOQualifier>();

			ConfigurationExtourneService confExtourneService = ConfigurationExtourneService.instance();
			boolean extourneEnveloppeTypeCredit1Autorise = confExtourneService.extourneEnveloppeTypeCreditAutorise(
					edc(), exerciceCourant(), ESection.SECTION_1);
			boolean extourneEnveloppeTypeCredit2Autorise = confExtourneService.extourneEnveloppeTypeCreditAutorise(
					edc(), exerciceCourant(), ESection.SECTION_2);
			boolean extourneEnveloppeTypeCredit3Autorise = confExtourneService.extourneEnveloppeTypeCreditAutorise(
					edc(), exerciceCourant(), ESection.SECTION_3);

			if (extourneEnveloppeTypeCredit1Autorise) {
				qualTypeCreditAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_1, true));
			}
			else {
				qualTypeCreditNonAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_1, false));
			}

			if (extourneEnveloppeTypeCredit2Autorise) {
				qualTypeCreditAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_2, true));
			}
			else {
				qualTypeCreditNonAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_2, false));
			}

			if (extourneEnveloppeTypeCredit3Autorise) {
				qualTypeCreditAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_3, true));
			}
			else {
				qualTypeCreditNonAutoriseList.add(buildTypeCreditSectionQualifier(ESection.SECTION_3, false));
			}

			if (qualTypeCreditAutoriseList.size() > 0) {
				qualTypeCreditAutorise = new ERXOrQualifier(qualTypeCreditAutoriseList);
			}
			if (qualTypeCreditNonAutoriseList.size() > 0) {
				qualTypeCreditNonAutorise = new ERXAndQualifier(qualTypeCreditNonAutoriseList);
			}

			return ERXQ.and(qualifierBase, qualTypeCreditAutorise, qualTypeCreditNonAutorise);
		}

		private EOQualifier buildTypeCreditSectionQualifier(ESection typeCreditSection, boolean include) {
			NSSelector selector = ERXQ.NE;
			if (include) {
				selector = ERXQ.EQ;
			}
			return new ERXKeyValueQualifier(
					_IDepenseExtourneCredits.TO_TYPE_CREDIT.append(EOTypeCredit.TCD_SECT_KEY).key(),
					selector,
					typeCreditSection.section());
		}
	}

}