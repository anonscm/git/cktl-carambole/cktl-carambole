/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

// Generated by the WOLips Templateengine Plug-in at 5 janv. 2007 12:18:54

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.MyComponent;
import org.cocktail.carambole.server.reports.PrintFactory;
import org.cocktail.carambole.server.utilitaires.UtilString;
import org.cocktail.fwkcktldepense.server.exception.CommandeImpressionException;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeImpression;
import org.cocktail.fwkcktldepense.server.finder.FinderAdresse;
import org.cocktail.fwkcktldepense.server.finder.FinderCommandeImpression;
import org.cocktail.fwkcktldepense.server.finder.FinderCompte;
import org.cocktail.fwkcktldepense.server.finder.FinderJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderStructure;
import org.cocktail.fwkcktldepense.server.finder.FinderTelephoneFax;
import org.cocktail.fwkcktldepense.server.impression.XMLCommande;
import org.cocktail.fwkcktldepense.server.metier.EOAdresse;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeImpression;
import org.cocktail.fwkcktldepense.server.metier.EOCompte;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.metier.EOStructure;
import org.cocktail.fwkcktldepense.server.process.ProcessCommande;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;

import com.ibm.icu.text.DecimalFormat;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXStringUtilities;

public class Imprimer extends MyComponent {

	private static final long serialVersionUID = 1L;

	public final static Logger logger = Logger.getLogger(Imprimer.class);

	private String onloadJS = null;
	private EOCommande commande = null;
	private NSArray<EOAdresse> lesAdressesDuFournisseur;
	private EOAdresse uneAdresseFournisseur;
	private EOAdresse uneAdresseFournisseurSelectionnee;
	private String observations, strRecherche;
	private NSArray<EOStructure> lesServices;
	private EOStructure unService;
	private EOStructure unServiceSelectionne;
	private NSArray<EOAdresse> lesAdressesDuService;
	private EOAdresse uneAdresseService;
	private EOAdresse uneAdresseServiceSelectionnee;
	private NSArray<String> lesTelephonesDuService;
	private String unTelephoneService;
	private String unTelephoneServiceSelectionne;
	private NSArray<String> lesFaxsDuService;
	private String unFaxService, unFaxServiceSelectionne;

	public String strRechercheStructureLivraison;
	private NSArray<EOStructure> lesStructuresDeLivraison;

	private boolean isNePasImprimerAdrLivraisonChecked = false;
	private boolean isNePasImprimerLeMontantChecked = false;
	private EOStructure uneStructureLivraison;
	private EOStructure uneStructureLivraisonSelectionne;
	private NSArray<EOAdresse> lesAdressesDeLivraison;
	private EOAdresse uneAdresseLivraison;
	private EOAdresse uneAdresseLivraisonSelectionnee;
	private NSArray<String> lesTelephonesDeLaStructureDeLivraison;
	private String unTelephoneStructureDeLivraison;
	private String unTelephoneStructureDeLivraisonSelectionne;
	private NSArray<String> lesFaxsDeLaStructureDeLivraison;
	private String unFaxStructureDeLivraison;
	private String unFaxStructureLivraisonSelectionne;
	public NSArray<EOStructure> lesStructureDeLivraisonSelectionne;

	public boolean showRechercheStrLivraison;

	public Imprimer(WOContext context) {
		super(context);
		commande = laCommandeEnCours();
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		MyAjaxPage.injectResourcesInResponse(res, ctx);
		String message = session.getAlertMessage();
		if (message != null && message.equals("") == false) {
			String str = "<script language=\"javascript\" type=\"text/javascript\">\n";
			str += "alert(\"" + message + "\");\n";
			str += "</script>";
			res.appendContentString(str);
			session.setAlertMessage(null);
		}
	}

	@SuppressWarnings("unchecked")
	public void initialiser() {
		commande = laCommandeEnCours();
		EOCommandeImpression commandeImpression = FinderCommandeImpression.getCommandeImpression(commande.editingContext(), commande);
		if (commandeImpression != null) {
			setUneAdresseFournisseurSelectionnee(commandeImpression.adresseFournisseur());
			setObservations(commandeImpression.cimpInfosImpression());
			if (commandeImpression.structureService() != null) {
				setUnServiceSelectionne(commandeImpression.structureService());
				setUneStructureLivraisonSelectionne(commandeImpression.structureService());
			}
			if (commandeImpression.adresseService() != null) {
				setUneAdresseServiceSelectionnee(commandeImpression.adresseService());
			}
		}
		else {
			NSArray<EOStructure> services = getLesServices();
			if (services != null && services.count() == 1) {
				setUnServiceSelectionne((EOStructure) services.lastObject());
				setUneStructureLivraisonSelectionne((EOStructure) services.lastObject());
			}
			if (getObservations() == null) {
				String tmp = myApp().config().stringForKey("CARAMBOLE_PREREMPLIR_OBSERVATIONS_IMPRIMER");
				if ((tmp != null) && (tmp.length() > 0)) {
					setObservations(UtilString.replaceWithValuesFromMap(tmp, session.connectedUserInfo().toHashtable()));
				}
			}
		}
		showRechercheStrLivraison = false;
		isNePasImprimerAdrLivraisonChecked = false;
		isNePasImprimerLeMontantChecked = false;
	}

	public void reset() {
		uneAdresseFournisseur = null;
		uneAdresseFournisseurSelectionnee = null;
		uneAdresseLivraison = null;
		uneAdresseLivraisonSelectionnee = null;
		observations = "";
		strRecherche = "";
		strRechercheStructureLivraison = "";
		showRechercheStrLivraison = false;
		isNePasImprimerAdrLivraisonChecked = false;
		isNePasImprimerLeMontantChecked = false;
	}

	public String getOnloadJS() {
		return onloadJS;
	}

	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	public String styleDivImprimer() {
		String styleDivImprimer = "";
		String onload = getOnloadJS();

		if (onload != null && onload.equals("") == false) {
			styleDivImprimer = "display:none;";
		}
		return styleDivImprimer;
	}

	public WOComponent neFaitRien() {
		return null;
	}

	public WOComponent switchShowRechercheStrLivraison() {
		lesStructureDeLivraisonSelectionne = null;
		showRechercheStrLivraison = !showRechercheStrLivraison;
		return null;
	}

	public WOComponent validerModifServiceLivraison() {
		if (lesStructureDeLivraisonSelectionne != null)
			setUneStructureLivraisonSelectionne((EOStructure) lesStructureDeLivraisonSelectionne.lastObject());
		return switchShowRechercheStrLivraison();
	}

	public WOComponent rechercher() {
		NSMutableDictionary<String, Object> args = new NSMutableDictionary<String, Object>();
		args.setObjectForKey("*" + strRechercheStructureLivraison + "*", "llStructure");
		lesStructuresDeLivraison = FinderStructure.getComposantesEtServices(commande.editingContext(), "*" + strRechercheStructureLivraison + "*");
		if (lesStructuresDeLivraison != null && lesStructuresDeLivraison.count() == 1)
			lesStructureDeLivraisonSelectionne = lesStructuresDeLivraison;
		return null;
	}

	public WOComponent annuler() {
		DetailCommande nextPage = null;
		EOCommande commande = laCommandeEnCours();

		reset();
		nextPage = (DetailCommande) pageWithName(DetailCommande.class.getName());
		nextPage.setLaCommande(commande);

		return nextPage;

	}

	public WOComponent fermer() {
		Main nextPage = null;

		nextPage = (Main) pageWithName(Main.class.getName());

		return nextPage;

	}

	public WOActionResults imprimer() {
		return imprimer(false);
	}

	public WOActionResults imprimerDepenseAuComptant() {
		return imprimer(true);
	}

	public WOActionResults imprimer(boolean isDepenseAuComptant) {
		Boolean imprimerViaSix = session.isImprimerBonCommandeViaSIX();
		logger.info("Impression du bon de commande via " + (imprimerViaSix ? "six" : "jasper"));
		EOEditingContext edc = commande.editingContext();
		Hashtable<String, Object> params = new Hashtable<String, Object>();

		params.put(Application.XML_PRINTER_DRIVER, myApp().config().get(Application.XML_PRINTER_DRIVER));
		params.put(Application.SIX_SERVICE_HOST, myApp().config().get(Application.SIX_SERVICE_HOST));
		params.put(Application.SIX_SERVICE_PORT, myApp().config().get(Application.SIX_SERVICE_PORT));
		params.put(Application.SIX_USE_COMPRESSION, myApp().config().get(Application.SIX_USE_COMPRESSION));
		FactoryCommandeImpression fci = new FactoryCommandeImpression(params, myApp().getParam("SIXID_BONCOMMANDE"), 0);
		try {
			String telService = "";
			if (getUnTelephoneServiceSelectionne() != null) {
				telService = getUnTelephoneServiceSelectionne();
			}
			String faxService = "";
			if (getUnFaxServiceSelectionne() != null) {
				faxService = getUnFaxServiceSelectionne();
			}

			String faxLivraison = null;
			if (!isNePasImprimerAdrLivraisonChecked && getUnFaxStructureLivraisonSelectionne() != null) {
				faxLivraison = getUnFaxStructureLivraisonSelectionne();
			}
			String telLivraison = null;
			if (!isNePasImprimerAdrLivraisonChecked && getUnTelephoneStructureDeLivraisonSelectionne() != null) {
				telLivraison = getUnTelephoneStructureDeLivraisonSelectionne();
			}
			EOAdresse adresseLivraison = null;
			if (!isNePasImprimerAdrLivraisonChecked) {
				adresseLivraison = getUneAdresseLivraisonSelectionnee();
			}
			EOStructure structureLivraison = null;
			if (!isNePasImprimerAdrLivraisonChecked)
				structureLivraison = getUneStructureLivraisonSelectionne();

			EOCommandeImpression commandeImpression = fci.creer(edc, getObservations(), new NSTimestamp(), new NSTimestamp(), faxLivraison, telLivraison, faxService, telService, getUneAdresseFournisseurSelectionnee(), adresseLivraison, getUneAdresseServiceSelectionnee(), structureLivraison,
					getUnServiceSelectionne(), utilisateurInContext(edc), commande);
			commandeImpression.setDepenseAuComptant(isDepenseAuComptant);
			try {
				ProcessCommande.imprimer(session.dataBus(), edc, commandeImpression, utilisateurInContext(edc));
			} catch (FactoryException e) {
				throw new CommandeImpressionException("Impossible d enregistrer les informations liees a l'impression de la commande " + commande.commNumero());
			}

			if (imprimerViaSix) {
				return imprimerViaSIX(commandeImpression, fci);
			}
			else {
				return imprimerViaFrmk(commandeImpression, fci);
			}

		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					session.setAlertMessage(alertMessage);
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				session.setAlertMessage(alertMessage);
			}
			return null;
		} catch (RuntimeException e1) {
			session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}

	}

	private WOActionResults imprimerViaSIX(EOCommandeImpression commandeImpression, FactoryCommandeImpression fci) {
		int jobId = fci.imprimer(commandeImpression, isNePasImprimerLeMontantChecked(), myApp().test());
		if (jobId != -1) {
			CktlDataResponse larep = new CktlDataResponse();
			try {
				InputStream pdfStream = fci.getPdfStream(jobId);
				if (pdfStream == null) {
					throw new Exception("Le flux PDF est nul pour le jobId " + jobId);
				}

				larep.setContent(pdfStream, fci.getPdfStreamSize(jobId), CktlDataResponse.MIME_PDF);
				larep.setHeader("attachment; filename=" + "Bon_De_Commande_" + jobId + ".pdf", "Content-Disposition");

				if (myApp().test()) {

					System.out.println("Impression du fichier 'Bon_De_Commande_" + jobId + ".pdf' dans " + System.getProperty("java.io.tmpdir"));
					CktlLog.log("Impression du fichier 'Bon_De_Commande_" + jobId + ".pdf' dans " + System.getProperty("java.io.tmpdir"));
					FileOutputStream fs = new FileOutputStream(System.getProperty("java.io.tmpdir") + "/Bon_De_Commande_" + jobId + ".pdf");
					fs.write(larep.content().bytes());
					fs.close();
				}

				return larep;
			} catch (Exception e) {
				System.err.println("Exception lors de la récupération du jobid six " + jobId);
				CktlLog.log("Exception lors de la récupération du jobid six " + jobId);
				e.printStackTrace();
				throw new CommandeImpressionException(CommandeImpressionException.jobIdIntrouvable + " : " + e.getMessage());
			}
		}
		return null;
	}

	private WOActionResults imprimerViaFrmk(EOCommandeImpression commandeImpression, FactoryCommandeImpression fci) {
		try {
			StringWriter myStringWriter = new StringWriter();
			CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
			XMLCommande.signatairesInXml(myCktlXMLWriter, (NSArray<EOEngagementBudget>) commandeImpression.commande().commandeEngagements().valueForKeyPath(EOCommandeEngagement.ENGAGEMENT_BUDGET_KEY));
			myCktlXMLWriter.close();
			String signatairesAsXml = myStringWriter.toString();
			String infoSuivi = null;

			EOJefyAdminParametre parametre = FinderJefyAdminParametre.getParametre(commandeImpression.editingContext(), "URL_FOURNISSEUR", commandeImpression.commande().exercice());
			if (parametre != null && !ERXStringUtilities.stringIsNullOrEmpty(parametre.parValue())) {
				String url = parametre.parValue();
				String login = null;
				String password = null;
				EOCompte compte = FinderCompte.getCompteFournisseur(commande.editingContext(), commande.fournisseur());
				if (compte != null) {
					login = compte.cptLogin();
					password = compte.cptPasswd();
				}
				//On n'affiche pas le login mot de passe (pb securité)
				infoSuivi = "Pour consulter la situation de vos remboursements/factures : " + url;
			}

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("masquerMontantsFournisseur", isNePasImprimerLeMontantChecked());

			params.put("infoSuivi", infoSuivi);
			params.put("signatairesAsXml", signatairesAsXml);
			params.put("isDepenseComptant", commandeImpression.isDepenseAuComptant());

			//			String cimpid = "" + EOUtilities.primaryKeyForObject(commandeImpression.editingContext(), commandeImpression).valueForKey(EOCommandeImpression.CIMP_ID_KEY);
			if (commandeImpression.cimpIdProc() == null) {
				throw new Exception("Identifiant de la commande_impression nul.");
			}
			BigDecimal cimpid = BigDecimal.valueOf(commandeImpression.cimpIdProc().doubleValue());

			NSData data = PrintFactory.printBonDeCommande(session, cimpid, params);
			String fileName = "bondecommande_" + commande.exercice().exeExercice() + "-" + new DecimalFormat("0000000").format(commande.commNumero()) + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer le bon de commande " + fileName);
			}
			WOActionResults res = PrintFactory.afficherPdf(data, fileName);
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			session.setAlertMessage(e.getMessage() + " " + e.toString() + e.getCause().toString());
			return null;
		}
	}

	public NSArray<EOAdresse> getLesAdressesDuFournisseur() {
		if (lesAdressesDuFournisseur == null) {
			if (commande != null) {
				lesAdressesDuFournisseur = FinderAdresse.getAdressesPourFournisseur(commande.editingContext(), commande.fournisseur());
			}
		}
		return lesAdressesDuFournisseur;
	}

	//	public void setLesAdressesDuFournisseur(NSArray lesAdressesDuFournisseur) {
	//		this.lesAdressesDuFournisseur = lesAdressesDuFournisseur;
	//	}

	public EOAdresse getUneAdresseFournisseurSelectionnee() {
		if (uneAdresseFournisseurSelectionnee == null) {
			if (lesAdressesDuFournisseur != null && lesAdressesDuFournisseur.count() > 0) {
				uneAdresseFournisseurSelectionnee = (EOAdresse) lesAdressesDuFournisseur.objectAtIndex(0);
			}
		}
		return uneAdresseFournisseurSelectionnee;
	}

	public void setUneAdresseFournisseurSelectionnee(EOAdresse uneAdresseFournisseurSelectionnee) {
		this.uneAdresseFournisseurSelectionnee = uneAdresseFournisseurSelectionnee;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public String getStrRecherche() {
		return strRecherche;
	}

	public void setStrRecherche(String strRecherche) {
		this.strRecherche = strRecherche;
	}

	public NSArray<EOStructure> getLesServices() {
		if (commande != null) {
			lesServices = FinderStructure.getStructuresPourCommande(commande.editingContext(), commande);
		}
		return lesServices;
	}

	//	public void setLesServices(NSArray<EOStructure> lesServices) {
	//		this.lesServices = lesServices;
	//	}

	public EOStructure getUnService() {
		return unService;
	}

	public void setUnService(EOStructure unService) {
		this.unService = unService;
	}

	public EOStructure getUnServiceSelectionne() {
		return unServiceSelectionne;
	}

	public void setUnServiceSelectionne(EOStructure unServiceSelectionne) {
		this.unServiceSelectionne = unServiceSelectionne;
		if (unServiceSelectionne != null) {
			EOEditingContext edc = unServiceSelectionne.editingContext();
			lesAdressesDuService = FinderAdresse.getAdressesPourStructure(edc, unServiceSelectionne);
			lesTelephonesDuService = FinderTelephoneFax.getTelephonesPourStructure(edc, unServiceSelectionne);
			lesFaxsDuService = FinderTelephoneFax.getFaxPourStructure(edc, unServiceSelectionne);
			if (lesAdressesDuService != null && lesAdressesDuService.count() == 1) {
				setUneAdresseServiceSelectionnee((EOAdresse) lesAdressesDuService.lastObject());
			}
			if (lesTelephonesDuService != null && lesTelephonesDuService.count() == 1) {
				setUnTelephoneServiceSelectionne((String) lesTelephonesDuService.lastObject());
			}
			if (lesFaxsDuService != null && lesFaxsDuService.count() == 1) {
				setUnFaxServiceSelectionne((String) lesFaxsDuService.lastObject());
			}
			//            //permet d'avoir par defaut l'adresse de livraison correspondant au service !!!
			//            if(strRechercheStructureLivraison==null || "".equals(strRechercheStructureLivraison))
			//            {
			//                strRechercheStructureLivraison = this.unServiceSelectionne.libelle();
			//                getLesStructuresDeLivraison();
			//            }
			//setLesStructuresDeLivraison(new NSArray(this.unServiceSelectionne));
			setUneStructureLivraisonSelectionne(this.unServiceSelectionne);
		}
		else {
			lesAdressesDuService = null;
			uneAdresseServiceSelectionnee = null;
			lesTelephonesDuService = null;
			unTelephoneServiceSelectionne = null;
			lesFaxsDuService = null;
			unFaxServiceSelectionne = null;
			uneStructureLivraisonSelectionne = null;
		}

	}

	public boolean isAdressesServiceDisabled() {
		boolean isAdressesServiceDisabled = true;
		if (getUnServiceSelectionne() != null) {
			isAdressesServiceDisabled = false;
		}
		return isAdressesServiceDisabled;
	}

	public boolean isAdressesLivraisonDisabled() {
		return lesAdressesDeLivraison == null || lesAdressesDeLivraison.count() == 0 || getUneStructureLivraisonSelectionne() == null;
	}

	public boolean isTelLivraisonDisabled() {
		return lesTelephonesDeLaStructureDeLivraison == null || lesTelephonesDeLaStructureDeLivraison.count() == 0 || getUneStructureLivraisonSelectionne() == null;
	}

	public boolean isFaxLivraisonDisabled() {
		return lesFaxsDeLaStructureDeLivraison == null || lesFaxsDeLaStructureDeLivraison.count() == 0 || getUneStructureLivraisonSelectionne() == null;
	}

	public NSArray<EOAdresse> getLesAdressesDuService() {
		return lesAdressesDuService;
	}

	//	public void setLesAdressesDuService(NSArray lesAdressesDuService) {
	//		this.lesAdressesDuService = lesAdressesDuService;
	//	}

	public EOAdresse getUneAdresseService() {
		return uneAdresseService;
	}

	public void setUneAdresseService(EOAdresse uneAdresseService) {
		this.uneAdresseService = uneAdresseService;
	}

	public EOAdresse getUneAdresseServiceSelectionnee() {
		return uneAdresseServiceSelectionnee;
	}

	public void setUneAdresseServiceSelectionnee(EOAdresse uneAdresseServiceSelectionnee) {
		if (uneAdresseServiceSelectionnee != null) {
			this.uneAdresseServiceSelectionnee = uneAdresseServiceSelectionnee;
			setUneAdresseLivraisonSelectionnee(this.uneAdresseServiceSelectionnee);
		}
	}

	public NSArray<String> getLesTelephonesDuService() {
		return lesTelephonesDuService;
	}

	//	public void setLesTelephonesDuService(NSArray lesTelephonesDuService) {
	//		this.lesTelephonesDuService = lesTelephonesDuService;
	//	}

	public EOAdresse getUneAdresseFournisseur() {
		return uneAdresseFournisseur;
	}

	public void setUneAdresseFournisseur(EOAdresse uneAdresseFournisseur) {
		this.uneAdresseFournisseur = uneAdresseFournisseur;
	}

	public String getUnTelephoneService() {
		return unTelephoneService;
	}

	public void setUnTelephoneService(String unTelephoneService) {
		this.unTelephoneService = unTelephoneService;
	}

	public String getUnTelephoneServiceSelectionne() {
		return unTelephoneServiceSelectionne;
	}

	public void setUnTelephoneServiceSelectionne(String unTelephoneServiceSelectionne) {
		if (unTelephoneServiceSelectionne != null) {
			this.unTelephoneServiceSelectionne = unTelephoneServiceSelectionne;
			setUnTelephoneStructureDeLivraisonSelectionne(this.unTelephoneServiceSelectionne);
		}
	}

	public NSArray<String> getLesFaxsDuService() {
		return lesFaxsDuService;
	}

	//	public void setLesFaxsDuService(NSArray lesFaxsDuService) {
	//		this.lesFaxsDuService = lesFaxsDuService;
	//	}

	public String getUnFaxService() {
		return unFaxService;
	}

	public void setUnFaxService(String unFaxService) {
		this.unFaxService = unFaxService;
	}

	public String getUnFaxServiceSelectionne() {
		return unFaxServiceSelectionne;
	}

	public void setUnFaxServiceSelectionne(String unFaxServiceSelectionne) {
		if (unFaxServiceSelectionne != null) {
			this.unFaxServiceSelectionne = unFaxServiceSelectionne;
			setUnFaxStructureLivraisonSelectionne(this.unFaxServiceSelectionne);
		}
	}

	//Ajout des adresses de livraison
	public NSArray<EOAdresse> getLesAdressesDeLivraison() {
		if (lesAdressesDeLivraison == null) {
			if (uneStructureLivraisonSelectionne != null) {
				lesAdressesDeLivraison = FinderAdresse.getAdressesPourStructure(uneStructureLivraisonSelectionne.editingContext(), uneStructureLivraisonSelectionne);
			}
			else
				lesAdressesDeLivraison = NSArray.emptyArray();
		}
		return lesAdressesDeLivraison;
	}

	//	public void setLesAdressesDeLivraison(NSArray lesAdressesDeLivraison) {
	//		this.lesAdressesDeLivraison = lesAdressesDeLivraison;
	//	}

	public EOAdresse getUneAdresseLivraison() {
		return uneAdresseLivraison;
	}

	public void setUneAdresseLivraison(EOAdresse uneAdresseLivraison) {
		this.uneAdresseLivraison = uneAdresseLivraison;
	}

	public EOAdresse getUneAdresseLivraisonSelectionnee() {
		return uneAdresseLivraisonSelectionnee;
	}

	public void setUneAdresseLivraisonSelectionnee(EOAdresse uneAdresseLivraisonSelectionnee) {
		if (uneAdresseLivraisonSelectionnee != null)
			this.uneAdresseLivraisonSelectionnee = uneAdresseLivraisonSelectionnee;
	}

	public NSArray<String> getLesFaxsDeLaStructureDeLivraison() {
		return lesFaxsDeLaStructureDeLivraison;
	}

	//	public void setLesFaxsDeLaStructureDeLivraison(NSArray lesFaxsDeLaStructureDeLivraison) {
	//		this.lesFaxsDeLaStructureDeLivraison = lesFaxsDeLaStructureDeLivraison;
	//	}

	public NSArray<String> getLesTelephonesDeLaStructureDeLivraison() {
		return lesTelephonesDeLaStructureDeLivraison;
	}

	//	public void setLesTelephonesDeLaStructureDeLivraison(NSArray lesTelephonesDeLaStructureDeLivraison) {
	//		this.lesTelephonesDeLaStructureDeLivraison = lesTelephonesDeLaStructureDeLivraison;
	//	}

	public NSArray<EOStructure> getLesStructuresDeLivraison() {
		if (strRechercheStructureLivraison == null || "".equals(strRechercheStructureLivraison) || strRechercheStructureLivraison.length() <= 2) {
			strRechercheStructureLivraison = null;
			return NSArray.emptyArray();
		}
		if (lesStructuresDeLivraison != null)
			return lesStructuresDeLivraison;
		return NSArray.emptyArray();
	}

	//	public void setLesStructuresDeLivraison(NSArray lesStructuresDeLivraison) {
	//		this.lesStructuresDeLivraison = lesStructuresDeLivraison;
	//	}

	public EOStructure getUneStructureLivraison() {
		return uneStructureLivraison;
	}

	public void setUneStructureLivraison(EOStructure uneStructureLivraison) {
		this.uneStructureLivraison = uneStructureLivraison;
	}

	public EOStructure getUneStructureLivraisonSelectionne() {
		return uneStructureLivraisonSelectionne;
	}

	public void setUneStructureLivraisonSelectionne(EOStructure uneStructureLivraisonSelectionne) {
		this.uneStructureLivraisonSelectionne = uneStructureLivraisonSelectionne;
		if (uneStructureLivraisonSelectionne != null) {
			this.lesAdressesDeLivraison = FinderAdresse.getAdressesPourStructure(uneStructureLivraisonSelectionne.editingContext(), this.uneStructureLivraisonSelectionne);
			this.lesTelephonesDeLaStructureDeLivraison = FinderTelephoneFax.getTelephonesPourStructure(uneStructureLivraisonSelectionne.editingContext(), this.uneStructureLivraisonSelectionne);
			this.lesFaxsDeLaStructureDeLivraison = FinderTelephoneFax.getFaxPourStructure(uneStructureLivraisonSelectionne.editingContext(), this.uneStructureLivraisonSelectionne);
			if (this.lesAdressesDeLivraison != null && this.lesAdressesDeLivraison.count() == 1) {
				uneAdresseLivraisonSelectionnee = (EOAdresse) this.lesAdressesDeLivraison.lastObject();
			}
			else
				uneAdresseLivraisonSelectionnee = null;
			if (this.lesTelephonesDeLaStructureDeLivraison != null && this.lesTelephonesDeLaStructureDeLivraison.count() == 1) {
				unTelephoneStructureDeLivraisonSelectionne = (String) this.lesTelephonesDeLaStructureDeLivraison.lastObject();
			}
			else
				unTelephoneStructureDeLivraisonSelectionne = null;
			if (this.lesFaxsDeLaStructureDeLivraison != null && this.lesFaxsDeLaStructureDeLivraison.count() == 1) {
				unFaxStructureLivraisonSelectionne = (String) this.lesFaxsDeLaStructureDeLivraison.lastObject();
			}
			else
				unFaxStructureLivraisonSelectionne = null;
		}
		else {
			lesAdressesDeLivraison = null;
			uneAdresseLivraisonSelectionnee = null;
			lesTelephonesDeLaStructureDeLivraison = null;
			unTelephoneStructureDeLivraisonSelectionne = null;
			lesFaxsDeLaStructureDeLivraison = null;
			unFaxStructureLivraisonSelectionne = null;
		}
	}

	public String getUnFaxStructureDeLivraison() {
		return unFaxStructureDeLivraison;
	}

	public void setUnFaxStructureDeLivraison(String unFaxStructureDeLivraison) {
		this.unFaxStructureDeLivraison = unFaxStructureDeLivraison;
	}

	public String getUnFaxStructureLivraisonSelectionne() {
		return unFaxStructureLivraisonSelectionne;
	}

	public void setUnFaxStructureLivraisonSelectionne(String unFaxStructureLivraisonSelectionne) {
		if (unFaxStructureLivraisonSelectionne != null)
			this.unFaxStructureLivraisonSelectionne = unFaxStructureLivraisonSelectionne;
	}

	public String getUnTelephoneStructureDeLivraison() {
		return unTelephoneStructureDeLivraison;
	}

	public void setUnTelephoneStructureDeLivraison(String unTelephoneStructureDeLivraison) {
		this.unTelephoneStructureDeLivraison = unTelephoneStructureDeLivraison;
	}

	public String getUnTelephoneStructureDeLivraisonSelectionne() {
		return unTelephoneStructureDeLivraisonSelectionne;
	}

	public void setUnTelephoneStructureDeLivraisonSelectionne(String unTelephoneStructureDeLivraisonSelectionne) {
		//ce test permet de rafraichir le numéro de telephone, après avoir changé de service !!!
		if (unTelephoneStructureDeLivraisonSelectionne != null)
			this.unTelephoneStructureDeLivraisonSelectionne = unTelephoneStructureDeLivraisonSelectionne;
	}

	public boolean isNePasImprimerAdrLivraisonChecked() {
		return isNePasImprimerAdrLivraisonChecked;
	}

	public void setIsNePasImprimerAdrLivraisonChecked(boolean isNePasImprimerAdrLivraisonChecked) {
		this.isNePasImprimerAdrLivraisonChecked = isNePasImprimerAdrLivraisonChecked;
	}

	public boolean isImprimerDepenseAuComptantPossible() {
		boolean isImprimerDepenseAuComptantPossible = false;
		if (commande != null)
			isImprimerDepenseAuComptantPossible = commande.isDepenseAuComptantPossible();
		return isImprimerDepenseAuComptantPossible;

	}

	public EOCommande getCommande() {
		return commande;
	}

	public void setCommande(EOCommande commande) {
		this.commande = commande;
	}

	public boolean isNePasImprimerLeMontantChecked() {
		return isNePasImprimerLeMontantChecked;
	}

	public void setIsNePasImprimerLeMontantChecked(boolean isNePasImprimerLeMontantChecked) {
		this.isNePasImprimerLeMontantChecked = isNePasImprimerLeMontantChecked;
	}

	public boolean isAfficherBoutonFermer() {
		boolean isAfficherBoutonFermer = false;

		if (getOnloadJS() != null && getOnloadJS().equals("") == false) {
			isAfficherBoutonFermer = true;
		}
		return isAfficherBoutonFermer;
	}
}