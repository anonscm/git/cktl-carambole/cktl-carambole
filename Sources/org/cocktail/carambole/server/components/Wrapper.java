/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.VersionMe;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

public class Wrapper extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	public String onload = "";
	private NSArray<String> lesItemsMenu = new NSArray<String>();
	private String userName = null;

	public Wrapper(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public boolean isAfficherMenu() {
		boolean isAfficherMenu = canGetValueForBinding("itemsMenu");

		return isAfficherMenu;
	}

	public WOComponent sortir() {
		if (nestedEdc() != null) {
			session.setNestedEdc(null);
		}
		try {
			session.reset();
		} catch (Exception e) {
		}

		return session.logout();
	}

	public String getOnload() {
		onload = (String) valueForBinding("onload");
		return onload;
	}

	public void setOnload(String onload) {
		this.onload = onload;
	}

	public String copyright() {
		return ((Application) application()).copyright();
		//		return VersionMe.copyright();
	}

	public String bdServerId() {
		return Application.bdServerId;
	}

	public String version() {
		return VersionMe.htmlAppliVersion();
	}

	public NSArray<String> getLesItemsMenu() {
		return lesItemsMenu;
	}

	public void setLesItemsMenu(NSArray<String> itemsMenu) {
		lesItemsMenu = itemsMenu;
	}

	public void fermerMessage() {
		session.setAlertMessage(null);
		setOnload("");
		return;
	}

	public boolean isAfficherAlerte() {
		boolean isAfficherAlerte = false;
		String alertMsg = session.getAlertMessage();
		if (alertMsg != null && alertMsg.equals("") == false) {
			isAfficherAlerte = true;
		}
		return isAfficherAlerte;
	}

	public String userName() {
		if (userName == null) {
			CktlUserInfo userInfo = session.connectedUserInfo();
			if (userInfo != null) {
				userName = userInfo.nomEtPrenom();
			}
		}
		return userName;
	}

	public String pageTitle() {
		if (hasBinding("pageTitle")) {
			return "- " + (String) valueForBinding("pageTitle");
		}
		else {
			return null;
		}
	}

}