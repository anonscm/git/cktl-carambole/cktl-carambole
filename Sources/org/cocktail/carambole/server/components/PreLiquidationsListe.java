/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.controleurs.PreLiquidationsListeCtrl;
import org.cocktail.carambole.server.controleurs._IPreLiquidationsListeCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Affiche une liste de PréLiquidation pour sélection par utilisateur.
 * 
 * @binding ctrl _IPreLiquidationsListeCtrl Controleur facultatif
 * @binding BINDING_onSelectPreLiquidationCallBack String Nom de la methode a appeler lors de la selection d'une preLiquidation).
 * @author rprin
 */
public class PreLiquidationsListe extends MyAjaxComponent {
	private static final long serialVersionUID = 1L;

	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_dg = "dg";
	public static final String BINDING_onSelectPreLiquidationCallBack = "onSelectPreLiquidationCallBack";
	public static final String BINDING_selectedDico = "selectedDico";

	public NSDictionary<String, Object> unDepDico = null;

	private _IPreLiquidationsListeCtrl ctrl;

	public PreLiquidationsListe(WOContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public NSArray<NSDictionary<String, Object>> lesPDepenseBudgets() {
		return dg().displayedObjects();
	}

	public _IPreLiquidationsListeCtrl ctrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (_IPreLiquidationsListeCtrl) valueForBinding(BINDING_ctrl);
			}
			else {
				ctrl = new PreLiquidationsListeCtrl();
			}
		}
		return ctrl;

	}

	public WOComponent filtrer() {
		return null;
	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(BINDING_dg);
	}

	public WOActionResults onSelectPreLiquidation() {
		setValueForBinding(unDepDico, BINDING_selectedDico);
		if (hasBinding(BINDING_onSelectPreLiquidationCallBack)) {
			return performParentAction((String) valueForBinding(BINDING_onSelectPreLiquidationCallBack));
		}
		return null;
	}

	public Boolean isDppServiceFait() {
		return Boolean.valueOf(unDepDico != null && unDepDico.valueForKey("NOMPRENOMSF") != null && (!NSKeyValueCoding.NullValue.equals(unDepDico.valueForKey("NOMPRENOMSF"))));
	}
}