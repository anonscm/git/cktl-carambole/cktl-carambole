/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.B2bCxmlDirectAction;
import org.cocktail.carambole.server.NavigationCtrl;
import org.cocktail.carambole.server.Session;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLBuyerCookie;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLTransaction;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.appserver.ERXRedirect;

public class B2bCxmlPunchOutGUI extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;
	public final static Logger logger = Logger.getLogger(B2bCxmlPunchOutGUI.class);
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";
	public static final String BINDING_editingContext = "editingContext";
	private static final String BINDING_ERREUR_MSG = "erreurMsg";
	private static final String BINDING_FOURNIS_B2B_CXML_PARAM = "fournisB2bCxmlParam";
	private static final String BINDING_CODE_EXER = "codeExer";
	private static final String BINDING_TYPE_ACHAT = "typeAchat";
	private static final String BINDING_ATTRIBUTION = "attribution";
	private static final String BINDING_ONCLICK = "onClick";
	private static final String BINDING_EXE_ORDRE = "exeOrdre";
	private static final String BINDING_COMM_ID = "commId";

	private String punchOutBrowserFormPostURL = null;

	public B2bCxmlPunchOutGUI(WOContext context) {
		super(context);
		String url = B2bCxmlDirectAction.punchOutOrderFeedBackUrl(context());
		if (logger.isDebugEnabled()) {
			logger.debug("punchOutBrowserFormPostURL=" + url);
		}
		punchOutBrowserFormPostURL = url;
	}

	public WOActionResults onPunchOut() {
		WOActionResults res = null;
		setErreurMsg(null);
		CXMLBuyerCookie buyerCookie = new CXMLBuyerCookie(persId(), fbcpId(), typaId(), ceOrdre(), exeOrdre(), attOrdre(), commId(), sessionId());

		CXMLParameters cxmlParameters = fournisB2bCxmlParam().toCxmlParameters();
		try {
			CXMLTransaction transaction = new CXMLTransaction();
			ApplicationUser appUser = new ApplicationUser(edc(), persId());
			String startPage = transaction.creerTransactionPunchOutSetupRequestCreate(edc(), appUser, cxmlParameters, buyerCookie, punchOutBrowserFormPostURL);
			//FIXME sauver la session (pb timeout) ?

			res = new ERXRedirect(context());
			((ERXRedirect) res).setUrl(startPage);

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			String errMsg = e.getMessage();
			errMsg = MyStringCtrl.firstLine(errMsg);
			((Session) session()).setAlertMessage(errMsg);
			return NavigationCtrl.goToSavedBonDeCommande((Session) session(), ((Session) session()).getLaCommandeEnCours(), null, "Articles");
		}

	}

	private void setErreurMsg(String message) {
		setValueForBinding(BINDING_ERREUR_MSG, message);

	}

	public String getPunchOutBrowserFormPostURL() {
		return punchOutBrowserFormPostURL;
	}

	public Integer persId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	public EOCodeExer codeExer() {
		return (EOCodeExer) valueForBinding(BINDING_CODE_EXER);
	}

	public EOAttribution attribution() {
		return (EOAttribution) valueForBinding(BINDING_ATTRIBUTION);
	}

	public EOTypeAchat typeAchat() {
		return (EOTypeAchat) valueForBinding(BINDING_TYPE_ACHAT);
	}

	public String sessionId() {
		return session().sessionID();
	}

	public Integer exeOrdre() {
		return (Integer) valueForBinding(BINDING_EXE_ORDRE);
	}

	public Long commId() {
		return (Long) valueForBinding(BINDING_COMM_ID);
	}

	public Integer fbcpId() {
		if (fournisB2bCxmlParam() != null) {
			return (Integer) EOUtilities.primaryKeyForObject(fournisB2bCxmlParam().editingContext(), fournisB2bCxmlParam()).allValues().lastObject();
		}
		return null;
	}

	public Integer ceOrdre() {
		if (codeExer() != null) {
			return (Integer) EOUtilities.primaryKeyForObject(codeExer().editingContext(), codeExer()).allValues().lastObject();
		}
		return null;
	}

	public Integer typaId() {
		if (typeAchat() != null) {
			return (Integer) EOUtilities.primaryKeyForObject(typeAchat().editingContext(), typeAchat()).allValues().lastObject();
		}
		return null;
	}

	public Integer attOrdre() {
		if (attribution() != null) {
			return (Integer) EOUtilities.primaryKeyForObject(attribution().editingContext(), attribution()).valueForKey(EOAttribution.ATT_ORDRE_KEY);
		}
		return null;
	}

	public EOFournisB2bCxmlParam fournisB2bCxmlParam() {
		return (EOFournisB2bCxmlParam) valueForBinding(BINDING_FOURNIS_B2B_CXML_PARAM);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public EOEditingContext edc() {
		if (hasBinding(BINDING_editingContext)) {
			return (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return mySession().defaultEditingContext();
	}

	public Boolean isPunchoutEnabled() {
		return Boolean.valueOf(fournisB2bCxmlParam() != null && codeExer() != null && persId() != null && fournisB2bCxmlParam().isPunchOutEnabled());
	}

	public Boolean isParamReadyForPunchOut() {
		return Boolean.valueOf(fournisB2bCxmlParam() != null && fournisB2bCxmlParam().isPunchOutEnabled());
	}

	public String onClick() {
		return (String) valueForBinding(BINDING_ONCLICK);
	}

	public String linkPunchOutOnClick() {
		String res = openWaitDialogFunctionName() + "();return true;";
		if (onClick() != null) {
			res = onClick() + res;
		}
		return res;
	}

	public String linkPunchOutText() {
		return "Accéder au site marchand <br/>" + fournisB2bCxmlParam().libelle() + "";
	}

	public String openWaitDialogFunctionName() {
		return "openWaitDialogB2bConnexion_" + getComponentId();
	}

	public String waitingPanelMsg() {
		return "Connexion au site marchand en cours...<br/><br/>" + fournisB2bCxmlParam().fbcpName();
	}

}