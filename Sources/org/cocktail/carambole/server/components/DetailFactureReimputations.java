/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.util.UUID;

import org.cocktail.carambole.server.MyComponent;
import org.cocktail.carambole.server.reports.PrintFactory;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOReimputation;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationAction;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationBudget;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationConvention;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAction;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewBudget;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewConvention;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationNewPlanco;
import org.cocktail.fwkcktldepense.server.metier.EOReimputationPlanco;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

/**
 * Detail des reimputation pour une facture (depenseBudget).
 * 
 * @author rprin
 */
public class DetailFactureReimputations extends MyComponent {
	private static final long serialVersionUID = 1L;

	public static final String BINDING_depenseBudget = "depenseBudget";

	public EOReimputation uneReimputation;

	public EOReimputationBudget uneReimputationBudget;
	public EOReimputationNewBudget uneReimputationNewBudget;

	public EOReimputationAction uneReimputationAction;
	public EOReimputationNewAction uneReimputationNewAction;

	public EOReimputationPlanco uneReimputationPlanco;
	public EOReimputationNewPlanco uneReimputationNewPlanco;

	public EOReimputationConvention uneReimputationConvention;
	public EOReimputationNewConvention uneReimputationNewConvention;

	public EOReimputationAnalytique uneReimputationAnalytique;
	public EOReimputationNewAnalytique uneReimputationNewAnalytique;

	public EOReimputationHorsMarche uneReimputationHorsMarche;
	public EOReimputationNewHorsMarche uneReimputationNewHorsMarche;

	private String expansionId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_") + "_expand";

	public DetailFactureReimputations(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		super.appendToResponse(arg0, arg1);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public EODepenseBudget depenseBudget() {
		return (EODepenseBudget) valueForBinding(BINDING_depenseBudget);
	}

	public NSArray<EOReimputation> getReimputations() {
		if (depenseBudget() != null) {
			return depenseBudget().reimputations(null, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputation.SORT_REIM_DATE_DESC
			}), true);
		}
		return NSArray.emptyArray();
	}

	public Boolean hasReimputations() {
		return Boolean.valueOf(depenseBudget() != null && depenseBudget().reimputations().count() > 0);
	}

	public NSArray<EOReimputationBudget> sourcesAvant() {
		if (uneReimputation != null) {
			NSArray<EOReimputationBudget> res = uneReimputation.reimputationBudgets(null, null, false);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationBudget.SORT_LIBELLE_ASC
			}));
		}
		return null;
	}

	public NSArray<EOReimputationNewBudget> sourcesApres() {
		if (uneReimputation != null) {
			NSArray<EOReimputationNewBudget> res = uneReimputation.reimputationNewBudgets(null, null, true);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationNewBudget.SORT_LIBELLE_ASC
			}));
		}
		return null;
	}

	public String sourceAvant() {
		if (uneReimputationBudget == null) {
			return null;
		}
		return uneReimputationBudget.libelle();
	}

	public String sourceApres() {
		if (uneReimputationNewBudget == null) {
			return null;
		}
		return uneReimputationNewBudget.libelle();
	}

	public NSArray<EOReimputationAction> actionsAvant() {
		if (uneReimputation != null) {
			NSArray<EOReimputationAction> res = uneReimputation.reimputationActions(null, null, false);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationAction.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOReimputationNewAction> actionsApres() {
		if (uneReimputation != null) {
			NSArray<EOReimputationNewAction> res = uneReimputation.reimputationNewActions(null, null, true);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationNewAction.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public String actionAvant() {
		if (uneReimputationAction == null) {
			return null;
		}
		return uneReimputationAction.libelle();
	}

	public String actionApres() {
		if (uneReimputationNewAction == null) {
			return null;
		}
		return uneReimputationNewAction.libelle();
	}

	public NSArray<EOReimputationPlanco> plancosAvant() {
		if (uneReimputation != null) {
			NSArray<EOReimputationPlanco> res = uneReimputation.reimputationPlancos(null, null, false);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationPlanco.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOReimputationNewPlanco> plancosApres() {
		if (uneReimputation != null) {
			NSArray<EOReimputationNewPlanco> res = uneReimputation.reimputationNewPlancos(null, null, true);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationNewPlanco.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public String plancoAvant() {
		if (uneReimputationPlanco == null) {
			return null;
		}
		return uneReimputationPlanco.libelle();
	}

	public String plancoApres() {
		if (uneReimputationNewPlanco == null) {
			return null;
		}
		return uneReimputationNewPlanco.libelle();
	}

	public NSArray<EOReimputationConvention> conventionsAvant() {
		if (uneReimputation != null) {
			NSArray<EOReimputationConvention> res = uneReimputation.reimputationConventions(null, null, false);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationConvention.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOReimputationNewConvention> conventionsApres() {
		if (uneReimputation != null) {
			if (uneReimputation != null) {
				NSArray<EOReimputationNewConvention> res = uneReimputation.reimputationNewConventions(null, null, true);
				return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
						EOReimputationNewConvention.SORT_LIBELLE_ASC
				}));
			}
		}
		return NSArray.emptyArray();
	}

	public String conventionAvant() {
		if (uneReimputationConvention == null) {
			return null;
		}
		return uneReimputationConvention.libelle();
	}

	public String conventionApres() {
		if (uneReimputationNewConvention == null) {
			return null;
		}
		return uneReimputationNewConvention.libelle();
	}

	public NSArray<EOReimputationAnalytique> analytiquesAvant() {
		if (uneReimputation != null) {
			NSArray<EOReimputationAnalytique> res = uneReimputation.reimputationAnalytiques(null, null, false);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationAnalytique.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOReimputationNewAnalytique> analytiquesApres() {
		if (uneReimputation != null) {
			NSArray<EOReimputationNewAnalytique> res = uneReimputation.reimputationNewAnalytiques(null, null, true);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationNewAnalytique.SORT_LIBELLE_ASC
			}));
		}
		return NSArray.emptyArray();
	}

	public String analytiqueAvant() {
		if (uneReimputationAnalytique == null) {
			return null;
		}
		return uneReimputationAnalytique.libelle();
	}

	public String analytiqueApres() {
		if (uneReimputationNewAnalytique == null) {
			return null;
		}
		return uneReimputationNewAnalytique.libelle();
	}

	public Boolean hasConventionsAvant() {
		return Boolean.valueOf(conventionsAvant().count() > 0);
	}

	public Boolean hasConventionsApres() {
		return Boolean.valueOf(conventionsApres().count() > 0);
	}

	public Boolean hasCodeAnalytiquesAvant() {
		return Boolean.valueOf(analytiquesAvant().count() > 0);
	}

	public Boolean hasCodeAnalytiquesApres() {
		return Boolean.valueOf(analytiquesApres().count() > 0);
	}

	public Boolean sourceChanged() {
		boolean res = false;
		NSArray<EOReimputationBudget> avant = sourcesAvant();
		NSArray<EOReimputationNewBudget> apres = sourcesApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewBudget rapres = apres.objectAtIndex(i);
				EOReimputationBudget ravant = avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle())) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public Boolean analytiqueChanged() {
		boolean res = false;
		NSArray<EOReimputationAnalytique> avant = analytiquesAvant();
		NSArray<EOReimputationNewAnalytique> apres = analytiquesApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewAnalytique rapres = apres.objectAtIndex(i);
				EOReimputationAnalytique ravant = avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle()) || rapres.reanHtSaisie().doubleValue() != ravant.reanHtSaisie().doubleValue() || rapres.reanMontantBudgetaire().doubleValue() != ravant.reanMontantBudgetaire().doubleValue()
						|| rapres.reanTtcSaisie().doubleValue() != ravant.reanTtcSaisie().doubleValue()) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public Boolean conventionChanged() {
		boolean res = false;
		NSArray<EOReimputationConvention> avant = conventionsAvant();
		NSArray<EOReimputationNewConvention> apres = conventionsApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewConvention rapres = apres.objectAtIndex(i);
				EOReimputationConvention ravant = avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle()) || rapres.recoHtSaisie().doubleValue() != ravant.recoHtSaisie().doubleValue() || rapres.recoMontantBudgetaire().doubleValue() != ravant.recoMontantBudgetaire().doubleValue()
						|| rapres.recoTtcSaisie().doubleValue() != ravant.recoTtcSaisie().doubleValue()) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public Boolean plancoChanged() {
		boolean res = false;
		NSArray<EOReimputationPlanco> avant = plancosAvant();
		NSArray<EOReimputationNewPlanco> apres = plancosApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewPlanco rapres = (EOReimputationNewPlanco) apres.objectAtIndex(i);
				EOReimputationPlanco ravant = (EOReimputationPlanco) avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle()) || rapres.repcHtSaisie().doubleValue() != ravant.repcHtSaisie().doubleValue() || rapres.repcMontantBudgetaire().doubleValue() != ravant.repcMontantBudgetaire().doubleValue()
						|| rapres.repcTtcSaisie().doubleValue() != ravant.repcTtcSaisie().doubleValue()) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public Boolean actionChanged() {
		boolean res = false;
		NSArray<EOReimputationAction> avant = actionsAvant();
		NSArray<EOReimputationNewAction> apres = actionsApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewAction rapres = (EOReimputationNewAction) apres.objectAtIndex(i);
				EOReimputationAction ravant = (EOReimputationAction) avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle()) || rapres.reacHtSaisie().doubleValue() != ravant.reacHtSaisie().doubleValue() || rapres.reacMontantBudgetaire().doubleValue() != ravant.reacMontantBudgetaire().doubleValue()
						|| rapres.reacTtcSaisie().doubleValue() != ravant.reacTtcSaisie().doubleValue()) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public NSArray<EOReimputationHorsMarche> horsMarchesAvant() {
		if (uneReimputation != null) {
			return uneReimputation.reimputationHorsMarches(null, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationHorsMarche.SORT_LIBELLE_ASC
			}), false);
		}
		return NSArray.emptyArray();
	}

	public NSArray<EOReimputationNewHorsMarche> horsMarchesApres() {
		if (uneReimputation != null) {
			return uneReimputation.reimputationNewHorsMarches(null, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOReimputationNewHorsMarche.SORT_LIBELLE_ASC
			}), false);
		}
		return NSArray.emptyArray();
	}

	public String horsMarcheAvant() {
		if (uneReimputationHorsMarche == null) {
			return null;
		}
		return uneReimputationHorsMarche.libelle();
	}

	public String horsMarcheApres() {
		if (uneReimputationNewHorsMarche == null) {
			return null;
		}
		return uneReimputationNewHorsMarche.libelle();
	}

	public Boolean horsMarcheChanged() {
		boolean res = false;
		NSArray<EOReimputationHorsMarche> avant = horsMarchesAvant();
		NSArray<EOReimputationNewHorsMarche> apres = horsMarchesApres();
		res = (apres.count() != avant.count());
		if (!res) {
			for (int i = 0; i < apres.count(); i++) {
				EOReimputationNewHorsMarche rapres = (EOReimputationNewHorsMarche) apres.objectAtIndex(i);
				EOReimputationHorsMarche ravant = (EOReimputationHorsMarche) avant.objectAtIndex(i);
				if (!rapres.libelle().equals(ravant.libelle()) || rapres.rehmHtSaisie().doubleValue() != ravant.rehmHtSaisie().doubleValue() || rapres.rehmMontantBudgetaire().doubleValue() != ravant.rehmMontantBudgetaire().doubleValue()
						|| rapres.rehmTtcSaisie().doubleValue() != ravant.rehmTtcSaisie().doubleValue()) {
					res = true;
				}
			}
		}
		return Boolean.valueOf(res);
	}

	public String expansionText() {
		return "D\u00E9tail";
	}

	public String expansionId() {
		return expansionId + uneReimputation.reimNumero();
	}

	public WOActionResults imprimer() {
		try {

			Integer reimId = (Integer) EOUtilities.primaryKeyForObject(uneReimputation.editingContext(), uneReimputation).allValues().lastObject();

			//	Integer reimId = (Integer) EOUtilities.primaryKeyForObject(edc(), uneReimputation).valueForKey(EOReimputation.REIM_ID_KEY);

			NSData data = PrintFactory.printReimputation(session, reimId);
			String fileName = "reimputation_" + reimId + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la reimputation " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		} catch (Throwable e) {
			e.printStackTrace();
			session.setAlertMessage(e.getMessage() + " " + e.toString());
			return null;
		}
	}
}