/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Iterator;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.controleurs.LiquidationCtrl;
import org.cocktail.carambole.server.controleurs.PreLiquidationCtrl;
import org.cocktail.carambole.server.controleurs._ILiquidationCtrl;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderModePaiement;
import org.cocktail.fwkcktldepense.server.finder.FinderProrata;
import org.cocktail.fwkcktldepense.server.finder.FinderRib;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EORibFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.service.ConfigurationExtourneService;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class Liquidation2 extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	private String onloadJS = null;
	protected boolean isModificationEnCours = false;
	private NSMutableDictionary<String, NSArray<EOTypeAction>> dicoLesActions = null;
	private NSMutableDictionary<String, NSArray<EOPlanComptable>> dicoLesPlansComptable = null;
	private NSMutableDictionary<String, NSArray<EOConvention>> dicoLesConventions = null;
	private NSMutableDictionary<String, NSArray<EOCodeAnalytique>> dicoLesCodesAnalytique = null;

	public boolean isRibDisabled = true;
	public boolean isFactureDisabled = true;

	private String filterTextModeDePaiement = null;
	private NSArray<EOModePaiement> modesDePaiement = null;
	private EOModePaiement unModeDePaiement = null;
	private EOModePaiement unModeDePaiementSelectionne;
	private NSArray<EORibFournisseur> ribs = null;
	public EORibFournisseur unRib = null;
	private EORibFournisseur unRibSelectionne = null;

	private EODepensePapier laDepensePapier = null;
	private NSTimestamp dateFacture = null;
	private NSTimestamp dateReception = null;
	private NSTimestamp dateServiceFait = null;

	private _IDepenseBudget uneDepenseBudget = null;
	private NSArray<EOTauxProrata> lesTauxDeProratas = null;
	public EOTauxProrata unTauxDeProrata = null;
	private EOTauxProrata unTauxDeProrataSelectionne = null;

	// Repartition par code de nomenclature
	private _IDepenseBudget uneDepenseBudgetForHorsMarche = null;
	private NSArray<EOCodeExer> lesCodesNomenclature = null;

	// Repartition par action
	private _IDepenseBudget uneDepenseBudgetForAction = null;
	private NSArray<EOTypeAction> lesActions = null;

	// Repartition par imputation (plan comptable)
	private _IDepenseBudget uneDepenseBudgetForImputation = null;
	private _IDepenseControlePlanComptable uneDepenseCtrlImputation = null;
	private NSArray<EOPlanComptable> lesImputations = null;

	//private int indexInventaire;
	private EOInventaire unInventaire = null;

	// Repartition par Convention
	private _IDepenseBudget uneDepenseBudgetForConvention = null;
	private _IDepenseControleConvention uneDepenseCtrlConvention = null;
	private NSArray<EOConvention> lesConventions = null;

	// Repartition par Code analytiques
	private _IDepenseBudget uneDepenseBudgetForAnalytique = null;
	private _IDepenseControleAnalytique uneDepenseCtrlAnalytique = null;
	private NSArray<EOCodeAnalytique> lesCodeAnalytiques = null;

	private _ILiquidationCtrl ctrl;
	public Boolean refreshDataEcritures;

	public Integer engIndex;

	//private EOAttribution attribution;

	public Liquidation2(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		if (onloadJS != null) {
			onloadJS = null;
		}
	}

	protected void initialisation() {
		if (isPreLiquidation()) {
			setCtrl(new PreLiquidationCtrl(laDepensePapier));
		}
		else {
			setCtrl(new LiquidationCtrl(laDepensePapier));
		}
		EOEditingContext edc = laDepensePapier.editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);
		modesDePaiement = FinderModePaiement.getModePaiements(edc, exerciceCourant(), utilisateur);

		// filtre les modes de paiement si possible
		EOTypeCredit typeCreditUnique = laDepensePapier.typesCreditsSurSectionUnique();
		if (typeCreditUnique != null) {
			modesDePaiement = filtrerModePaiement(edc, exerciceCourant(), typeCreditUnique, modesDePaiement);
		}

		unModeDePaiementSelectionne = laDepensePapier.modePaiement();
		if (unModeDePaiementSelectionne != null) {
			isFactureDisabled = false;
			initRibs();
		}

		updCodesNomenclatures();

		lesTauxDeProratas = null;
		dicoLesActions = new NSMutableDictionary<String, NSArray<EOTypeAction>>();
		dicoLesPlansComptable = new NSMutableDictionary<String, NSArray<EOPlanComptable>>();
		dicoLesConventions = new NSMutableDictionary<String, NSArray<EOConvention>>();
		dicoLesCodesAnalytique = new NSMutableDictionary<String, NSArray<EOCodeAnalytique>>();
		updDicos(edc, utilisateur);
		ctrl.resetEngagementLiquidables(edc(), utilisateur);
	}

	private NSArray<EOModePaiement> filtrerModePaiement(EOEditingContext edc, EOExercice exerciceCourant, EOTypeCredit typeCredit, NSArray<EOModePaiement> modesPaiement) {
		ConfigurationExtourneService confExtourneService = ConfigurationExtourneService.instance();
		NSMutableArray<EOModePaiement> modesPaiementFiltres = new NSMutableArray<EOModePaiement>(modesPaiement);

		// verifie les parametres par section :
		// si les deux parametres associes sont desactives alors on retire les modes de paiement Extourne.
		boolean filtrerModesPaiement = false;
		EOTypeCredit.ESection section = typeCredit.section();
		boolean extourneEnveloppeTypeCreditAutorise =
				confExtourneService.extourneEnveloppeTypeCreditAutorise(edc, exerciceCourant.exerciceSuivant(), section);
		boolean extourneFlecheeTypeCreditAutorise =
				confExtourneService.extourneFlecheeTypeCreditAutorise(edc, exerciceCourant.exerciceSuivant(), section);
		if (!extourneEnveloppeTypeCreditAutorise && !extourneFlecheeTypeCreditAutorise) {
			filtrerModesPaiement = true;
		}

		if (filtrerModesPaiement) {
			Iterator<EOModePaiement> iteModesPaiement = modesPaiementFiltres.iterator();
			while (iteModesPaiement.hasNext()) {
				EOModePaiement modePaiement = iteModesPaiement.next();
				if (EOModePaiement.MODE_DOM_EXTOURNE.equals(modePaiement.modDom())) {
					iteModesPaiement.remove();
				}
			}
		}

		return modesPaiementFiltres;
	}

	protected void initRibs() {
		if (unModeDePaiementSelectionne != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(unModeDePaiementSelectionne, "modePaiement");
			bindings.setObjectForKey(laDepensePapier.fournisseur(), "fournisseur");
			ribs = FinderRib.getRibs(edc(), bindings);

			if (laDepensePapier.ribFournisseur() != null && ribs.contains(laDepensePapier.ribFournisseur())) {
				unRibSelectionne = laDepensePapier.ribFournisseur();
			}
			else {
				laDepensePapier.setRibFournisseurRelationship(null);
			}
			if (unRibSelectionne != null) {
				isRibDisabled = false;
			}
		}
		else {
			ribs = NSArray.emptyArray();
		}
	}

	protected void updCodesNomenclatures() {
		if (laCommandeEnCours() != null) {
			lesCodesNomenclature = FinderCodeExer.getCodeExerPourCommande(getLaDepensePapier().editingContext(), laCommandeEnCours());
		}
		else {
			lesCodesNomenclature = NSArray.emptyArray();
		}
	}

	public void updDicos(EOEditingContext edc, EOUtilisateur utilisateur) {
		NSArray<? extends _IDepenseBudget> depensesBudget = getCtrl().getLesDepenseBudgets();
		Enumeration<? extends _IDepenseBudget> enumDepensesBudget = depensesBudget.objectEnumerator();
		while (enumDepensesBudget.hasMoreElements()) {
			_IDepenseBudget uneDepenseBudget = enumDepensesBudget.nextElement();
			uneDepenseBudget.resetTypeActionsPossibles();
			NSArray<EOTypeAction> actions = uneDepenseBudget.getTypeActionsPossibles(edc, utilisateur);
			dicoLesActions.setObjectForKey(actions, uneDepenseBudget.getSource().libelleCourt());
			uneDepenseBudget.resetPlanComptablesPossibles();
			NSArray<EOPlanComptable> plansComptable = uneDepenseBudget.getPlanComptablesPossibles(edc, utilisateur);
			dicoLesPlansComptable.setObjectForKey(plansComptable, uneDepenseBudget.getSource().codeOrganEtTypeCredit());
			uneDepenseBudget.resetConventionsPossibles();
			NSArray<EOConvention> conventions = uneDepenseBudget.getConventionsPossibles(edc, utilisateur);
			dicoLesConventions.setObjectForKey(conventions, uneDepenseBudget.getSource().libelleCourt());
			uneDepenseBudget.resetCodeAnalytiquesPossibles();
			NSArray<EOCodeAnalytique> codesAnalytique = uneDepenseBudget.getCodeAnalytiquesPossibles(edc, utilisateur);
			dicoLesCodesAnalytique.setObjectForKey(codesAnalytique, uneDepenseBudget.getSource().libelleCourt());
		}
	}

	protected void updDicos() {
		EOEditingContext edc = laDepensePapier.editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);
		updDicos(edc, utilisateur);
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UNE_LIQUIDATION_KEY, Menu.ANNULER_UNE_LIQUIDATION_KEY
		});

		return items;
	}

	public WOComponent selectModeDePaiement() {
		isRibDisabled = true;
		EOModePaiement mode = unModeDePaiementSelectionne;
		setUnModeDePaiementSelectionne(mode);
		if (mode != null) {
			if (mode.isRibObligatoire()) {
				isRibDisabled = false;
				isFactureDisabled = true;
				NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
				bindings.setObjectForKey(mode, "modePaiement");
				bindings.setObjectForKey(laDepensePapier.fournisseur(), "fournisseur");
				ribs = FinderRib.getRibs(laDepensePapier.editingContext(), bindings);
				if (ribs != null && ribs.count() == 1) {
					EORibFournisseur unRib = (EORibFournisseur) ribs.lastObject();
					setUnRibSelectionne(unRib);
					isFactureDisabled = false;
				}
				else if (ribs != null && ribs.count() > 1 && getAttribution() != null && getAttribution().toRibFournisseur() != null && ribs.indexOfObject(getAttribution().toRibFournisseur()) > NSArray.NotFound) {
					setUnRibSelectionne(getAttribution().toRibFournisseur());
				}
				else {
					setUnRibSelectionne(null);
				}
			}
			else {
				ribs = null;
				setUnRibSelectionne(null);
				isFactureDisabled = false;
			}
			//si c'est un mode de paiement extourne, on préremplit les dates de facture
			if (EOModePaiement.MODE_DOM_EXTOURNE.equals(mode.modDom())) {
				NSTimestamp dateFinExercice = laDepensePapierEnCours().exercice().getDateFinExercice();
				laDepensePapierEnCours().setDppDateFacture(dateFinExercice);
				laDepensePapierEnCours().setDppDateReception(dateFinExercice);
				if (!isPreLiquidation()) {
					laDepensePapierEnCours().setDppDateServiceFait(dateFinExercice);
				}
			}
		}
		else {
			ribs = null;
			unRibSelectionne = null;
			dateFacture = null;
			dateReception = null;
			dateServiceFait = null;
			isRibDisabled = true;
			isFactureDisabled = true;
		}
		return null;
	}

	public WOComponent selectRib() {
		if (unRibSelectionne != null) {
			isFactureDisabled = false;
		}
		else {
			isFactureDisabled = true;
			dateFacture = null;
			dateReception = null;
			dateServiceFait = null;
		}
		return null;
	}

	protected boolean isFactureComplete() {
		boolean isFactureComplete = false;
		if (!isFactureDisabled) {
			BigDecimal montantFactureHT = laDepensePapier.dppHtInitial();
			BigDecimal montantFactureTTC = laDepensePapier.dppTtcInitial();
			if (montantFactureHT != null && montantFactureHT.floatValue() >= 0 &&
					montantFactureTTC != null && montantFactureTTC.floatValue() > 0 &&
					montantFactureTTC.floatValue() >= montantFactureHT.floatValue()) {
				isFactureComplete = true;
			}
		}
		return isFactureComplete;
	}

	public WOActionResults submitFacture() {
		isModificationEnCours = false;
		return null;
	}

	public WOActionResults submitEngagement() {
		isModificationEnCours = false;
		return null;
	}

	public WOActionResults submitProrata() {
		isModificationEnCours = false;
		EOEditingContext edc = laDepensePapierEnCours().editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);
		updDicos(edc, utilisateur);
		return null;
	}

	public WOActionResults neFaitRien() {
		isModificationEnCours = false;
		return null;
	}

	public String getFilterTextModeDePaiement() {
		return filterTextModeDePaiement;
	}

	public void setFilterTextModeDePaiement(String filterTextModeDePaiement) {
		this.filterTextModeDePaiement = filterTextModeDePaiement;
	}

	public NSArray<EOModePaiement> getModesDePaiement() {
		return modesDePaiement;
	}

	public void setModesDePaiement(NSArray<EOModePaiement> modesDePaiement) {
		this.modesDePaiement = modesDePaiement;
	}

	public EOModePaiement getUnModeDePaiementSelectionne() {
		if (laDepensePapier != null) {
			unModeDePaiementSelectionne = laDepensePapier.modePaiement();
		}
		return unModeDePaiementSelectionne;
	}

	public void setUnModeDePaiementSelectionne(EOModePaiement unModeDePaiementSelectionne) {
		this.unModeDePaiementSelectionne = unModeDePaiementSelectionne;
		if (laDepensePapier != null) {
			laDepensePapier.setModePaiementRelationship(unModeDePaiementSelectionne);
			refreshDataEcritures = Boolean.TRUE;
		}
	}

	public NSArray<EORibFournisseur> getRibs() {
		return ribs;
	}

	public void setRibs(NSArray<EORibFournisseur> ribs) {
		this.ribs = ribs;
	}

	public EORibFournisseur getUnRibSelectionne() {
		if (laDepensePapier != null) {
			unRibSelectionne = laDepensePapier.ribFournisseur();
		}
		return unRibSelectionne;
	}

	public void setUnRibSelectionne(EORibFournisseur unRibSelectionne) {
		this.unRibSelectionne = unRibSelectionne;
		if (laDepensePapier != null) {
			laDepensePapier.setRibFournisseurRelationship(unRibSelectionne);
		}
	}

	public EODepensePapier getLaDepensePapier() {
		return laDepensePapier;
	}

	public void setLaDepensePapier(EODepensePapier laDepensePapier) {
		this.laDepensePapier = laDepensePapier;
		initialisation();
	}

	public NSTimestamp getDateFacture() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateFacture();
		}
		return date;
	}

	public void setDateFacture(NSTimestamp dateFacture) {
		if (dateFacture != null && dateFacture.equals(this.dateFacture) == false) {
			EODepensePapier dp = getLaDepensePapier();
			if (dp != null) {
				dp.setDppDateFacture(dateFacture);
				this.dateFacture = dateFacture;
			}
		}
	}

	public NSTimestamp getDateReception() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateReception();
		}
		return date;
	}

	public void setDateReception(NSTimestamp dateReception) {
		if (dateReception != null && dateReception.equals(this.dateReception) == false) {
			EODepensePapier dp = getLaDepensePapier();
			if (dp != null) {
				dp.setDppDateReception(dateReception);
				this.dateReception = dateReception;
			}
		}
	}

	public NSTimestamp getDateServiceFait() {
		NSTimestamp date = null;
		if (laDepensePapier != null) {
			date = laDepensePapier.dppDateServiceFait();
		}
		return date;
	}

	public void setDateServiceFait(NSTimestamp dateServiceFait) {
		if (dateServiceFait != null && dateServiceFait.equals(this.dateServiceFait) == false) {
			EODepensePapier dp = getLaDepensePapier();
			if (dp != null) {
				dp.setDppDateServiceFait(dateServiceFait);
				this.dateServiceFait = dateServiceFait;
			}
		}
	}

	public boolean isAfficherEngagements() {
		boolean isAfficherEngagements = false;

		if (laDepensePapier != null) {
			isAfficherEngagements = true;
		}
		return isAfficherEngagements;
	}

	public Boolean isCocheSoldeeDisabled() {
		return Boolean.valueOf(uneDepenseBudget == null || !isFactureComplete() || !ctrl.isEngagementLiquidable(uneDepenseBudget.engagementBudget()));
	}

	public Boolean isEngagementDisabled() {
		if (uneDepenseBudget == null || !isFactureComplete()) {
			return Boolean.TRUE;
		}
		NSArray<? extends _IDepenseBudget> lesDepenses = laDepensePapier.currentDepenseBudgets();
		return Boolean.valueOf(lesDepenses == null || lesDepenses.count() == 0);
	}

	public _IDepenseBudget getUneDepenseBudget() {
		return uneDepenseBudget;
	}

	public void setUneDepenseBudget(_IDepenseBudget uneDepenseBudget) {
		this.uneDepenseBudget = uneDepenseBudget;
	}

	public boolean isProrataDisabled() {
		boolean isProrataDisabled = true;
		if (laCommandeEnCours() == null || laCommandeEnCours().isTauxProrataModifiableLiquidation(laDepensePapier.editingContext())) {
			isProrataDisabled = false;
		}
		return isProrataDisabled;
	}

	public NSArray<EOTauxProrata> getLesTauxDeProratas() {
		if (getUneDepenseBudget().engagementBudget().organ() != null) {
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(exerciceCourant(), "exercice");
			bindings.setObjectForKey(getUneDepenseBudget().engagementBudget().organ(), "organ");
			lesTauxDeProratas = FinderProrata.getTauxProratas(laDepensePapier.editingContext(), bindings);
		}
		return lesTauxDeProratas;
	}

	//	public void setLesTauxDeProratas(NSArray<EOTauxProrata> lesTauxDeProratas) {
	//		this.lesTauxDeProratas = lesTauxDeProratas;
	//	}

	public EOTauxProrata getUnTauxDeProrataSelectionne() {
		return unTauxDeProrataSelectionne;
	}

	public void setUnTauxDeProrataSelectionne(EOTauxProrata unTauxDeProrataSelectionne) {
		if (isModificationEnCours == false) {
			this.unTauxDeProrataSelectionne = unTauxDeProrataSelectionne;
			getUneDepenseBudget().setTauxProrata(unTauxDeProrataSelectionne);
			isModificationEnCours = true;
		}
	}

	public BigDecimal getDepHtSaisie() {
		BigDecimal depHt = null;

		if (getUneDepenseBudget() != null) {
			depHt = getUneDepenseBudget().depHtSaisie();
		}
		return depHt;
	}

	public void setDepHtSaisie(BigDecimal depHtSaisie) {
		if (isModificationEnCours == false && depHtSaisie != null) {
			BigDecimal oldValue = getDepHtSaisie();
			if (oldValue != null && oldValue.floatValue() != depHtSaisie.floatValue() ||
					oldValue == null && depHtSaisie != null) {
				getUneDepenseBudget().setDepHtSaisie(depHtSaisie);
				isModificationEnCours = true;
			}
		}
	}

	public BigDecimal getDepTtcSaisie() {
		BigDecimal depTtc = null;

		if (getUneDepenseBudget() != null) {
			depTtc = getUneDepenseBudget().depTtcSaisie();
		}
		return depTtc;
	}

	public void setDepTtcSaisie(BigDecimal depTtcSaisie) {
		if (isModificationEnCours == false && depTtcSaisie != null) {
			BigDecimal oldValue = getDepTtcSaisie();
			if (oldValue != null && oldValue.floatValue() != depTtcSaisie.floatValue() ||
					oldValue == null && depTtcSaisie != null) {
				getUneDepenseBudget().setDepTtcSaisie(depTtcSaisie);
				isModificationEnCours = true;
			}
		}
	}

	public boolean getDepIsSoldee() {
		return ((EODepenseBudget) getUneDepenseBudget()).solde();
	}

	public void setDepIsSoldee(boolean depIsSoldee) {
		boolean oldValue = ((EODepenseBudget) getUneDepenseBudget()).solde();
		if (oldValue != depIsSoldee) {
			((EODepenseBudget) getUneDepenseBudget()).setSolde(depIsSoldee);
		}
	}

	public BigDecimal engTotalHT() {
		BigDecimal engTotalHT = BigDecimal.valueOf(0);
		if (laDepensePapier != null && laDepensePapier.currentDepenseBudgets() != null) {
			engTotalHT = (BigDecimal) laDepensePapier.currentDepenseBudgets().valueForKeyPath("@sum.depHtSaisie");
		}
		return engTotalHT;
	}

	public BigDecimal engTotalTTC() {
		BigDecimal engTotalTTC = BigDecimal.valueOf(0);

		if (laDepensePapier != null && laDepensePapier.currentDepenseBudgets() != null) {
			engTotalTTC = (BigDecimal) laDepensePapier.currentDepenseBudgets().valueForKeyPath("@sum.depTtcSaisie");
			;
		}

		return engTotalTTC;
	}

	// Repartitions
	public Boolean isAfficherRepartitions() {
		boolean isAfficherRepartitions = false;
		EODepensePapier dp = getLaDepensePapier();

		boolean isComplete = (isPreLiquidation() && dp != null ? dp.isCompletePourSF() : dp.isComplete());

		if (dp != null && isComplete) {
			BigDecimal montantFactureHT = getLaDepensePapier().dppHtInitial();
			BigDecimal totalHT = engTotalHT();
			BigDecimal montantFactureTTC = getLaDepensePapier().dppTtcInitial();
			BigDecimal totalTTC = engTotalTTC();

			if (montantFactureHT != null && montantFactureHT.floatValue() >= 0 && totalHT != null &&
					montantFactureTTC != null && montantFactureTTC.floatValue() > 0 && totalTTC != null &&
					montantFactureHT.floatValue() == totalHT.floatValue() &&
					montantFactureTTC.floatValue() == totalTTC.floatValue()) {
				isAfficherRepartitions = true;
			}
		}
		return Boolean.valueOf(isAfficherRepartitions);
	}

	// Repartition par CodeExer
	public _IDepenseBudget getUneDepenseBudgetForHorsMarche() {
		return uneDepenseBudgetForHorsMarche;
	}

	public void setUneDepenseBudgetForHorsMarche(_IDepenseBudget uneDepenseBudgetForHorsMarche) {
		this.uneDepenseBudgetForHorsMarche = uneDepenseBudgetForHorsMarche;
	}

	public NSArray<EOCodeExer> getLesCodesNomenclature() {
		return lesCodesNomenclature;
	}

	public void setLesCodesNomenclature(NSArray<EOCodeExer> lesCodesNomenclature) {
		this.lesCodesNomenclature = lesCodesNomenclature;
	}

	// Repartition par actions
	public _IDepenseBudget getUneDepenseBudgetForAction() {
		return uneDepenseBudgetForAction;
	}

	public void setUneDepenseBudgetForAction(_IDepenseBudget uneDepenseBudgetForAction) {
		this.uneDepenseBudgetForAction = uneDepenseBudgetForAction;
	}

	public NSArray<EOTypeAction> getLesActions() {
		_IDepenseBudget db = getUneDepenseBudgetForAction();
		lesActions = (NSArray<EOTypeAction>) dicoLesActions.objectForKey(db.getSource().libelleCourt());
		return lesActions;
	}

	public void setLesActions(NSArray<EOTypeAction> lesActions) {
		this.lesActions = lesActions;
	}

	// Repartition par imputations
	public _IDepenseBudget getUneDepenseBudgetForImputation() {
		return uneDepenseBudgetForImputation;
	}

	public void setUneDepenseBudgetForImputation(_IDepenseBudget uneDepenseBudgetForImputation) {
		this.uneDepenseBudgetForImputation = uneDepenseBudgetForImputation;
	}

	public _IDepenseControlePlanComptable getUneDepenseCtrlImputation() {
		return uneDepenseCtrlImputation;
	}

	public void setUneDepenseCtrlImputation(_IDepenseControlePlanComptable uneDepenseCtrlImputation) {
		this.uneDepenseCtrlImputation = uneDepenseCtrlImputation;
	}

	public NSArray<EOPlanComptable> getLesImputations() {
		_IDepenseBudget db = getUneDepenseBudgetForImputation();
		lesImputations = (NSArray<EOPlanComptable>) dicoLesPlansComptable.objectForKey(db.getSource().codeOrganEtTypeCredit());
		return lesImputations;
	}

	public void setLesImputations(NSArray<EOPlanComptable> lesImputations) {
		this.lesImputations = lesImputations;
	}

	public boolean isInventaireIncompletEtObligatoire() {
		boolean isInventaireIncompletEtObligatoire = true;
		_IDepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		if (dcpc.isInventaireObligatoire() && dcpc.isGood()) {
			isInventaireIncompletEtObligatoire = false;
		}
		return isInventaireIncompletEtObligatoire;
	}

	public EOInventaire getUnInventaire() {
		return unInventaire;
	}

	public void setUnInventaire(EOInventaire unInventaire) {
		this.unInventaire = unInventaire;
	}

	public WOComponent ajouterInventaires() {
		_IDepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		InventaireListeSelection nextPage = (InventaireListeSelection) pageWithName(InventaireListeSelection.class.getName());
		nextPage.setDepense(dcpc);
		return nextPage;
	}

	public WOComponent supprimerInventaire() {
		_IDepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		EOInventaire inventaire = getUnInventaire();
		if (dcpc != null && inventaire != null) {
			getCtrl().supprimerInventaire(dcpc, inventaire);
		}
		return null;
	}

	public int nbreDepenseControleImputation() {
		_IDepenseBudget depenseBudget = getUneDepenseBudgetForImputation();
		NSArray<? extends _IDepenseControlePlanComptable> ctrlPlanComptables = depenseBudget.depenseControlePlanComptables();
		int nbreImputations = ctrlPlanComptables.count();
		for (int i = 0; i < ctrlPlanComptables.count(); i++) {
			_IDepenseControlePlanComptable dcpc = (_IDepenseControlePlanComptable) ctrlPlanComptables.objectAtIndex(i);
			if (dcpc.isInventaireAffichable()) {
				nbreImputations += 1; // Pour le titre des colonnes
				nbreImputations += dcpc.inventaires().count(); // Pour les inventaires deja saisis
			}
		}
		return nbreImputations;
	}

	// Repartition par Conventions
	public _IDepenseBudget getUneDepenseBudgetForConvention() {
		return uneDepenseBudgetForConvention;
	}

	public void setUneDepenseBudgetForConvention(_IDepenseBudget uneDepenseBudgetForConvention) {
		this.uneDepenseBudgetForConvention = uneDepenseBudgetForConvention;
	}

	public _IDepenseControleConvention getUneDepenseCtrlConvention() {
		return uneDepenseCtrlConvention;
	}

	public void setUneDepenseCtrlConvention(_IDepenseControleConvention uneDepenseCtrlConvention) {
		this.uneDepenseCtrlConvention = uneDepenseCtrlConvention;
	}

	public NSArray<EOConvention> getLesConventions() {
		_IDepenseBudget db = getUneDepenseBudgetForConvention();
		lesConventions = (NSArray<EOConvention>) dicoLesConventions.objectForKey(db.getSource().libelleCourt());
		return lesConventions;
	}

	public void setLesConventions(NSArray<EOConvention> lesConventions) {
		this.lesConventions = lesConventions;
	}

	public EOConvention getUneConventionSelectionnee() {
		return getUneDepenseCtrlConvention().convention();
	}

	public boolean isConventionPourcentageDisabled() {
		boolean isConventionPourcentageDisabled = true;
		if (getUneDepenseCtrlConvention().convention() != null && getUneDepenseCtrlConvention().convention().isRessourceAffectee() == false) {
			isConventionPourcentageDisabled = false;
		}
		return isConventionPourcentageDisabled;
	}

	public BigDecimal getUneDepenseCtrlConventionPourcentage() {
		return getUneDepenseCtrlConvention().pourcentage();
	}

	public void setUneDepenseCtrlConventionPourcentage(BigDecimal uneDepenseCtrlConventionPourcentage) {

		if (isModificationEnCours == false && uneDepenseCtrlConventionPourcentage != null) {
			BigDecimal oldValue = this.uneDepenseCtrlConvention.pourcentage();
			if (oldValue != null && oldValue.floatValue() != uneDepenseCtrlConventionPourcentage.floatValue() ||
					oldValue == null && uneDepenseCtrlConventionPourcentage != null) {
				getUneDepenseCtrlConvention().setPourcentage(uneDepenseCtrlConventionPourcentage);
				isModificationEnCours = true;
			}
		}
	}

	public BigDecimal getUneDepenseCtrlConventionMontant() {
		return getUneDepenseCtrlConvention().dconTtcSaisie();
	}

	public void setUneDepenseCtrlConventionMontant(BigDecimal uneDepenseCtrlConventionMontant) {

		if (isModificationEnCours == false && uneDepenseCtrlConventionMontant != null) {
			BigDecimal oldValue = getUneDepenseCtrlConventionMontant();
			if (oldValue != null && oldValue.floatValue() != uneDepenseCtrlConventionMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlConventionMontant != null) {
				getUneDepenseCtrlConvention().setMontantTtc(uneDepenseCtrlConventionMontant);
				isModificationEnCours = true;
			}
		}
	}

	public WOComponent supprimerRepartConvention() {
		getCtrl().supprimerDepenseControleConvention(edc(), getUneDepenseCtrlConvention());
		return null;
	}

	// Repartition par Codes analytiques
	public _IDepenseBudget getUneDepenseBudgetForAnalytique() {
		return uneDepenseBudgetForAnalytique;
	}

	public void setUneDepenseBudgetForAnalytique(_IDepenseBudget uneDepenseBudgetForAnalytique) {
		this.uneDepenseBudgetForAnalytique = uneDepenseBudgetForAnalytique;
	}

	public _IDepenseControleAnalytique getUneDepenseCtrlAnalytique() {
		return uneDepenseCtrlAnalytique;
	}

	public void setUneDepenseCtrlAnalytique(_IDepenseControleAnalytique uneDepenseCtrlAnalytique) {
		this.uneDepenseCtrlAnalytique = uneDepenseCtrlAnalytique;
	}

	public NSArray<EOCodeAnalytique> getLesCodeAnalytiques() {
		_IDepenseBudget db = getUneDepenseBudgetForAnalytique();
		lesCodeAnalytiques = (NSArray<EOCodeAnalytique>) dicoLesCodesAnalytique.objectForKey(db.getSource().libelleCourt());
		return lesCodeAnalytiques;
	}

	public void setLesCodeAnalytiques(NSArray<EOCodeAnalytique> lesCodeAnalytiques) {
		this.lesCodeAnalytiques = lesCodeAnalytiques;
	}

	public boolean isAnalytiquePourcentageDisabled() {
		boolean isAnalytiquePourcentageDisabled = true;
		if (getUneDepenseCtrlAnalytique().codeAnalytique() != null) {
			isAnalytiquePourcentageDisabled = false;
		}
		return isAnalytiquePourcentageDisabled;
	}

	public BigDecimal getUneDepenseCtrlAnalytiquePourcentage() {
		return getUneDepenseCtrlAnalytique().pourcentage();
	}

	public void setUneDepenseCtrlAnalytiquePourcentage(BigDecimal uneDepenseCtrlAnalytiquePourcentage) {
		if (isModificationEnCours == false && uneDepenseCtrlAnalytiquePourcentage != null) {
			BigDecimal oldValue = this.uneDepenseCtrlAnalytique.pourcentage();
			if (oldValue != null && oldValue.floatValue() != uneDepenseCtrlAnalytiquePourcentage.floatValue() ||
					oldValue == null && uneDepenseCtrlAnalytiquePourcentage != null) {
				getUneDepenseCtrlAnalytique().setPourcentage(uneDepenseCtrlAnalytiquePourcentage);
				isModificationEnCours = true;
			}
		}
	}

	public BigDecimal getUneDepenseCtrlAnalytiqueMontant() {
		return getUneDepenseCtrlAnalytique().danaTtcSaisie();
	}

	public void setUneDepenseCtrlAnalytiqueMontant(BigDecimal uneDepenseCtrlAnalytiqueMontant) {
		if (isModificationEnCours == false && uneDepenseCtrlAnalytiqueMontant != null) {
			BigDecimal oldValue = getUneDepenseCtrlAnalytiqueMontant();
			if (oldValue != null && oldValue.floatValue() != uneDepenseCtrlAnalytiqueMontant.floatValue() ||
					oldValue == null && uneDepenseCtrlAnalytiqueMontant != null) {
				getUneDepenseCtrlAnalytique().setMontantTtc(uneDepenseCtrlAnalytiqueMontant);
				isModificationEnCours = true;
			}
		}
	}

	public WOComponent supprimerRepartAnalytique() {
		getCtrl().supprimerDepenseControleAnalytique(edc(), getUneDepenseCtrlAnalytique());
		return null;
	}

	public BigDecimal getMontantHTFacture() {
		BigDecimal montant = null;
		if (laDepensePapier != null) {
			montant = laDepensePapier.dppHtInitial();
		}
		return montant;
	}

	public void setMontantHTFacture(BigDecimal montantHTFacture) {
		if (laDepensePapier != null) {
			BigDecimal oldValue = laDepensePapier.dppHtInitial();
			if (oldValue != null && montantHTFacture != null && oldValue.floatValue() != montantHTFacture.floatValue() ||
					oldValue == null && montantHTFacture != null) {
				//	this.montantHTFacture = montantHTFacture;
				if (montantHTFacture != null) {
					laDepensePapier.setDppHtInitial(montantHTFacture);
				}
			}
		}
	}

	public BigDecimal getMontantTTCFacture() {
		BigDecimal montant = null;
		if (laDepensePapier != null) {
			montant = laDepensePapier.dppTtcInitial();
		}
		return montant;
	}

	public void setMontantTTCFacture(BigDecimal montantTTCFacture) {
		if (laDepensePapier != null) {
			BigDecimal oldValue = this.laDepensePapier.dppTtcInitial();
			if (oldValue != null && montantTTCFacture != null && oldValue.floatValue() != montantTTCFacture.floatValue() ||
					oldValue == null && montantTTCFacture != null) {
				//this.montantTTCFacture = montantTTCFacture;
				if (montantTTCFacture != null) {
					laDepensePapier.setDppTtcInitial(montantTTCFacture);
				}
			}
		}
	}

	public boolean isAjouterInventaireDisabled() {
		boolean isAjouterInventaireDisabled = true;
		_IDepenseControlePlanComptable dcpc = getUneDepenseCtrlImputation();
		if (dcpc != null && dcpc.isInventairesPossibles()) {
			isAjouterInventaireDisabled = false;
		}
		return isAjouterInventaireDisabled;
	}

	public String getOnloadJS() {
		if (onloadJS == null) {
			onloadJS = "var mdp = document.getElementById('ModeDePaiement');if (mdp!=null && mdp.value.length==0) Field.activate('ModeDePaiement');";
		}
		return onloadJS;
	}

	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	public EOModePaiement getUnModeDePaiement() {
		return unModeDePaiement;
	}

	public void setUnModeDePaiement(EOModePaiement unModeDePaiement) {
		this.unModeDePaiement = unModeDePaiement;
	}

	/**
	 * @return true si on peut afficher les informations de liquidation complète.
	 */
	public Boolean isAfficherLiquidationComplete() {
		//FIXME SF voir si ce controle est necessaire
		return Boolean.TRUE;
	}

	public Boolean isPreLiquidation() {
		return session.isServiceFacturierDisponible() && getLaDepensePapier().isPreLiquidation();
	}

	public _ILiquidationCtrl getCtrl() {
		return ctrl;
	}

	public void setCtrl(_ILiquidationCtrl ctrl) {
		this.ctrl = ctrl;
	}

	public void setEcritureDetail(EOEcritureDetail ecd) {
		if (getLaDepensePapier() != null) {
			getLaDepensePapier().setEcritureDetailRelationship(ecd.localInstanceIn(getLaDepensePapier().editingContext()));
		}
	}

	public EOEcritureDetail getEcritureDetail() {
		return (getLaDepensePapier() != null ? getLaDepensePapier().ecritureDetail() : null);
	}

	public Boolean isModePaiementEmargeable() {
		return Boolean.valueOf(getLaDepensePapier() != null && getLaDepensePapier().modePaiement() != null && getLaDepensePapier().modePaiement().isEmargementObligatoire());
	}

	public Boolean isConventionDisabled() {
		boolean isConventionDisabled = true;
		EOConvention convention = getUneDepenseCtrlConvention().convention();
		if (convention == null || (convention != null && convention.isRessourceAffectee() == false)) {
			isConventionDisabled = false;
		}
		return Boolean.valueOf(isConventionDisabled);
	}

	public String factureDateReceptionOnComplete() {
		if (!isPreLiquidation()) {
			return "function(oC) {Field.activate('FactureServiceFait');}";
		}
		return "function(oC) {Field.activate('FactureNbrePJ');}";
	}

	public String getPageTitle() {
		if (!isPreLiquidation()) {
			return "Création d'une liquidation";
		}
		return "Création d'une pré-liquidation";
	}

	public Boolean isMontantBudGreaterThanResteEng() {
		return Boolean.valueOf(getUneDepenseBudget().depMontantBudgetaire().compareTo(getUneDepenseBudget().engagementBudget().engMontantBudgetaireReste()) > 0);
	}

	public String getContainerEngMtBudClass() {
		if (isMontantBudGreaterThanResteEng()) {
			return "caramboleSpanWarning cktl_action_info";
		}
		return "";
	}

	public String getContainerEngMtBudTitle() {
		if (isMontantBudGreaterThanResteEng()) {
			return "Attention : Le montant budgétaire dépasse le reste engagé. Assurez-vous qu'il ne s'agit pas là d'une erreur avant d'enregistrer la liquidation.";
		}
		return "";
	}

	public String containerEngMtId() {
		return getComponentId() + "_EngMtBud";
	}

	public EOFournis getFournis() {
		if (getLaDepensePapier() != null) {
			return EOFournis.fetchByKeyValue(edc(), EOFournis.FOU_ORDRE_KEY, getLaDepensePapier().fournisseur().fouOrdre());
		}
		return null;
	}

	public EOTauxProrata getTauxProrata() {
		return uneDepenseBudget.tauxProrata();
	}

	public void setTauxProrata(EOTauxProrata tauxProrata) {
		uneDepenseBudget.setTauxProrata(tauxProrata);
	}

	public String engTTCId() {
		return "engTTC" + engIndex;
	}

	public String engHTId() {
		return "engHT" + engIndex;
	}

	public String engSourceProrataId() {
		return "engSourceProrataId" + engIndex;
	}

	public String engSoldeId() {
		return "engSolde" + engIndex;
	}

	public String engRowContainerId() {
		return "engRow" + engIndex;
	}

	public EOAttribution getAttribution() {
		if (laCommandeEnCours() != null) {
			return laCommandeEnCours().attribution();
		}
		return null;
	}

	/**
	 * @return l'objet depenseControleHorsMarche s'il est unique
	 */
	public _IDepenseControleMarche getDepenseCtrlMarcheUnique() {
		//if (laCommandeEnCours() == null) {
		if (laDepensePapierEnCours().currentDepenseBudgets().count() == 1 && laDepensePapierEnCours().currentDepenseBudgets().objectAtIndex(0).depenseControleMarches() != null && laDepensePapierEnCours().currentDepenseBudgets().objectAtIndex(0).depenseControleMarches().count() == 1) {
			return (laDepensePapierEnCours().currentDepenseBudgets().objectAtIndex(0)).depenseControleMarches().objectAtIndex(0);
		}
		//}
		return null;
	}

	protected boolean isModificationEnCours() {
		return isModificationEnCours;
	}

	protected void setModificationEnCours(boolean isModificationEnCours) {
		this.isModificationEnCours = isModificationEnCours;
	}

	public EOTypeAchat typeAchat() {
		if (laCommandeEnCours() != null) {
			return laCommandeEnCours().typeAchat();
		}
		return null;
	}

	@Override
	public EOEditingContext edc() {
		return getLaDepensePapier().editingContext();
	}

	@Override
	public EOExercice exerciceCourant() {
		if (laCommandeEnCours() != null) {
			return laCommandeEnCours().exercice();
		}
		return super.exerciceCourant();
	}
	
	public NSArray<EOCodeExer> lesCodesExerDeLaDepense() {
		if (laCommandeEnCours() != null) {
			return laCommandeEnCours().codesExer();
		}
		return NSArray.emptyArray();
	}

}
