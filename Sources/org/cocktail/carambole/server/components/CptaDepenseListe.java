/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlcompta.server.metier.EODepense;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class CptaDepenseListe extends MyAjaxComponent {
	private static final long serialVersionUID = 1L;
	public static final String BINDING_actionFiltrerName = "actionFiltrerName";
	public static final String BINDING_dg = "dg";
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	public static final String BINDING_showFiltres = "showFiltres";

	public static final String ROW_KEY = "unObjet.";

	public static final String COL_ORGAN_KEY = EODepense.TO_ORGAN_KEY + "." + EOOrgan.LONG_STRING_KEY;
	public static final String COL_DEP_NUMERO = EODepense.DEP_NUMERO_KEY;

	public static final String COL_FOURNISSEUR_KEY = EODepense.TO_FOURNIS_KEY + "." + EOFournis.TO_PERSONNE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	public static final String COL_PCO_KEY = EODepense.TO_PLAN_COMPTABLE_EXER_KEY + "." + EOPlanComptableExer.PCONUM_ET_LIBELLE_KEY;

	public static final String COL_DATE_FACTURE_KEY = EODepense.DEP_DATE_FOURNIS_KEY;
	public static final String COL_DATE_SERVICE_KEY = EODepense.DEP_DATE_SERVICE_KEY;
	public static final String COL_DEP_HT = EODepense.DEP_HT_KEY;
	public static final String COL_DEP_TVA = EODepense.DEP_TVA_KEY;
	public static final String COL_DEP_TTC = EODepense.DEP_TTC_KEY;

	public EOEnterpriseObject unObjet = null;

	private NSArray<CktlAjaxTableViewColumn> colonnes;

	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
	static {

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Budget");
		col1.setOrderKeyPath(COL_ORGAN_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_ORGAN_KEY, "emptyValue");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, COL_ORGAN_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Numéro");
		col2.setOrderKeyPath(COL_DEP_NUMERO);
		//CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(IND_KEY+COL_NOM_KEY, " ");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DEP_NUMERO, "emptyValue");
		col2.setAssociations(ass2);
		col2.setRowCssStyle("white-space: normal;");
		col2.setRowCssClass("alignToCenter");
		_colonnesMap.takeValueForKey(col2, COL_DEP_NUMERO);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Date facture");
		//col4.setComponent(FouValideColonne.class.getName());
		col4.setOrderKeyPath(COL_DATE_FACTURE_KEY);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DATE_FACTURE_KEY, null);
		col4.setAssociations(ass4);
		ass4.setDateformat("dd/MM/yyyy");
		col4.setRowCssClass("alignToCenter");
		//ass4.setFormatter("application.appDateFormatter");
		_colonnesMap.takeValueForKey(col4, COL_DATE_FACTURE_KEY);

		CktlAjaxTableViewColumn colDateService = new CktlAjaxTableViewColumn();
		colDateService.setLibelle("Date service fait");
		//col4.setComponent(FouValideColonne.class.getName());
		colDateService.setOrderKeyPath(COL_DATE_SERVICE_KEY);
		CktlAjaxTableViewColumnAssociation assDateService = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DATE_SERVICE_KEY, null);
		colDateService.setAssociations(assDateService);
		assDateService.setDateformat("dd/MM/yyyy");
		colDateService.setRowCssClass("alignToCenter");
		//ass4.setFormatter("application.appDateFormatter");
		_colonnesMap.takeValueForKey(colDateService, COL_DATE_SERVICE_KEY);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Imputation comptable");
		col5.setOrderKeyPath(COL_PCO_KEY);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_PCO_KEY, "emptyValue");
		col5.setAssociations(ass5);
		_colonnesMap.takeValueForKey(col5, COL_PCO_KEY);

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Fournisseur");
		//	col6.setComponent(CktlAjaxTVCheckBoxCell.class.getName());
		col6.setOrderKeyPath(COL_FOURNISSEUR_KEY);
		col6.setRowCssClass("");
		col6.setRowCssStyle("white-space: normal;");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_FOURNISSEUR_KEY, "emptyValue");
		col6.setAssociations(ass6);
		_colonnesMap.takeValueForKey(col6, COL_FOURNISSEUR_KEY);

		CktlAjaxTableViewColumn ht = new CktlAjaxTableViewColumn();
		ht.setLibelle("HT");
		ht.setOrderKeyPath(COL_DEP_HT);
		ht.setRowCssClass("alignToRight");
		CktlAjaxTableViewColumnAssociation assht = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DEP_HT, "emptyValue");
		ht.setAssociations(assht);
		assht.setFormatter(Application.APP2DECIMALES_FORMATTER_KEY);
		_colonnesMap.takeValueForKey(ht, COL_DEP_HT);

		CktlAjaxTableViewColumn ttc = new CktlAjaxTableViewColumn();
		ttc.setLibelle("TTC");
		ttc.setOrderKeyPath(COL_DEP_TTC);
		ttc.setRowCssClass("alignToRight");
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(ROW_KEY + COL_DEP_TTC, "emptyValue");
		ttc.setAssociations(ass3);
		ass3.setFormatter(Application.APP2DECIMALES_FORMATTER_KEY);
		_colonnesMap.takeValueForKey(ttc, COL_DEP_TTC);

	}

	/** Tableau contenant les clés identiofiant les colonnes à afficher par défaut. */
	public static NSArray<String> DEFAULT_COLONNES_KEYS = new NSArray<String>(new String[] {
			COL_ORGAN_KEY, COL_DEP_NUMERO, COL_DATE_FACTURE_KEY, COL_FOURNISSEUR_KEY, COL_DATE_SERVICE_KEY, COL_PCO_KEY, COL_DEP_HT, COL_DEP_TTC
	});

	public CptaDepenseListe(WOContext context) {
		super(context);
	}

	public String depenseTbViewId() {
		return getComponentId() + "_depenseTbViewId";
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.objectForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS;
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}

		return keys;
	}

	public String emptyValue() {
		return "";
	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(BINDING_dg);
	}

	public WOActionResults filtrer() {
		dg().setSelectedObjects(NSArray.EmptyArray);
		return (WOActionResults) performParentAction((String) valueForBinding(BINDING_actionFiltrerName));
	}

	public Boolean showFiltres() {
		return booleanValueForBinding(BINDING_showFiltres, Boolean.TRUE);
	}

	public String listeContainerId() {
		return getComponentId() + "_listeContainer";
	}

	public EOEnterpriseObject getUnObjet() {
		return unObjet;
	}

	public void setUnObjet(EOEnterpriseObject unObjet) {
		this.unObjet = unObjet;
	}

}