/**
 * 
 */
package org.cocktail.carambole.server.components.service;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.Session;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeEngagement;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;

import com.webobjects.foundation.NSArray;

/**
 * @author Raymond NANEON <raymond.naneon at utt.fr> 5 nov. 2014
 */
public class ServiceMailLiquidation {

	private Application application;
	private Session session;
	private EODepensePapier depensePapier;

	/**
	 * @param myApp
	 */
	public void setApplication(Application myApp) {
		this.application = myApp;
	}

	public Application getApplication() {
		return application;
	}

	public List<AdresseMail> getAdressesMailForDemandeSF() {

		List<AdresseMail> adresses = new ArrayList<AdresseMail>();
		if (isMailServiceFacturierParametre()) {
			setMailAServiceEnChargeServiceFacturier(adresses);
		} else {
			setMailCreateurCommande(adresses);
			setMailCreateurEngagement(adresses);
		}
		return adresses;
	}

	/**
	 * @return
	 */
	public boolean isMailServiceFacturierParametre() {
		return getApplication().mailCentralisationCertification() != null && getApplication().mailCentralisationCertification().length() > 0 && !getApplication().mailCentralisationCertification().equals("certification@univ.org");
	}

	/**
	 * 
	 */
	protected void setMailAServiceEnChargeServiceFacturier(List<AdresseMail> adresses) {
		adresses.add(new AdresseMail(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_TO, getApplication().mailCentralisationCertification()));
	}

	/**
	 * 
	 */
	protected void setMailCreateurEngagement(List<AdresseMail> adresses) {
		NSArray<EOCommandeEngagement> comeng = getDepensePapier().commande().commandeEngagements();
		for (int i = 0; i < comeng.count(); i++) {
			EOEngagementBudget eng = ((EOCommandeEngagement) comeng.objectAtIndex(i)).engagementBudget();
			Integer persIdEng = eng.utilisateur().persId();
			ApplicationUser appUserEng = getUtilisateurApplication(persIdEng);
			if (appUserEng.getEmails().count() > 0) {
				adresses.add(new AdresseMail(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_CC, (String) appUserEng.getEmails().objectAtIndex(0)));
			}
		}
	}

	/**
	 * 
	 */
	protected void setMailCreateurCommande(List<AdresseMail> adresses) {
		Integer persId = getDepensePapier().commande().utilisateur().persId();
		ApplicationUser appUser = getUtilisateurApplication(persId);
		NSArray<String> emails = appUser.getEmails();
		if (emails.count() > 0) {
			adresses.add(new AdresseMail(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_TO, (String) emails.objectAtIndex(0)));
		}
	}

	/**
	 * @param persId
	 * @return
	 */
	private ApplicationUser getUtilisateurApplication(Integer persId) {
		ApplicationUser appUser = new ApplicationUser(getSession().defaultEditingContext(), persId);
		return appUser;
	}

	/**
	 * @param session
	 */
	public void setSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

	/**
	 * @param laDepensePapierEnCours
	 */
	public void setLaDepensePapier(EODepensePapier laDepensePapierEnCours) {
		this.depensePapier = laDepensePapierEnCours;

	}

	public EODepensePapier getDepensePapier() {
		return depensePapier;
	}

	public class AdresseMail {

		private String option;
		private String adresse;

		/**
		 * 
		 */
		public AdresseMail(String option, String adresse) {
			// TODO Auto-generated constructor stub
			this.option = option;
			this.adresse = adresse;
		}

		/**
		 * @return the option
		 */
		public String getOption() {
			return option;
		}

		/**
		 * @return the adresse
		 */
		public String getAdresse() {
			return adresse;
		}

	}

}
