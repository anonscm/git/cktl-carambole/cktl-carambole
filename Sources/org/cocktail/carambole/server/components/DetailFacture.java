/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class DetailFacture extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	private static final String PAGE_TITLE = "Détail d'une facture";

	public EODepensePapier laFacture = null;

	public DetailFacture(WOContext context) {
		super(context);
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.DETAILLER_UNE_COMMANDE_KEY,
				Menu.ENGAGEMENT_KEY,
				Menu.SUPPRIMER_UNE_FACTURE_KEY,
				Menu.ACCUEIL_KEY,
				Menu.RETOURNER_A_LA_LISTE_DES_FACTURES_KEY
		});

		if (!session.isServiceFacturierDisponible().booleanValue()) {
			items = new NSArray<String>(new String[] {
					Menu.DETAILLER_UNE_COMMANDE_KEY,
					Menu.ENGAGEMENT_KEY,
					Menu.ACCUEIL_KEY,
					Menu.RETOURNER_A_LA_LISTE_DES_FACTURES_KEY
			});
		}
		return items;
	}

	public EODepensePapier getLaFacture() {
		return laFacture;
	}

	public void setLaFacture(EODepensePapier laFacture) {
		this.laFacture = laFacture;
	}

	public boolean isAfficherRepartCN() {
		boolean isAfficherRepartCN = true;
		EOCommande commande = laCommandeEnCours();
		if (commande != null) {
			if (commande.isSurUnMarche()) {
				isAfficherRepartCN = false;
			}
		}
		return isAfficherRepartCN;
	}

	public String pageTitle() {
		return PAGE_TITLE;
	}
}