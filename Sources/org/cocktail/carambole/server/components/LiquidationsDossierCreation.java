/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.controleurs.LiquidationDossierCreationCtrl;
import org.cocktail.carambole.server.reports.PrintFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOBordereau;
import org.cocktail.fwkcktldepense.server.finder.FinderDepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.MyNumberCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class LiquidationsDossierCreation extends MyAjaxComponent {
	private static final String PAGE_TITLE = "Création d'un dossier de liquidation";
	private static final long serialVersionUID = 4622945179443554793L;
	private LiquidationDossierCreationCtrl ctrl;
	private NSArray<String> liquideurList;
	private NSArray<NSMutableDictionary> fournisseurList;

	public LiquidationsDossierCreation(WOContext context) {
		super(context);
		ctrl = new LiquidationDossierCreationCtrl(this);
		getDg().queryBindings().remove("ADR_NUMERO");
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ACCUEIL_KEY
		});

		return items;
	}

	public String containerLiquidationsSelectionId() {
		return getMainContainerId() + "_LiquidationsSelection";
	}

	public String containerLiquidationsSelectionneesId() {
		return getMainContainerId() + "_LiquidationsSelectionnees";
	}

	public String containerActionId() {
		return getMainContainerId() + "_Action";
	}

	public WODisplayGroup getDg() {
		return session.accueilControleur().getDgLiquidationsAttente();
	}

	public void refreshData() {
		@SuppressWarnings("unchecked")
		NSDictionary<String, Object> bdgs = getDg().queryBindings();
		liquideurList = FinderDepensePapier.getRawRowLiquidationsLiquideursPourDossier(edc(), bdgs);
		fournisseurList = FinderDepensePapier.getRawRowLiquidationsFourniseursPourDossier(edc(), bdgs);
		NSArray<NSDictionary<String, Object>> lesLiquidations = FinderDepensePapier.getRawRowLiquidationsPourDossier(edc(), bdgs);
		getDg().setObjectArray(lesLiquidations);

	}

	public WOActionResults creerDossiers() {
		EOUtilisateur utilisateur = session.getUtilisateur();
		EOExercice exercice = exerciceCourant();

		@SuppressWarnings("unchecked")
		NSArray<Integer> res = ctrl.creerDossiers(edc(), getDg().selectedObjects(), utilisateur, exercice);
		if (res.count() > 0) {
			LiquidationsDossierSuivi nextPage = (LiquidationsDossierSuivi) pageWithName(LiquidationsDossierSuivi.class.getName());
			nextPage.setShowFiltresForBordereaux(Boolean.FALSE);

			//Recuperer les bordereaux crees
			NSArray<EOBordereau> bords = EOBordereau.fetchAllDossiersLiquidation(edc(), EOBordereau.qualifierOrForNSArrayOfValues(EOBordereau.BOR_ID_KEY, res), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
					EOBordereau.SORT_GES_CODE, EOBordereau.SORT_BOR_NUM_DESC
			}));
			nextPage.dgForBordereaux().setObjectArray(bords);

			nextPage.setInfoMsg(bords.count() > 1 ? bords.count() + " dossiers de liquidation ont été créés." : bords.count() + " dossier de liquidation a été créé.");

			nextPage.refreshDepenses();
			return nextPage;
		}

		return null;

	}

	public WOActionResults imprimerLiquidationsAttente() {
		String chaine = "";
		@SuppressWarnings("unchecked")
		NSArray<NSDictionary<String, Object>> dicos = getDg().selectedObjects();
		for (int i = 0; i < dicos.count(); i++) {
			chaine = chaine + ((NSDictionary<String, Object>) dicos.objectAtIndex(i)).valueForKey(EODepenseControlePlanComptable.DPCO_ID_COLKEY) + ",";
		}
		chaine = chaine + "-1";

		try {
			//Integer borId = (Integer) EOUtilities.primaryKeyForObject(bord.editingContext(), bord).allValues().lastObject();

			NSData data = PrintFactory.printLiquidationEnAttente(session, chaine);
			String fileName = "liquidationsAttente" + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer le dossier de liquidation " + fileName);
			}
			WOActionResults res = PrintFactory.afficherPdf(data, fileName);
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			session.setAlertMessage(e.getMessage() + " " + e.toString());
			return null;
		}
	}

	public WOActionResults onFiltrer() {
		refreshData();
		//NSMutableDictionary bdg = getDg().queryBindings();
		return null;
	}

	public NSArray<String> liquideurList() {
		return liquideurList;
	}

	public NSArray<NSMutableDictionary> fournisseurList() {
	    return fournisseurList;
	}
	
	public Number montantSelection() {
		return MyNumberCtrl.computeSumForKeyBigDecimal(getDg().selectedObjects(), "DPCO_TTC_SAISIE");
	}

	public String containerRecapId() {
		return getComponentId() + "_recap";
	}

	public String pageTitle() {
		return PAGE_TITLE;
	}
}