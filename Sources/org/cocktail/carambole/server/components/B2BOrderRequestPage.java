/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.util.UUID;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktlb2b.cxml.server.engine.ProxyProperties;
import org.cocktail.fwkcktldepense.server.finder.FinderStructure;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class B2BOrderRequestPage extends MyAjaxComponent implements CktlResourceProvider {

	private static final long serialVersionUID = 1L;
	private String orderResult;
	private String orderResultError;

	public final static String BINDING_id = "id";

	private String componentUniqueId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_");
	private String componentId;
	private String mainContainerId;

	private EOCommande commande;

	public B2BOrderRequestPage(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		MyAjaxPage.injectResourcesInResponse(response, context);
	}

	public EOCommande getCommande() {
		return commande;
	}

	public String getOrderID() {
		return getCommande().commIdProc().toString();
	}

	public EOFournisB2bCxmlParam getB2bParam() {
		return ((EOB2bCxmlPunchoutmsg) getCommande().toB2bCxmlPunchoutmsgs().objectAtIndex(0)).toFournisB2bCxmlParam();
	}

	public CXMLParameters getCxmlParameters() {
		CXMLParameters cxmlParameters = getB2bParam().toCxmlParameters();
		cxmlParameters.setLocalDomain(myApp().config().stringForKey("ANNUAIRE_DOMAINE"));
		return cxmlParameters;
	}

	public ProxyProperties getProxyProperties() {
		return mySession().getProxyProperties();
	}

	public String getOrderResult() {
		return orderResult;
	}

	public void setOrderResult(String orderResult) {
		this.orderResult = orderResult;
	}

	public String getOrderResultError() {
		return orderResultError;
	}

	public void setOrderResultError(String orderResultError) {
		this.orderResultError = orderResultError;
	}

	/**
	 * @return l'ID = le binding ID. Si celui-ci n'est pas defini, on en genere un unique.
	 */
	public String getComponentId() {
		if (componentId == null) {
			if (hasBinding(BINDING_id)) {
				componentId = (String) valueForBinding(BINDING_id);
			}
			else {
				componentId = componentUniqueId;
			}
		}
		return componentId;
	}

	/**
	 * @return Un identifiant pour le container principal du composant si besoin. (id du composant+_mainContainer).
	 */
	public String getMainContainerId() {
		if (mainContainerId == null) {
			mainContainerId = getComponentId() + "_mainContainer";
		}
		return mainContainerId;
	}

	public WOActionResults doNothing() {
		return null;
	}

	public void setCommande(EOCommande commande) {
		this.commande = commande;
	}

	public Integer getUtilisateurPersId() {
		return Integer.valueOf(mySession().connectedUserInfo().persId().intValue());
	}

	public NSArray<EOStructure> getLesServices() {
		NSMutableArray<EOStructure> res = new NSMutableArray<EOStructure>();
		if (getCommande() != null) {
			NSArray<org.cocktail.fwkcktldepense.server.metier.EOStructure> strucs = FinderStructure.getStructuresPourCommande(getCommande().editingContext(), getCommande());
			for (int i = 0; i < strucs.count(); i++) {
				org.cocktail.fwkcktldepense.server.metier.EOStructure struct = (org.cocktail.fwkcktldepense.server.metier.EOStructure) strucs.objectAtIndex(i);
				res.addObject(EOStructure.fetchByKeyValue(struct.editingContext(), EOStructure.C_STRUCTURE_KEY, EOUtilities.primaryKeyForObject(struct.editingContext(), struct).valueForKey(org.cocktail.fwkcktldepense.server.metier.EOStructure.C_STRUCTURE_KEY)));
			}
		}
		return res.immutableClone();
	}

	public void injectResources(WOResponse response, WOContext context) {
		MyAjaxPage.injectResourcesInResponse(response, context);
	}
}