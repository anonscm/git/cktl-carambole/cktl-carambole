/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.factory.FactoryArticle;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommande;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommandeControlePlanComptable;
import org.cocktail.fwkcktldepense.server.finder.FinderArticle;
import org.cocktail.fwkcktldepense.server.finder.FinderBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderProrata;
import org.cocktail.fwkcktldepense.server.finder.FinderTva;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCatalogueArticle;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCodeMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeBudget;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOCommandeControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.process.ProcessPreference;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class BonDeCommande2 extends CommandeComponent {
	private static final long serialVersionUID = 1L;
	public static final String TAB_INFORMATIONS = "Informations";
	public static final String TAB_SOURCES = "Sources";
	public static final String TAB_REPARTITIONS = "Repartitions";
	public static final String TAB_ARTICLES = "Articles";

	EOCommande laCommande = null;
	String onloadJS = null;
	String selectedTab = null;

	private NSMutableDictionary<String, NSArray<EOTypeAction>> dicoLesActions = null;
	private NSMutableDictionary<String, NSArray<EOPlanComptable>> dicoLesPlansComptable = null;
	private NSMutableDictionary<String, NSArray<EOConvention>> dicoLesConventions = null;
	private NSMutableDictionary<String, NSArray<EOCodeAnalytique>> dicoLesCodesAnalytique = null;

	// Informations
	public EOExercice unExercice = null;
	public EOExercice exerciceSelectionne = null;

	// Sources
	Boolean isAfficherOngletSources = null;
	public int indexProratas;
	EOCommandeBudget uneCommandeBudget = null;
	EOTauxProrata unTauxProrata = null;
	BigDecimal uneCommandeBudgetDisponible = BigDecimal.valueOf(0);

	// Articles
	boolean modificationEnCours = false;
	String articleReference = null;
	String articleLibelle = null;
	BigDecimal articlePrixHt = null;
	EOTva articleTva = null;
	BigDecimal articleQte = null;
	BigDecimal articlePrixTtc = null;
	public int indexArticles;
	EOArticle unArticle = null;
	//	EOCatalogue unArticleSelectionnable = null;
	//NSArray lesArticlesSelectionnes = null;
	//NSArray attributionsCommunes = null;
	//NSArray typesAchatCommuns = null;
	//EOAttribution uneAttributionCommune = null;
	//EOAttribution uneAttributionCommuneSelectionnee = null;
	//EOTypeAchat unTypeAchatCommun = null;
	//EOTypeAchat unTypeAchatCommunSelectionne = null;
	EOTva unTauxTVA = null;
	NSArray<EOTva> lesTauxTVA = null;
	EOCodeExer unCodeNomenclature = null;
	String unCodeNomenclatureValue = null;
	EOCodeMarche unCodeMarche = null;
	String unCodeMarcheValue = null;
	EOAttribution uneAttribution = null;

	// Repartitions
	NSDictionary<String, Object> uneLigneHorsMarche = null;
	EOCommandeBudget uneCmdeBudget = null, uneCmdeMarcheBudget = null;
	EOCommandeControleHorsMarche uneCmdeCtrlHorsMarche = null;
	EOCommandeControleMarche uneCmdeCtrlMarche = null;
	public int indexRepartCmdeMarcheSources, indexRepartSourcesCodeExer, indexCmdeMarcheBudgets, indexCmdeBudgets;
	BigDecimal uneCmdeCtrlHorsMarcheHtSaisie = BigDecimal.valueOf(0);
	BigDecimal uneCmdeCtrlHorsMarcheTtcSaisie = BigDecimal.valueOf(0);
	BigDecimal uneCmdeCtrlMarcheHtSaisie = BigDecimal.valueOf(0);
	BigDecimal uneCmdeCtrlMarcheTtcSaisie = BigDecimal.valueOf(0);

	public FactoryCommandeControleAction fcca = new FactoryCommandeControleAction();
	public FactoryCommandeControlePlanComptable fccpc = new FactoryCommandeControlePlanComptable();
	public FactoryCommandeControleConvention fccc = new FactoryCommandeControleConvention();
	private EOCommandeControleConvention uneCmdeCtrlConvention = null;
	public FactoryCommandeControleAnalytique fccan = new FactoryCommandeControleAnalytique();
	// Repartition par action
	private EOCommandeBudget uneCmdeBudgetForAction = null;
	private NSArray<EOTypeAction> lesActions = null;

	// Repartition par imputation (plan comptable)
	private EOCommandeBudget uneCmdeBudgetForImputation = null;
	private NSArray<EOPlanComptable> lesImputations = null;

	public int indexInventaire;
	EOInventaire unInventaire = null;
	BigDecimal unInventaireMontant = null;

	// Repartition par Convention
	private EOCommandeBudget uneCmdeBudgetForConvention = null;
	private NSArray<EOConvention> lesConventions = null;

	// Repartition par Code analytiques
	private EOCommandeBudget uneCmdeBudgetForCodeAnalytique = null;
	private NSArray<EOCodeAnalytique> lesCodeAnalytiques = null;

	public BonDeCommande2(WOContext context) {
		super(context);
		if (getSelectedTab() == null) {
			setSelectedTab(TAB_INFORMATIONS);
		}
	}

	private void initialisationDesRepartitions() {
		EOCommande commande = laCommande;
		EOEditingContext edc = commande.editingContext();
		EOUtilisateur utilisateur = utilisateurInContext(edc);
		dicoLesActions = new NSMutableDictionary<String, NSArray<EOTypeAction>>();
		dicoLesPlansComptable = new NSMutableDictionary<String, NSArray<EOPlanComptable>>();
		dicoLesConventions = new NSMutableDictionary<String, NSArray<EOConvention>>();
		dicoLesCodesAnalytique = new NSMutableDictionary<String, NSArray<EOCodeAnalytique>>();
		NSArray<EOCommandeBudget> commandeBudgets = commande.commandeBudgets();
		Enumeration<EOCommandeBudget> enumCommandeBudgets = commandeBudgets.objectEnumerator();
		while (enumCommandeBudgets.hasMoreElements()) {
			EOCommandeBudget uneCommandeBudget = (EOCommandeBudget) enumCommandeBudgets.nextElement();
			NSArray<EOTypeAction> actions = uneCommandeBudget.getTypeActionsPossibles(edc, utilisateur);
			dicoLesActions.setObjectForKey(actions, uneCommandeBudget.source().libelle());
			NSArray<EOPlanComptable> plansComptable = uneCommandeBudget.getPlanComptablesPossibles(edc, utilisateur);
			dicoLesPlansComptable.setObjectForKey(plansComptable, uneCommandeBudget.source().libelle());
			NSArray<EOConvention> conventions = uneCommandeBudget.getConventionsPossibles(edc, utilisateur);
			dicoLesConventions.setObjectForKey(conventions, uneCommandeBudget.source().libelle());
			NSArray<EOCodeAnalytique> codesAnalytique = uneCommandeBudget.getCodeAnalytiquesPossibles(edc, utilisateur);
			dicoLesCodesAnalytique.setObjectForKey(codesAnalytique, uneCommandeBudget.source().libelle());
		}
	}

	public String idForLIInformations() {
		String idForLIInformations = "Informations";
		String tab = getSelectedTab();
		if (tab == null || (tab.equals(TAB_INFORMATIONS))) {
			idForLIInformations = "tab_actif";
		}

		return idForLIInformations;
	}

	public String classForLIInformations() {
		String classForLIInformations = "";
		String tab = getSelectedTab();
		if (tab == null || (tab.equals(TAB_INFORMATIONS))) {
			classForLIInformations = "tabberactive";
		}

		return classForLIInformations;
	}

	public boolean isInformationsGood() {
		boolean isInformationsGood = false;
		EOCommande commande = laCommande;

		if (commande != null && ZStringUtil.isEmpty(commande.commLibelle()) == false) {
			isInformationsGood = true;
		}

		return isInformationsGood;
	}

	public String idForLISources() {
		String idForLISources = "tab_sources";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_SOURCES))) {
			idForLISources = "tab_actif";
		}

		return idForLISources;
	}

	public String classForLISources() {
		String classForLISources = "";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_SOURCES))) {
			classForLISources = "tabberactive";
		}

		return classForLISources;
	}

	public String idForLIArticles() {
		String idForLIArticles = "tab_articles";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_ARTICLES))) {
			idForLIArticles = "tab_actif";
		}

		return idForLIArticles;
	}

	public String classForLIArticles() {
		String classForLIArticles = "";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_ARTICLES))) {
			classForLIArticles = "tabberactive";
		}

		return classForLIArticles;
	}

	public boolean isArticlesGood() {
		boolean isArticlesGood = false;
		EOCommande commande = laCommande;

		if (commande != null && commande.articles() != null
				&& commande.articles().count() > 0) {
			for (int i = 0; i < commande.articles().count(); i++) {
				EOCodeExer codeExer = (EOCodeExer) commande.articles().objectAtIndex(i).codeExer();
				if (codeExer == null) {
					return isArticlesGood;
				}
			}
			isArticlesGood = true;
		}

		return isArticlesGood;
	}

	public String idForLIRepartitions() {
		String idForLIRepartitions = "tab_repartitions";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_REPARTITIONS))) {
			idForLIRepartitions = "tab_actif";
		}

		return idForLIRepartitions;
	}

	public String classForLIRepartitions() {
		String classForLIRepartitions = "";
		String tab = getSelectedTab();
		if (tab != null && (tab.equals(TAB_REPARTITIONS))) {
			classForLIRepartitions = "tabberactive";
		}

		return classForLIRepartitions;
	}

	public boolean isRepartitionsGood() {
		boolean isRepartitionsGood = false;
		EOCommande commande = laCommande;

		if (commande != null) {
			if (commande.commandeBudgets() != null && commande.commandeBudgets().count() > 0) {
				isRepartitionsGood = true;
				NSArray<EOCommandeBudget> cmdeBudgets = commande.commandeBudgets();
				for (int i = 0; i < cmdeBudgets.count(); i++) {
					EOCommandeBudget commandeBudget = (EOCommandeBudget) cmdeBudgets.objectAtIndex(i);
					if (!commandeBudget.isComplet()) {
						isRepartitionsGood = false;
						break;
					}
				}
			}
		}

		return isRepartitionsGood;
	}

	public boolean isCreditsGood() {
		boolean isRepartitionsGood = false;
		EOCommande commande = laCommande;

		if (commande != null) {
			if (commande.commandeBudgets() != null && commande.commandeBudgets().count() > 0) {
				isRepartitionsGood = true;
				NSArray<EOCommandeBudget> cmdeBudgets = commande.commandeBudgets();
				for (int i = 0; i < cmdeBudgets.count(); i++) {
					EOCommandeBudget commandeBudget = (EOCommandeBudget) cmdeBudgets.objectAtIndex(i);
					if (commandeBudget.tauxProrata() == null) {
						isRepartitionsGood = false;
						break;
					}
				}
			}
		}

		return isRepartitionsGood;
	}

	public String containerSourcesClass() {
		String containerSourcesClass = "tabbertab";
		String tab = getSelectedTab();
		if (tab == null || !(tab.equals(TAB_SOURCES))) {
			containerSourcesClass += " tabbertabhide";
		}
		return containerSourcesClass;
	}

	public String containerArticlesClass() {
		String containerArticlesClass = "tabbertab";
		String tab = getSelectedTab();
		if (tab == null || !(tab.equals(TAB_ARTICLES))) {
			containerArticlesClass += " tabbertabhide";
		}
		return containerArticlesClass;
	}

	public String containerRepartitionsClass() {
		String containerRepartitionsClass = "tabbertab";
		String tab = getSelectedTab();
		if (tab == null || !(tab.equals(TAB_REPARTITIONS))) {
			containerRepartitionsClass += " tabbertabhide";
		}
		return containerRepartitionsClass;
	}

	public boolean isTabInformationsSelectionne() {
		boolean isTabSelectionne = true;
		String tab = getSelectedTab();
		if (tab != null && tab.equals(TAB_INFORMATIONS) == false) {
			isTabSelectionne = false;
		}
		return isTabSelectionne;
	}

	public boolean isTabSourcesSelectionne() {
		boolean isTabSelectionne = false;
		String tab = getSelectedTab();
		if (tab != null && tab.equals(TAB_SOURCES)) {
			isTabSelectionne = true;
		}
		return isTabSelectionne;
	}

	public boolean isTabArticlesSelectionne() {
		boolean isTabSelectionne = false;
		String tab = getSelectedTab();
		if (tab != null && tab.equals(TAB_ARTICLES)) {
			isTabSelectionne = true;
		}
		return isTabSelectionne;
	}

	public boolean isTabRepartitionsSelectionne() {
		boolean isTabSelectionne = false;
		String tab = getSelectedTab();
		if (tab != null && tab.equals(TAB_REPARTITIONS)) {
			isTabSelectionne = true;
		}
		return isTabSelectionne;
	}

	public WOComponent afficherInformations() {
		setSelectedTab(TAB_INFORMATIONS);
		return null;
	}

	public WOComponent afficherSources() {
		setSelectedTab(TAB_SOURCES);
		return null;
	}

	public WOComponent afficherArticles() {
		setSelectedTab(TAB_ARTICLES);
		return null;
	}

	public WOComponent afficherRepartitions() {
		setSelectedTab(TAB_REPARTITIONS);
		if (laCommande.isRepartitionAutomatiquePossible()) {
			laCommande.setRepartitionAutomatiquePossible();
		}
		initialisationDesRepartitions();
		return null;
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = null;

		if (selectedTab != null) {
			if (selectedTab.equalsIgnoreCase(TAB_INFORMATIONS)) {
				items = itemsMenuInformations();
			}
			else if (selectedTab.equalsIgnoreCase(TAB_SOURCES)) {
				items = itemsMenuSources();
			}
			else if (selectedTab.equalsIgnoreCase(TAB_ARTICLES)) {
				items = itemsMenuArticles();
			}
			else if (selectedTab.equalsIgnoreCase(TAB_REPARTITIONS)) {
				items = itemsMenuRepartitions();
			}
		}

		return items;
	}

	public NSArray<String> itemsMenuInformations() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"EnregistrerUneCommande",
				"AnnulerUneCommande"
		});

		return items;
	}

	public NSArray<String> itemsMenuSources() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"EnregistrerUneCommande",
				"AjouterDesSources",
				"AnnulerUneCommande"
		});

		return items;
	}

	public NSArray<String> itemsMenuArticles() {
		NSArray<String> items = new NSArray<String>();
		if (!laCommandeEnCours().isCommandeB2b()) {
			items = new NSArray<String>(new String[] {
					"EnregistrerUneCommande",
					"CreerUnNouvelArticle",
					"RechercherDesArticles",
					"AnnulerUneCommande"
			});
		}
		else {
			items = new NSArray<String>(new String[] {
					"EnregistrerUneCommande",
					Menu.B2B_MODIFIER_LES_ARTICLES_KEY,
					"AnnulerUneCommande"
			});

		}

		return items;
	}

	public NSArray<String> itemsMenuRepartitions() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"EnregistrerUneCommande",
				"AnnulerUneCommande"
		});

		return items;
	}

	public boolean isNouvelleCommande() {
		boolean isNouvelleCommande = false;

		if (laCommande == null || laCommande.commNumero() == null || laCommande.commDate() == null || laCommande.fournisseur() == null) {
			isNouvelleCommande = true;
		}

		return isNouvelleCommande;
	}

	public boolean isAfficherLISources() {
		if (isAfficherOngletSources == null) {
			EOCommande commande = laCommande;
			isAfficherOngletSources = Boolean.FALSE;
			if (commande != null) {
				isAfficherOngletSources = utilisateurInContext(commande.editingContext()).isDroitEngager(commande);
			}
		}

		return isAfficherOngletSources.booleanValue();
	}

	public boolean isAfficherLIRepartitions() {
		if (!isAfficherLISources()) {
			return false;
		}
		boolean isAfficherLIRepartitions = false;
		EOCommande commande = laCommande;
		if (commande != null &&
				commande.commandeBudgets() != null &&
				commande.commandeBudgets().count() > 0 &&
				commande.articles() != null &&
				commande.articles().count() > 0) {

			isAfficherLIRepartitions = true;
		}
		return isAfficherLIRepartitions;
	}

	// Informations
	public boolean isPopupExercicesDisabled() {
		boolean isPopupExercicesDisabled = true;
		NSArray<EOExercice> exercices = session.accueilControleur().exercices();
		EOCommande commande = getLaCommande();
		if (commande != null) {
			NSArray<EOArticle> articles = commande.articles();
			NSArray<EOCommandeBudget> commandeBudgets = commande.commandeBudgets();
			if (isNouvelleCommande() &&
					exercices != null && exercices.count() > 1 &&
					(articles == null || articles.count() == 0) &&
					(commandeBudgets == null || commandeBudgets.count() == 0)) {
				isPopupExercicesDisabled = false;
			}
		}
		return isPopupExercicesDisabled;
	}

	// Sources
	public void supprimerUneSource() {
		FactoryCommande fc = session.getFactoryCommande();
		EOCommandeBudget laCommandeBudget = getUneCommandeBudget();

		// fc.supprimerSource(edc(), laCommande, laCommandeBudget);
		fc.supprimerSource(laCommande.editingContext(), laCommande, laCommandeBudget);
	}

	public NSArray<EOTauxProrata> lesProratas() {
		NSArray<EOTauxProrata> lesProratas = null;
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		EOCommandeBudget uneCommandeBudget = getUneCommandeBudget();
		EOOrgan unOrgan = uneCommandeBudget.organ();

		bindings.setObjectForKey(uneCommandeBudget.exercice(), "exercice");
		bindings.setObjectForKey(unOrgan, "organ");
		lesProratas = FinderProrata.getTauxProratasValides(laCommande.editingContext(), bindings);

		return lesProratas;
	}

	public EOCommandeBudget getUneCommandeBudget() {
		return uneCommandeBudget;
	}

	public void setUneCommandeBudget(EOCommandeBudget uneCommandeBudget) {
		this.uneCommandeBudget = uneCommandeBudget;
	}

	public EOTauxProrata getUnTauxProrata() {
		return unTauxProrata;
	}

	public void setUnTauxProrata(EOTauxProrata unTauxProrata) {
		this.unTauxProrata = unTauxProrata;
	}

	// Informations
	public String dateDuJour() {
		return DateCtrl.currentDateString();
	}

	// Articles

	public boolean isAuMoinsUnArticle() {
		boolean isAuMoinsUnArticle = true;
		if (dgArticles() != null && dgArticles().allObjects().count() == 0) {
			isAuMoinsUnArticle = false;
		}
		return isAuMoinsUnArticle;
	}

	public boolean isDisabledAjouterArticles() {
		boolean isDisabledAjouterArticles = true;
		if (dgArticles() != null && dgArticles().selectedObjects() != null && dgArticles().selectedObjects().count() > 0) {
			isDisabledAjouterArticles = false;
		}
		return isDisabledAjouterArticles;
	}

	// TODO Deplacer cette methode sur la session
	// avec un EDC en parametre
	public NSArray<EOTva> lesTauxTVA() {
		if (lesTauxTVA == null) {
			lesTauxTVA = FinderTva.getTvas(laCommande.editingContext());
		}
		return lesTauxTVA;
	}

	/*
	 * public NSArray lesCodesNomenclature() { NSArray lesCodesNomenclature = null; EOAttribution attribution = getUneAttribution(); EOEditingContext
	 * edc = laCommande.editingContext();
	 * 
	 * if (attribution != null) { lesCodesNomenclature = FinderCodeExer.getCodeExers(edc, attribution); } else { EOExercice exercice =
	 * FinderExercice.getExercicePourDate(edc, new NSTimestamp()); lesCodesNomenclature = FinderCodeExer.getCodeExerValidesPourExercice(edc,
	 * exercice); } return lesCodesNomenclature; }
	 */
	//	public NSArray lesCodesMarches() {
	//		NSArray lesCodesMarches = new NSArray();
	//
	//		return lesCodesMarches;
	//	}

	//	public NSArray lesAttributions() {
	//		NSArray lesAttributions = new NSArray();
	//
	//		return lesAttributions;
	//	}

	public WOComponent creerArticle() {
		return null;
	}

	public WOComponent modifierArticle() {
		ArticleModification nextPage = (ArticleModification) pageWithName("ArticleModification");
		nextPage.setArticle(getUnArticle());
		return nextPage;
	}

	public void supprimerUnArticle() {
		FactoryArticle fctArt = new FactoryArticle();

		fctArt.supprimer(laCommande.editingContext(), getUnArticle());
	}

	public WOComponent neFaitRien() {
		modificationEnCours = false;
		return null;
	}

	public EOArticle getUnArticle() {
		return unArticle;
	}

	public void setUnArticle(EOArticle unArticle) {
		this.unArticle = unArticle;
	}

	public String getArticleReference() {
		return getUnArticle().artReference();
	}

	public void setArticleReference(String reference) {
		if (!modificationEnCours) {
			getUnArticle().setArtReference(reference);
			modificationEnCours = true;
		}
	}

	public String getArticleLibelle() {
		return getUnArticle().artLibelleCourt();
	}

	public void setArticleLibelle(String libelle) {
		if (!modificationEnCours) {
			unArticle.setArtLibelle(libelle);
			modificationEnCours = true;
		}
	}

	public BigDecimal getArticlePrixHt() {
		return unArticle.artPrixHt();
	}

	public void setArticlePrixHt(BigDecimal articlePrixHt) {
		if (!modificationEnCours && articlePrixHt != null) {
			BigDecimal oldValue = getArticlePrixHt();
			if ((oldValue != null && oldValue.compareTo(articlePrixHt) != 0) ||
					(oldValue == null && articlePrixHt != null)) {
				this.articlePrixHt = articlePrixHt;
				unArticle.setArtPrixHt(articlePrixHt);
				modificationEnCours = true;
			}
		}
	}

	public EOTva getArticleTva() {
		return unArticle.tva();
	}

	public void setArticleTva(EOTva articleTva) {
		if (!modificationEnCours) {
			EOTva oldValue = getArticleTva();
			if ((oldValue != null && oldValue.equals(articleTva) == false) ||
					(oldValue == null && articleTva != null)) {
				this.articleTva = articleTva;
				//Rod 25/11/09 : modif pour coller aux templates velocity
				//unArticle.setTva(articleTva);
				unArticle.setTva(articleTva);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getArticleQte() {
		return unArticle.artQuantite();
	}

	public void setArticleQte(BigDecimal articleQte) {
		if (!modificationEnCours && articleQte != null) {
			BigDecimal oldValue = getArticleQte();
			if ((oldValue != null && oldValue.compareTo(articleQte) != 0) ||
					(oldValue == null && articleQte != null)) {
				this.articleQte = articleQte;
				unArticle.setArtQuantite(articleQte);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getArticlePrixTtc() {
		return unArticle.artPrixTtc();
	}

	public void setArticlePrixTtc(BigDecimal articlePrixTtc) {
		if (!modificationEnCours && articlePrixTtc != null) {
			BigDecimal oldValue = getArticlePrixTtc();
			if ((oldValue != null && oldValue.compareTo(articlePrixTtc) != 0) ||
					(oldValue == null && articlePrixTtc != null)) {
				this.articlePrixTtc = articlePrixTtc;
				unArticle.setArtPrixTtc(articlePrixTtc);
				modificationEnCours = true;
			}
		}
	}

	public void rechercherArticles() {
		@SuppressWarnings("unchecked")
		NSMutableDictionary<String, Object> bdgs = dgArticles().queryBindings();
		bdgs.setObjectForKey(laCommande.exercice(), "exercice");
		String codeFou = (String) bdgs.objectForKey("fouCode");
		if (codeFou != null && codeFou.equals("") == false) {
			codeFou += "*";
			bdgs.setObjectForKey(codeFou, "fouCode");
		}
		NSArray<EOCatalogueArticle> lesArticles = FinderArticle.getArticles(laCommande.editingContext(), bdgs);
		dgArticles().setObjectArray(lesArticles);
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public boolean IsCNGood() {
		boolean IsCNGood = false;
		EOArticle article = unArticle;

		if (article != null && article.codeExer() != null) {
			IsCNGood = true;
		}

		return IsCNGood;
	}
	
	//	public EOCatalogue getUnArticleSelectionnable() {
	//		return unArticleSelectionnable;
	//	}
	//
	//	public void setUnArticleSelectionnable(EOCatalogue unArticleSelectionnable) {
	//		this.unArticleSelectionnable = unArticleSelectionnable;
	//	}

	//	public boolean isArticleEligible() {
	//		boolean isArticleEligible = true;
	//		EOCatalogue article = getUnArticleSelectionnable();
	//		NSArray articlesDejaPresents = null;
	//		NSArray articles = laCommande.articles();
	//		if (articles != null && articles.count() > 0) {
	//			if (laCommande.isSurUnMarche()) {
	//				articlesDejaPresents = (NSArray) articles.valueForKeyPath(EOArticle.ATTRIBUTION_KEY);
	//			}
	//			else {
	//				articlesDejaPresents = (NSArray) articles.valueForKeyPath(EOArticle.ARTICLE_CATALOGUE_KEY);
	//			}
	//
	//			if (articlesDejaPresents.containsObject(article)) {
	//				isArticleEligible = false;
	//			}
	//		}
	//
	//		return isArticleEligible;
	//	}

	//
	//	public boolean isAfficherSelectionArticles() {
	//		boolean isAfficherSelectionArticles = true;
	//		if (getAttributionsCommunes() != null || getTypesAchatCommuns() != null) {
	//			isAfficherSelectionArticles = false;
	//		}
	//		return isAfficherSelectionArticles;
	//	}

	/*
	 * public boolean isValiderChoixMarcheTypeAchatDisabled() { boolean isValiderChoixMarcheTypeAchatDisabled = true;
	 * 
	 * if ((getUnTypeAchatCommunSelectionne() != null && getUneAttributionCommuneSelectionnee() == null) || (getUnTypeAchatCommunSelectionne() == null
	 * && getUneAttributionCommuneSelectionnee() != null)) { isValiderChoixMarcheTypeAchatDisabled = false; } return
	 * isValiderChoixMarcheTypeAchatDisabled; }
	 */
	//
	//	public WOComponent ajouterLesArticlesSelectionnes() {
	//		NSArray articles = dgArticles().selectedObjects();
	//		EOAttribution lAttribution = (EOAttribution) dgArticles().queryBindings().objectForKey("attribution");
	//		EOTypeAchat leTypeAchat = (EOTypeAchat) dgArticles().queryBindings().objectForKey("typeAchat");
	//
	//		if (articles.count() == 0) {
	//			articles = getLesArticlesSelectionnes();
	//		}
	//		if (lAttribution == null) {
	//			lAttribution = getUneAttributionCommuneSelectionnee();
	//		}
	//		if (leTypeAchat == null) {
	//			leTypeAchat = getUnTypeAchatCommunSelectionne();
	//		}
	//		try {
	//			session.getFactoryCommande().ajouterArticles(laCommande.editingContext(), laCommande, null, articles, lAttribution, leTypeAchat);
	//		} catch (FactoryException e) {
	//			session.setAlertMessage(e.getMessageFormatte());
	//		}
	//
	//		dgArticles().setSelectedObjects(null);
	//		lesArticlesSelectionnes = null;
	//
	//		return null;
	//	}

	//	public NSArray getAttributionsCommunes() {
	//		return attributionsCommunes;
	//	}
	//
	//	public void setAttributionsCommunes(NSArray attributionsCommunes) {
	//		this.attributionsCommunes = attributionsCommunes;
	//	}

	//	public NSArray getTypesAchatCommuns() {
	//		return typesAchatCommuns;
	//	}
	//
	//	public void setTypesAchatCommuns(NSArray typesAchatCommuns) {
	//		this.typesAchatCommuns = typesAchatCommuns;
	//	}

	//	public EOAttribution getUneAttributionCommune() {
	//		return uneAttributionCommune;
	//	}
	//
	//	public void setUneAttributionCommune(EOAttribution uneAttributionCommune) {
	//		this.uneAttributionCommune = uneAttributionCommune;
	//	}

	//	public EOTypeAchat getUnTypeAchatCommun() {
	//		return unTypeAchatCommun;
	//	}
	//
	//	public void setUnTypeAchatCommun(EOTypeAchat unTypeAchatCommun) {
	//		this.unTypeAchatCommun = unTypeAchatCommun;
	//	}
	//
	//	public EOAttribution getUneAttributionCommuneSelectionnee() {
	//		return uneAttributionCommuneSelectionnee;
	//	}
	//
	//	public void setUneAttributionCommuneSelectionnee(
	//			EOAttribution uneAttributionCommuneSelectionnee) {
	//		this.uneAttributionCommuneSelectionnee = uneAttributionCommuneSelectionnee;
	//	}
	//
	//	public EOTypeAchat getUnTypeAchatCommunSelectionne() {
	//		return unTypeAchatCommunSelectionne;
	//	}
	//
	//	public void setUnTypeAchatCommunSelectionne(EOTypeAchat unTypeAchatCommunSelectionne) {
	//		this.unTypeAchatCommunSelectionne = unTypeAchatCommunSelectionne;
	//	}

	//	public NSArray getLesArticlesSelectionnes() {
	//		return lesArticlesSelectionnes;
	//	}
	//
	//	public void setLesArticlesSelectionnes(NSArray lesArticlesSelectionnes) {
	//		this.lesArticlesSelectionnes = lesArticlesSelectionnes;
	//	}

	public String onClickOnglet() {
		String onClickOnglet = "document.getElementById('Form";
		onClickOnglet += getSelectedTab() + "').submit();return true;";
		return onClickOnglet;
	}

	public String popUpProratasId() {
		return "PopUpProratas_" + indexProratas;
	}

	public String articlePHTId() {
		return "ArticlePHT_" + indexArticles;
	}

	public String articleTVAId() {
		return "ArticleTVA_" + indexArticles;
	}

	public String articleQteId() {
		return "ArticleQte_" + indexArticles;
	}

	public String articlePTTCId() {
		return "ArticlePTTC_" + indexArticles;
	}

	public String articleTotalTTCId() {
		return "ArticleTotalTTC_" + indexArticles;
	}

	public String repartSourcesCodeExerHTId() {
		return "RepartSourcesCodeExerHT_" + indexCmdeBudgets + "_" + indexRepartSourcesCodeExer;
	}

	public String repartSourcesCodeExerTTCId() {
		return "RepartSourcesCodeExerTTC_" + indexCmdeBudgets + "_" + indexRepartSourcesCodeExer;
	}

	public String repartCmdeMarcheSourcesTotalHTId() {
		return "RepartCmdeMarcheSourcesTotalHT_" + indexCmdeMarcheBudgets + "_" + indexRepartCmdeMarcheSources;
	}

	public String repartCmdeMarcheSourcesTotalTTCId() {
		return "RepartCmdeMarcheSourcesTotalTTC_" + indexCmdeMarcheBudgets + "_" + indexRepartCmdeMarcheSources;
	}

	public NSDictionary<String, Object> getUneLigneHorsMarche() {
		return uneLigneHorsMarche;
	}

	public void setUneLigneHorsMarche(NSDictionary<String, Object> uneLigneHorsMarche) {
		this.uneLigneHorsMarche = uneLigneHorsMarche;
	}

	public EOCommandeBudget getUneCmdeBudget() {
		return uneCmdeBudget;
	}

	public void setUneCmdeBudget(EOCommandeBudget uneCmdeBudget) {
		this.uneCmdeBudget = uneCmdeBudget;
	}

	public EOCommandeControleHorsMarche getUneCmdeCtrlHorsMarche() {
		return uneCmdeCtrlHorsMarche;
	}

	public void setUneCmdeCtrlHorsMarche(EOCommandeControleHorsMarche uneCmdeCtrlHorsMarche) {
		this.uneCmdeCtrlHorsMarche = uneCmdeCtrlHorsMarche;
	}

	public BigDecimal getUneCmdeCtrlHorsMarcheHtSaisie() {
		return getUneCmdeCtrlHorsMarche().chomHtSaisie();
	}

	public void setUneCmdeCtrlHorsMarcheHtSaisie(BigDecimal uneCmdeCtrlHorsMarcheHtSaisie) {
		if (modificationEnCours == false && uneCmdeCtrlHorsMarcheHtSaisie != null) {
			BigDecimal oldValue = getUneCmdeCtrlHorsMarcheHtSaisie();
			if (oldValue != null && oldValue.floatValue() != uneCmdeCtrlHorsMarcheHtSaisie.floatValue() ||
					oldValue == null && uneCmdeCtrlHorsMarcheHtSaisie != null) {
				// this.uneCmdeCtrlHorsMarcheHtSaisie = uneCmdeCtrlHorsMarcheHtSaisie;
				getUneCmdeCtrlHorsMarche().setChomHtSaisie(uneCmdeCtrlHorsMarcheHtSaisie);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getUneCmdeCtrlHorsMarcheTtcSaisie() {
		return getUneCmdeCtrlHorsMarche().chomTtcSaisie();
	}

	public void setUneCmdeCtrlHorsMarcheTtcSaisie(BigDecimal uneCmdeCtrlHorsMarcheTtcSaisie) {
		if (modificationEnCours == false && uneCmdeCtrlHorsMarcheTtcSaisie != null) {
			BigDecimal oldValue = getUneCmdeCtrlHorsMarcheTtcSaisie();
			if (oldValue != null && oldValue.floatValue() != uneCmdeCtrlHorsMarcheTtcSaisie.floatValue() ||
					oldValue == null && uneCmdeCtrlHorsMarcheTtcSaisie != null) {
				// this.uneCmdeCtrlHorsMarcheTtcSaisie = uneCmdeCtrlHorsMarcheTtcSaisie;
				getUneCmdeCtrlHorsMarche().setChomTtcSaisie(uneCmdeCtrlHorsMarcheTtcSaisie);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getUneCmdeCtrlMarcheHtSaisie() {
		return getUneCmdeCtrlMarche().cmarHtSaisie();
	}

	public void setUneCmdeCtrlMarcheHtSaisie(BigDecimal uneCmdeCtrlMarcheHtSaisie) {
		if (modificationEnCours == false && uneCmdeCtrlMarcheHtSaisie != null) {
			BigDecimal oldValue = getUneCmdeCtrlMarcheHtSaisie();
			if (oldValue != null && oldValue.floatValue() != uneCmdeCtrlMarcheHtSaisie.floatValue() ||
					oldValue == null && uneCmdeCtrlMarcheHtSaisie != null) {
				getUneCmdeCtrlMarche().setCmarHtSaisie(uneCmdeCtrlMarcheHtSaisie);
				modificationEnCours = true;
			}
		}
	}

	public BigDecimal getUneCmdeCtrlMarcheTtcSaisie() {
		return getUneCmdeCtrlMarche().cmarTtcSaisie();
	}

	public void setUneCmdeCtrlMarcheTtcSaisie(BigDecimal uneCmdeCtrlMarcheTtcSaisie) {
		if (modificationEnCours == false && uneCmdeCtrlMarcheTtcSaisie != null) {
			BigDecimal oldValue = getUneCmdeCtrlMarcheTtcSaisie();
			if (oldValue != null && oldValue.floatValue() != uneCmdeCtrlMarcheTtcSaisie.floatValue() ||
					oldValue == null && uneCmdeCtrlMarcheTtcSaisie != null) {
				getUneCmdeCtrlMarche().setCmarTtcSaisie(uneCmdeCtrlMarcheTtcSaisie);
				modificationEnCours = true;
			}
		}
	}

	public EOTva getUnTauxTVA() {
		return unTauxTVA;
	}

	public void setUnTauxTVA(EOTva unTauxTVA) {
		this.unTauxTVA = unTauxTVA;
	}

	public EOCommandeControleMarche getUneCmdeCtrlMarche() {
		return uneCmdeCtrlMarche;
	}

	public void setUneCmdeCtrlMarche(EOCommandeControleMarche uneCmdeCtrlMarche) {
		this.uneCmdeCtrlMarche = uneCmdeCtrlMarche;
	}

	public EOCommandeBudget getUneCmdeBudgetForAction() {
		return uneCmdeBudgetForAction;
	}

	public void setUneCmdeBudgetForAction(EOCommandeBudget uneCmdeBudgetForAction) {
		this.uneCmdeBudgetForAction = uneCmdeBudgetForAction;
	}

	public NSArray<EOTypeAction> getLesActions() {
		EOCommandeBudget db = getUneCmdeBudgetForAction();
		lesActions = (NSArray<EOTypeAction>) dicoLesActions.objectForKey(db.source().libelle());
		return lesActions;
	}

	public void setLesActions(NSArray<EOTypeAction> lesActions) {
		this.lesActions = lesActions;
	}

	public EOCommandeBudget getUneCmdeMarcheBudget() {
		return uneCmdeMarcheBudget;
	}

	public void setUneCmdeMarcheBudget(EOCommandeBudget uneCmdeMarcheBudget) {
		this.uneCmdeMarcheBudget = uneCmdeMarcheBudget;
	}

	/*
	 * public boolean afficheRepartSourceLibelle() { boolean afficheRepartSourceLibelle = false;
	 * 
	 * if (indexCmdeControleAction == 0) { afficheRepartSourceLibelle = true; }
	 * 
	 * return afficheRepartSourceLibelle; }
	 */
	public int nbreCmdeControleAction() {
		return getUneCmdeBudgetForAction().commandeControleActions().count();
	}

	public WOComponent repartitionAutomatique() {
		laCommande.setRepartitionAutomatiquePossible();
		return null;
	}

	public NSArray<EOPlanComptable> getLesImputations() {
		EOCommandeBudget db = getUneCmdeBudgetForImputation();
		lesImputations = (NSArray<EOPlanComptable>) dicoLesPlansComptable.objectForKey(db.source().libelle());
		return lesImputations;
	}

	public void setLesImputations(NSArray<EOPlanComptable> lesImputations) {
		this.lesImputations = lesImputations;
	}

	public EOCommandeBudget getUneCmdeBudgetForImputation() {
		return uneCmdeBudgetForImputation;
	}

	public void setUneCmdeBudgetForImputation(EOCommandeBudget uneCmdeBudgetForImputation) {
		this.uneCmdeBudgetForImputation = uneCmdeBudgetForImputation;
	}

	public NSArray<EOConvention> getLesConventions() {
		EOCommandeBudget db = getUneCmdeBudgetForConvention();
		lesConventions = (NSArray<EOConvention>) dicoLesConventions.objectForKey(db.source().libelle());
		return lesConventions;
	}

	public void setLesConventions(NSArray<EOConvention> lesConventions) {
		this.lesConventions = lesConventions;
	}

	//
	public EOCommandeControleConvention getUneCmdeCtrlConvention() {
		return uneCmdeCtrlConvention;
	}

	public void setUneCmdeCtrlConvention(EOCommandeControleConvention uneCmdeCtrlConvention) {
		this.uneCmdeCtrlConvention = uneCmdeCtrlConvention;
	}

	public EOCommandeBudget getUneCmdeBudgetForConvention() {
		return uneCmdeBudgetForConvention;
	}

	public void setUneCmdeBudgetForConvention(EOCommandeBudget uneCmdeBudgetForConvention) {
		this.uneCmdeBudgetForConvention = uneCmdeBudgetForConvention;
	}

	public Boolean isConventionModifiable() {
		return !isConventionDisabled();

	}

	public boolean isConventionDisabled() {
		boolean isConventionDisabled = true;
		EOConvention convention = getUneCmdeCtrlConvention().convention();
		if (convention == null || (convention != null && convention.isRessourceAffectee() == false)) {
			isConventionDisabled = false;
		}
		return isConventionDisabled;
	}

	public NSArray<EOCodeAnalytique> getLesCodeAnalytiques() {
		EOCommandeBudget db = getUneCmdeBudgetForCodeAnalytique();
		lesCodeAnalytiques = dicoLesCodesAnalytique.objectForKey(db.source().libelle());
		return lesCodeAnalytiques;
	}

	public void setLesCodeAnalytiques(NSArray<EOCodeAnalytique> lesCodeAnalytiques) {
		this.lesCodeAnalytiques = lesCodeAnalytiques;
	}

	public EOCommandeBudget getUneCmdeBudgetForCodeAnalytique() {
		return uneCmdeBudgetForCodeAnalytique;
	}

	public void setUneCmdeBudgetForCodeAnalytique(EOCommandeBudget uneCmdeBudgetForCodeAnalytique) {
		this.uneCmdeBudgetForCodeAnalytique = uneCmdeBudgetForCodeAnalytique;
	}

	public EOCodeExer getUnCodeNomenclature() {
		return unCodeNomenclature;
	}

	public void setUnCodeNomenclature(EOCodeExer unCodeNomenclature) {
		this.unCodeNomenclature = unCodeNomenclature;
	}

	public EOAttribution getUneAttribution() {
		return uneAttribution;
	}

	public void setUneAttribution(EOAttribution uneAttribution) {
		this.uneAttribution = uneAttribution;
	}

	public EOCodeMarche getUnCodeMarche() {
		return unCodeMarche;
	}

	public void setUnCodeMarche(EOCodeMarche unCodeMarche) {
		this.unCodeMarche = unCodeMarche;
	}

	public String getUnCodeMarcheValue() {
		return unCodeMarcheValue;
	}

	public void setUnCodeMarcheValue(String unCodeMarcheValue) {
		this.unCodeMarcheValue = unCodeMarcheValue;
	}

	public String getUnCodeNomenclatureValue() {
		return unCodeNomenclatureValue;
	}

	public void setUnCodeNomenclatureValue(String unCodeNomenclatureValue) {
		this.unCodeNomenclatureValue = unCodeNomenclatureValue;
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		String message = session.getAlertMessage();
		if (message != null && message.equals("") == false) {
			String str = "<script language=\"javascript\" type=\"text/javascript\">\n";
			str += "alert(\"" + message + "\");\n";
			str += "</script>";
			res.appendContentString(str);
			session.setAlertMessage(null);
		}
		if (onloadJS != null)
			onloadJS = null;
	}

	public BigDecimal getUneCommandeBudgetDisponible() {
		EOCommandeBudget cmdeBud = getUneCommandeBudget();
		if (cmdeBud != null && cmdeBud.source() != null) {
			NSArray<EOBudgetExecCredit> budgets = FinderBudget.getBudgetsPourSource(laCommande.editingContext(), cmdeBud.source());
			if (budgets != null && budgets.count() > 0) {
				uneCommandeBudgetDisponible = ((EOBudgetExecCredit) budgets.lastObject()).bdxcDisponible();
			}
		}
		return uneCommandeBudgetDisponible;
	}

	public void setUneCommandeBudgetDisponible(BigDecimal uneCommandeBudgetDisponible) {
		this.uneCommandeBudgetDisponible = uneCommandeBudgetDisponible;
	}

	public String getOnloadJS() {
		return onloadJS;
	}

	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	public EOCommande getLaCommande() {
		return laCommande;
	}

	public void setLaCommande(EOCommande laCommande) {
		this.laCommande = laCommande;
	}

	public void ajouterUneSourceAuxPreferes() {
		EOCommandeBudget laCommandeBudget = getUneCommandeBudget();
		EOOrgan organ = laCommandeBudget.organ();
		ProcessPreference.insererOrganPrefere(session.dataBus(), utilisateurInContext(nestedEdc()), organ);
	}

	public EOExercice getExerciceSelectionne() {
		if (laCommande != null && laCommande.exercice() != null) {
			EOExercice exercice = laCommande.exercice();
			if (exercice.editingContext().equals(edc()) == false) {
				// L'exercice selectionne doit-etre dans l'edc de la liste des exercices possibles
				// Lors de la modification d'une commande, il faut donc recuperer l'exercice selectionne
				// a partir de l'exercice de la commande
				exerciceSelectionne = exercice.exerciceInContext(edc());
			}
		}
		return exerciceSelectionne;
	}

	public void setExerciceSelectionne(EOExercice exerciceSelectionne) {
		this.exerciceSelectionne = exerciceSelectionne;
		if (exerciceSelectionne != null && laCommande != null) {
			session.getFactoryCommande().affecterExercice(laCommande, exerciceSelectionne.exerciceInContext(laCommande.editingContext()));
			// Reinitialisation du boolean pour forcer le recalcul
			isAfficherOngletSources = null;
		}
	}

	public void setTauxProrata(EOTauxProrata prorata) {
		if (uneCommandeBudget != null) {
			uneCommandeBudget.setTauxProrataRelationship(prorata);
		}
	}

	public EOTauxProrata getTauxProrata() {
		if (uneCommandeBudget != null) {
			return uneCommandeBudget.tauxProrata();
		}
		return null;

	}

	public String popupProrataNoSelectionString() {
		if (myApp().preselectionTauxProrata().booleanValue()) {
			return null;
		}
		return "-";
	}

	public Boolean isSupprimerArticleDisabled() {
		return Boolean.valueOf(laCommandeEnCours().isCommandeB2b());
	}

	public Boolean isNotArticleModifiable() {
		return Boolean.valueOf(laCommandeEnCours().isSurUnMarcheCatalogue() || laCommandeEnCours().isCommandeB2b());
	}

	public static String openWaitDialogFunctionNameForModifPanier() {
		return "openWaitDialogB2bConnexion_ModifPanier";
	}

	public String getOpenWaitDialogFunctionNameForModifPanier() {
		return openWaitDialogFunctionNameForModifPanier();
	}

	public String waitingPanelMsgModifPanier() {
		return "Connexion au site marchand en cours...<br/><br/>" + (fournisB2bCxmlParam() != null ? fournisB2bCxmlParam().libelle() : "");
	}

	public EOFournisB2bCxmlParam fournisB2bCxmlParam() {
		if (laCommandeEnCours().toB2bCxmlPunchoutmsgs().count() > 0) {
			EOFournisB2bCxmlParam fournisB2bCxmlParam = ((EOB2bCxmlPunchoutmsg) laCommandeEnCours().toB2bCxmlPunchoutmsgs().lastObject()).toFournisB2bCxmlParam();
			return fournisB2bCxmlParam;
		}
		return null;
	}

	public static String modifPanierOnClick() {
		String res = openWaitDialogFunctionNameForModifPanier() + "();return true;";
		return res;
	}

	public Boolean isNotArticleQteModifiable() {
		Boolean res = Boolean.FALSE;
		if (laCommandeEnCours().isCommandeB2b()) {
			if (laCommandeEnCours().toB2bCxmlOrderRequests().count() > 0) {
				res = Boolean.TRUE;
			}
		}
		return res;
	}

	public String pageTitle() {
		return "Création d'une commande";
	}

	public EOFournis getFournis() {
		if (getLaCommande() == null) {
			return null;
		}
		if (getLaCommande().fournisseur() == null) {
			return null;
		}
		EOFournis fournis = EOFournis.fetchByKeyValue(getLaCommande().fournisseur().editingContext(), EOFournis.FOU_ORDRE_KEY, getLaCommande().fournisseur().fouOrdre());
		return fournis;
	}
}