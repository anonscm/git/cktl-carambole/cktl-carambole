/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

// Generated by the WOLips Templateengine Plug-in at 13 juil. 2007 07:22:38

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class CommandeListe extends PreCommandeListe {

	private static final long serialVersionUID = 1L;
	private String onloadJS = null;
	private NSArray<NSDictionary<String, Object>> lesCommandes = null;
	public NSArray<NSDictionary<String, Object>> lesCommandesSelectionnees = null;
	// public NSArray lesUtilisateurs = null;
	public EOUtilisateur unUtilisateur = null;
	public Boolean isFiltreEtatPreCommandeChecked = Boolean.FALSE;
	public Boolean filtreEtatPreCommande = Boolean.TRUE;
	public Boolean isFiltreEtatPartiellementEngageeChecked = Boolean.FALSE;
	public Boolean filtreEtatPartiellementEngagee = Boolean.TRUE;
	public Boolean isFiltreEtatEngageeChecked = Boolean.FALSE;
	public Boolean filtreEtatEngagee = Boolean.TRUE;
	public Boolean isFiltreEtatPartiellementSoldeeChecked = Boolean.FALSE;
	public Boolean filtreEtatPartiellementSoldee = Boolean.TRUE;
	public Boolean isFiltreEtatSoldeeChecked = Boolean.FALSE;
	public Boolean filtreEtatSoldee = Boolean.TRUE;
	public Boolean isFiltreEtatAnnuleeChecked = Boolean.FALSE;
	public Boolean filtreEtatAnnulee = Boolean.TRUE;
	public Boolean isToutesLesCommandesSelectionnees = Boolean.FALSE;

	public boolean isBasculeEnCours = false;

	public CommandeListe(WOContext context) {
		super(context);
		setDgActif(dgCommande());
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
		if (onloadJS != null)
			onloadJS = null;
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				"Accueil"
		});
		WODisplayGroup dg = dgCommande();
		if (dg != null && dg.allObjects().count() > 0) {
			items = new NSArray<String>(new String[] {
					"ModifierLaRecherche",
					"Accueil"
			});
		}
		if (dg != null && dg.selectedObjects() != null && dg.selectedObjects().count() > 0) {
			items = new NSArray<String>(new String[] {
					"ModifierLaRecherche",
					"SolderEnMasse",
					"DupliquerEnMasse",
					"BasculerEnMasse",
					"Accueil"
			});
		}
		return items;
	}

	public String classForTypeEtatCommande() {
		String typeEtat = (String) getLaCommande().objectForKey("TYPEETAT");
		return typeEtat.toLowerCase();
	}

	public boolean isUtilisateurAvecDroitsTraitementEnMasse() {
		boolean isUtilisateurAvecDroitsTraitementEnMasse = false;
		if (dg != null && dg.allObjects().count() > 0) {
			EOExercice exercice = (EOExercice) dg.queryBindings().objectForKey("exercice");
			if (exercice != null && utilisateur().isUtilisateurEnMasse(exercice, null)) {
				isUtilisateurAvecDroitsTraitementEnMasse = true;
			}
		}
		return isUtilisateurAvecDroitsTraitementEnMasse;
	}

	public WOActionResults rechercher() {
		WODisplayGroup dgC = getDg();
		WODisplayGroup dgE = dgEngagement();
		dgE.setSelectedObjects(null);
		dgE.setObjectArray(null);
		WODisplayGroup dgF = dgFacture();
		dgF.setSelectedObjects(null);
		dgF.setObjectArray(null);
		@SuppressWarnings("unchecked")
		NSMutableDictionary<String, Object> bdgs = dgC.queryBindings();

		if (bdgs.objectForKey("exercice") == null) {
			EOExercice exercice = FinderExercice.getExercicePourDate(edc(), new NSTimestamp());
			bdgs.setObjectForKey(exercice, "exercice");
		}
		if (bdgs.objectForKey("utilisateur") == null) {
			EOUtilisateur utilisateur = utilisateurInContext(edc());
			bdgs.setObjectForKey(utilisateur, "utilisateur");
		}
		NSMutableArray<String> typesEtatLibelle = new NSMutableArray<String>();
		if (isFiltreEtatPreCommandeChecked != null && isFiltreEtatPreCommandeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PRECOMMANDE);
		}
		if (isFiltreEtatPartiellementEngageeChecked != null && isFiltreEtatPartiellementEngageeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);
		}
		if (isFiltreEtatEngageeChecked != null && isFiltreEtatEngageeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_ENGAGEE);
		}
		if (isFiltreEtatPartiellementSoldeeChecked != null && isFiltreEtatPartiellementSoldeeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE);
		}
		if (isFiltreEtatSoldeeChecked != null && isFiltreEtatSoldeeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_SOLDEE);
		}
		if (isFiltreEtatAnnuleeChecked != null && isFiltreEtatAnnuleeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_ANNULEE);
		}
		bdgs.setObjectForKey(typesEtatLibelle, "lesTypesEtat");
		int nbCommandes = FinderCommande.getRawRowCountCommandes(edc(), bdgs);
		if (nbCommandes > 0 && nbCommandes <= 500) {
			lesCommandes = FinderCommande.getRawRowCommandes(edc(), bdgs);
			session.setAlertMessage(null);
		}
		else {
			lesCommandes = null;
			if (nbCommandes > 500) {
				session.setAlertMessage(nbCommandes + " commandes trouvées. Veuillez affiner la recherche.");
			}
			else {
				session.setAlertMessage("Aucune commande trouvée.");
			}
		}
		dgC.setObjectArray(lesCommandes);
		dgC.setSelectedObject(null);

		return null;
		// return filtrer();
	}

	public WOComponent detailler() {
		WOComponent nextPage = super.detailler();

		return nextPage;
	}

	@SuppressWarnings("unchecked")
	public WOComponent filtrer() {
		NSMutableDictionary<String, Object> bdgs = dg.queryBindings();

		NSMutableArray<String> typesEtatLibelle = new NSMutableArray<String>();
		if (isFiltreEtatPreCommandeChecked != null && isFiltreEtatPreCommandeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PRECOMMANDE);
		}
		if (isFiltreEtatPartiellementEngageeChecked != null && isFiltreEtatPartiellementEngageeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);
		}
		if (isFiltreEtatEngageeChecked != null && isFiltreEtatEngageeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_ENGAGEE);
		}
		if (isFiltreEtatPartiellementSoldeeChecked != null && isFiltreEtatPartiellementSoldeeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE);
		}
		if (isFiltreEtatSoldeeChecked != null && isFiltreEtatSoldeeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_SOLDEE);
		}
		if (isFiltreEtatAnnuleeChecked != null && isFiltreEtatAnnuleeChecked.booleanValue()) {
			typesEtatLibelle.addObject(EOCommande.ETAT_ANNULEE);
		}
		bdgs.setObjectForKey(typesEtatLibelle, "lesTypesEtat");
		EOUtilisateur createur = (EOUtilisateur) bdgs.objectForKey("createur");

		lesCommandes = dg.allObjects();
		if (createur != null) {
			bdgs.setObjectForKey(createur, "createur");
			//EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("NOMPRENOM=%@", new NSArray(createur.nomPrenom()));
			EOQualifier qual = ERXQ.equals("NOMPRENOM", createur.nomPrenom());
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, qual);
		}

		if (lesCommandes != null && lesCommandes.count() > 0 &&
				typesEtatLibelle != null && typesEtatLibelle.count() > 0) {
			EOOrQualifier qualTypeEtat = null;
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			Enumeration<String> enumTypeEtat = typesEtatLibelle.objectEnumerator();
			while (enumTypeEtat.hasMoreElements()) {
				String unEtat = (String) enumTypeEtat.nextElement();
				//				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("TYPEETAT=%@", new NSArray(unEtat));
				EOQualifier qual = ERXQ.equals("TYPEETAT", unEtat);
				orQualifiers.addObject(qual);
			}
			qualTypeEtat = new EOOrQualifier(orQualifiers);
			lesCommandes = EOQualifier.filteredArrayWithQualifier(lesCommandes, qualTypeEtat);
		}

		return null;
	}

	public Boolean isFiltreEtatEngageeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_ENGAGEE)) {
					isFiltreEtatEngageeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatEngageeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatEngageeChecked;
	}

	public void setIsFiltreEtatEngageeChecked(Boolean isFiltreEtatEngageeChecked) {
		this.isFiltreEtatEngageeChecked = isFiltreEtatEngageeChecked;
	}

	public Boolean isFiltreEtatPreCommandeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_PRECOMMANDE)) {
					isFiltreEtatPreCommandeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatPreCommandeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatPreCommandeChecked;
	}

	public void setIsFiltreEtatPreCommandeChecked(Boolean isFiltreEtatPreCommandeChecked) {
		this.isFiltreEtatPreCommandeChecked = isFiltreEtatPreCommandeChecked;
	}

	public Boolean isFiltreEtatAnnuleeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_ANNULEE)) {
					isFiltreEtatAnnuleeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatAnnuleeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatAnnuleeChecked;
	}

	public void setIsFiltreEtatAnnuleeChecked(Boolean isFiltreEtatAnnuleeChecked) {
		this.isFiltreEtatAnnuleeChecked = isFiltreEtatAnnuleeChecked;
	}

	public Boolean isFiltreEtatPartiellementEngageeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE)) {
					isFiltreEtatPartiellementEngageeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatPartiellementEngageeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatPartiellementEngageeChecked;
	}

	public void setIsFiltreEtatPartiellementEngageeChecked(Boolean isFiltreEtatPartiellementEngageeChecked) {
		this.isFiltreEtatPartiellementEngageeChecked = isFiltreEtatPartiellementEngageeChecked;
	}

	public Boolean isFiltreEtatPartiellementSoldeeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE)) {
					isFiltreEtatPartiellementSoldeeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatPartiellementSoldeeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatPartiellementSoldeeChecked;
	}

	public void setIsFiltreEtatPartiellementSoldeeChecked(Boolean isFiltreEtatPartiellementSoldeeChecked) {
		this.isFiltreEtatPartiellementSoldeeChecked = isFiltreEtatPartiellementSoldeeChecked;
	}

	public Boolean isFiltreEtatSoldeeChecked() {
		if (dg != null) {
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dg.queryBindings();
			if (bdgs != null) {
				@SuppressWarnings("unchecked")
				NSArray<String> lesTypesEtat = (NSArray<String>) bdgs.objectForKey("lesTypesEtat");
				if (lesTypesEtat != null && lesTypesEtat.containsObject(EOCommande.ETAT_SOLDEE)) {
					isFiltreEtatSoldeeChecked = Boolean.TRUE;
				}
				else {
					isFiltreEtatSoldeeChecked = Boolean.FALSE;
				}
			}
		}
		return isFiltreEtatSoldeeChecked;
	}

	public void setIsFiltreEtatSoldeeChecked(Boolean isFiltreEtatSoldeeChecked) {
		this.isFiltreEtatSoldeeChecked = isFiltreEtatSoldeeChecked;
	}

	public Boolean isFiltreEtatAnnulee() {
		return filtreEtatAnnulee;
	}

	public void setFiltreEtatAnnulee(Boolean filtreEtatAnnulee) {
		this.filtreEtatAnnulee = filtreEtatAnnulee;
	}

	public Boolean isFiltreEtatEngagee() {
		return filtreEtatEngagee;
	}

	public void setFiltreEtatEngagee(Boolean filtreEtatEngagee) {
		this.filtreEtatEngagee = filtreEtatEngagee;
	}

	public Boolean isFiltreEtatPartiellementEngagee() {
		return filtreEtatPartiellementEngagee;
	}

	public void setFiltreEtatPartiellementEngagee(Boolean filtreEtatPartiellementEngagee) {
		this.filtreEtatPartiellementEngagee = filtreEtatPartiellementEngagee;
	}

	public Boolean isFiltreEtatPartiellementSoldee() {
		return filtreEtatPartiellementSoldee;
	}

	public void setFiltreEtatPartiellementSoldee(Boolean filtreEtatPartiellementSoldee) {
		this.filtreEtatPartiellementSoldee = filtreEtatPartiellementSoldee;
	}

	public Boolean isFiltreEtatSoldee() {
		return filtreEtatSoldee;
	}

	public void setFiltreEtatSoldee(Boolean filtreEtatSoldee) {
		this.filtreEtatSoldee = filtreEtatSoldee;
	}

	public String getOnloadJS() {
		return onloadJS;
	}

	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	@SuppressWarnings("unchecked")
	public NSArray<NSDictionary<String, Object>> getLesCommandes() {
		if (lesCommandes == null) {
			lesCommandes = getDg().allObjects();
		}
		return lesCommandes;
	}

	public void setLesCommandes(NSArray<NSDictionary<String, Object>> lesCommandes) {
		this.lesCommandes = lesCommandes;
	}

	public WOComponent selectionnerCommandes() {
		return null;
	}

	public WOComponent toggleSelectCommandes() {
		WODisplayGroup dgC = getDg();

		if (isToutesLesCommandesSelectionnees.booleanValue() == false) {
			dgC.setSelectedObjects(dgC.allObjects());
			isToutesLesCommandesSelectionnees = Boolean.TRUE;
		}
		else {
			dgC.setSelectedObjects(null);
			isToutesLesCommandesSelectionnees = Boolean.FALSE;
		}

		return null;
	}

	public Boolean getIsToutesLesCommandesSelectionnees() {
		//		if (getDg()!=null && getDg().selectedObjects().count()==getDg().allObjects().count()) {
		//			isToutesLesCommandesSelectionnees = Boolean.TRUE;
		//		} else {
		//			isToutesLesCommandesSelectionnees = Boolean.FALSE;
		//		}
		return isToutesLesCommandesSelectionnees;
	}

	public void setIsToutesLesCommandesSelectionnees(Boolean isToutesLesCommandesSelectionnees) {
		this.isToutesLesCommandesSelectionnees = isToutesLesCommandesSelectionnees;
		if (isToutesLesCommandesSelectionnees.booleanValue()) {
			getDg().setSelectedObjects(getDg().allObjects());
		}
		else {
			getDg().setSelectedObjects(null);
		}
		// lesCommandesSelectionnees = dg.selectedObjects();
	}

	public NSArray<NSDictionary<String, Object>> getLesCommandesSelectionnees() {
		return lesCommandesSelectionnees;
	}

	public void setLesCommandesSelectionnees(NSArray<NSDictionary<String, Object>> lesCommandesSelectionnees) {
		this.lesCommandesSelectionnees = lesCommandesSelectionnees;
		if (!isBasculeEnCours()) {
			getDg().setSelectedObjects(lesCommandesSelectionnees);
		}
	}

	/**
	 * @return the isBasculeEnCours
	 */
	public boolean isBasculeEnCours() {
		return isBasculeEnCours;
	}

	/**
	 * @param isBasculeEnCours the isBasculeEnCours to set
	 */
	public void setIsBasculeEnCours(boolean isBasculeEnCours) {
		this.isBasculeEnCours = isBasculeEnCours;
	}

	public String containerUtilId() {
		return getComponentId() + "_utils";
	}

}