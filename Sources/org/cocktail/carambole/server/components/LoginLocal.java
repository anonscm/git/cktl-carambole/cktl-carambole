/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.VersionMe;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;

/**
 * Le gestionnaire de la page de login "local". L'authentification de l'utilisateur est effectuee par l'application elle-meme.
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public class LoginLocal extends LoginCAS {

	private static final long serialVersionUID = 1L;
	/** Le gestionnaire des evenements de composant de login local. */
	private CktlLoginResponder loginResponder;

	/**
	 * Cree une nouvelle instance de la page.
	 */
	public LoginLocal(WOContext context) {
		super(context);
	}

	/**
	 * Retourne la reference vers une instance de gestionnaire des evenements du composant de login local.
	 */
	public CktlLoginResponder loginResponder() {
		return loginResponder;
	}

	/**
	 * Definit le gestionnaire des evenements du composant de login local.
	 */
	public void registerLoginResponder(CktlLoginResponder responder) {
		loginResponder = responder;
	}

	public String copyright() {
		return ((Application) application()).copyright();
		//		return VersionMe.copyright();
	}

	public String bdServerId() {
		return Application.bdServerId;
	}

	public String version() {
		return VersionMe.htmlAppliVersion();
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "effects.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "wonder.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwonder.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxwebext.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwindowsopeners.js");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		CktlAjaxUtils.addScriptResourceInHead(context, response, "controls.js");
		//
		//	CktlAjaxUtils.addScriptResourceInHead(context, response, null, "scripts/filterlist.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, null, "scripts/dateformat.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, null, "scripts/montantformat.js");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonVert.css");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/mac_os_x.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, null, "css/carambole.css");
	}

}