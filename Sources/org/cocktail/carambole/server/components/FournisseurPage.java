/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktldepense.server.FwkCktlDepense;
import org.cocktail.fwkcktldepense.server.FwkCktlDepenseHelper;
import org.cocktail.fwkcktldepense.server.FwkCktlDepenseParamManager;
import org.cocktail.fwkcktldepense.server.finder.FinderFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

public class FournisseurPage extends MyAjaxComponent implements CktlResourceProvider {

	private static final long serialVersionUID = 1L;
	private EOFournis selectedFournisseur;
	private Boolean resetSrchForm = Boolean.FALSE;

	public FournisseurPage(WOContext context) {
		super(context);
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ACCUEIL_KEY
		});
		return items;
	}

	public EOFournis getSelectedFournisseur() {
		return selectedFournisseur;
	}

	public void setSelectedFournisseur(EOFournis selectedFournisseur) {
		this.selectedFournisseur = selectedFournisseur;
	}

	public WOActionResults onSelectionnerFournisseur() {
		session.defaultEditingContext().revert();
		Accueil accueil = (Accueil) session.getSavedPageWithName(Accueil.class.getName());
		if (selectedFournisseur != null) {
			EOFournisseur fournisseur = FinderFournisseur.getFournisseur(selectedFournisseur.editingContext(), selectedFournisseur.fouCode());
			if (fournisseur != null) {
				return accueil.creerCommandeAvecFournisseur(fournisseur);
			}
		}
		return accueil;
	}

	public Boolean getResetSrchForm() {
		return resetSrchForm;
	}

	public void setResetSrchForm(Boolean resetSrchForm) {
		this.resetSrchForm = resetSrchForm;
	}

	@Override
	public void appendToResponse(WOResponse arg0, WOContext arg1) {
		super.appendToResponse(arg0, arg1);
		//		CktlERXResponseRewriter.addStylesheetResourceInHead(arg0, arg1, "FwkCktlThemes.framework", "css/" + "CktlCommon.css");
		//		CktlERXResponseRewriter.addStylesheetResourceInHead(arg0, arg1, "FwkCktlThemes.framework", "css/" + "CktlCommonVert.css");
	}

	public String formId() {
		return "fournisseurForm";
	}

	public void injectResources(WOResponse response, WOContext context) {
		MyAjaxPage.injectResourcesInResponse(response, context);
	}

	public String typeFournisseurDefaut() {
		return FwkCktlDepense.paramManager.getParam(FwkCktlDepenseParamManager.CARAMBOLE_TYPE_FOURNISSEUR_DEFAUT);
	}

}