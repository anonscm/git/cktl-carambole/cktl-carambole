/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.Session;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLTransaction;
import org.cocktail.fwkcktlb2b.cxml.server.engine.ProxyProperties;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemOut;
import org.cocktail.fwkcktldepense.server.b2b.factory.B2bUtils;
import org.cocktail.fwkcktldepense.server.b2b.process.ProcessGestionCommandeB2b;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlOrderRequest;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXResponseRewriter;
import er.extensions.eof.ERXEC;

public class B2BOrderRequestUI extends CktlAjaxWOComponent implements CktlResourceProvider {

	private static final long serialVersionUID = 1L;
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";
	//	public final static String BINDING_cxmlParameters = "cxmlParameters";
	//public final static String BINDING_fournisB2bCxmlParam = "fournisB2bCxmlParam";
	public final static String BINDING_commande = "commande";

	public final static String BINDING_proxyProperties = "proxyProperties";
	//public final static String BINDING_itemOuts = "itemOuts";
	public final static String BINDING_result = "result";
	public final static String BINDING_resultError = "resultError";
	public final static String BINDING_updateContainerID = "updateContainerID";
	public final static String BINDING_services = "services";
	//public final static String BINDING_orderID = "orderID";

	public final static String RESULT_CANCELED = "0";
	public final static String RESULT_ERROR = "-1";
	public final static String RESULT_SUCCEED = "1";

	private EOCommande commande;
	//private EOFournis fournis;

	private IPersonne shipToPersonne;
	private EOAdresse shipToPostalAdresse;
	public EOPersonneTelephone unShipToPersonneTelephone;
	public EOPersonneTelephone selectedShipToTelephone;
	private Map<EOPersonneTelephone, Boolean> unShipToPersonneTelephoneSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public EOPersonneTelephone unShipToPersonneFax;
	public EOPersonneTelephone selectedShipToFax;
	private Map<EOPersonneTelephone, Boolean> unShipToPersonneFaxSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public String unShipToEmail;
	public String selectedShipToEmail;
	private Map<String, Boolean> unShipToEmailSelectedDic = new LinkedHashMap<String, Boolean>();
	private EORepartPersonneAdresse shipToSelectedRepartPersonneAdresse;

	private IPersonne billToPersonne;
	private EOAdresse billToPostalAdresse;
	public EOPersonneTelephone unBillToPersonneTelephone;
	public EOPersonneTelephone selectedBillToTelephone;
	private Map<EOPersonneTelephone, Boolean> unBillToPersonneTelephoneSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public EOPersonneTelephone unBillToPersonneFax;
	public EOPersonneTelephone selectedBillToFax;
	private Map<EOPersonneTelephone, Boolean> unBillToPersonneFaxSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public String unBillToEmail;
	public String selectedBillToEmail;
	private Map<String, Boolean> unBillToEmailSelectedDic = new LinkedHashMap<String, Boolean>();
	private EORepartPersonneAdresse billToSelectedRepartPersonneAdresse;

	private IPersonne contactPersonne;
	private EOAdresse contactPostalAdresse;
	public EOPersonneTelephone unContactPersonneTelephone;
	public EOPersonneTelephone selectedContactTelephone;
	private Map<EOPersonneTelephone, Boolean> unContactPersonneTelephoneSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public EOPersonneTelephone unContactPersonneFax;
	public EOPersonneTelephone selectedContactFax;
	private Map<EOPersonneTelephone, Boolean> unContactPersonneFaxSelectedDic = new LinkedHashMap<EOPersonneTelephone, Boolean>();
	public String unContactEmail;
	public String selectedContactEmail;
	private Map<String, Boolean> unContactEmailSelectedDic = new LinkedHashMap<String, Boolean>();
	private EORepartPersonneAdresse contactSelectedRepartPersonneAdresse;

	private String erreurSaisieMessage;

	private String orderIDSucceeded;

	public EOItemOut unItemOut;
	private NSArray<EOItemOut> itemOuts;

	private ApplicationUser appUser;

	public IPersonne unServiceFacturation;
	//public IPersonne selectedServiceFacturation;

	public IPersonne unServiceLivraison;
	public EOB2bCxmlOrderRequest unOrderRequest;

	//public IPersonne selectedServiceLivraison;

	public B2BOrderRequestUI(WOContext context) {
		super(context);
	}

	/**
	 * @return la valeur du binding utilisateurPersId.
	 */
	public Integer getUtilisateurPersId() {
		if (valueForBinding(BINDING_utilisateurPersId) == null) {
			System.err.println("**** Le binding utilisateurPersId n'est pas renseigné pour le composant " + name());
		}
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	public ApplicationUser getAppUser() {
		if (appUser == null) {
			try {
				setAppUser(new ApplicationUser(edc(), null, getUtilisateurPersId()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return appUser;
	}

	public void setAppUser(ApplicationUser appUser) {
		this.appUser = appUser;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		ERXResponseRewriter.addStylesheetResourceInHead(response, context, frameworkName() + ".framework", "css/" + frameworkName() + ".css");
	}

	public EOCommande getCommande() {
		if (commande == null || !commande.equals((EOCommande) valueForBinding(BINDING_commande))) {
			commande = (EOCommande) valueForBinding(BINDING_commande);
			itemOuts = null;
		}
		return commande;
	}

	public NSArray<EOItemOut> getItemOuts() {
		if (itemOuts == null) {
			return B2bUtils.articlesToItemOuts(new EOEditingContext(), getCommande().articles());
		}
		return itemOuts;
	}

	public EOFournisB2bCxmlParam getFournisB2bCxmlParam() {
		return ((EOB2bCxmlPunchoutmsg) getCommande().toB2bCxmlPunchoutmsgs().objectAtIndex(0)).toFournisB2bCxmlParam();
	}

	public EOFournis getFournis() {
		return asFournis(getFournisB2bCxmlParam().toFournisseur());
	}

	public CXMLParameters getCxmlParameters() {
		return getFournisB2bCxmlParam().toCxmlParameters();
	}

	public ProxyProperties getProxyProperties() {
		return (ProxyProperties) valueForBinding(BINDING_proxyProperties);
	}

	public String getErreurSaisieMessage() {
		String tmp = erreurSaisieMessage;
		return tmp;
	}

	public void setErreurSaisieMessage(String erreurSaisieMessage) {
		this.erreurSaisieMessage = erreurSaisieMessage;
	}

	public BigDecimal itemsTotal() {
		BigDecimal total = new BigDecimal(0).setScale(2);
		if (getItemOuts() != null) {
			for (int i = 0; i < getItemOuts().count(); i++) {
				EOItemOut itemOut = (EOItemOut) getItemOuts().objectAtIndex(i);
				total = total.add(itemOut.itemDetail().unitPrice().money().getVal().multiply(new BigDecimal(itemOut.quantity().intValue()).setScale(2)));
			}
		}
		return total;
	}

	public String containerLivraisonID() {
		return getMainContainerId() + "_Livraison";
	}

	public String containerFacturationID() {
		return getMainContainerId() + "_Facturation";
	}

	public String containerContactID() {
		return getMainContainerId() + "_Contact";
	}

	public EOQualifier qualifierForTypeAdressesLivraison() {
		return null;
		//		return EOTypeAdresse.QUAL_TADR_CODE_LIVR;
	}

	public EOQualifier qualifierForTypeAdressesFacturation() {
		return null;
		//return EOTypeAdresse.QUAL_TADR_CODE_FACT;
	}

	public IPersonne getShipToPersonne() {
		return shipToPersonne;
	}

	public void setShipToPersonne(IPersonne toShipPersonne) {
		this.shipToPersonne = toShipPersonne;
		updateShipToPersonneTelephoneSelectedDic();
		updateShipToPersonneFaxSelectedDic();
		updateShipToEmailSelectedDic();
		updateShipToPostalAdress();
	}

	private void updateShipToPostalAdress() {
		shipToSelectedRepartPersonneAdresse = null;
		if (getShipToPersonne() != null) {
			NSArray<EORepartPersonneAdresse> res = getShipToPersonne().toRepartPersonneAdresses();

			for (int i = 0; i < res.count(); i++) {
				EORepartPersonneAdresse pt = (EORepartPersonneAdresse) res.objectAtIndex(i);
				if (EORepartPersonneAdresse.RPA_PRINCIPAL_OUI.equals(pt.rpaPrincipal())) {
					setShipToSelectedRepartPersonneAdresse(pt);
				}
			}
		}
	}

	private void updateShipToPersonneTelephoneSelectedDic() {
		unShipToPersonneTelephoneSelectedDic.clear();
		if (getShipToPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getShipToPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}
				unShipToPersonneTelephoneSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unShipToPersonneTelephoneSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unShipToPersonneTelephoneSelectedDic.keySet().size() == 1) {
				setSelectedShipToTelephone((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateShipToPersonneFaxSelectedDic() {

		unShipToPersonneFaxSelectedDic.clear();
		if (getShipToPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getShipToPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}
				unShipToPersonneFaxSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unShipToPersonneFaxSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unShipToPersonneFaxSelectedDic.keySet().size() == 1) {
				setSelectedShipToFax((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateShipToEmailSelectedDic() {
		unShipToEmailSelectedDic.clear();
		setSelectedShipToEmail(null);
		//ajouter les emails de l'utilisateur
		NSArray<String> emails = getAppUser().getEmails();
		for (int i = 0; i < emails.count(); i++) {
			unShipToEmailSelectedDic.put(emails.objectAtIndex(i), Boolean.TRUE);
			if (i == 0) {
				setSelectedShipToEmail((String) emails.objectAtIndex(0));
			}

		}

		if (getShipToPersonne() != null) {
			//Recuperer les secretaires du service
			if (getShipToPersonne() instanceof EOStructure) {
				NSArray<EOIndividu> secretariats = ((EOStructure) getShipToPersonne()).toSecretariatsAsIndividus();
				for (int i = 0; i < secretariats.count(); i++) {
					EOIndividu secr = (EOIndividu) secretariats.objectAtIndex(i);
					NSArray<String> emails2 = secr.getEmails(EOCompte.QUAL_CPT_VLAN_P_R);
					for (int j = 0; j < emails2.count(); j++) {
						if (j == 0) {
							unShipToEmailSelectedDic.put(emails2.objectAtIndex(j), Boolean.TRUE);
						}
					}
				}
			}
		}

	}

	public EOAdresse getShipToPostalAdresse() {
		return shipToPostalAdresse;
	}

	public void setShipToPostalAdresse(EOAdresse toShipPostalAdresse) {
		this.shipToPostalAdresse = toShipPostalAdresse;
	}

	public NSArray<EOPersonneTelephone> getShipToPersonneTelephones() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unShipToPersonneTelephoneSelectedDic.keySet());
		return res;
	}

	public NSArray<EOPersonneTelephone> getShipToPersonneFaxes() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unShipToPersonneFaxSelectedDic.keySet());
		return res;
	}

	public NSArray<String> getShipToEmails() {
		NSArray<String> res = new NSArray<String>(unShipToEmailSelectedDic.keySet());
		return res;
	}

	public EORepartPersonneAdresse getShipToSelectedRepartPersonneAdresse() {
		return shipToSelectedRepartPersonneAdresse;
	}

	public void setShipToSelectedRepartPersonneAdresse(EORepartPersonneAdresse shipToSelectedRepartPersonneAdresse) {
		this.shipToSelectedRepartPersonneAdresse = shipToSelectedRepartPersonneAdresse;
	}

	public Boolean unShipToPersonneTelephoneSelected() {
		return (Boolean) unShipToPersonneTelephoneSelectedDic.get(unShipToPersonneTelephone);
	}

	public void setUnShipToPersonneTelephoneSelected(Boolean val) {
		unShipToPersonneTelephoneSelectedDic.put(unShipToPersonneTelephone, val);
	}

	public Boolean unShipToPersonneFaxSelected() {
		return (Boolean) unShipToPersonneFaxSelectedDic.get(unShipToPersonneFax);
	}

	public void setUnShipToPersonneFaxSelected(Boolean val) {
		unShipToPersonneFaxSelectedDic.put(unShipToPersonneFax, val);
	}

	public Boolean unShipToEmailSelected() {
		return (Boolean) unShipToEmailSelectedDic.get(unShipToEmail);
	}

	public void setUnShipToEmailSelected(Boolean val) {
		unShipToEmailSelectedDic.put(unShipToEmail, val);
	}

	public IPersonne getBillToPersonne() {
		return billToPersonne;
	}

	public void setBillToPersonne(IPersonne toBillPersonne) {
		this.billToPersonne = toBillPersonne;
		updateBillToPersonneTelephoneSelectedDic();
		updateBillToPersonneFaxSelectedDic();
		updateBillToEmailSelectedDic();
		updateBillToPostalAdress();
	}

	private void updateBillToPostalAdress() {
		billToSelectedRepartPersonneAdresse = null;
		if (getBillToPersonne() != null) {
			NSArray<EORepartPersonneAdresse> res = getBillToPersonne().toRepartPersonneAdresses();

			for (int i = 0; i < res.count(); i++) {
				EORepartPersonneAdresse pt = (EORepartPersonneAdresse) res.objectAtIndex(i);
				if (EORepartPersonneAdresse.RPA_PRINCIPAL_OUI.equals(pt.rpaPrincipal())) {
					setBillToSelectedRepartPersonneAdresse(pt);
				}
			}
		}
	}

	private void updateBillToPersonneTelephoneSelectedDic() {
		unBillToPersonneTelephoneSelectedDic.clear();
		if (getBillToPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getBillToPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}

				unBillToPersonneTelephoneSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unBillToPersonneTelephoneSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unBillToPersonneTelephoneSelectedDic.keySet().size() == 1) {
				setSelectedBillToTelephone((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateBillToPersonneFaxSelectedDic() {
		unBillToPersonneFaxSelectedDic.clear();
		if (getBillToPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getBillToPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}
				unBillToPersonneFaxSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unBillToPersonneFaxSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unBillToPersonneFaxSelectedDic.keySet().size() == 1) {
				setSelectedBillToFax((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateBillToEmailSelectedDic() {
		unBillToEmailSelectedDic.clear();
		//ajouter les emails de l'utilisateur
		NSArray<String> emails = getAppUser().getEmails();
		for (int i = 0; i < emails.count(); i++) {
			unBillToEmailSelectedDic.put(emails.objectAtIndex(i), Boolean.TRUE);
			if (i == 0) {
				setSelectedBillToEmail((String) emails.objectAtIndex(0));
			}
		}

		if (getBillToPersonne() != null) {
			//Recuperer les secretaires du service
			if (getBillToPersonne() instanceof EOStructure) {
				NSArray<EOIndividu> secretariats = ((EOStructure) getBillToPersonne()).toSecretariatsAsIndividus();
				for (int i = 0; i < secretariats.count(); i++) {
					EOIndividu secr = (EOIndividu) secretariats.objectAtIndex(i);
					NSArray<String> emails2 = secr.getEmails(EOCompte.QUAL_CPT_VLAN_P_R);
					for (int j = 0; j < emails2.count(); j++) {
						if (j == 0) {
							unBillToEmailSelectedDic.put(emails2.objectAtIndex(j), Boolean.TRUE);
						}
					}
				}
			}
		}
	}

	public EOAdresse getBillToPostalAdresse() {
		return billToPostalAdresse;
	}

	public void setBillToPostalAdresse(EOAdresse toBillPostalAdresse) {
		this.billToPostalAdresse = toBillPostalAdresse;
	}

	public NSArray<EOPersonneTelephone> getBillToPersonneTelephones() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unBillToPersonneTelephoneSelectedDic.keySet());
		return res;
	}

	public NSArray<EOPersonneTelephone> getBillToPersonneFaxes() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unBillToPersonneFaxSelectedDic.keySet());
		return res;
	}

	public NSArray<String> getBillToEmails() {
		NSArray<String> res = new NSArray<String>(unBillToEmailSelectedDic.keySet());
		return res;
	}

	public EORepartPersonneAdresse getBillToSelectedRepartPersonneAdresse() {
		return billToSelectedRepartPersonneAdresse;
	}

	public void setBillToSelectedRepartPersonneAdresse(EORepartPersonneAdresse billToSelectedRepartPersonneAdresse) {
		this.billToSelectedRepartPersonneAdresse = billToSelectedRepartPersonneAdresse;
	}

	public Boolean unBillToPersonneTelephoneSelected() {
		return (Boolean) unBillToPersonneTelephoneSelectedDic.get(unBillToPersonneTelephone);
	}

	public void setUnBillToPersonneTelephoneSelected(Boolean val) {
		unBillToPersonneTelephoneSelectedDic.put(unBillToPersonneTelephone, val);
	}

	public Boolean unBillToPersonneFaxSelected() {
		return (Boolean) unBillToPersonneFaxSelectedDic.get(unBillToPersonneFax);
	}

	public void setUnBillToPersonneFaxSelected(Boolean val) {
		unBillToPersonneFaxSelectedDic.put(unBillToPersonneFax, val);
	}

	public Boolean unBillToEmailSelected() {
		return (Boolean) unBillToEmailSelectedDic.get(unBillToEmail);
	}

	public void setUnBillToEmailSelected(Boolean val) {
		unBillToEmailSelectedDic.put(unBillToEmail, val);
	}

	public IPersonne getContactPersonne() {
		if (contactPersonne == null) {
			setContactPersonne(PersonneDelegate.fetchPersonneByPersId(edc(), getAppUser().getPersId()));
		}
		return contactPersonne;
	}

	public void setContactPersonne(IPersonne toContactPersonne) {
		this.contactPersonne = toContactPersonne;
		updateContactPersonneTelephoneSelectedDic();
		updateContactPersonneFaxSelectedDic();
		updateContactEmailSelectedDic();
	}

	private void updateContactPersonneTelephoneSelectedDic() {
		unContactPersonneTelephoneSelectedDic.clear();
		if (getContactPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getContactPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}

				unContactPersonneTelephoneSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unContactPersonneTelephoneSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unContactPersonneTelephoneSelectedDic.keySet().size() == 1) {
				setSelectedContactTelephone((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateContactPersonneFaxSelectedDic() {
		unContactPersonneFaxSelectedDic.clear();
		if (getContactPersonne() != null) {
			NSArray<EOPersonneTelephone> res = getContactPersonne().toPersonneTelephones();
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX, EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
			})));
			boolean atLeastOneChecked = false;
			for (int i = 0; i < res.count(); i++) {
				EOPersonneTelephone pt = (EOPersonneTelephone) res.objectAtIndex(i);
				if (pt.isTelPrincipal().booleanValue()) {
					atLeastOneChecked = true;
				}
				unContactPersonneFaxSelectedDic.put(pt, pt.isTelPrincipal());
			}
			if (res.count() > 0 && !atLeastOneChecked) {
				unContactPersonneFaxSelectedDic.put(res.objectAtIndex(0), Boolean.TRUE);
			}
			if (unContactPersonneFaxSelectedDic.keySet().size() == 1) {
				setSelectedContactFax((EOPersonneTelephone) res.objectAtIndex(0));
			}
		}
	}

	private void updateContactEmailSelectedDic() {
		unContactEmailSelectedDic.clear();
		//ajouter les emails du contact
		if (getContactPersonne() != null) {
			NSMutableArray<String> res = new NSMutableArray<String>();
			NSArray<EOCompte> comptes = getContactPersonne().toComptes(EOCompte.QUAL_CPT_VALIDE_OUI);
			comptes = EOSortOrdering.sortedArrayUsingKeyOrderArray(comptes, new NSArray<EOSortOrdering>(EOCompte.SORT_VLANS_PRIORITE));

			for (int i = 0; i < comptes.count(); i++) {
				EOCompte compte = ((EOCompte) comptes.objectAtIndex(i));
				NSArray<EOCompteEmail> cptEmails = compte.toCompteEmails();
				for (int j = 0; j < cptEmails.count(); j++) {
					String email = ((EOCompteEmail) cptEmails.objectAtIndex(j)).getEmailFormatte();
					if (res.indexOfObject(email) == NSArray.NotFound) {
						res.addObject(email);
					}
				}

			}

			for (int i = 0; i < res.count(); i++) {
				unContactEmailSelectedDic.put(res.objectAtIndex(i), Boolean.TRUE);
				if (i == 0) {
					setSelectedContactEmail((String) res.objectAtIndex(0));
				}
			}

		}
	}

	public EOAdresse getContactPostalAdresse() {
		return contactPostalAdresse;
	}

	public void setContactPostalAdresse(EOAdresse toBillPostalAdresse) {
		this.contactPostalAdresse = toBillPostalAdresse;
	}

	public NSArray<EOPersonneTelephone> getContactPersonneTelephones() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unContactPersonneTelephoneSelectedDic.keySet());
		return res;
	}

	public NSArray<EOPersonneTelephone> getContactPersonneFaxes() {
		NSArray<EOPersonneTelephone> res = new NSArray<EOPersonneTelephone>(unContactPersonneFaxSelectedDic.keySet());
		return res;
	}

	public NSArray<String> getContactEmails() {
		NSArray<String> res = new NSArray<String>(unContactEmailSelectedDic.keySet());
		return res;
	}

	public EORepartPersonneAdresse getContactSelectedRepartPersonneAdresse() {
		return contactSelectedRepartPersonneAdresse;
	}

	public void setContactSelectedRepartPersonneAdresse(EORepartPersonneAdresse contactSelectedRepartPersonneAdresse) {
		this.contactSelectedRepartPersonneAdresse = contactSelectedRepartPersonneAdresse;
	}

	public Boolean unContactPersonneTelephoneSelected() {
		return (Boolean) unContactPersonneTelephoneSelectedDic.get(unContactPersonneTelephone);
	}

	public void setUnContactPersonneTelephoneSelected(Boolean val) {
		unContactPersonneTelephoneSelectedDic.put(unContactPersonneTelephone, val);
	}

	public Boolean unContactPersonneFaxSelected() {
		return (Boolean) unContactPersonneFaxSelectedDic.get(unContactPersonneFax);
	}

	public void setUnContactPersonneFaxSelected(Boolean val) {
		unContactPersonneFaxSelectedDic.put(unContactPersonneFax, val);
	}

	public Boolean unContactEmailSelected() {
		return (Boolean) unContactEmailSelectedDic.get(unContactEmail);
	}

	public void setUnContactEmailSelected(Boolean val) {
		unContactEmailSelectedDic.put(unContactEmail, val);
	}

	public EOPersonneTelephone getSelectedShipToTelephone() {
		return selectedShipToTelephone;
	}

	public void setSelectedShipToTelephone(EOPersonneTelephone selectedShipToTelephone) {
		this.selectedShipToTelephone = selectedShipToTelephone;
	}

	public EOPersonneTelephone getSelectedShipToFax() {
		return selectedShipToFax;
	}

	public void setSelectedShipToFax(EOPersonneTelephone selectedShipToFax) {
		this.selectedShipToFax = selectedShipToFax;
	}

	public String getSelectedShipToEmail() {
		return selectedShipToEmail;
	}

	public void setSelectedShipToEmail(String selectedShipToEmail) {
		this.selectedShipToEmail = selectedShipToEmail;
	}

	public String getSelectedBillToEmail() {
		return selectedBillToEmail;
	}

	public void setSelectedBillToEmail(String selectedBillToEmail) {
		this.selectedBillToEmail = selectedBillToEmail;
	}

	public EOPersonneTelephone getSelectedBillToTelephone() {
		return selectedBillToTelephone;
	}

	public void setSelectedBillToTelephone(EOPersonneTelephone selectedBillToTelephone) {
		this.selectedBillToTelephone = selectedBillToTelephone;
	}

	public EOPersonneTelephone getSelectedBillToFax() {
		return selectedBillToFax;
	}

	public void setSelectedBillToFax(EOPersonneTelephone selectedBillToFax) {
		this.selectedBillToFax = selectedBillToFax;
	}

	public String getSelectedContactEmail() {
		return selectedContactEmail;
	}

	public void setSelectedContactEmail(String selectedContactEmail) {
		this.selectedContactEmail = selectedContactEmail;
	}

	public EOPersonneTelephone getSelectedContactTelephone() {
		return selectedContactTelephone;
	}

	public void setSelectedContactTelephone(EOPersonneTelephone selectedContactTelephone) {
		this.selectedContactTelephone = selectedContactTelephone;
	}

	public EOPersonneTelephone getSelectedContactFax() {
		return selectedContactFax;
	}

	public void setSelectedContactFax(EOPersonneTelephone selectedContactFax) {
		this.selectedContactFax = selectedContactFax;
	}

	public WOActionResults onSendOrderRequest() {
		try {
			setOrderIDSucceeded(null);

			Long requisitionID = Long.valueOf(getCommande().commNumero().longValue());
			Long orderID = null;

			try {
				orderID = Long.valueOf(commId().longValue());
			} catch (NumberFormatException e) {
				throw new Exception("L'identifiant de commande doit etre numerique");
			}
			if (getItemOuts() == null || getItemOuts().count() == 0) {
				throw new Exception("Aucun article a commander.");
			}
			if (getShipToPersonne() == null) {
				throw new Exception("Veuillez selectionner un service pour la livraison");
			}
			if (getShipToSelectedRepartPersonneAdresse() == null) {
				throw new Exception("Veuillez selectionner une adresse de livraison");
			}
			if (getBillToPersonne() == null) {
				throw new Exception("Veuillez selectionner un service pour la facturation");
			}
			if (getBillToSelectedRepartPersonneAdresse() == null) {
				throw new Exception("Veuillez selectionner une adresse de facturation");
			}

			//construire le msg
			CXMLTransaction transaction = new CXMLTransaction();
			//			EOEditingContext edForCxml = new EOEditingContext();
			EOEditingContext edForCxml = ERXEC.newEditingContext();
			EOCxml cxmlOrderRequest = transaction.creerCxmlOrderRequestForTransaction(edForCxml, getAppUser(), getCxmlParameters(), orderID, requisitionID, getItemOuts(),
					getShipToPersonne(), getShipToSelectedRepartPersonneAdresse(), getSelectedShipToEmail(), getSelectedShipToTelephone(), getSelectedShipToFax(),
					getBillToPersonne(), getBillToSelectedRepartPersonneAdresse(), getSelectedBillToEmail(), getSelectedBillToTelephone(), getSelectedBillToFax(),
					getContactPersonne(), null, getSelectedContactEmail(), getSelectedContactTelephone(), null, getProxyProperties());

			String res = ProcessGestionCommandeB2b.b2bSendOrderRequest(mySession().dataBus(), edc(), ((Session) mySession()).getLaCommandeEnCours(), cxmlOrderRequest, getCxmlParameters(), getProxyProperties(), getUtilisateurPersId(), getFournisB2bCxmlParam());

			setOrderIDSucceeded(res);
			setResult(RESULT_SUCCEED);

		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				setErreurSaisieMessage("NullPointerException : " + (e.getCause() != null ? e.getCause().getMessage() : ""));
			}
			else {
				setErreurSaisieMessage(e.getMessage());
			}
			edc().revert();
			setResult(RESULT_ERROR);
		}
		return null;

		//
		//
		//
		//			//Construire le cXML
		//			String iso = "FR";
		//			if (getShipToSelectedRepartPersonneAdresse() != null) {
		//				if (getShipToSelectedRepartPersonneAdresse().toAdresse() != null && getShipToSelectedRepartPersonneAdresse().toAdresse().toPays() != null) {
		//					iso = getShipToSelectedRepartPersonneAdresse().toAdresse().toPays().codeIso().toUpperCase();
		//				}
		//			}
		//
		//			EOPostalAddress postalAdressShipTo = CXMLEntityFactoryFromGrhum.createEOPostalAddress(edc(), getShipToSelectedRepartPersonneAdresse().toTypeAdresse().tadrLibelle(), getShipToPersonne(), getShipToSelectedRepartPersonneAdresse().toAdresse());
		//			EOEmail emailShipTo = CXMLEntityFactoryFromGrhum.createEOEmail(edc(), "", getSelectedShipToEmail());
		//			EOPhone phoneShipTo = CXMLEntityFactoryFromGrhum.createEOPhone(edc(), "", getSelectedShipToTelephone());
		//			EOFax faxShipTo = CXMLEntityFactoryFromGrhum.createEOFax(edc(), "", getSelectedShipToFax());
		//			EOAddress addressShipTo = CXMLEntityFactoryFromGrhum.createEOAddress(edc(), "", "Livraison", "fr", iso, postalAdressShipTo, phoneShipTo, faxShipTo, emailShipTo, null);
		//
		//			EOPostalAddress postalAdressBillTo = CXMLEntityFactoryFromGrhum.createEOPostalAddress(edc(), getBillToSelectedRepartPersonneAdresse().toTypeAdresse().tadrLibelle(), getBillToPersonne(), getBillToSelectedRepartPersonneAdresse().toAdresse());
		//			EOEmail emailBillTo = CXMLEntityFactoryFromGrhum.createEOEmail(edc(), "", getSelectedBillToEmail());
		//			EOPhone phoneBillTo = CXMLEntityFactoryFromGrhum.createEOPhone(edc(), "", getSelectedBillToTelephone());
		//			EOFax faxBillTo = CXMLEntityFactoryFromGrhum.createEOFax(edc(), "", getSelectedBillToFax());
		//			EOAddress addressBillTo = CXMLEntityFactoryFromGrhum.createEOAddress(edc(), "", "Facturation", "fr", iso, postalAdressBillTo, phoneBillTo, faxBillTo, emailBillTo, null);
		//
		//			EOCxml cxmlOrderRequest = CXMLMessageFactory.createOrderRequest(edc(), getCxmlParameters(), new NSTimestamp(), orderID, EOOrderRequestHeader.TYPE_NEW, addressBillTo, null, addressShipTo, getItemOuts());
		//
		//			//CXMLEntiyFactory.validateAllObjectsInEc(edc());
		//
		//			edc().saveChanges();
		//
		//			System.out.println(CXMLMessageFactory.toXml(cxmlOrderRequest));
		//
		//			CXMLTransaction transaction = new CXMLTransaction();
		//			String res = transaction.creerTransactionOrderRequest(appUser, cxmlOrderRequest, getCxmlParameters(), getProxyProperties());
		//			setOrderIDSucceeded(res);
		//			setResult(RESULT_SUCCEED);
		//
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//			if (e instanceof NullPointerException) {
		//				setErreurSaisieMessage("NullPointerException : " + (e.getCause() != null ? e.getCause().getMessage() : ""));
		//			}
		//			else {
		//				setErreurSaisieMessage(e.getMessage());
		//			}
		//			setResult(RESULT_ERROR);
		//		}
		//		return null;
	}

	public WOActionResults onCancel() {
		setOrderIDSucceeded(null);
		setResult(RESULT_CANCELED);
		setOrderIDSucceeded(null);
		DetailCommande nextPage = (DetailCommande) pageWithName("DetailCommande");
		nextPage.setLaCommande(commande);
		return nextPage;

	}

	public String getOrderIDSucceeded() {
		return orderIDSucceeded;
	}

	public void setOrderIDSucceeded(String orderIDSucceeded) {
		this.orderIDSucceeded = orderIDSucceeded;
	}

	public Boolean isOrderSucceeded() {
		return Boolean.valueOf(orderIDSucceeded != null);
	}

	public String getOrderSucceededMsg() {
		String tmp = "La commande ID = " + orderIDSucceeded + " a été correctement transmise à " + getFournisB2bCxmlParam().libelle();
		//setOrderIDSucceeded(null);
		return tmp;
	}

	public String getResult() {
		return (String) valueForBinding(BINDING_result);
	}

	public void setResult(String result) {
		setResultError(null);
		if (RESULT_ERROR.equals(result)) {
			setResultError(erreurSaisieMessage);
		}
		setValueForBinding(result, BINDING_result);
	}

	public void setResultError(String resultError) {
		setValueForBinding(resultError, BINDING_resultError);
	}

	public String updateContainerID() {
		if (hasBinding(BINDING_updateContainerID)) {
			return (String) valueForBinding(BINDING_updateContainerID);
		}
		return getMainContainerId();
	}

	/**
	 * Transforme un EOFournisseur en EOFournis;
	 *
	 * @return
	 */
	public EOFournis asFournis(EOFournisseur fournisseur) {
		Integer fouOrdre = (Integer) EOUtilities.primaryKeyForObject(fournisseur.editingContext(), fournisseur).valueForKey(EOFournisseur.FOU_ORDRE_KEY);
		return EOFournis.fetchByKeyValue(fournisseur.editingContext(), EOFournis.FOU_ORDRE_KEY, fouOrdre);
	}

	public Integer commId() {
		return (Integer) EOUtilities.primaryKeyForObject(getCommande().editingContext(), getCommande()).valueForKey(EOCommande.COMM_ID_KEY);
	}

	public WOActionResults onClose() {
		setOrderIDSucceeded(null);
		DetailCommande nextPage = (DetailCommande) pageWithName("DetailCommande");
		nextPage.setLaCommande(commande);
		return nextPage;
	}

	@SuppressWarnings("unchecked")
	public NSArray<? extends IPersonne> services() {
		return (NSArray<? extends IPersonne>) valueForBinding(BINDING_services);
	}

	public Boolean hasServices() {
		return Boolean.valueOf(services().count() > 0);
	}

	public NSArray<EOB2bCxmlOrderRequest> orderRequests() {
		getCommande().editingContext().invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(getCommande().editingContext().globalIDForObject(getCommande())));
		return getCommande().toB2bCxmlOrderRequests();
	}

	public void injectResources(WOResponse response, WOContext context) {
		MyAjaxPage.injectResourcesInResponse(response, context);
	}

}