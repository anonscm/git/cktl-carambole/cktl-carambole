/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyComponent;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;

import com.webobjects.appserver.WOContext;

public class Commander extends MyComponent {

	private static final long serialVersionUID = 1L;
	EOCommande laCommande = null;

	public Commander(WOContext context) {
		super(context);
	}

	public void afficherBdCPrecedent() {
		int activeTab = new Integer(session.onLoadBdCShowTab()).intValue();
		activeTab = activeTab - 1;
		session.setOnLoadBdCShowTab(String.valueOf(activeTab));
	}

	public void afficherBdCSuivant() {
		int activeTab = new Integer(session.onLoadBdCShowTab()).intValue();
		activeTab = activeTab + 1;
		session.setOnLoadBdCShowTab(String.valueOf(activeTab));
	}

	public void afficherBdCInfos() {
		session.setOnLoadBdCShowTab("0");
	}

	public void afficherBdCSources() {
		session.setOnLoadBdCShowTab("1");
	}

	public void afficherBdCArticles() {
		session.setOnLoadBdCShowTab("2");
	}

	public void afficherBdCRepartitions() {
		session.setOnLoadBdCShowTab("3");
	}

	public EOCommande getLaCommande() {
		return laCommande;
	}

	public void setLaCommande(EOCommande laCommande) {
		this.laCommande = laCommande;
	}

}