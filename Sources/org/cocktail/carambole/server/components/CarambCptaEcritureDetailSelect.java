/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxSelectComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldepense.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktldepense.server.metier.EOEcritureDetailAEmarger;
import org.cocktail.fwkcktldepense.server.metier.EOModePaiement;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXQ;

/**
 * Selection d'une ecriture comptable pour emargement.
 * 
 * @binding modePaiement mode de paiement pour filtrage.
 * @author rprin
 */
public class CarambCptaEcritureDetailSelect extends MyAjaxComponent {

	private static final long serialVersionUID = 4359790436113570436L;

	private static final String OBJET_SELECTIONNE = CktlAjaxSelectComponent.CURRENT_OBJ_KEY;
	private static final String BINDING_SELECTION = "selection";
	private static final String LC_KEY = EOEcritureDetail.ECR_NUMERO_KEY;
	private static final String LL_KEY = EOEcritureDetail.ECD_LIBELLE_KEY;
	private static final String DEBIT_KEY = EOEcritureDetail.ECD_DEBIT_KEY;
	private static final String CREDIT_KEY = EOEcritureDetail.ECD_CREDIT_KEY;
	private static final String PCONUM_KEY = EOEcritureDetail.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY;

	private static final String BINDING_MODE_PAIEMENT = "modePaiement";

	private static final String BINDING_SHOW_DEBIT = "showDebit";
	private static final String BINDING_SHOW_CREDIT = "showCredit";

	private NSArray<CktlAjaxTableViewColumn> colonnes;
	private String filtre;
	private String containerId;
	private String modalWindowId;

	private EOEcritureDetail selection;

	public CarambCptaEcritureDetailSelect(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WOActionResults valider() {
		setValueForBinding(selection, BINDING_SELECTION);
		CktlAjaxWindow.close(context(), getModalWindowId());
		return null;
	}

	public WOActionResults editer() {
		return null;
	}

	public EOEcritureDetail getSelection() {
		return (EOEcritureDetail) valueForBinding(BINDING_SELECTION);
	}

	public void setSelection(EOEcritureDetail value) {
		selection = value;
		//setValueForBinding(value, BINDING_SELECTION);
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();

			// Colonne Libelle
			CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
			col.setLibelle("Numéro");
			col.setOrderKeyPath(LC_KEY);
			String keyPath = ERXQ.keyPath(OBJET_SELECTIONNE, LC_KEY);
			CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne Libelle long
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Libellé");
			col.setOrderKeyPath(LL_KEY);
			keyPath = ERXQ.keyPath(OBJET_SELECTIONNE, LL_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne Compte
			col = new CktlAjaxTableViewColumn();
			col.setLibelle("Compte");
			col.setOrderKeyPath(PCONUM_KEY);
			keyPath = ERXQ.keyPath(OBJET_SELECTIONNE, PCONUM_KEY);
			ass = new CktlAjaxTableViewColumnAssociation(
					keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);

			// Colonne Debit
			if (!Boolean.FALSE.equals(valueForBinding(BINDING_SHOW_DEBIT))) {
				col = new CktlAjaxTableViewColumn();
				col.setLibelle("Débit");
				col.setOrderKeyPath(DEBIT_KEY);
				keyPath = ERXQ.keyPath(OBJET_SELECTIONNE, DEBIT_KEY);
				ass = new CktlAjaxTableViewColumnAssociation(
						keyPath, "");
				col.setAssociations(ass);
				colTmp.add(col);
			}

			// Colonne Credit
			if (!Boolean.FALSE.equals(valueForBinding(BINDING_SHOW_CREDIT))) {
				col = new CktlAjaxTableViewColumn();
				col.setLibelle("Crédit");
				col.setOrderKeyPath(CREDIT_KEY);
				keyPath = ERXQ.keyPath(OBJET_SELECTIONNE, CREDIT_KEY);
				ass = new CktlAjaxTableViewColumnAssociation(
						keyPath, "");
				col.setAssociations(ass);
				colTmp.add(col);
			}

			colonnes = colTmp.immutableClone();
		}
		return colonnes;
	}

	public EOQualifier getQualifier() {
		if (getFiltre() != null) {
			return ERXQ.containsAny(new NSArray<String>(LC_KEY, LL_KEY), getFiltre());
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	public NSArray<EOEcritureDetail> getAllEcritureDetailAEmarger() {
		NSArray<EOEcritureDetailAEmarger> ecrs = EOEcritureDetailAEmarger.fetchAll(edc(), ERXQ.equals(EOEcritureDetailAEmarger.MODE_PAIEMENT_KEY, getModePaiement()), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOEcritureDetailAEmarger.SORT_NUMERO_ASC
		}));
		return NSArrayCtrl.flattenArray((NSArray<NSArray<EOEcritureDetail>>) ecrs.valueForKey(EOEcritureDetailAEmarger.ECRITURE_DETAIL_KEY));
	}

	public String getContainerId() {
		if (containerId == null)
			containerId = ERXWOContext.safeIdentifierName(context(), true);
		return containerId;
	}

	public String getModalWindowId() {
		if (modalWindowId == null)
			modalWindowId = ERXWOContext.safeIdentifierName(context(), true);
		return modalWindowId;
	}

	public String getFiltre() {
		return filtre;
	}

	public void setFiltre(String filtre) {
		this.filtre = filtre;
	}

	public EOModePaiement getModePaiement() {
		return (EOModePaiement) valueForBinding(BINDING_MODE_PAIEMENT);
	}

	public Boolean isEditing() {
		return (Boolean) valueForBinding("isEditing");
	}

	public Boolean isReadOnly() {
		return (Boolean) valueForBinding("isReadOnly");
	}

	public String onComplete() {
		return "function(){CAW.close('" + getModalWindowId() + "_win');}";
		//	return "function(){parent.Windows.closeAll();}";
	}

	public WOActionResults annuler() {
		return null;
	}

}