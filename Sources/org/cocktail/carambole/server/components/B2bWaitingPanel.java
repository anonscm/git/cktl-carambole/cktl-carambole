/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;

import er.ajax.AjaxDynamicElement;
import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;

public class B2bWaitingPanel extends AjaxDynamicElement {

	public B2bWaitingPanel(String name, NSDictionary<String, WOAssociation> associations, WOElement children) {
		super(name, associations, children);
	}

	private static final String BINDING_MSG = "msg";
	private static final String BINDING_OPENWAITFUNCTIONNAME = "openWaitFunctionName";

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		WOComponent component = context.component();

		StringBuilder s = new StringBuilder("");
		s.append(openWaitFunctionName(component) + "=function() ");
		s.append(" {");
		s.append("Dialog.info('");
		s.append("<div style=\"width: 250px; height:150px; background-color: #DCFBE4; 4px solid #80C65A; color: black;\"><table><tbody>");
		s.append("<tr><td align=\"center\" valign=\"middle\">");
		s.append(CktlAjaxUtils.jsEncode(msg(component)));
		s.append("</td></tr>");
		s.append("");
		s.append("</tbody></table></div>");
		s.append("',{width:250, height:150, showProgress: true});");
		s.append("}");

		AjaxUtils.appendScriptHeader(response);
		response.appendContentString(s.toString());
		AjaxUtils.appendScriptFooter(response);

	}

	public String openWaitFunctionName(WOComponent component) {
		return (String) valueForBinding(BINDING_OPENWAITFUNCTIONNAME, component);
	}

	public String msg(WOComponent component) {
		return (String) valueForBinding(BINDING_MSG, component);
	}

	@Override
	protected void addRequiredWebResources(WOResponse response, WOContext context) {
		CktlAjaxUtils.addScriptResourceInHead(context, response, "controls.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "effects.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "wonder.js");

		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwonder.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxwebext.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/cktlwindowsopeners.js");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");
	}

	@Override
	public WOActionResults handleRequest(WORequest worequest, WOContext wocontext) {
		WOResponse response = AjaxUtils.createResponse(worequest, wocontext);

		return response;
	}

}