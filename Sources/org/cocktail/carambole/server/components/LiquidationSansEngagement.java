/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktldepense.server.finder.FinderAttribution;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.finder.FinderProrata;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOBudgetExecCredit;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCtrlSeuilMAPA;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOLotNomenclature;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOSource;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.SourceCreditsEngagementTO;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.TypeAchatConfiguration;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlSourceCreditSelectCtrl;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlTypeAchatSelectionCtrl;
import org.cocktail.fwkcktldepenseguiajax.serveur.enums.ETypeMarche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;

/**
 * Controleur d'interface pour la création d'une liquidation sans disposer d'engagement préalable.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class LiquidationSansEngagement extends Liquidation2 {
	private static final Set<ETypeMarche> TYPES_MARCHE = EnumSet.of(ETypeMarche.MARCHE, ETypeMarche.HORS_MARCHE);

	private static final long serialVersionUID = 1L;
	private MyCktlTypeAchatSelectionCtrl typeAchatSelectionCtrl;
	private CktlSourceCreditSelectCtrl sourceCreditSelectCtrl;
	private SourceCreditsEngagementTO sourceCreditTO;
	//private EOFournisseur fournisseur;

	private _IDepenseBudget uneDepenseBudgetForMarche = null;

	public class MyCktlTypeAchatSelectionCtrl extends CktlTypeAchatSelectionCtrl {

		public MyCktlTypeAchatSelectionCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice) {
			super(session, edc, exercice);
		}

		@Override
		public WOActionResults selectTypeAchat() {
			WOActionResults res = super.selectTypeAchat();
			if (isTypeAchatMarche()) {
				for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
					getCtrl().supprimerDepenseControleHorsMarches(edc(), unDb);
				}

			}
			else {
				for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
					getCtrl().supprimerDepenseControleMarches(edc(), unDb);
				}
			}
			selectAttribution();
			updCodesNomenclatures();
			return res;
		}

		@Override
		public WOActionResults selectAttribution() {
			WOActionResults res = super.selectAttribution();
			updateDepenseCtrlMarcheWithAttribution();
			return res;
		}

		@Override
		public void reset() {
			super.reset();
			//TypeAchatTO to = new TypeAchatTO();
			if (getLaDepensePapier().isSurMarche()) {
				getTypeAchatTO().setTypeMarcheSelectionne(ETypeMarche.MARCHE);
				getTypeAchatTO().setTypeAchat(null);
				getTypeAchatTO().setCodeNomenclatureSelectionne(null);
				getTypeAchatTO().setAttributionSelectionnee(getLaDepensePapier().depenseBudgets().objectAtIndex(0).depenseControleMarches().objectAtIndex(0).attribution());
				for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
					getCtrl().supprimerDepenseControleHorsMarches(edc(), unDb);
				}
			}
			else {
				getTypeAchatTO().setTypeMarcheSelectionne(ETypeMarche.HORS_MARCHE);
				getTypeAchatTO().setTypeAchat(null);
				getTypeAchatTO().setCodeNomenclatureSelectionne(null);
				getTypeAchatTO().setAttributionSelectionnee(null);
				for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
					getCtrl().supprimerDepenseControleMarches(edc(), unDb);
				}
			}
			synchroniseTypeAchatFromTypeMarche();
		}

	}

	@Override
	protected void initialisation() {
		this.typeAchatSelectionCtrl = new MyCktlTypeAchatSelectionCtrl(mySession(), edc(), getExercice());
		super.initialisation();
		resetComponent();
		if (getFournisseur() != null) {
			try {
				initForFournisseur();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

	}

	protected void updateDepenseCtrlMarcheWithAttribution() {
		for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
			getCtrl().supprimerDepenseControleMarches(edc(), unDb);
		}
		if (getAttribution() != null) {
			for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
				if (unDb.getSource().tauxProrata() != null) {
					getCtrl().creerDepenseControleMarche(edc(), unDb.depHtSaisie(), unDb.depTvaSaisie(), unDb.depTtcSaisie(), unDb.depMontantBudgetaire(), BigDecimal.valueOf(100d), getAttribution(), unDb.exercice(), unDb);
				}
			}
		}
	}

	public LiquidationSansEngagement(WOContext context) {
		super(context);

		//resetComponent();
	}

	public String getPageTitle() {
		String prefix = "Création d'une pré-liquidation";
		if (!isPreLiquidation()) {
			prefix = "Création d'une liquidation";
		}
		return prefix.concat(" sans engagement");
	}

	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UNE_LIQUIDATION_SANS_ENGAGEMENT_KEY, Menu.ANNULER_UNE_LIQUIDATION_KEY
		});

		return items;
	}

	private EOExercice getExercice() {
		return exerciceCourant();
	}

	public void resetComponent() {
		sourceCreditTO = new SourceCreditsEngagementTO();
		if (typeAchatSelectionCtrl != null) {
			typeAchatSelectionCtrl.reset();
			typeAchatSelectionCtrl.updateConfiguration(defaultTypeAchatConfiguration());
			updCodesNomenclatures();
		}
	}

	public CktlTypeAchatSelectionCtrl getTypeAchatSelectionCtrl() {
		if (typeAchatSelectionCtrl == null) {
			this.typeAchatSelectionCtrl = new MyCktlTypeAchatSelectionCtrl(mySession(), edc(), getExercice());
			//			if (getLaDepensePapier().isSurMarche()) {
			//				EOAttribution att = getLaDepensePapier().depenseBudgets().objectAtIndex(0).depenseControleMarches().objectAtIndex(0).attribution();
			//				typeAchatSelectionCtrl.setCurrentAttribution(att);
			//			}

		}
		return typeAchatSelectionCtrl;
	}

	public EOFournisseur getFournisseur() {
		return getLaDepensePapier().fournisseur();
		//return fournisseur;
	}

	public void setFournisseur(EOFournisseur fournisseur) {
		getLaDepensePapier().setFournisseurRelationship(fournisseur);
		//this.fournisseur = fournisseur;
	}

	public CktlSourceCreditSelectCtrl getSourceCreditSelectCtrl() {
		if (sourceCreditSelectCtrl == null) {
			this.sourceCreditSelectCtrl = new CktlSourceCreditSelectCtrl(mySession(), edc(), getExercice());
		}
		return sourceCreditSelectCtrl;
	}

	public WOActionResults onFournisseurSelect() {
		try {
			for (_IDepenseBudget unDb : getLaDepensePapier().currentDepenseBudgets()) {
				getCtrl().supprimerDepenseControleHorsMarches(edc(), unDb);
				getCtrl().supprimerDepenseControleMarches(edc(), unDb);
			}
			initForFournisseur();
		} catch (Exception e) {
			addSimpleErrorMessage("Erreur", e);
		}

		return null;
	}

	private void initForFournisseur() throws Exception {
		resetComponent();
		updateTypeAchat(edc(), getExercice(), getFournisseur());
		getLaDepensePapier().setFournisseurRelationship(getFournisseur());
		initRibs();
		updCodesNomenclatures();
		updateEngagementBudget();
	}

	private TypeAchatConfiguration defaultTypeAchatConfiguration() {
		TypeAchatConfiguration defaultTypeAchatConfig = new TypeAchatConfiguration();
		for (ETypeMarche currentTypeMarche : TYPES_MARCHE) {
			defaultTypeAchatConfig.add(currentTypeMarche, false);
		}
		return defaultTypeAchatConfig;
	}

	public void updateTypeAchat(EOEditingContext editingContext, EOExercice exercice, EOFournisseur fournisseur) {
		if (typeAchatSelectionCtrl != null) {
			List<EOAttribution> attributions = listerAttributionsValides(editingContext, fournisseur);
			List<EOCtrlSeuilMAPA> codesNomenclatures = new NSArray<EOCtrlSeuilMAPA>();
			TypeAchatConfiguration configuration = buildTypeAchatConfiguration(attributions, codesNomenclatures);
			typeAchatSelectionCtrl.updateConfiguration(configuration);
		}
	}

	private List<EOAttribution> listerAttributionsValides(EOEditingContext editingContext, EOFournisseur fournisseur) {
		if (fournisseur == null) {
			return Collections.emptyList();
		}

		Map<String, Object> criteriaMap = new HashMap<String, Object>();
		criteriaMap.put(FinderAttribution.KEY_DATE, new Date());
		criteriaMap.put(FinderAttribution.KEY_FOURNISSEUR, fournisseur);
		return FinderAttribution.getAttributionsValides(editingContext, criteriaMap);
	}

	//	private List<EOCtrlSeuilMAPA> listerSeuilsMAPA(EOEditingContext editingContext, EOExercice exercice) {
	//		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
	//		bindings.setObjectForKey(exercice, FinderCtrlSeuilMAPA.BINDING_EXERCICE);
	//		bindings.setObjectForKey(EOTypeAchat.fetchByKeyValue(editingContext, EOTypeAchat.TYPA_LIBELLE_KEY, EOTypeAchat.MARCHE_NON_FORMALISE), FinderCtrlSeuilMAPA.BINDING_TYPE_ACHAT);
	//		return FinderCtrlSeuilMAPA.getCtrlSeuilMAPAValides(editingContext, bindings);
	//	}

	private TypeAchatConfiguration buildTypeAchatConfiguration(List<EOAttribution> attributions,
			List<EOCtrlSeuilMAPA> codesNomenclatures) {
		//TODO gerer ca sous forme de builder.

		// tous les types d'achat sont actifs par defaut.
		TypeAchatConfiguration typeAchatConfiguration = defaultTypeAchatConfiguration();

		// gestion attributions
		if (attributions.isEmpty()) {
			typeAchatConfiguration.changerEtat(EnumSet.of(ETypeMarche.MARCHE), false);
		}
		else {
			typeAchatConfiguration.setAttributions(attributions);
		}

		// on met a jour les codes nomenclatures
		typeAchatConfiguration.setCodesNomenclatures(codesNomenclatures);

		return typeAchatConfiguration;
	}

	public String getContainerApresFournisseurId() {
		return getComponentId() + "_apresFournisseur";
	}

	@Override
	public void setMontantHTFacture(BigDecimal montantHTFacture) {
		super.setMontantHTFacture(montantHTFacture);

	}

	@Override
	public void setMontantTTCFacture(BigDecimal montantTTCFacture) {
		super.setMontantTTCFacture(montantTTCFacture);
	}

	public WOActionResults submitMontant() {
		try {
			updateEngagementBudget();
		} catch (Exception e) {
			addSimpleErrorMessage("Erreur", e);
		}

		return null;
	}

	protected void updateDepenseBudgetFromEngagementBudget(_IDepenseBudget db) {
		db.setDepHtSaisie(db.engagementBudget().engHtSaisie());
		db.setDepTtcSaisie(db.engagementBudget().engTtcSaisie());
		db.setTauxProrata(db.engagementBudget().tauxProrata());
		db.setDepMontantBudgetaire(db.engagementBudget().engMontantBudgetaire());
	}

	protected void updateEngagementBudget() throws Exception {
		if (getLaDepensePapier().currentDepenseBudgets().count() == 1) {
			if (getLaDepensePapier().dppHtInitial() != null && getLaDepensePapier().dppTtcInitial() != null) {
				_IDepenseBudget db = getLaDepensePapier().currentDepenseBudgets().objectAtIndex(0);

				EOEngagementBudget eb = db.engagementBudget();
				eb.setFournisseurRelationship(getFournisseur());
				eb.setEngHtSaisie(getLaDepensePapier().dppHtInitial());
				eb.setEngTtcSaisie(getLaDepensePapier().dppTtcInitial());
				eb.setEngLibelle(getLaDepensePapier().dppNumeroFacture());
				BigDecimal engTvaSaisie = eb.engTtcSaisie().subtract(eb.engHtSaisie());

				EOTauxProrata tauxProrata = getSourceCreditTO().getTauxProrataSelectionne();
				if (tauxProrata != null) {
					BigDecimal engMontantBudgetaire = tauxProrata.montantBudgetaire(eb.engHtSaisie(), engTvaSaisie);
					eb.setEngMontantBudgetaire(engMontantBudgetaire);
					eb.setEngMontantBudgetaireReste(engMontantBudgetaire);
					eb.setTauxProrataRelationship(tauxProrata);
				}
				else {
					eb.setEngMontantBudgetaire(BigDecimal.ZERO);
					eb.setEngMontantBudgetaireReste(BigDecimal.ZERO);
				}
				updateDepenseBudgetFromEngagementBudget(db);
				updateDepenseCtrlMarcheWithAttribution();
				updDicos();
			}
			//creerEngagementBudget();
			//eb.initialiserDepenseBudget(edc(), getLaDepensePapier(), utilisateurInContext(edc()), true);
		}
	}

	protected SourceCreditsEngagementTO getSourceCreditTO() {
		return sourceCreditTO;
	}

	protected void setSourceCreditTO(SourceCreditsEngagementTO sourceCreditTO) {
		this.sourceCreditTO = sourceCreditTO;
	}

	@Override
	public Boolean isEngagementDisabled() {
		return Boolean.TRUE;
	}

	@Override
	public Boolean isAfficherRepartitions() {
		Boolean res = super.isAfficherRepartitions();
		if (res) {
			//Verifier que tous les depenses budgets présents sont complets (organ, prorata, tc)
			NSArray<? extends _IDepenseBudget> dbs = getLaDepensePapier().currentDepenseBudgets();
			for (_IDepenseBudget aDepenseBudget : dbs) {
				EOEngagementBudget eb = aDepenseBudget.engagementBudget();
				if (eb.source() == null || !eb.source().isComplet()) {
					return Boolean.FALSE;
				}
			}
		}
		return res;
	}

	private SourceCreditsEngagementTO convertBudget(EOBudgetExecCredit credit) {
		SourceCreditsEngagementTO sourceTO = new SourceCreditsEngagementTO();
		sourceTO.setBudgetExecCredit(credit);
		NSArray<EOTauxProrata> creditTauxProrataList = credit.getTauxProratas(edc());
		sourceTO.setTauxProrataDisponibles(creditTauxProrataList);
		sourceTO.setTauxProrataSelectionne(getTauxProrataDefautForCreditBudgetaire(creditTauxProrataList));
		return sourceTO;
	}

	/**
	 * @param creditTauxProrataList un tableau représentant les taux de prorata disponibles
	 * @return Le taux de prorata à utiliser par défaut pour la création d'une liquidation sur credit budgetaire.
	 */
	protected EOTauxProrata getTauxProrataDefautForCreditBudgetaire(NSArray<EOTauxProrata> creditTauxProrataList) {
		if (creditTauxProrataList != null && creditTauxProrataList.size() > 0) {
			EOTauxProrata tauxProrataParDefaut = creditTauxProrataList.get(0);
			return tauxProrataParDefaut;
		}
		return null;
	}

	public NSArray<EOTauxProrata> loadTauxProrata(EOBudgetExecCredit credit) {
		EOOrgan creditOrgan = credit.organ();
		if (creditOrgan == null) {
			return NSArray.emptyArray();
		}

		// TODO idealement utilisee un Map ici
		NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
		bindings.setObjectForKey(getExercice(), FinderProrata.BINDING_EXERCICE);
		bindings.setObjectForKey(creditOrgan, FinderProrata.BINDING_ORGAN);

		return FinderProrata.getTauxProratasValides(edc(), bindings);
	}

	/**
	 * Ce qu'on fait lorsque l'utilisateur affecte des crédits budgétaires.
	 * 
	 * @throws Exception
	 */
	protected void processAjouterCredits() throws Exception {
		EOBudgetExecCredit creditSelectionne = getSourceCreditSelectCtrl().getSourceCreditsSelectionnee();
		this.sourceCreditTO = convertBudget(creditSelectionne);
		getUneDepenseBudget().engagementBudget().setOrganRelationship(getSourceCreditTO().getBudgetExecCredit().organ());
		getUneDepenseBudget().engagementBudget().setTypeCreditRelationship(getSourceCreditTO().getBudgetExecCredit().typeCredit());
		getUneDepenseBudget().engagementBudget().setTauxProrataRelationship(getSourceCreditTO().getTauxProrataSelectionne());
		getUneDepenseBudget().setSource(new EOSource(getSourceCreditTO().getBudgetExecCredit().organ(), getSourceCreditTO().getBudgetExecCredit().typeCredit(), getSourceCreditTO().getTauxProrataSelectionne(), getExercice()));
		updateEngagementBudget();
	}

	public WOActionResults ajouterCreditsCallback() {
		try {
			processAjouterCredits();
			AjaxUpdateContainer.updateContainerWithID("ContainerEngagement", context());
			AjaxUpdateContainer.updateContainerWithID("ContainerRepartitions", context());
		} catch (Exception e) {
			addSimpleErrorMessage("Erreur", e);
		}
		return null;
	}

	public WOActionResults annulerCreditsCallback() {
		return null;
	}

	public WOActionResults submitProrata() {
		WOActionResults res = super.submitProrata();
		try {
			if (ESourceCreditType.BUDGET.equals(getUneDepenseBudget().getSourceTypeCredit())) {
				getSourceCreditTO().setTauxProrataSelectionne(getUneDepenseBudget().getSource().tauxProrata());
			}
			getUneDepenseBudget().updateFromSource();
			updateEngagementBudget();
		} catch (Exception e) {
			addSimpleErrorMessage("Erreur", e);
		}
		return res;
	}

	@Override
	protected void updCodesNomenclatures() {
		//si typeAchat non marché
		if (typeAchat() != null) {
			//EOFournisseur fournisseur = getFournisseur();
			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
			bindings.setObjectForKey(getExercice(), "exercice");
			//Comme on n'affiche pas les types d'achats, on les prend tous
			//bindings.setObjectForKey(typeAchat(), "typeAchat");
			//			if (fournisseur != null && (typeAchat().typaLibelle().equals(EOTypeAchat.MONOPOLE) ||
			//					typeAchat().typaLibelle().equals(EOTypeAchat.TROIS_CMP))) {
			//				bindings.setObjectForKey(fournisseur, "fournisseur");
			//			}
			setLesCodesNomenclature(FinderCodeExer.getCodeExerValides(edc(), bindings));
		}
		else {
			setLesCodesNomenclature(new NSArray<EOCodeExer>());
		}
	}

	@Override
	public EOTypeAchat typeAchat() {
		if (getTypeAchatSelectionCtrl() == null || getTypeAchatSelectionCtrl().getTypeAchatTO() == null) {
			return null;
		}
		EOTypeAchat typeAchat = getTypeAchatSelectionCtrl().getTypeAchatTO().getTypeAchat();
		return typeAchat;
	}

	@Override
	public EOAttribution getAttribution() {
		return getTypeAchatSelectionCtrl().getTypeAchatTO().getAttributionSelectionnee();
	}

	@Override
	public boolean isProrataDisabled() {
		boolean res = super.isProrataDisabled();
		return res || (getUneDepenseBudget().engagementBudget().organ() == null);
	}

	public Boolean isTypeAchatHorsMarche() {
		return typeAchat() != null;
	}

	public _IDepenseBudget getUneDepenseBudgetForMarche() {
		return uneDepenseBudgetForMarche;
	}

	public void setUneDepenseBudgetForMarche(_IDepenseBudget uneDepenseBudgetForMarche) {
		this.uneDepenseBudgetForMarche = uneDepenseBudgetForMarche;
	}

	public Boolean sourceBudgetSelectDisabled() {
		return !(getLaDepensePapier().dppTtcInitial() != null && getLaDepensePapier().dppTtcInitial().floatValue() != 0);
	}
	
	public NSArray<EOCodeExer> lesCodesExerDeLaDepense() {
		NSMutableArray<EOCodeExer> lesCodesExer = new NSMutableArray<EOCodeExer>();
		if (isTypeAchatHorsMarche()) {
			if (getLaDepensePapier().depenseBudgets() != null) {
				for (EODepenseBudget dpb : getLaDepensePapier().depenseBudgets()) {
					for (EODepenseControleHorsMarche dpchm : dpb.depenseControleHorsMarches()) {
						if (dpchm.codeExer() != null) {
							lesCodesExer.add(dpchm.codeExer());
						}
					}
				}
			}
		} else {
			if (getAttribution() != null) {
				for (EOLotNomenclature ln : getAttribution().lot().lotNomenclatures()) {
					if (ln.codeExer() != null && ln.codeExer().exercice().equals(getExercice())) {
						lesCodesExer.add(ln.codeExer());
					}
				}
			}
		}
		
		return lesCodesExer;
	}
}