/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.NavigationCtrl;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryArticle;
import org.cocktail.fwkcktldepense.server.finder.FinderCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class ArticleModification extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	private EOArticle article = null;
	private String avertissement = "";
	private String filterTextCodeNomenclature = null;
	private NSArray<EOCodeExer> lesCodesNomenclature = null;
	public EOCodeExer unCodeNomenclature = null;
	private boolean isCatalogue = false;

	public ArticleModification(WOContext context) {
		super(context);
	}

	public WOComponent modifier() {
		EOCommande commande = laCommandeEnCours();
		FactoryArticle fa = new FactoryArticle();
		try {
			fa.modifierArticle(getArticle());
		} catch (FactoryException e) {
			session.setAlertMessage(e.getMessageFormatte());
		}
		if (commande.isRepartitionAutomatiquePossible()) {
			commande.setRepartitionAutomatiquePossible();
		}
		return NavigationCtrl.goToSavedBonDeCommande(session, commande, null, "Articles");
	}

	public boolean isPopupCodesNomenclatureDisabled() {
		boolean isPopupCodesNomenclatureDisabled = false;
		EOCommande commande = laCommandeEnCours();
		if (commande != null) {
			EOTypeAchat typeAchat = commande.typeAchat();
			EOAttribution attribution = commande.attribution();

			if (attribution != null && attribution.lot().isCatalogue() ||
					(typeAchat != null && typeAchat.typaLibelle().equals(EOTypeAchat.MONOPOLE))) {
				isPopupCodesNomenclatureDisabled = true;
			}
		}
		else {
			isPopupCodesNomenclatureDisabled = true;
		}
		return isPopupCodesNomenclatureDisabled;
	}

	public WOComponent neFaitRien() {
		return null;
	}

	public EOArticle getArticle() {
		return article;
	}

	public void setArticle(EOArticle article) {
		this.article = article;
	}

	public String getAvertissement() {
		return avertissement;
	}

	public void setAvertissement(String avertissement) {
		this.avertissement = avertissement;
	}

	public NSArray<EOCodeExer> getLesCodesNomenclature() {
		lesCodesNomenclature = FinderCodeExer.getCodeExerPourCommande(laCommandeEnCours().editingContext(), laCommandeEnCours());
		return lesCodesNomenclature;
	}

	public void setLesCodesNomenclature(NSArray<EOCodeExer> lesCodesNomenclature) {
		this.lesCodesNomenclature = lesCodesNomenclature;
	}

	public boolean isCatalogue() {
		return isCatalogue;
	}

	public void setIsCatalogue(boolean isCatalogue) {
		this.isCatalogue = isCatalogue;
	}

	public String getFilterTextCodeNomenclature() {
		return filterTextCodeNomenclature;
	}

	public void setFilterTextCodeNomenclature(String filterTextCodeNomenclature) {
		this.filterTextCodeNomenclature = filterTextCodeNomenclature;
	}
}