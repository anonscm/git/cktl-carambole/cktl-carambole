/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementBudget;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata;
import org.cocktail.fwkcktldepense.server.metier.EOTauxProrata.ETypeMontant;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepense.server.metier.SourceCreditExtourneLiquidation;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * Liquidation à partir d'une liquidation d'extourne.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class LiquidationSurExtourneMandat extends LiquidationSurExtourneEnveloppe {

	private static final long serialVersionUID = 1L;

	public static final String PAGE_NAME = LiquidationSurExtourneMandat.class.getName();

	private EODepenseBudget laLiquidationExtourne;

	public LiquidationSurExtourneMandat(WOContext context) {
		super(context);
	}

	public void setLaLiquidationExtourne(EODepenseBudget liquidationExtourne) {
		this.laLiquidationExtourne = liquidationExtourne;
	}

	/**
	 * @return La liquidation d'extourne
	 */
	public EODepenseBudget getLaLiquidationExtourne() {
		return laLiquidationExtourne;
	}

	@Override
	protected void initialisation() {
		super.initialisation();
		getLaDepensePapier().setFournisseurRelationship(getLaLiquidationExtourne().depensePapier().fournisseur());
		getLaDepensePapier().currentDepenseBudgets().objectAtIndex(0).setSourceTypeCredit(ESourceCreditType.EXTOURNE);
	}

	@Override
	public String getPageTitle() {
		String prefix = "Création d'une pré-liquidation";
		if (!isPreLiquidation()) {
			prefix = "Création d'une liquidation";
		}
		return prefix.concat(" sur mandat (liquidation) d'extourne");
	}

	@Override
	public NSArray<String> itemsMenu() {
		NSArray<String> items = new NSArray<String>(new String[] {
				Menu.ENREGISTRER_UNE_LIQUIDATION_SUR_EXTOURNE_MANDAT_KEY, Menu.ANNULER_UNE_LIQUIDATION_KEY
		});
		return items;
	}

	public String getCommandeLibelle() {
		return (laCommandeEnCours() != null ? laCommandeEnCours().commLibelle() : getLaLiquidationExtourne().depensePapier().dppNumeroFacture());
	}

	@Override
	public EOCommande laCommandeEnCours() {
		if (getLaLiquidationExtourne().engagementBudget().commande() != null) {
			return getLaLiquidationExtourne().engagementBudget().commande();
		}
		return null;
	}

	@Override
	protected void updateEngagementBudget() throws Exception {

		EODepensePapier dpp = getLaDepensePapier();
		_IDepenseBudget dbSurExtourne = dpp.currentDepenseBudgets().objectAtIndex(0);
		_IDepenseBudget dbSurBudget;

		//traiter la liquidation d'extourne comme une source de credit
		if (!ESourceCreditType.EXTOURNE.equals(dbSurExtourne.getSourceTypeCredit())) {
			throw new Exception("Erreur : le type de la source de credit devait etre EXTOURNE");
		}
		if (dbSurExtourne.getSource().tauxProrata() == null) {
			return;
		}

		SourceCreditExtourneLiquidation src = (SourceCreditExtourneLiquidation) dbSurExtourne.getSource();
		//recalculer le disponible sur la source en fonction du taux de prorata sélectionné et du TTC + HT de la source
		EOTauxProrata tp = src.tauxProrata();
		BigDecimal srcDispo = src.getDisponible().abs();

		BigDecimal dppMontantTtc = laDepensePapierEnCours().dppTtcInitial();
		BigDecimal dppMontantHt = laDepensePapierEnCours().dppHtInitial();
		BigDecimal dppMontantTva = dppMontantTtc.subtract(dppMontantHt);
		//montant budgetaire calculé dans l'hypothese ou la depense ne se fait que sur le source extournée
		BigDecimal dppMontantBud = tp.montantBudgetaire(dppMontantHt, dppMontantTva);

		if (dppMontantBud.compareTo(srcDispo) > 0) {
			//on a un depassement, on cree/met à jour un depenseBudget sur exercice en cours
			dbSurExtourne.updateFromSource();
			NSDictionary<ETypeMontant, BigDecimal> montants = dbSurExtourne.tauxProrata().htEtTtcAPartirMontantBudgetaireDispo(dppMontantHt, dppMontantTtc, srcDispo);
			dbSurExtourne.setDepHtSaisie(montants.get(ETypeMontant.HT));
			dbSurExtourne.setDepTtcSaisie(montants.get(ETypeMontant.TTC));
			dbSurExtourne.setDepMontantBudgetaire(srcDispo);

			if (dpp.currentDepenseBudgets().count() > 1) {
				dbSurBudget = dpp.currentDepenseBudgets().objectAtIndex(1);
			}
			else {
				FactoryEngagementBudget feb = new FactoryEngagementBudget();
				EOTypeApplication typeApplication = FinderTypeApplication.getTypeApplication(edc(), EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
				EOEngagementBudget ebSurBudget = feb.creer(edc());
				ebSurBudget.setFournisseurRelationship(getFournisseur());
				ebSurBudget.setEngLibelle(getLaDepensePapier().dppNumeroFacture());
				ebSurBudget.setEngHtSaisie(BigDecimal.ZERO);
				ebSurBudget.setEngTtcSaisie(BigDecimal.ZERO);
				ebSurBudget.setEngMontantBudgetaire(BigDecimal.ZERO);
				ebSurBudget.setEngMontantBudgetaireReste(BigDecimal.ZERO);
				ebSurBudget.setTypeApplicationRelationship(typeApplication);
				ebSurBudget.setExerciceRelationship(exerciceCourant());
				ebSurBudget.setUtilisateurRelationship(utilisateur());
				dbSurBudget = getCtrl().getFdb().creer(edc(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, dpp,
						ebSurBudget, ebSurBudget.tauxProrata(), ebSurBudget.exercice(), utilisateur(), ESourceCreditType.BUDGET, ebSurBudget.source());
				//dbSurBudget.setSourceTypeCredit(ESourceCreditType.BUDGET);
			}

			BigDecimal dbbMontantTtc = laDepensePapierEnCours().dppTtcInitial().subtract(dbSurExtourne.depTtcSaisie());
			BigDecimal dbbMontantHt = laDepensePapierEnCours().dppHtInitial().subtract(dbSurExtourne.depHtSaisie());
			BigDecimal dbbMontantTva = dbbMontantTtc.subtract(dbbMontantHt);
			if (dbSurBudget.getSource().tauxProrata() != null) {
				dbSurBudget.updateFromSource();
				BigDecimal dbbMontantBud = dbSurBudget.getSource().tauxProrata().montantBudgetaire(dbbMontantHt, dbbMontantTva);
				EOEngagementBudget ebSurBudget = dbSurBudget.engagementBudget();
				ebSurBudget.setEngHtSaisie(dbbMontantHt);
				ebSurBudget.setEngTtcSaisie(dbbMontantTtc);
				ebSurBudget.setEngMontantBudgetaire(dbbMontantBud);
				ebSurBudget.setEngMontantBudgetaireReste(dbbMontantBud);
				updateDepenseBudgetFromEngagementBudget(dbSurBudget);
			}

		}
		else {
			//pas de depassement, on supprime  les depenseBudgets et engagementBudget eventuels sur budget exercice
			_IFactoryDepenseBudget fdb = getCtrl().getFdb();
			FactoryEngagementBudget feb = new FactoryEngagementBudget();
			for (int i = dpp.currentDepenseBudgets().count() - 1; i > 0; i--) {
				feb.supprimer(edc(), dpp.currentDepenseBudgets().objectAtIndex(i).engagementBudget());
				fdb.supprimer(edc(), dpp.currentDepenseBudgets().objectAtIndex(i));
			}
			dbSurExtourne.updateFromSource();
			dbSurExtourne.setDepHtSaisie(dppMontantHt);
			dbSurExtourne.setDepTtcSaisie(dppMontantTtc);
			dbSurExtourne.setDepMontantBudgetaire(dppMontantBud);

		}
		updateDepenseCtrlMarcheWithAttribution();
		updDicos();

	}

	public boolean getDepIsSoldee() {
		return ((EODepenseBudget) getUneDepenseBudget()).solde();
	}

	public void setDepIsSoldee(boolean depIsSoldee) {
		boolean oldValue = ((EODepenseBudget) getUneDepenseBudget()).solde();
		if (oldValue != depIsSoldee) {
			((EODepenseBudget) getUneDepenseBudget()).setSolde(depIsSoldee);
		}
	}

	public Boolean isCocheSoldeeDisabled() {
		return Boolean.valueOf(getUneDepenseBudget() == null || !isFactureComplete());
	}

	public Boolean isMontantBudGreaterThanResteEng() {
		return Boolean.FALSE;
	}

	@Override
	public EOAttribution getAttribution() {
		if (getLaLiquidationExtourne().depenseControleMarches().count() > 0) {
			return getLaLiquidationExtourne().depenseControleMarches().objectAtIndex(0).attribution();
		}
		return null;
	}

}