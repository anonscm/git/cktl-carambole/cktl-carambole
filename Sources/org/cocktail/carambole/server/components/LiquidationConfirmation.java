/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.components;

import java.util.List;

import javax.mail.MessagingException;

import org.cocktail.carambole.server.MyAjaxPage;
import org.cocktail.carambole.server.components.service.ServiceMailLiquidation;
import org.cocktail.carambole.server.components.service.ServiceMailLiquidation.AdresseMail;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector.DestinataireListeElt;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.finder.FinderDepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOIndividu;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.process.ProcessDepense;
import org.cocktail.fwkcktldepense.server.process.ProcessPreDepense;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LiquidationConfirmation extends MyAjaxPage {

	private static final long serialVersionUID = 1L;
	private String onLoadJS = null;
	private NSArray<EODepensePapier> facturesEnDouble = null;
	public EODepensePapier uneFactureEnDouble = null;
	private ServiceMailLiquidation service;
	private CktlAjaxDestinatairesListeSelector.DestinataireListe destinatairesForDemandeSF = null;

	public LiquidationConfirmation(WOContext context) {
		super(context);
		service = new ServiceMailLiquidation();
		service.setApplication(myApp());
	}

	public void reset() {
		onLoadJS = null;
		facturesEnDouble = null;
		uneFactureEnDouble = null;
	}

	public void appendToResponse(WOResponse res, WOContext ctx) {
		super.appendToResponse(res, ctx);
	}

	public boolean isFactureNumeroDejaExistant() {
		boolean isFactureNumeroDejaExistant = false;

		if (laCommandeEnCours() != null) {
			NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			dico.setObjectForKey(laCommandeEnCours(), "commande");
			dico.setObjectForKey(laDepensePapierEnCours().dppNumeroFacture(), "dppNumeroFacture");

			facturesEnDouble = FinderDepensePapier.getDepensePapierPourNumeroFacture(laDepensePapierEnCours().editingContext(), dico);
			if (facturesEnDouble != null && facturesEnDouble.count() > 0) {
				isFactureNumeroDejaExistant = true;
			}
		}
		return isFactureNumeroDejaExistant;
	}

	public WOComponent modifier() {
		return null;
	}

	public WOComponent liquider() {
		WOComponent nextPage = null;
		try {
			EODepensePapier dp = laDepensePapierEnCours();
			EOEditingContext edc = dp.editingContext();
			switch (dp.getLiquidationType()) {
			case SUR_COMMANDE:

				if (isPreLiquidation()) {
					if (getDestinatairesForDemandeSF().hasEmailsNonValides()) {
						throw new FactoryException("Certaines adresses email ne sont pas valides. Veuillez les corriger avant de poursuivre.", true);
					}

					ProcessPreDepense.enregistrer(session.dataBus(), edc, dp, utilisateurInContext(edc));

					try {
						envoyerEmailDemandeCertification();
					} catch (MessagingException e) {
						e.printStackTrace();
						session.setAlertMessage("La liquidation a bien été enregistrée, mais la demande de certification par email n'a pas été correctement envoyée pour la raison suivante :" + e.getMessage());
						setOnLoadJS("parent.MenuUpdate();");
					} catch (Exception e) {
						e.printStackTrace();
						session.setAlertMessage("La liquidation a bien été enregistrée, mais la demande de certification par email n'a pas été correctement envoyée pour la raison suivante :" + e.getMessage());
						setOnLoadJS("parent.MenuUpdate();");
					}
				}
				else {
					ProcessDepense.enregistrer(session.dataBus(), edc, dp, utilisateurInContext(edc));
				}

				break;
			case SANS_ENGAGEMENT:
				dp.validateObjectMetier();
				EODepenseBudget db = dp.depenseBudgets().objectAtIndex(0);
				ProcessDepense.enregistrerLiquidationDirecte(session.dataBus(), edc, dp, db.engagementBudget().organ(), db.engagementBudget().typeCredit(), db.engagementBudget().engLibelle(), db.engagementBudget().typeApplication(), db.engagementBudget().utilisateur());
				break;

			case SUR_EXTOURNE_ENVELOPPE:
				dp.validateObjectMetier();
				ProcessDepense.enregistrerLiquidationSurExtourne(session.dataBus(), edc, dp, dp.depenseBudgets().objectAtIndex(0), (dp.depenseBudgets().count() > 1 ? dp.depenseBudgets().objectAtIndex(1) : null), dp.utilisateur());
				break;

			case SUR_EXTOURNE:
				dp.validateObjectMetier();
				ProcessDepense.enregistrerLiquidationSurExtourne(session.dataBus(), edc, dp, dp.depenseBudgets().objectAtIndex(0), (dp.depenseBudgets().count() > 1 ? dp.depenseBudgets().objectAtIndex(1) : null), dp.utilisateur());
				break;

			default:
				throw new FactoryException("Le type de la depensePapier n'est pas spécifié.", true);
			}

			nextPage = pageWithName(LiquidationApresEnregistrement.class.getName());
		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				if (e.isInformatif()) {
					if (myApp().test()) {
						e.printStackTrace();
					}
					// Exception contenant un message d'information pour l'utilisateur
					session.setAlertMessage(alertMessage);
					setOnLoadJS("parent.MenuUpdate();");
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				nextPage = pageWithName(LiquidationApresEnregistrement.class.getName());
			}
		} catch (RuntimeException e1) {
			session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}
		return nextPage;
	}

	public Boolean isPreLiquidation() {
		return laDepensePapierEnCours().isPreLiquidation();
	}

	public NSArray<EODepensePapier> getFacturesEnDouble() {
		return facturesEnDouble;
	}

	public void setFacturesEnDouble(NSArray<EODepensePapier> facturesEnDouble) {
		this.facturesEnDouble = facturesEnDouble;
	}

	public EODepensePapier getUneFactureEnDouble() {
		return uneFactureEnDouble;
	}

	public void setUneFactureEnDouble(EODepensePapier uneFactureEnDouble) {
		this.uneFactureEnDouble = uneFactureEnDouble;
	}

	public String getOnLoadJS() {
		return onLoadJS;
	}

	public void setOnLoadJS(String onLoadJS) {
		this.onLoadJS = onLoadJS;
	}

	public CktlAjaxDestinatairesListeSelector.DestinataireListe getDestinatairesForDemandeSF() {
		if (destinatairesForDemandeSF == null) {
			fillDestinatairesForDemandeSF();
		}
		return destinatairesForDemandeSF;
	}

	protected void fillDestinatairesForDemandeSF() {	
		reinitialiserListeDestinataires();
		service.setSession(session);
		service.setLaDepensePapier(laDepensePapierEnCours());
		List<AdresseMail> listeAdresses = service.getAdressesMailForDemandeSF();
		
		for(AdresseMail adresse : listeAdresses) {
			destinatairesForDemandeSF.addObjectIfAdresseNotPresent(adresse.getOption(), null, adresse.getAdresse());
		}
	}


	/**
	 * @return
	 */
	private void reinitialiserListeDestinataires() {
		destinatairesForDemandeSF = new CktlAjaxDestinatairesListeSelector.DestinataireListe();
	}

	public Boolean isServiceFaitRempli() {
		return Boolean.FALSE;
	}

	public Boolean isLiquidationComplete() {
		return Boolean.FALSE;
	}

	public Boolean IsNotificationEnvoyee() {
		return Boolean.FALSE;
	}

	public void envoyerEmailDemandeCertification() throws Exception {
		CktlMailMessage msg = buildDemandeCertificationSFMail();
		msg.send();
	}

	public CktlMailMessage buildDemandeCertificationSFMail() throws Exception {
		CktlUserInfoDB utilisateurInfos = new CktlUserInfoDB(session.dataBus());
		EOUtilisateur utilisateur = utilisateurInContext(laDepensePapierEnCours().editingContext());
		EOIndividu individu = utilisateur.individu();
		Integer noIndividu = (Integer) EOUtilities.primaryKeyForObject(individu.editingContext(), individu).allValues().lastObject();
		utilisateurInfos.individuForNoIndividu(noIndividu, true);
		String emailFrom = utilisateurInfos.email();

		//Recuperer l'adresse des destinataires
		NSArray<String> tos = destinatairesForDemandeSF.getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_TO);
		String to = tos.componentsJoinedByString(",");

		NSArray<String> ccs = destinatairesForDemandeSF.getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_CC);
		//		String cc = ccs.componentsJoinedByString(",");

		NSArray<String> bccs = destinatairesForDemandeSF.getEmailsForOption(DestinataireListeElt.DESTINATAIRE_OPTION_BCC);
		//	String bcc = bccs.componentsJoinedByString(",");

		String hostmail = EOGrhumParametres.parametrePourCle(laDepensePapierEnCours().editingContext(), EOFournis.GRHUM_HOST_MAIL_PARAMKEY);
		if (hostmail == null) {
			throw new NullPointerException("Parametre GRHUM " + EOFournis.GRHUM_HOST_MAIL_PARAMKEY + " non defini.");
		}

		String reply = emailFrom;
		EOCommande commandeAssociee = laDepensePapierEnCours().commandeAssociee();
		String subject = "Demande de certification de service fait (fact. " + laDepensePapierEnCours().dppNumeroFacture() + " / " + laDepensePapierEnCours().fournisseur().libelle() + ")";
		String body = " Merci de certifier le service fait dès que ce sera le cas pour la facture suivante : \n " + "\n" +
				"N° facture         : " + laDepensePapierEnCours().dppNumeroFacture() + "\n" +
				"Date Facture       : " + myApp().appDateFormatter.format(laDepensePapierEnCours().dppDateFacture()) + "\n" +
				"Montant Budgétaire : " + myApp().app2DecimalesFormatter().format(laDepensePapierEnCours().montantBudgetaire()) + "\n" +
				"Fournisseur        : " + laDepensePapierEnCours().fournisseur().libelle() + "\n" +
				"Date commande      : " + myApp().appDateFormatter.format(commandeAssociee.commDate()) + "\n" +
				"N° commande        : " + commandeAssociee.commNumero() + "\n" +
				"Référence commande : " + commandeAssociee.commReference() + "\n" +
				"Libellé commande   : " + commandeAssociee.commLibelle();

		String dppId = laDepensePapierEnCours().dppIdProc().toString();

		body = body + "\n\n" + myApp().getApplicationURL(mySession().context()) + "/wa/DAConsultFact?dppId=" + dppId;
		body = body + "\n\nCe message a été généré automatiquement à partir de l'application Carambole.";

		CktlMailMessage tmp = new CktlMailMessage(hostmail);

		if (myApp().test()) {
			body = "MSG ENVOYE PAR CARAMBOLE POUR TESTS\n" +
					"TO : " + to + "\n" +
					"CC : " + ccs.componentsJoinedByString(",") + "\n" +
					"BCC : " + bccs.componentsJoinedByString(",") + "\n\n" +
					"MSG ORIGINAL : \n" + body;
			if (myApp().appAdminMail() == null) {
				throw new NullPointerException("Parametre de configuration APP_ADMIN_MAIL non defini.");
			}
			tmp.initMessage(emailFrom, myApp().appAdminMail(), subject, body);
		}
		//EVOLUTION ICI ==>DT0006651: Service Facturier - Demande de certification de service fait //
        else if( service.isMailServiceFacturierParametre() ) {
          tmp.initMessage(emailFrom, myApp().mailCentralisationCertification(), subject, body);
          String[] accs = new String[] {};
          String[] abccs = new String[] {};

          for (int i = 0; i < ccs.count(); i++) {
              String string = ccs.objectAtIndex(i);
              accs[i] = string;
          }
          for (int i = 0; i < bccs.count(); i++) {
              String string = bccs.objectAtIndex(i);
              abccs[i] = string;
          }

          tmp.addCCs(accs);
          tmp.addBCCs(abccs);
          tmp.setReplyTo(reply);
              }

		//FIN EVOLUTION //
		else {
			tmp.initMessage(emailFrom, to, subject, body);
			String[] accs = new String[] {};
			String[] abccs = new String[] {};

			for (int i = 0; i < ccs.count(); i++) {
				String string = ccs.objectAtIndex(i);
				accs[i] = string;
			}
			for (int i = 0; i < bccs.count(); i++) {
				String string = bccs.objectAtIndex(i);
				abccs[i] = string;
			}

			tmp.addCCs(accs);
			tmp.addBCCs(abccs);
			//tmp.addTOs((String[]) tos.toArray());
			tmp.setReplyTo(reply);
		}

		return tmp;

	}
}
