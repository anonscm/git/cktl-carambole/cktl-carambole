/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.carambole.server;

import org.cocktail.fwkcktldepense.server.b2b.factory.FactoryCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODirectAction;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Permet de gerer les retours des panier à partir des sites b2b cxml.
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class B2bCxmlDirectAction extends WODirectAction {

	public B2bCxmlDirectAction(WORequest arg0) {
		super(arg0);
	}

	//Rod b2b

	public WOActionResults punchOutOrderFeedBackAction() {
		WORequest request = context().request();
		try {
			Session session = null;
			//String wosid = getSessionIDForRequest(request);
			//System.out.println("SID = " + wosid);
			//On tente de restaurer la session
			session = (Session) existingSession();
			//			if (existingSession() != null) {
			//				session = (Session) Application.application().restoreSessionWithID(wosid, context());
			//			}

			if (session == null) {
				//System.out.println("session NON recuperee");
				return pageWithName("CktlTimeoutPage");
			}
			else {
				//System.out.println("session recuperee");
				EOEditingContext edc = null;
				if (session.getLaCommandeEnCours() != null) {
					edc = session.getLaCommandeEnCours().editingContext();
				}
				if (edc == null) {
					edc = session.defaultEditingContext();
				}

				//memoriser le message de punchOut

				FactoryCxmlPunchoutmsg factoryCxmlPunchoutmsg = new FactoryCxmlPunchoutmsg();
				String xmlMsg = (String) request.formValueForKey("cxml-urlencoded");

				//System.out.println("msg recu : " + xmlMsg);

				if (MyStringCtrl.isEmpty(xmlMsg)) {
					throw new Exception("Aucun message cXML transmis dans cxml-urlencoded");
				}
				EOB2bCxmlPunchoutmsg pomsg = factoryCxmlPunchoutmsg.creerPunchoutmsg(edc, xmlMsg);

				//creer precommande a partir de punchOutOrder
				//FactoryCommande fa = new FactoryCommande();
				EOCommande commande = session.factoryCommande.creer(edc, pomsg, session.getLaCommandeEnCours());

				//rediriger sur page commande
				session.setLaCommandeEnCours(commande);
				commande.reconstruireTableaux();

				return NavigationCtrl.goToBonDeCommande(context(), commande, null, "Articles");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}

	}

	public static String punchOutOrderFeedBackUrl(WOContext context) {
		CktlWebApplication cktlApp = (CktlWebApplication) WOApplication.application();
		String appInstanceURL = cktlApp.getApplicationInstanceURL(context);
		StringBuffer actionUrl = new StringBuffer(appInstanceURL);
		if (!appInstanceURL.endsWith("/")) {
			actionUrl.append("/");
		}
		actionUrl.append("wa/");
		actionUrl.append("B2bCxmlDirectAction/punchOutOrderFeedBack");
		actionUrl.append("?" + WOApplication.application().sessionIdKey() + "=");
		actionUrl.append(context.session().sessionID());

		return actionUrl.toString();
	}
}
