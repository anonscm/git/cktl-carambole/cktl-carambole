/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import org.cocktail.carambole.server.components.Accueil;
import org.cocktail.carambole.server.components.CommandeApresEnregistrement;
import org.cocktail.carambole.server.components.Imprimer;
import org.cocktail.carambole.server.components.LoginCAS;
import org.cocktail.carambole.server.components.LoginLocal;
import org.cocktail.carambole.server.components.Main;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlLogin;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORequest;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Emmanuel GEZE <emgeze at gmail.com>
 * @author rprin
 */
public class DirectAction extends CktlWebAction {
	private String loginComment;

	public DirectAction(WORequest aRequest) {
		super(aRequest);
	}

	public Session jefySession() {
		return (Session) session();
	}

	public Application jefyApp() {
		return (Application) WOApplication.application();
	}

	/**
	 * Execute l'action par defaut de l'application Jefyco. Elle affiche la page de connexion e l'application.
	 */
	public WOActionResults myDefaultAction() {
		if (useCasService())
			return loginCASPage();
		else
			return loginNoCasPage(null);
	}

	public WOActionResults defaultAction() {
		return pageWithName(Main.class.getName());
	}

	/**
	 * Retourne la page avec la description des services et directActions de l'application.
	 */
	public WOActionResults servicesAction() {
		return pageWithName("Services");
	}

	//	public WOActionResults validateLoginAction() {
	//		WORequest req = context().request();
	//		String login = StringCtrl.normalize((String) req.formValueForKey("login"));
	//		String password = StringCtrl.normalize((String) req.formValueForKey("pwd"));
	//		boolean erreurLogin = false;
	//		boolean erreurBD = false;
	//		boolean erreurPassword = false;
	//		boolean erreurDroits = false;
	//		Session session = (Session) context().session();
	//		CktlLoginResponder loginResponder = getNewLoginResponder(null);
	//		CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
	//		if (login.length() == 0) {
	//			erreurLogin = true;
	//		}
	//		else if (!loginResponder.acceptLoginName(login)) {
	//			erreurDroits = true;
	//		}
	//		else {
	//			if (password == null)
	//				password = "";
	//			loggedUserInfo.setRootPass(loginResponder.getRootPassword());
	//			loggedUserInfo.setAcceptEmptyPass(loginResponder.acceptEmptyPassword());
	//			loggedUserInfo.compteForLogin(login, password, true);
	//			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
	//				erreurLogin = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_COMPTE);
	//				erreurPassword = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_PASSWORD);
	//				erreurBD = (loggedUserInfo.errorCode() == CktlUserInfo.ERROR_SOURCE);
	//				if (loggedUserInfo.errorMessage() != null)
	//					CktlLog.rawLog(">> Erreur | " + loggedUserInfo.errorMessage());
	//			}
	//		}
	//		if (erreurLogin || erreurBD || erreurPassword || erreurDroits) {
	//			session.setConnectedUserInfo(loggedUserInfo);
	//			LoginLocal nextPage = (LoginLocal) pageWithName("LoginLocal");
	//			return nextPage;
	//		}
	//		else {
	//			session.setConnectedUserInfo(loggedUserInfo);
	//			String erreur = session.setConnectedUser(loggedUserInfo.login());
	//			if (erreur != null) {
	//				CktlLog.log("CONNEXION IMPOSSIBLE : " + erreur);
	//				return getErrorPage(erreur);
	//			}
	//			else {
	//				NSArray utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", session.connectedUserInfo().persId());
	//				if (utilisateurs != null && utilisateurs.count() == 1) {
	//					EOUtilisateur utilisateur = (EOUtilisateur) utilisateurs.lastObject();
	//					if (utilisateur.isValide()) {
	//						CktlLog.log("user login : " + loggedUserInfo.login() + " - OK");
	//						session.setUtilisateur(utilisateur);
	//					}
	//					else {
	//						return getErrorPage("Vous n'&ecirc;tes plus autoris&eacute; &agrave; utiliser cette application.");
	//					}
	//				}
	//				else {
	//					return getErrorPage("Vous n'&ecirc;tes pas autoris&eacute; &agrave; utiliser cette application.");
	//				}
	//			}
	//		}
	//
	//		return loginResponder.loginAccepted(null);
	//	}

	public WOActionResults loginCasSuccessPage(String s) {
		return loginCasSuccessPage(s, null);
	}

	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		WOActionResults nextPage = null;
		String errorMsg = jefySession().setConnectedUser(netid);

		if (errorMsg == null) {
			NSArray utilisateurs = EOUtilities.objectsMatchingKeyAndValue(jefySession().defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", jefySession().connectedUserInfo().persId());
			if (utilisateurs != null && utilisateurs.count() == 1) {
				EOUtilisateur utilisateur = (EOUtilisateur) utilisateurs.lastObject();

				Session session = (Session) session();
				session.setUtilisateur(utilisateur);
				nextPage = this.getDestPage(session, actionParams);
			}
			else {
				nextPage = loginCasFailurePage(errorMsg, null);
			}
		}
		else {
			nextPage = loginCasFailurePage(errorMsg, null);
		}
		return nextPage;
	}

	public WOActionResults loginCasFailurePage(String errorMessage, String errorCode) {
		StringBuffer msg = new StringBuffer();
		msg.append("Une erreur s'est produite lors de l'authentification de l'utilisateur");
		if (errorMessage != null)
			msg.append("&nbsp;:<br><br>").append(errorMessage);
		CktlLog.log(" user login - Erreur : ", errorMessage);
		return getErrorPage(msg.toString());
	}

	public WOActionResults loginNoCasPage() {
		return loginNoCasPage(null);
	}

	public WOActionResults loginNoCasPage(NSDictionary actionParams) {
		LoginLocal page = (LoginLocal) pageWithName("LoginLocal");
		page.setTitleComment(loginComment());
		page.registerLoginResponder(getNewLoginResponder(actionParams));
		return page;
	}

	public WOActionResults loginCASPage() {
		LoginCAS page = (LoginCAS) pageWithName("LoginCAS");
		page.setTitleComment(loginComment());
		page.setCASLoginLink(casLoginLink());
		return page;
	}

	/**
	  *
	   */
	public WOComponent getErrorPage(String errorMessage) {
		CktlAlertPage page = (CktlAlertPage) cktlApp.pageWithName("CktlAlertPage", context());
		page.showMessage(null, cktlApp.name() + " : ERREUR", errorMessage,
				null, null, null, CktlAlertPage.ERROR, null);
		return page;
	}

	public String loginComment() {
		return loginComment;
	}

	public void setLoginComment(String comment) {
		loginComment = comment;
	}

	public String casLoginLink() {
		return null;
	}

	public CktlLoginResponder getNewLoginResponder(NSDictionary actionParams) {
		return new DefaultLoginResponder(actionParams);
	}

	public WOActionResults mailUsersAction() {
		String destinataire = cktlApp.config().stringForKey("APP_ADMIN_MAIL");
		WORequest req = request();
		if (req.formValueForKey("dest") != null) {
			destinataire = (String) req.formValueForKey("dest");
		}
		cktlApp.mailBus().sendMail(destinataire,
				cktlApp.config().stringForKey("APP_ADMIN_MAIL"), null,
				"[" + cktlApp.name() + "]Utilisateur connectés à l'application ",
				"Liste des emails : \n" + ((Application) WOApplication.application()).utilisateurs().componentsJoinedByString(","));
		return defaultAction();
	}

	public WOComponent pageCommandeRechercheAvanceeAction() {
		return pageWithName("CommandeRechercheAvancee");
	}

	public WOComponent pageCommandeChoixDuplicationAction() {
		return pageWithName("CommandeChoixDuplication");
	}

	public WOComponent pageCommandeAttestationMAPAAction() {
		return pageWithName("CommandeAttestationMAPA");
	}

	public WOComponent pageCommandePrestationInterneAction() {
		return pageWithName("CommandePrestationInterne");
	}

	public WOComponent pageCommandeApresEnregistrementAction() {
		CommandeApresEnregistrement nextPage = (CommandeApresEnregistrement) pageWithName("CommandeApresEnregistrement");
		return nextPage;
	}

	public WOComponent pageImprimerCommandeAction() {
		Imprimer nextPage = (Imprimer) pageWithName("Imprimer");
		nextPage.initialiser();
		return nextPage;
	}

	public WOActionResults imprimerCommandeAction() {
		Imprimer nextPage = null;
		WORequest request = context().request();
		Session session = (Session) context().session();
		String utlOrdre = (String) request.formValueForKey("utlOrdre");

		nextPage = (Imprimer) pageWithName("Imprimer");

		if (utlOrdre != null && utlOrdre.equals("") == false) {
			EOEditingContext edc = session.defaultEditingContext();
			EOUtilisateur utilisateur = FinderUtilisateur.getUtilisateur(edc, new Integer(utlOrdre));

			if (utilisateur != null) {
				String exeOrdre = (String) request.formValueForKey("exeOrdre");
				String numCmde = (String) request.formValueForKey("numCmde");
				if (exeOrdre != null && exeOrdre.equals("") == false && numCmde != null && numCmde.equals("") == false) {
					EOExercice exercice = FinderExercice.getExercice(edc, new Integer(exeOrdre));

					EOCommande commande = FinderCommande.getCommande(edc, exercice, new Integer(numCmde));
					if (commande != null) {
						if (commande.isImprimable()) {
							CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
							loggedUserInfo.compteForPersId(utilisateur.persId(), true);
							session.setConnectedUserInfo(loggedUserInfo);
							session.setUtilisateur(utilisateur);
							session.setLaCommandeEnCours(commande);
							nextPage.initialiser();
							nextPage.setOnloadJS("openListeSelectionWin(\'Imprimer\',null,\'Impression de commande\',null,\'Contenu\',true)");
						}
						else {
							nextPage.setOnloadJS("alert('Cette commande ne peut pas etre imprimée.');");
						}
					}
					else {
						nextPage.setOnloadJS("alert('Commande nº " + numCmde + " dans exercice " + exeOrdre + " introuvable.');");
					}
				}
				else {
					nextPage.setOnloadJS("alert('exeOrdre et numCmde obligatoires pour imprimer la commande.');");
				}
			}
			else {
				nextPage.setOnloadJS("alert('Utilisateur introuvable ou non valide (utl_ordre = " + utlOrdre + ").');");
			}
		}
		else {
			nextPage.setOnloadJS("alert('utlOrdre obligatoire pour imprimer la commande.');");
		}

		return nextPage;
	}

	public WOComponent pageLiquidationConfirmationAction() {
		WOComponent nextPage = pageWithName("LiquidationConfirmation");
		return nextPage;
	}

	public WOComponent pageCommandeChoixDuplicationPourBasculeEnMasseAction() {
		return pageWithName("CommandeChoixDuplicationPourBasculeEnMasse");
	}

	public WOComponent pageCommandeChoixDuplicationEnMasseAction() {
		return pageWithName("CommandeChoixDuplicationEnMasse");
	}

	public class DefaultLoginResponder implements CktlLoginResponder {
		private NSDictionary actionParams;

		public DefaultLoginResponder(NSDictionary actionParams) {
			this.actionParams = actionParams;
		}

		public NSDictionary actionParams() {
			return actionParams;
		}

		public WOComponent loginAccepted(CktlLogin loginComponent) {
			//			Session session = (Session) context().session();
			Session session = (Session) loginComponent.cktlSession();
			session.setConnectedUserInfo(loginComponent.loggedUserInfo());
			String erreur = session.setConnectedUser(loginComponent.loggedUserInfo().login());
			if (erreur != null) {
				CktlLog.log("CONNEXION IMPOSSIBLE : " + erreur);
				return getErrorPage(erreur);
			}
			else {
				NSArray utilisateurs = EOUtilities.objectsMatchingKeyAndValue(session.defaultEditingContext(), EOUtilisateur.ENTITY_NAME, "persId", session.connectedUserInfo().persId());
				if (utilisateurs != null && utilisateurs.count() == 1) {
					EOUtilisateur utilisateur = (EOUtilisateur) utilisateurs.lastObject();
					if (utilisateur.isValide()) {
						CktlLog.log("user login : " + loginComponent.loggedUserInfo().login() + " - OK");
						session.setUtilisateur(utilisateur);
					}
					else {
						return getErrorPage("Vous n'&ecirc;tes plus autoris&eacute; &agrave; utiliser cette application.");
					}
				}
				else {
					return getErrorPage("Vous n'&ecirc;tes pas autoris&eacute; &agrave; utiliser cette application.");
				}
			}
			//return loginResponder.loginAccepted(null);
			return getDestPage(session, actionParams);
		}

		public boolean acceptLoginName(String loginName) {
			return cktlApp.acceptLoginName(loginName);
		}

		public boolean acceptEmptyPassword() {
			return cktlApp.config().booleanForKey("ACCEPT_EMPTY_PASSWORD");
		}

		public String getRootPassword() {
			return cktlApp.getRootPassword();
		}
	}

	@SuppressWarnings("rawtypes")
	public WOComponent getDestPage(Session session, NSDictionary actionParams) {
		WOComponent nextPage = null;

		CktlLog.log("login : " + session.connectedUserInfo().login() + ", type : accueil - OK");
		nextPage = (Accueil) cktlApp.pageWithName("Accueil", session.context());
		return nextPage;
	}

}