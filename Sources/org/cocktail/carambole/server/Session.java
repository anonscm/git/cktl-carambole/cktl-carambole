/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.cocktail.carambole.server.controleurs.AccueilCtrl;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlb2b.cxml.server.engine.ProxyProperties;
import org.cocktail.fwkcktldepense.server.factory.FactoryCommande;
import org.cocktail.fwkcktldepense.server.factory.FactoryEngagementBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderDevise;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.finder.FinderJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderOrgan;
import org.cocktail.fwkcktldepense.server.finder.FinderParametre;
import org.cocktail.fwkcktldepense.server.finder.FinderTva;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EODevise;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOJefyAdminParametre;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOParametre;
import org.cocktail.fwkcktldepense.server.metier.EOTva;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EODatabaseChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.CktlAjaxModalDialog;
import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXThreadStorage;

public class Session extends CocktailAjaxSession implements ICktlDepenseGuiAjaxSession {
	public static Logger log = Logger.getLogger(Session.class.getName());
	private static final long serialVersionUID = 1L;

	private EOEditingContext nestedEdc = null;

	private NSArray<EOTva> lesTauxDeTVA = null;

	private EOUtilisateur utilisateur = null;
	private Boolean utilisateurAvecDroit = null;
	public FactoryCommande factoryCommande = new FactoryCommande(true);
	private FactoryEngagementBudget factoryEngagementBudget = new FactoryEngagementBudget(true);
	//	private NSArray lesCommandes = null;
	private EOCommande laCommandeEnCours = null;
	private String alertMessage = null;
	private String messageErreur = null;

	//private NSArray lesSources = null;
	//private NSArray lesMarches = null;
	//private NSArray lesExercices = null;
	//private NSArray lesCodesMarche = null;
	//private NSArray lesCodesMarcheFour = null;
	//private NSArray<EOCtrlSeuilMAPA> lesSeuilsMAPA = null;
	//private NSArray lesEtats = null;
	// NSArray lesConventions = null;
	public String onLoadBdCShowTab = "0";
	private WODisplayGroup dgCommandes;
	private WODisplayGroup dgArticles;
	public WODisplayGroup dgActif = null;

	private EOEngagementBudget lEngagementBudgetEnCours = null;

	// private EOCodeExer dernierCodeNomenclatureUtilise = null;
	private EODepensePapier laDepensePapierEnCours = null;
	public EODepenseBudget laDepenseBudgetReversement = null;
	private EODepenseBudget laDepenseBudgetReimputation = null;
	private EODepenseBudget laLiquidationExtourneEnCours = null;
	private String reimpLibelle;

	private Map<EOExercice, Boolean> isServiceFacturierDispoParExercice = new HashMap<EOExercice, Boolean>();

	private final int DatabaseChannelCountMax = 3;

	/**
	 * Formatteur à utiliser pour les donnees numeriques monetaires.
	 */
	public NSNumberFormatter monnaieFormatter;
	private AccueilCtrl accueilControleur;
	private ProxyProperties proxyProperties;

	// public Boolean isUtilisateurAvecDroitsTraitementEnMasse=null;

	@SuppressWarnings("rawtypes")
	public Session() {
		super();
		dgCommandes = new WODisplayGroup();
		dgArticles = new WODisplayGroup();
		// ERXEC.setTraceOpenEditingContextLocks(true);
		NSNotificationCenter.defaultCenter().addObserver(this,
				new NSSelector("registerNewDatabaseChannel", new Class[] {
						NSNotification.class
				}),
				EODatabaseContext.DatabaseChannelNeededNotification, null);

		proxyProperties = new ProxyProperties();
		proxyProperties.initFromSystem();
		proxyProperties.setSameProxy(true);

		//indiquer la classe a utiliser pour l'affichage des fenetres modales
		setWindowsClassName(CktlAjaxModalDialog.WINDOWS_CLASS_NAME_GREENLIGHTING);
	}

	public void registerNewDatabaseChannel(NSNotification notification) {
		EODatabaseContext databaseContext = (EODatabaseContext) notification.object();
		if (databaseContext.registeredChannels().count() < DatabaseChannelCountMax) {
			EODatabaseChannel channel = new EODatabaseChannel(databaseContext);
			databaseContext.registerChannel(channel);
		}
	}

	public void reset() {
		factoryCommande = null;
		factoryEngagementBudget = null;
		//lesCommandes = null;
		utilisateur = null;
		laCommandeEnCours = null;
		// dernierCodeNomenclatureUtilise = null;
		lEngagementBudgetEnCours = null;
		laDepensePapierEnCours = null;
		laDepenseBudgetReversement = null;
		//lesSources = null;
		//lesMarches = null;
		//lesEtats = null;
		// lesConventions = null;
		dgCommandes = null;
		dgArticles = null;
		//lesSeuilsMAPA = null;
		utilisateurAvecDroit = null;
	}

	/**
	 * Initialise l'ensemble des objets métiers sotckés en session et utiliser pour conserver l'état des écrans. Ces objets sont généralement utilisés
	 * par MenuCtrl pour déterminer les actions disponibles.
	 */
	public void initEtat() {
		setAlertMessage(null);
		setLaCommandeEnCours(null);
		setLEngagementBudgetEnCours(null);
		setLaDepensePapierEnCours(null);
		setReimpLibelle(null);
		setLaDepenseBudgetReimputation(null);
		setLaDepenseBudgetReversement(null);
		setLiquidationExtourneEnCours(null);
		accueilControleur().resetDgArrays();
	}

	public EOEditingContext nestedEdc() {
		return nestedEdc;
	}

	public void setNestedEdc(EOEditingContext nestedEdc) {
		if (this.nestedEdc != null && this.nestedEdc != nestedEdc) {
			this.nestedEdc.dispose();
		}
		this.nestedEdc = nestedEdc;
	}

	public EOUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(EOUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
		utilisateurAvecDroit = Boolean.FALSE;
		if (utilisateur != null) {
			if (utilisateur.isUtilisateurAvecDroits(null)) {
				utilisateurAvecDroit = Boolean.TRUE;
			}
			// Initialisation des controleurs
			// EOExercice unExerciceSelectionne = exerciceCourant();
			AccueilCtrl controleur = AccueilCtrl.createInstanceAndInitialize(utilisateur, sessionID());
			setAccueilControleur(controleur);
			initMonnaieFormatter();
		}
	}

	//	public EOExercice getExerciceCourant(EOEditingContext ed) {
	//		return FinderExercice.getExercicePourDate(ed, new NSTimestamp());
	//	}

	/*
	 * public NSArray lesMarches() { if (lesMarches == null) { EOEditingContext edc = nestedEdc(); if (edc == null) edc = defaultEditingContext();
	 * NSMutableDictionary bindings = new NSMutableDictionary(new NSTimestamp(), "date"); lesMarches = FinderMarche.getMarchesValides(edc, bindings);
	 * } return lesMarches; }
	 * 
	 * public NSArray lesExercicesInContext(EOEditingContext ed) { lesExercices = FinderExercice.getExercices(ed); // if (lesExercices == null) { //
	 * lesExercices = Finder.fetchSharedArrayWithNomTableWithSpec(EOExercice.ENTITY_NAME, null, null, defaultEditingContext(), true, null); // }
	 * return lesExercices; } public NSArray lesCodesMarcheInContext(EOEditingContext ed) { lesCodesMarche =
	 * Finder.fetchSharedArrayWithNomTableWithSpec(EOCodeMarche.ENTITY_NAME, null, null, defaultEditingContext(), true, null); if (lesCodesMarche ==
	 * null) { lesCodesMarche = Finder.fetchSharedArrayWithNomTableWithSpec(EOCodeMarche.ENTITY_NAME, null, null, defaultEditingContext(), true,
	 * null); } return lesCodesMarche; } public NSArray lesCodesMarcheFourInContext(EOEditingContext ed) { lesCodesMarcheFour =
	 * Finder.fetchSharedArrayWithNomTableWithSpec(EOCodeMarcheFour.ENTITY_NAME, null, null, defaultEditingContext(), true, null); if
	 * (lesCodesMarcheFour == null) { lesCodesMarcheFour = Finder.fetchSharedArrayWithNomTableWithSpec(EOCodeMarcheFour.ENTITY_NAME, null, null,
	 * defaultEditingContext(), true, null); } return lesCodesMarcheFour; }
	 * 
	 * public NSArray lesEtatsInContext(EOEditingContext ed) { if (lesEtats == null) { lesEtats =
	 * Finder.fetchSharedArrayWithNomTableWithSpec(EOTypeEtat.ENTITY_NAME, null, null, defaultEditingContext(), true, null); NSArray etatsDepense =
	 * new NSArray(new
	 * String[]{EOCommande.ETAT_ANNULEE,EOCommande.ETAT_ENGAGEE,EOCommande.ETAT_PARTIELLEMENT_ENGAGEE,EOCommande.ETAT_PARTIELLEMENT_SOLDEE
	 * ,EOCommande.ETAT_PRECOMMANDE,EOCommande.ETAT_SOLDEE}); NSArray qualifiers = new NSArray(); Enumeration enumEtatsDepense =
	 * etatsDepense.objectEnumerator(); while (enumEtatsDepense.hasMoreElements()) { String unEtat = (String) enumEtatsDepense.nextElement();
	 * EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY+"=%@", new NSArray(unEtat)); qualifiers =
	 * qualifiers.arrayByAddingObject(qual); } EOOrQualifier qualTypeEtatsDepense = new EOOrQualifier(qualifiers); lesEtats=new
	 * NSArray((NSArray)(EOQualifier.filteredArrayWithQualifier(lesEtats, qualTypeEtatsDepense))); } return lesEtats; }
	 */
	public NSArray<EOTva> lesTauxDeTva() {
		EOCommande commande = laCommandeEnCours;
		EOEditingContext ed = null;
		if (commande == null) {
			ed = defaultEditingContext();
		}
		else {
			ed = commande.editingContext();
		}
		lesTauxDeTVA = FinderTva.getTvas(ed);

		return lesTauxDeTVA;
	}

	public FactoryCommande getFactoryCommande() {
		return factoryCommande;
	}

	public void setFactoryCommande(FactoryCommande factoryCommande) {
		this.factoryCommande = factoryCommande;
	}

	public FactoryEngagementBudget getFactoryEngagement() {
		return factoryEngagementBudget;
	}

	public void setFactoryEngagementBudget(FactoryEngagementBudget factoryEngagementBudget) {
		this.factoryEngagementBudget = factoryEngagementBudget;
	}

	/*
	 * public NSArray getLesConventions() { EOCommande commande = laCommandeEnCours; EOEditingContext ed = null; if (commande==null) { ed =
	 * defaultEditingContext(); } else { ed = commande.editingContext(); } EOUtilisateur user = getUtilisateur(); user =
	 * (EOUtilisateur)ed.faultForGlobalID(user.editingContext().globalIDForObject(user), ed); NSDictionary bindings = new
	 * NSDictionary(user,"utilisateur"); lesConventions = FinderConvention.getConventions(ed, bindings); // if (lesConventions == null) { //
	 * EOEditingContext edc = nestedEdc(); // if (edc == null) edc = defaultEditingContext(); // NSDictionary bindings = new
	 * NSDictionary(getUtilisateur(),"utilisateur"); // lesConventions = FinderConvention.getConventions(edc, bindings); // } return lesConventions; }
	 * public void setLesConventions(NSArray lesConventions) { this.lesConventions = lesConventions; }
	 */

	public String onLoadBdCShowTab() {
		return onLoadBdCShowTab;
	}

	public void setOnLoadBdCShowTab(String onLoadBdCShowTab) {
		this.onLoadBdCShowTab = onLoadBdCShowTab;
	}

	public void setDGCommandes(WODisplayGroup dg) {
		dgCommandes = dg;

	}

	public WODisplayGroup getDgCommandes() {
		return dgCommandes;
	}

	public WODisplayGroup getDgArticles() {
		if (dgArticles == null) {
			dgArticles = new WODisplayGroup();
		}
		return dgArticles;
	}

	public void setDgArticles(WODisplayGroup dgArticles) {
		this.dgArticles = dgArticles;
	}

	public EOCommande getLaCommandeEnCours() {
		return laCommandeEnCours;
	}

	public void setLaCommandeEnCours(EOCommande laCommandeEnCours) {
		this.laCommandeEnCours = laCommandeEnCours;
	}

	public String getMessageErreur() {
		return messageErreur;
	}

	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	@SuppressWarnings("unchecked")
	public void initDGCommandes(EOUtilisateur unUtilisateur) {
		dgCommandes.setNumberOfObjectsPerBatch(17);
		// dgCommandes.queryBindings().setObjectForKey(unUtilisateur, "createur");
		// TODO Gerer le cas au il n'existe pas d'exercice courant
		EOExercice exercice = FinderExercice.getExercicePourDate(defaultEditingContext(), new NSTimestamp());
		dgCommandes.queryBindings().setObjectForKey(exercice, "exercice");
		if (utilisateurAvecDroit == null) {
			NSArray<EOOrgan> array = FinderOrgan.getOrgans(defaultEditingContext(), unUtilisateur, exercice);
			if (array != null && array.count() > 0) {
				utilisateurAvecDroit = Boolean.TRUE;
			}
			else {
				utilisateurAvecDroit = Boolean.FALSE;
			}
		}
		if (utilisateurAvecDroit.booleanValue()) {
			dgCommandes.queryBindings().setObjectForKey(unUtilisateur, "utilisateur");
		}
		else {
			dgCommandes.queryBindings().setObjectForKey(unUtilisateur, "createur");
		}

		NSMutableArray<String> typesEtatLibelle = new NSMutableArray<String>();
		// typesEtatLibelle.addObject(EOCommande.ETAT_PRECOMMANDE);
		typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);
		typesEtatLibelle.addObject(EOCommande.ETAT_ENGAGEE);
		typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE);
		typesEtatLibelle.addObject(EOCommande.ETAT_SOLDEE);
		dgCommandes.queryBindings().setObjectForKey(typesEtatLibelle, "lesTypesEtat");

		NSArray<EOCommande> lesCommandes = FinderCommande.getCommandes(defaultEditingContext(), dgCommandes.queryBindings());
		dgCommandes.setObjectArray(lesCommandes);
	}

	public EODepensePapier getLaDepensePapierEnCours() {
		return laDepensePapierEnCours;
	}

	public void setLaDepensePapierEnCours(EODepensePapier laDepensePapierEnCours) {
		this.laDepensePapierEnCours = laDepensePapierEnCours;
	}

	//	public NSArray<EOCtrlSeuilMAPA> getLesSeuilsMAPA() {
	//		if (getLaCommandeEnCours() != null && lesSeuilsMAPA == null) {
	//			NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
	//			bindings.setObjectForKey(getLaCommandeEnCours().exercice(), "exercice");
	//			lesSeuilsMAPA = FinderCtrlSeuilMAPA.getCtrlSeuilMAPAValides(getLaCommandeEnCours().editingContext(), bindings);
	//		}
	//		return lesSeuilsMAPA;
	//	}

	public String setConnectedUser(String arg0) {
		String str = super.setConnectedUser(arg0);
		if (connectedUserInfo() != null) {
			if (connectedUserInfo().email() != null) {
				if (((Application) WOApplication.application()).utilisateurs().indexOfIdenticalObject(connectedUserInfo().email()) == NSArray.NotFound)
					((Application) WOApplication.application()).utilisateurs().addObject(connectedUserInfo().email());
				System.out.println("----Connexion de " + connectedUserInfo().nomEtPrenom() + " (noIndividu =" + connectedUserInfo().noIndividu() + ") le " + (new NSTimestamp()) + " depuis IP : " + cktlApp.getRequestIPAddress(context().request()));
			}
			else {
				str = "Veuillez renseigner votre adresse email avant de vous connecter.";
			}
		}

		return str;
	}

	public void terminate() {
		NSMutableArray<String> users = ((Application) WOApplication.application()).utilisateurs();
		if (users != null && users.count() > 0) {
			if (connectedUserInfo() != null && connectedUserInfo().email() != null) {
				users.removeIdenticalObject(connectedUserInfo().email());
			}
		}
		super.terminate();
	}

	public Boolean getUtilisateurAvecDroit() {
		if (utilisateurAvecDroit == null) {
			EOExercice exercice = FinderExercice.getExercicePourDate(defaultEditingContext(), new NSTimestamp());
			NSArray<EOOrgan> array = FinderOrgan.getOrgans(defaultEditingContext(), getUtilisateur(), exercice);
			if (array != null && array.count() > 0) {
				utilisateurAvecDroit = Boolean.TRUE;
			}
			else {
				utilisateurAvecDroit = Boolean.FALSE;
			}
		}
		return utilisateurAvecDroit;
	}

	protected void setUtilisateurAvecDroit(Boolean utilisateurAvecDroit) {
		this.utilisateurAvecDroit = utilisateurAvecDroit;
	}

	public EODepenseBudget getLaDepenseBudgetReversement() {
		return laDepenseBudgetReversement;
	}

	public void setLaDepenseBudgetReversement(EODepenseBudget laDepenseBudgetAReverser) {
		this.laDepenseBudgetReversement = laDepenseBudgetAReverser;

	}

	public EODepenseBudget getLaDepenseBudgetReimputation() {
		return laDepenseBudgetReimputation;
	}

	public void setLaDepenseBudgetReimputation(EODepenseBudget laDepenseBudget) {
		this.laDepenseBudgetReimputation = laDepenseBudget;

	}

	public void setAccueilControleur(AccueilCtrl controleur) {
		accueilControleur = controleur;
	}

	public AccueilCtrl accueilControleur() {
		return accueilControleur;
	}

	public EOEngagementBudget getLEngagementBudgetEnCours() {
		return lEngagementBudgetEnCours;
	}

	public void setLEngagementBudgetEnCours(EOEngagementBudget lEngagementBudgetEnCours) {
		this.lEngagementBudgetEnCours = lEngagementBudgetEnCours;
	}

	public void setLiquidationExtourneEnCours(EODepenseBudget liquidationExtourne) {
		this.laLiquidationExtourneEnCours = liquidationExtourne;
	}

	public EODepenseBudget getLiquidationExtourneEnCours() {
		return this.laLiquidationExtourneEnCours;
	}

	public void initMonnaieFormatter() {
		//Créer le monnaieFormatter en fonction de la devise utilisee
		monnaieFormatter = new NSNumberFormatter();
		monnaieFormatter.setDecimalSeparator(",");
		monnaieFormatter.setThousandSeparator(" ");
		monnaieFormatter.setHasThousandSeparators(true);
		EODevise devise = FinderDevise.getDeviseEnCours(defaultEditingContext(), accueilControleur().getExerciceCourant());
		int nbDecimales = EODevise.defaultNbDecimales;
		if (devise != null)
			nbDecimales = devise.devNbDecimales().intValue();
		if (nbDecimales == 0) {
			monnaieFormatter.setPattern("# ##0;0;-# ##0");
		}
		else {
			String str = "# ##0.";
			String str1 = ";0,";
			String str2 = ";-# ##0.";

			for (int i = 0; i < nbDecimales; i++) {
				str += "0";
				str1 += "0";
				str2 += "0";
			}

			monnaieFormatter.setPattern(str + str1 + str2);
		}

	}

	public NSNumberFormatter getMonnaieFormatter() {
		return monnaieFormatter;
	}

	public void setMonnaieFormatter(NSNumberFormatter monnaieFormatter) {
		this.monnaieFormatter = monnaieFormatter;
	}

	public WODisplayGroup dgActif() {
		return dgActif;
	}

	public void setDgActif(WODisplayGroup dgActif) {
		this.dgActif = dgActif;
	}

	public String getReimpLibelle() {
		return reimpLibelle;
	}

	public void setReimpLibelle(String reimpLibelle) {
		this.reimpLibelle = reimpLibelle;
	}

	public ProxyProperties getProxyProperties() {
		return proxyProperties;
	}

	public void setProxyProperties(ProxyProperties proxyProperties) {
		this.proxyProperties = proxyProperties;
	}

	/**
	 * @return TRUE si le service facturier est disponible pour l'exercice (jefy_admin.parametre).
	 */
	public Boolean isServiceFacturierDisponible() {
		EOExercice exercice = accueilControleur().getExerciceCourant();
		if (isServiceFacturierDispoParExercice.get(exercice) == null) {
			isServiceFacturierDispoParExercice.put(exercice, Boolean.valueOf(EOJefyAdminParametre.OUI.equals(FinderJefyAdminParametre.getServiceFacturierDispo(defaultEditingContext(), exercice))));
			if (isServiceFacturierDispoParExercice.get(exercice)) {
				log.debug("Le service facturier est activé pour l'exercice " + exercice.exeExercice());
			}
			else {
				log.debug("Le service facturier est désactivé pour l'exercice " + exercice.exeExercice());
			}
		}

		return isServiceFacturierDispoParExercice.get(exercice);
		//		return Boolean.valueOf(EOJefyAdminParametre.OUI.equals(FinderJefyAdminParametre.getServiceFacturierDispo(defaultEditingContext(), exercice)));
	}

	public Boolean isLiquidationSurExtourneDisponible() {
		return ERXProperties.booleanForKey("org.cocktail.carambole.liquidationSurExtourneEnabled");
	}

	public Boolean isImprimerBonCommandeViaSIX() {
		return ERXProperties.booleanForKey("org.cocktail.carambole.bondecommande.impression.six");
	}

	@Override
	public void awake() {
		super.awake();
		if (getUtilisateur() != null) {
			//permet de passer le persId aux objets metiers qui exploitent cette clé
			ERXThreadStorage.takeValueForKey(getUtilisateur().persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			MDC.put("PERS_ID", getUtilisateur().persId());
		}

	}

	public Boolean autoriserCommandeHorsMarcheSurCodeMarche(EOExercice exercice) {
		EOParametre param = FinderParametre.getParametre(defaultEditingContext(), Application.AUTORISER_COMMANDES_HORSMARCHE_SUR_CODE_MARCHE_KEY, exercice);
		if (param == null) {
			return Boolean.TRUE;
		}
		return Boolean.valueOf("OUI".equals(param.parValue()));
	}

	public Boolean preselectionTauxProrata() {
		return ((Application) application()).preselectionTauxProrata();
	}

	public EOUtilisateur utilisateurInContext(EOEditingContext ed) {
		EOUtilisateur user = getUtilisateur();
		if (user == null) {
			throw new RuntimeException("utilisateur null");
		}
		if (!user.editingContext().equals(ed)) {
			user = (EOUtilisateur) ed.faultForGlobalID(user.editingContext().globalIDForObject(user), ed);
		}
		return user;

	}
}