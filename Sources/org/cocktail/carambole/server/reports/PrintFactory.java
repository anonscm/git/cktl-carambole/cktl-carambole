/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.reports;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.Session;
import org.cocktail.fwkcktldepense.server.finder.FinderJefyAdminParametre;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableDictionary;

public class PrintFactory {
	public static final String JASPER_REIMPUTATION = "reimputation/reimputation.jasper";
	public static final String JASPER_DOSSIER_LIQUIDATION = "dossierLiquidation/dossierLiquidation.jasper";
	public static final String JASPER_LIQUIDATIONS_EN_ATTENTE = "liquidationsAttente/liquidationsAttente.jasper";
	public static final String JASPER_BON_DE_COMMANDE = "BonDeCommande/BonDeCommande.jasper";
	public static final String JASPER_BON_DE_COMMANDE_COMPTANT = "BonDeCommande/BonDeCommande.jasper";

	public static NSData printReimputation(Session session, Integer reimId) throws Throwable {
		Application app = ((Application) Application.application());
		String univ = FinderJefyAdminParametre.getParametre(session.defaultEditingContext(), "UNIV", session.accueilControleur().getExerciceCourant()).parValue();
		NSMutableDictionary<String, Object> parametres = new NSMutableDictionary<String, Object>();
		parametres.takeValueForKey(reimId, "REIM_ID");

		parametres.takeValueForKey(univ, "UNIV");

		String reportFileName = app.getReportsLocation(JASPER_REIMPUTATION);
		Connection connection = app.getJDBCConnection();
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.putAll(parametres.hashtable());

		JrxmlReporter reporter = new JrxmlReporter();
		NSData res = reporter.printNow(null, connection, null, reportFileName, aMap, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);

		//		NSData res = CktlReporter.printJasperReportsNow(connection, null, reportFileName, aMap, CktlReporter.FORMATPDF, false, null);
		return res;
	}

	public static NSData printDossierLiquidation(Session session, String borIds) throws Throwable {
		Application app = ((Application) Application.application());
		NSMutableDictionary<String, Object> parametres = new NSMutableDictionary<String, Object>();
		parametres.takeValueForKey(borIds, "BORIDS");
		String reportFileName = app.getReportsLocation(JASPER_DOSSIER_LIQUIDATION);
		Connection connection = app.getJDBCConnection();
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.putAll(parametres.hashtable());

		JrxmlReporter reporter = new JrxmlReporter();
		NSData res = reporter.printNow(null, connection, null, reportFileName, aMap, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
		return res;
	}

	public static NSData printLiquidationEnAttente(Session session, String dpcoids) throws Throwable {
		Application app = ((Application) Application.application());
		//String univ = FinderJefyAdminParametre.getParametre(session.defaultEditingContext(), "UNIV", session.accueilControleur().exerciceCourant).parValue();
		NSMutableDictionary<String, Object> parametres = new NSMutableDictionary<String, Object>();
		parametres.takeValueForKey(dpcoids, "DPCOIDS");

		String reportFileName = app.getReportsLocation(JASPER_LIQUIDATIONS_EN_ATTENTE);
		Connection connection = app.getJDBCConnection();
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.putAll(parametres.hashtable());

		JrxmlReporter reporter = new JrxmlReporter();
		NSData res = reporter.printNow(null, connection, null, reportFileName, aMap, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);

		//		NSData res = CktlReporter.printJasperReportsNow(connection, null, reportFileName, aMap, CktlReporter.FORMATPDF, false, null);
		return res;
	}

	public static NSData printBonDeCommande(Session session, BigDecimal cimpid, Map<String, Object> params) throws Throwable {
		return _printBonDeCommande(session, cimpid, JASPER_BON_DE_COMMANDE, params);
	}

	public static NSData printBonDeCommandeComptant(Session session, BigDecimal cimpid, Map<String, Object> params) throws Throwable {
		return _printBonDeCommande(session, cimpid, JASPER_BON_DE_COMMANDE_COMPTANT, params);
	}

	private static NSData _printBonDeCommande(Session session, BigDecimal cimpid, String template, Map<String, Object> params) throws Throwable {
		Application app = ((Application) Application.application());

		NSMutableDictionary<String, Object> parametres = new NSMutableDictionary<String, Object>();
		parametres.takeValueForKey(cimpid, "cimpid");

		String reportFileName = app.getReportsLocation(template);
		Connection connection = app.getJDBCConnection();
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		aMap.putAll(parametres.hashtable());
		aMap.putAll(params);

		JrxmlReporter reporter = new JrxmlReporter();
		NSData res = reporter.printNow(null, connection, null, reportFileName, aMap, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, null);
		return res;
	}

	public static WOActionResults afficherPdf(NSData pdfData, String fileName) {
		CktlDataResponse resp = new CktlDataResponse();
		if (pdfData != null) {
			resp.setContent(pdfData, CktlDataResponse.MIME_PDF);
			resp.setFileName(fileName);
		}
		else {
			resp.setContent("");
			resp.setHeader("0", "Content-Length");
		}
		return resp.generateResponse();
	}
}
