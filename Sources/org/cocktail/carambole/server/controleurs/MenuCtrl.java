/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.Application;
import org.cocktail.carambole.server.B2bCxmlDirectAction;
import org.cocktail.carambole.server.NavigationCtrl;
import org.cocktail.carambole.server.components.Accueil;
import org.cocktail.carambole.server.components.ArticleCreation;
import org.cocktail.carambole.server.components.ArticleListeSelection;
import org.cocktail.carambole.server.components.ArticleRechercheAvancee;
import org.cocktail.carambole.server.components.B2BOrderRequestPage;
import org.cocktail.carambole.server.components.BonDeCommande2;
import org.cocktail.carambole.server.components.CommandeListe;
import org.cocktail.carambole.server.components.CommandeListeResultatsApresTraitementEnMasse;
import org.cocktail.carambole.server.components.DetailCommande;
import org.cocktail.carambole.server.components.DetailEngagementBudget;
import org.cocktail.carambole.server.components.DetailFacture;
import org.cocktail.carambole.server.components.EngagementListe;
import org.cocktail.carambole.server.components.FactureListe;
import org.cocktail.carambole.server.components.Informer;
import org.cocktail.carambole.server.components.Liquidation2;
import org.cocktail.carambole.server.components.LiquidationExtourneListe;
import org.cocktail.carambole.server.components.LiquidationSansEngagement;
import org.cocktail.carambole.server.components.LiquidationSurExtourneEnveloppe;
import org.cocktail.carambole.server.components.LiquidationSurExtourneMandat;
import org.cocktail.carambole.server.components.Menu;
import org.cocktail.carambole.server.components.PreCommandeListe;
import org.cocktail.carambole.server.components.PreLiquidationsListePage;
import org.cocktail.carambole.server.components.SourceListeSelection;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLBuyerCookie;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLParameters;
import org.cocktail.fwkcktlb2b.cxml.server.engine.CXMLTransaction;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemOut;
import org.cocktail.fwkcktldepense.server.b2b.factory.B2bUtils;
import org.cocktail.fwkcktldepense.server.exception.FactoryException;
import org.cocktail.fwkcktldepense.server.factory.FactoryArticle;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepensePapier;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderArticle;
import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderDepensePapier;
import org.cocktail.fwkcktldepense.server.finder.FinderEngagementBudget;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.metier.EOArticle;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier.ELiquidationType;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier.SourceCreditExtourneLiquidation;
import org.cocktail.fwkcktldepense.server.metier._ISourceCredit.ESourceCreditType;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOB2bCxmlPunchoutmsg;
import org.cocktail.fwkcktldepense.server.metier.b2b.EOFournisB2bCxmlParam;
import org.cocktail.fwkcktldepense.server.process.Process;
import org.cocktail.fwkcktldepense.server.process.ProcessCommande;
import org.cocktail.fwkcktldepense.server.process.ProcessDepense;
import org.cocktail.fwkcktldepense.server.process.ProcessEngagement;
import org.cocktail.fwkcktldepense.server.process.ProcessPreDepense;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;

public class MenuCtrl extends Object {
	public final static Logger logger = Logger.getLogger(MenuCtrl.class);

	private Menu component = null;

	public MenuCtrl() {
		super();
	}

	public MenuCtrl(Menu component) {
		super();
		this.component = component;
	}

	// Actions associees aux differents items

	// Menus communs a plusieurs pages

	public WOComponent accueil() {
		if (logger.isDebugEnabled()) {
			logger.debug("Retour à l'accueil (debug)");
		}
		Accueil nextPage = (Accueil) component.pageWithName("Accueil");
		nextPage.reset();
		return nextPage;
	}

	public WOComponent quitter() {
		if (component.nestedEdc() != null) {
			component.session.setNestedEdc(null);
		}
		EOEditingContext ed = component.edc();
		try {
			ed.unlock();
			component.session.reset();
		} catch (Exception e) {
		}

		return component.session.logout();
	}

	public boolean isRetourALaListeDisabled() {
		boolean isRetourALaListeDisabled = true;
		WODisplayGroup dg = component.session.dgActif();
		if (dg != null && dg.allObjects().count() > 0) {
			isRetourALaListeDisabled = false;
		}
		return isRetourALaListeDisabled;
	}

	public WOComponent retourALaListe() {
		WOComponent nextPage = null;
		WODisplayGroup dg = component.session.dgActif();
		if (dg != null) {
			WODisplayGroup dgPC = component.dgPreCommande();
			WODisplayGroup dgC = component.dgCommande();
			WODisplayGroup dgE = component.dgEngagement();
			WODisplayGroup dgF = component.dgFacture();
			if (dg.equals(dgPC)) {
				nextPage = component.pageWithName("PreCommandeListe");
				@SuppressWarnings("unchecked")
				NSDictionary<String, Object> bdgs = dgPC.queryBindings();
				NSArray<NSDictionary<String, Object>> lesPrecommandes = FinderCommande.getRawRowPreCommandes(component.edc(), bdgs);
				dgPC.setObjectArray(lesPrecommandes);
				dgPC.setSelectedObjects(null);
				((PreCommandeListe) nextPage).setDg(dgPC);
			}
			else if (dg.equals(dgC)) {
				nextPage = component.pageWithName("CommandeListe");
				@SuppressWarnings("unchecked")
				NSDictionary<String, Object> bdgs = dgC.queryBindings();
				NSArray<NSDictionary<String, Object>> lesCommandes = FinderCommande.getRawRowCommandes(component.edc(), bdgs);
				dgC.setObjectArray(lesCommandes);
				dgC.setSelectedObjects(null);
				((CommandeListe) nextPage).setDg(dgC);
			}
			else if (dg.equals(dgE)) {
				nextPage = component.pageWithName("EngagementListe");
				@SuppressWarnings("unchecked")
				NSDictionary<String, Object> bdgs = dgE.queryBindings();
				NSArray<NSDictionary<String, Object>> lesFactures = FinderEngagementBudget.getRawRowEngagements(component.edc(), bdgs);
				dgE.setObjectArray(lesFactures);
				dgE.setSelectedObjects(null);
				((EngagementListe) nextPage).setDg(dgE);
			}
			else if (dg.equals(dgF)) {
				nextPage = component.pageWithName("FactureListe");
				@SuppressWarnings("unchecked")
				NSDictionary<String, Object> bdgs = dgF.queryBindings();
				NSArray<NSDictionary<String, Object>> lesFactures = FinderDepensePapier.getRawRowDepensePapierPourNumeroFacture(component.edc(), bdgs);
				dgF.setObjectArray(lesFactures);
				dgF.setSelectedObjects(null);
				((FactureListe) nextPage).setDg(dgF);
			}
		}
		return nextPage;
	}

	// Menus de ListeDesCommandes

	public WOComponent modifierLaRecherche() {
		component.dgCommande().setSelectedObjects(null);
		component.dgCommande().setObjectArray(null);
		component.dgEngagement().setSelectedObjects(null);
		component.dgEngagement().setObjectArray(null);
		component.dgFacture().setSelectedObjects(null);
		component.dgFacture().setObjectArray(null);

		return null;
	}

	public boolean isSolderEnMasseDisabled() {
		boolean isSolderEnMasseDisabled = true;
		EOUtilisateur utilisateur = component.utilisateur();
		if (utilisateur != null) {
			WODisplayGroup dg = component.dgCommande();
			@SuppressWarnings("unchecked")
			NSArray<NSDictionary<String, Object>> lesCommandesATraiter = dg.selectedObjects();
			EOExercice exercice = (EOExercice) dg.queryBindings().objectForKey("exercice");
			if (utilisateur.isDroitSolderEnMasse(exercice, lesCommandesATraiter, true)) {
				isSolderEnMasseDisabled = false;
			}
		}
		return isSolderEnMasseDisabled;
	}

	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public WOComponent solderEnMasse() {
		WOComponent nextPage = null;
		WODisplayGroup dg = component.dgCommande();

		try {
			EOEditingContext edc = ERXEC.newEditingContext();
			EOExercice exercice = (EOExercice) dg.queryBindings().objectForKey("exercice");
			if (exercice != null) {
				EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(EOCommande.EXERCICE_KEY + "=%@", new NSArray(exercice));
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				NSArray resultats = null;
				// Transformation du tableau de dico en tableau de commandes
				NSArray<EOCommande> commandes = null;
				NSArray<NSDictionary<String, Object>> dicosCommande = dg.selectedObjects();
				Enumeration<NSDictionary<String, Object>> enumDicosCommande = dicosCommande.objectEnumerator();
				while (enumDicosCommande.hasMoreElements()) {
					NSDictionary<String, Object> unDicoCommande = enumDicosCommande.nextElement();
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCommande.COMM_NUMERO_KEY + "=%@", new NSArray<Object>(unDicoCommande.objectForKey("NUMERO")));
					qualifiers.addObject(qual);
				}
				EOOrQualifier qualNumCmde = new EOOrQualifier(qualifiers);
				EOQualifier qualCommandes = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
						qualExercice, qualNumCmde
				}));

				commandes = component.session.dataBus().fetchArray(edc, EOCommande.ENTITY_NAME, qualCommandes, null);

				resultats = ProcessCommande.solder(component.session.dataBus(), edc, commandes, component.utilisateurInContext(edc));
				component.session.setAlertMessage(null);

				nextPage = (CommandeListeResultatsApresTraitementEnMasse) component.pageWithName("CommandeListeResultatsApresTraitementEnMasse");
				EOQualifier qualResultatsOK = EOQualifier.qualifierWithQualifierFormat("erreur = %@", new NSArray<Object>(NSKeyValueCoding.NullValue));
				EOQualifier qualResultatsKO = EOQualifier.qualifierWithQualifierFormat("erreur != %@", new NSArray<Object>(NSKeyValueCoding.NullValue));
				((CommandeListeResultatsApresTraitementEnMasse) nextPage).setResultatsOK(EOQualifier.filteredArrayWithQualifier(resultats, qualResultatsOK));
				((CommandeListeResultatsApresTraitementEnMasse) nextPage).setResultatsKO(EOQualifier.filteredArrayWithQualifier(resultats, qualResultatsKO));
			}
			else {
				throw new FactoryException("Exercice = null dans les bindings: Impossible de solder en masse", false);
			}
		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					component.session.setAlertMessage(alertMessage);
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				component.session.setAlertMessage(alertMessage);
			}
		} catch (RuntimeException e1) {
			component.session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}

		return nextPage;
	}

	public boolean isDupliquerEnMasseDisabled() {
		boolean isDupliquerEnMasseDisabled = true;
		EOUtilisateur utilisateur = component.utilisateur();
		if (utilisateur != null) {
			WODisplayGroup dg = component.dgCommande();
			@SuppressWarnings("unchecked")
			NSArray<NSDictionary<String, Object>> lesCommandesATraiter = dg.selectedObjects();
			EOExercice exercice = (EOExercice) dg.queryBindings().objectForKey("exercice");
			if (exercice != null && lesCommandesATraiter != null && lesCommandesATraiter.count() > 0) {
				NSArray<EOExercice> exercicesOuvertsPourDuplication = component.session.accueilControleur().exercicesOuvertsPourDuplicationEnMasse;
				if (exercicesOuvertsPourDuplication != null && exercicesOuvertsPourDuplication.count() > 0) {
					isDupliquerEnMasseDisabled = false;
				}
			}
		}

		return isDupliquerEnMasseDisabled;
	}

	@SuppressWarnings("unchecked")
	public WOComponent dupliquerEnMasse() {
		//		CommandeListe nextPage = (CommandeListe) component.pageWithName("CommandeListe");
		WOComponent nextPage = component.session.getSavedPageWithName(CommandeListe.class.getName());
		WODisplayGroup dg = component.dgCommande();
		NSArray<NSDictionary<String, Object>> dicosCommande = dg.selectedObjects();

		if (dicosCommande != null && dicosCommande.count() > 0) {
			((CommandeListe) nextPage).setDg(dg);
			((CommandeListe) nextPage).setLesCommandes(dg.allObjects());
			((CommandeListe) nextPage).setLesCommandesSelectionnees(dicosCommande);
			((CommandeListe) nextPage).setIsBasculeEnCours(true);
			((CommandeListe) nextPage).setOnloadJS(createOpenWinJS("pageCommandeChoixDuplicationEnMasse", "Dupliquer des commandes"));

		}

		return nextPage;
	}

	public boolean isBasculerEnMasseDisabled() {
		boolean isBasculerEnMasseDisabled = true;
		EOUtilisateur utilisateur = component.utilisateur();
		if (utilisateur != null) {
			WODisplayGroup dg = component.dgCommande();
			@SuppressWarnings("unchecked")
			NSArray<NSDictionary<String, Object>> lesCommandesATraiter = dg.selectedObjects();
			EOExercice exercice = (EOExercice) dg.queryBindings().objectForKey("exercice");
			EOExercice exerciceDestination = component.exerciceDuJour();
			if (utilisateur.isDroitBasculerEnMasse(exercice, exerciceDestination, lesCommandesATraiter, true)) {
				isBasculerEnMasseDisabled = false;
			}
		}

		return isBasculerEnMasseDisabled;
	}

	@SuppressWarnings("unchecked")
	public WOComponent basculerEnMasse() {
		WOComponent nextPage = component.session.getSavedPageWithName(CommandeListe.class.getName());
		WODisplayGroup dg = component.dgCommande();
		NSArray<NSDictionary<String, Object>> dicosCommande = dg.selectedObjects();

		if (dicosCommande != null && dicosCommande.count() > 0) {
			((CommandeListe) nextPage).setDg(dg);
			((CommandeListe) nextPage).setLesCommandes(dg.allObjects());
			//			((CommandeListe) nextPage).setLesCommandesSelectionnees(dg.selectedObjects());
			((CommandeListe) nextPage).setLesCommandesSelectionnees(dicosCommande);
			((CommandeListe) nextPage).setIsBasculeEnCours(true);
			((CommandeListe) nextPage).setOnloadJS(createOpenWinJS("pageCommandeChoixDuplicationPourBasculeEnMasse", "Basculer des commandes"));
		}

		return nextPage;
	}

	// Menus de PreCommandeListe

	public WOComponent actualiserLesPreCommandes() {
		WODisplayGroup dgPC = component.session.accueilControleur().getDgPreCommandes();
		@SuppressWarnings("unchecked")
		NSDictionary<String, Object> bdgs = dgPC.queryBindings();
		NSArray<NSDictionary<String, Object>> lesPrecommandes = FinderCommande.getRawRowPreCommandes(component.edc(), bdgs);
		dgPC.setObjectArray(lesPrecommandes);

		return null;
	}

	// Menus de PreLiquidationsListe

	public WOComponent actualiserLesPreLiquidations() {
		WODisplayGroup dgPC = component.session.accueilControleur().getDgPreLiquidations();
		@SuppressWarnings("unchecked")
		NSDictionary<String, Object> bdgs = dgPC.queryBindings();
		NSArray<NSDictionary<String, Object>> lesPrecommandes = FinderDepensePapier.getRawRowPreLiquidation(component.edc(), bdgs);
		dgPC.setObjectArray(lesPrecommandes);

		return null;
	}

	// Menus de DetailCommande

	public boolean isModifierUneCommandeDisabled() {
		boolean isModifierUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isModifiable(commande.editingContext(), component.utilisateur(), null)) {
			isModifierUneCommandeDisabled = false;
		}

		return isModifierUneCommandeDisabled;
	}

	public WOComponent modifierUneCommande() {
		EOCommande laCommandeAModifier = component.laCommandeEnCours();
		component.laCommandeEnCours().reconstruireTableaux();

		return NavigationCtrl.goToBonDeCommande(component.context(), laCommandeAModifier, null, "Informations");

	}

	public boolean isDupliquerUneCommandeDisabled() {
		boolean isDupliquerUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		EOExercice exerciceDestination = FinderExercice.getExercicePourDate(component.edc(), new NSTimestamp());
		if (commande != null && commande.isDuplicable(exerciceDestination)) {
			isDupliquerUneCommandeDisabled = false;
		}
		return isDupliquerUneCommandeDisabled;
	}

	public WOComponent dupliquerUneCommande() {
		WOComponent nextPage = component.pageWithName("DetailCommande");
		EOCommande laCommandeADupliquer = component.laCommandeEnCours();
		component.session.setLaCommandeEnCours(laCommandeADupliquer);
		((DetailCommande) nextPage).setLaCommande(laCommandeADupliquer);
		((DetailCommande) nextPage).setOnloadJS(createOpenWinJS("pageCommandeChoixDuplication", "Dupliquer une commande"));
		return nextPage;
	}

	public boolean isLiquiderUneCommandeDisabled() {
		boolean isLiquiderUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isLiquidable(commande.editingContext(), component.utilisateur(), null)) {
			isLiquiderUneCommandeDisabled = false;
		}

		return isLiquiderUneCommandeDisabled;
	}

	public WOComponent liquiderUneCommande() {
		EOCommande commande = component.laCommandeEnCours();
		EOEditingContext edc = commande.editingContext();

		FactoryDepensePapier fdp = new FactoryDepensePapier();
		EODepensePapier laDepensePapier = fdp.creer(edc, BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), null, null, null, null, commande.exercice(), component.utilisateurInContext(edc), null, null, commande.fournisseur(), commande);
		if (component.session.isServiceFacturierDisponible().booleanValue()) {
			fdp.creerPreDepensesBudget(edc, laDepensePapier);
		}
		else {
			fdp.creerDepensesBudget(edc, laDepensePapier);
		}
		laDepensePapier.setLiquidationType(ELiquidationType.SUR_COMMANDE);
		return NavigationCtrl.goToLiquidation(component.context(), laDepensePapier);
	}

	public WOComponent liquiderSurExtourneMandat() {
		EODepenseBudget liquidationExtourne = component.mySession().getLiquidationExtourneEnCours();
		try {
			if (liquidationExtourne != null) {
				BigDecimal ht = liquidationExtourne.engagementBudget().engHtSaisie();
				BigDecimal ttc = liquidationExtourne.engagementBudget().engTtcSaisie();
				BigDecimal resteDispo = liquidationExtourne.tauxProrata().montantBudgetaire(ht, ttc.subtract(ht)).abs();
				if (resteDispo.compareTo(BigDecimal.ZERO) == 0) {
					throw new Exception("Cette liquidation d'extourne a été totalement consommée, Vous ne pouvez plus passer de liquidation définitive dessus.");
				}

				EOEditingContext edc = liquidationExtourne.editingContext();
				component.mySession().setLaCommandeEnCours(null);
				EOExercice exercice = component.mySession().accueilControleur().getExerciceCourant();
				EOUtilisateur utilisateur = component.utilisateurInContext(edc);

				FactoryDepensePapier fdp = new FactoryDepensePapier();
				EODepensePapier dpp = fdp.creer(edc);
				dpp.setExerciceRelationship(exercice);
				dpp.setUtilisateurRelationship(utilisateur);
				dpp.setLiquidationType(ELiquidationType.SUR_EXTOURNE);

				//on utilise l'engagement de la liquidation d'extourne.
				EOEngagementBudget engagementBudget = liquidationExtourne.engagementBudget();

				//Rod : en test (25/09/2012)
				if (Application.ACTIVER_SF_SUR_EXTOURNE && component.session.isServiceFacturierDisponible().booleanValue()) {
					FactoryPreDepenseBudget fdb = new FactoryPreDepenseBudget();
					EOPreDepenseBudget depenseBudget = fdb.creer(edc, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, dpp,
							engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, ESourceCreditType.EXTOURNE, new SourceCreditExtourneLiquidation(liquidationExtourne));
					engagementBudget.setSource(depenseBudget.getSource());
				}
				else {
					FactoryDepenseBudget fdb = new FactoryDepenseBudget();
					EODepenseBudget depenseBudget = fdb.creer(edc, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, dpp,
							engagementBudget, engagementBudget.tauxProrata(), engagementBudget.exercice(), utilisateur, ESourceCreditType.EXTOURNE, new SourceCreditExtourneLiquidation(liquidationExtourne));
					engagementBudget.setSource(depenseBudget.getSource());
				}

				dpp.setDppHtInitial(ht.abs());
				dpp.setDppTtcInitial(ttc.abs());
				return NavigationCtrl.goToLiquidationSurExtourneMandat(component.context(), dpp, liquidationExtourne);
			}
		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			e.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
			component.session.addSimpleErrorMessage("Erreur", e2);
		}
		return null;
	}

	public boolean isLiquiderSurExtourneMandatDisabled() {
		return component.getLiquidationExtourneEnCours() == null;
	}

	public boolean isSolderExtourneMandatDisabled() {
		boolean isSolderDisabled = true;
		EODepenseBudget dep = component.session.getLiquidationExtourneEnCours();
		if (dep != null) {
			isSolderDisabled = false;
		}
		return isSolderDisabled;
	}

	public WOComponent solderExtourneMandat() {
		LiquidationExtourneListe nextPage = null;
		EODepenseBudget dep = component.session.getLiquidationExtourneEnCours();
		try {
			if (dep != null) {
				EOEditingContext edc = dep.editingContext();
				ProcessEngagement.solderSansDroit(component.session.dataBus(), edc, dep.engagementBudget(), component.utilisateurInContext(edc));
				ERXEOControlUtilities.refaultObject(dep.engagementBudget());
				ERXEOControlUtilities.refaultObject(dep);
				nextPage = (LiquidationExtourneListe) component.session.getSavedPageWithName(LiquidationExtourneListe.PAGE_NAME);
				nextPage.appliquerFiltres();
			}

		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			nextPage = (LiquidationExtourneListe) component.pageWithName(LiquidationExtourneListe.PAGE_NAME);
			e.printStackTrace();
		}
		return nextPage;
	}

	public boolean isSolderUneCommandeDisabled() {
		boolean isSolderUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isSoldable(commande.editingContext(), component.utilisateur(), null)) {
			isSolderUneCommandeDisabled = false;
		}

		return isSolderUneCommandeDisabled;
	}

	public WOComponent solderUneCommande() {
		WOComponent nextPage = null;
		EOCommande laCommandeASolder = component.laCommandeEnCours();
		try {
			EOEditingContext edc = laCommandeASolder.editingContext();
			ProcessCommande.solder(component.session.dataBus(), edc, laCommandeASolder, component.utilisateurInContext(edc));
			nextPage = detail();
		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			nextPage = (DetailCommande) component.pageWithName("DetailCommande");
			((DetailCommande) nextPage).setLaCommande(component.laCommandeEnCours());
			// component.session.setItemsMenu(DetailCommande.itemsMenu());
			e.printStackTrace();
		}

		return nextPage;
	}

	public boolean isSupprimerUneCommandeDisabled() {
		boolean isSupprimerUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isSupprimable(commande.editingContext(), component.utilisateur(), null)) {
			isSupprimerUneCommandeDisabled = false;
		}

		return isSupprimerUneCommandeDisabled;
	}

	public boolean isSupprimerUnEngagementDisabled() {
		boolean res = true;
		EOEngagementBudget eb = component.session.getLEngagementBudgetEnCours();
		if (eb != null && eb.commande() == null && eb.isSupprimable(eb.editingContext(), component.utilisateur(), null)) {
			res = false;
		}
		return res;
	}

	public boolean isSupprimerUneFactureDisabled() {
		boolean isSupprimerUneFactureDisabled = true;

		EODepensePapier dpp = component.session.getLaDepensePapierEnCours();
		//FIXME pour une preliquidation, autoriser la suppression meme si engagement solde 

		if (dpp != null && dpp.isPreLiquidation() && dpp.isSupprimable() && dpp.commandeassociee().isLiquidable(dpp.editingContext(), component.utilisateur(), null)) {
			isSupprimerUneFactureDisabled = false;
		}
		return isSupprimerUneFactureDisabled;
	}

	public WOComponent supprimerUneCommande() {
		WOComponent nextPage = null;
		EOCommande laCommandeASupprimer = component.laCommandeEnCours();
		try {
			EOEditingContext edc = laCommandeASupprimer.editingContext();
			ProcessCommande.annuler(component.session.dataBus(), edc, laCommandeASupprimer, component.utilisateurInContext(edc));
			nextPage = detail();
		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			nextPage = (DetailCommande) component.pageWithName("DetailCommande");
			((DetailCommande) nextPage).setLaCommande(laCommandeASupprimer);
			// component.session.setItemsMenu(DetailCommande.itemsMenu());
			e.printStackTrace();
		}
		return nextPage;
	}

	public WOComponent supprimerUnEngagement() {
		WOComponent nextPage = null;
		EOEngagementBudget eb = component.session.getLEngagementBudgetEnCours();
		try {
			EOEditingContext edc = eb.editingContext();
			ProcessEngagement.annuler(component.session.dataBus(), edc, eb, component.utilisateurInContext(edc));
			nextPage = accueil();
		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			nextPage = (DetailEngagementBudget) component.pageWithName(DetailEngagementBudget.class.getName());
			e.printStackTrace();
		}
		return nextPage;
	}

	public WOComponent supprimerUneFacture() {
		WOComponent nextPage = null;
		EODepensePapier laDpp = component.laDepensePapierEnCours();
		try {
			EOEditingContext edc = laDpp.editingContext();
			EOCommande comm = laDpp.commandeAssociee();
			ProcessPreDepense.supprimer(component.session.dataBus(), edc, laDpp, component.utilisateurInContext(edc));
			component.session.setLaCommandeEnCours(comm);
			nextPage = detail();
		} catch (FactoryException e) {
			component.session.setAlertMessage(e.getMessageFormatte());
			nextPage = (DetailCommande) component.pageWithName("DetailFacture");
			((DetailCommande) nextPage).setLaDepensePapierEnCours(laDpp);
			// component.session.setItemsMenu(DetailCommande.itemsMenu());
			e.printStackTrace();
		}
		return nextPage;
	}

	public boolean isInformerDisabled() {
		boolean isInformerDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		// TODO A passer dans EOCommande
		if (commande != null &&
				(commande.typeEtat().tyetLibelle().equals(EOCommande.ETAT_PRECOMMANDE) ||
						commande.typeEtat().tyetLibelle().equals(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE) ||
				commande.typeEtat().tyetLibelle().equals(EOCommande.ETAT_ENGAGEE))) {
			isInformerDisabled = false;
		}

		return isInformerDisabled;
	}

	public WOComponent informer() {
		Informer nextPage = (Informer) component.pageWithName("Informer");
		nextPage.setCommande(component.laCommandeEnCours());
		return nextPage;
	}

	public boolean isImprimerUneCommandeDisabled() {
		boolean isImprimerUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isImprimable()) {
			isImprimerUneCommandeDisabled = false;
		}

		return isImprimerUneCommandeDisabled;
	}

	public DetailCommande imprimerUneCommande() {
		DetailCommande nextPage = (DetailCommande) component.pageWithName("DetailCommande");
		EOCommande commande = component.laCommandeEnCours();
		nextPage.setLaCommande(commande);
		nextPage.setOnloadJS(createOpenWinJS("pageImprimerCommande", "Impression de commande"));
		return nextPage;
	}

	public WOComponent b2bEnvoyerUneCommande() {
		B2BOrderRequestPage nextPage = (B2BOrderRequestPage) component.pageWithName(B2BOrderRequestPage.class.getName());
		EOCommande commande = component.laCommandeEnCours();
		nextPage.setCommande(commande);
		return nextPage;
	}

	public boolean isB2bEnvoyerUneCommandeDisabled() {
		boolean isB2bEnvoyerCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isEnvoyerB2bPossible()) {
			isB2bEnvoyerCommandeDisabled = !component.utilisateur().isDroitEnvoyerCommandeB2b();
			//isB2bEnvoyerCommandeDisabled = false;
		}
		return isB2bEnvoyerCommandeDisabled;
	}

	/**
	 * Permet d'acccéder au site distant en mode "modification de panier".
	 * 
	 * @return
	 */
	public WOComponent b2bModifierLesArticles() {
		WOComponent res = null;
		try {
			EOCommande commande = component.laCommandeEnCours();
			if (commande.isCommandeB2b() && commande.articles().count() > 0) {
				if (commande.toB2bCxmlOrderRequests().count() > 0) {
					throw new RuntimeException("Cette commande a deja ete envoyee au fournisseur, vous ne pouvez pas modifier les articles.");
				}

				String punchOutBrowserFormPostURL = B2bCxmlDirectAction.punchOutOrderFeedBackUrl(component.context());

				EOCodeExer codeExer = (EOCodeExer) commande.codesExer().lastObject();
				EOAttribution attribution = ((EOArticle) commande.articles().lastObject()).attribution();

				EOFournisB2bCxmlParam fournisB2bCxmlParam = ((EOB2bCxmlPunchoutmsg) commande.toB2bCxmlPunchoutmsgs().lastObject()).toFournisB2bCxmlParam();
				Integer persId = Integer.valueOf(component.mySession().connectedUserInfo().persId().intValue());
				Integer fbcpId = (Integer) EOUtilities.primaryKeyForObject(fournisB2bCxmlParam.editingContext(), fournisB2bCxmlParam).allValues().lastObject();
				Integer typaId = (commande.typeAchat() != null ? (Integer) EOUtilities.primaryKeyForObject(commande.typeAchat().editingContext(), commande.typeAchat()).allValues().lastObject() : null);
				Integer ceOrdre = (Integer) EOUtilities.primaryKeyForObject(codeExer.editingContext(), codeExer).allValues().lastObject();
				Integer exeOrdre = commande.exercice().exeExercice();
				Integer attOrdre = (attribution != null ? (Integer) EOUtilities.primaryKeyForObject(attribution.editingContext(), attribution).valueForKey(EOAttribution.ATT_ORDRE_KEY) : null);
				Long commId = Long.valueOf(commande.hashCode());
				String sessionId = component.mySession().sessionID();

				CXMLBuyerCookie buyerCookie = new CXMLBuyerCookie(persId, fbcpId, typaId, ceOrdre, exeOrdre, attOrdre, commId, sessionId);

				CXMLParameters cxmlParameters = fournisB2bCxmlParam.toCxmlParameters();
				try {
					CXMLTransaction transaction = new CXMLTransaction();
					ApplicationUser appUser = new ApplicationUser(commande.editingContext(), persId);
					NSArray<EOItemOut> itemOuts = B2bUtils.articlesToItemOuts(new EOEditingContext(), commande.articles());
					String startPage = transaction.creerTransactionPunchOutSetupRequestEdit(commande.editingContext(), appUser, cxmlParameters, buyerCookie, punchOutBrowserFormPostURL, itemOuts);

					res = new ERXRedirect(component.context());
					((ERXRedirect) res).setUrl(startPage);
					return res;
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);

				}

			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			component.session.setAlertMessage(MyStringCtrl.firstLine(e.getMessage()));
		}
		return null;

	}

	public boolean isB2bModifierLesArticlesDisabled() {
		boolean isB2bModifierLesArticlesDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isCommandeB2b() && commande.articles().count() > 0) {
			isB2bModifierLesArticlesDisabled = false;
		}
		return isB2bModifierLesArticlesDisabled;
	}

	public boolean isRetourALaFactureDisabled() {
		boolean isRetourALaFactureDisabled = true;
		EODepensePapier facture = component.session.getLaDepensePapierEnCours();
		if (facture != null) {
			isRetourALaFactureDisabled = false;
		}
		return isRetourALaFactureDisabled;
	}

	public WOComponent retourALaFacture() {
		DetailFacture nextPage = null;
		EODepensePapier facture = component.session.getLaDepensePapierEnCours();
		if (facture != null) {
			nextPage = (DetailFacture) component.pageWithName("DetailFacture");
			nextPage.setLaFacture(facture);
			component.session.setLaDepensePapierEnCours(facture);
			component.session.setLaCommandeEnCours(facture.commandeassociee());
		}
		return nextPage;
	}

	public boolean isEngagementDisabled() {
		boolean isRetourALaFactureDisabled = true;
		EOEngagementBudget eng = component.session.getLEngagementBudgetEnCours();
		if (eng != null) {
			isRetourALaFactureDisabled = false;
		}
		return isRetourALaFactureDisabled;
	}

	public WOComponent engagement() {
		DetailEngagementBudget nextPage = null;
		EOEngagementBudget eng = component.session.getLEngagementBudgetEnCours();
		if (eng != null) {
			nextPage = (DetailEngagementBudget) component.pageWithName(DetailEngagementBudget.class.getName());
			nextPage.setEngagementBudget(eng);
			component.session.setLEngagementBudgetEnCours(eng);
			if (eng.commande() != null) {
				component.session.setLaCommandeEnCours(eng.commande());
			}
		}
		return nextPage;
	}

	// Menus du detail d'un engagement

	public boolean isAfficherCommandeDetailleeDisabled() {
		boolean isAfficherCommandeDetailleeDisabled = true;

		EOEngagementBudget engagement = component.session.getLEngagementBudgetEnCours();
		if (engagement != null && engagement.commande() != null) {
			isAfficherCommandeDetailleeDisabled = false;
		}

		return isAfficherCommandeDetailleeDisabled;
	}

	public WOComponent afficherCommandeDetaillee() {

		DetailCommande nextPage = (DetailCommande) component.pageWithName("DetailCommande");
		EOEngagementBudget engagement = component.session.getLEngagementBudgetEnCours();
		EOCommande commande = engagement.commande();
		nextPage.setLaCommande(commande);
		component.session.setLaCommandeEnCours(commande);

		return nextPage;
	}

	// Menus du detail d'une facture

	public boolean isReverserUneFactureDisabled() {
		boolean isReverserUneFactureDisabled = true;

		return isReverserUneFactureDisabled;
	}

	public WOComponent reverserUneFacture() {
		return null;
	}

	public boolean isDetaillerUneCommandeDisabled() {
		boolean isDetaillerUneCommandeDisabled = false;
		EODepensePapier dp = component.session.getLaDepensePapierEnCours();
		if (dp == null) {
			isDetaillerUneCommandeDisabled = true;
		}
		else {
			EOCommande commande = dp.commandeassociee();
			if (commande == null) {
				isDetaillerUneCommandeDisabled = true;
			}
		}
		return isDetaillerUneCommandeDisabled;
	}

	public WOComponent detaillerUneCommande() {
		WOComponent nextPage = null;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null) {
			nextPage = (DetailCommande) component.pageWithName("DetailCommande");
			((DetailCommande) nextPage).setLaCommande(commande);
		}
		else {
			nextPage = accueil();
		}
		if (component.session.dgActif == component.dgFacture()) {
			((DetailCommande) nextPage).setRetourALaFacturePossible(true);
		}
		return nextPage;
	}

	public boolean isRetournerALaListeDesFacturesDisabled() {
		boolean isRetournerALaListeDesFacturesDisabled = true;
		WODisplayGroup dgF = component.dgFacture();
		if ((dgF != null && dgF.allObjects().count() > 0)) {
			isRetournerALaListeDesFacturesDisabled = false;
		}

		dgF = component.dgPreLiquidation();
		if ((dgF != null && dgF.allObjects().count() > 0)) {
			isRetournerALaListeDesFacturesDisabled = false;
		}

		return isRetournerALaListeDesFacturesDisabled;
	}

	public WOComponent retournerALaListeDesFactures() {
		if (component.dgPreLiquidation() != null && component.dgPreLiquidation().allObjects().count() > 0) {
			PreLiquidationsListePage nextPage = (PreLiquidationsListePage) component.pageWithName("PreLiquidationsListePage");
			WODisplayGroup dgPC = component.dgPreLiquidation();
			@SuppressWarnings("unchecked")
			NSDictionary<String, Object> bdgs = dgPC.queryBindings();
			NSArray<NSDictionary<String, Object>> lesLiquidations = FinderDepensePapier.getRawRowPreLiquidation(component.edc(), bdgs);
			dgPC.setObjectArray(lesLiquidations);
			nextPage.setDg(dgPC);
			return nextPage;
		}
		else {
			FactureListe nextPage = (FactureListe) component.pageWithName("FactureListe");
			nextPage.setDg(component.dgFacture());
			component.session.setLaDepensePapierEnCours(null);
			component.session.setLaCommandeEnCours(null);
			return nextPage;
		}

	}

	// Menus de DetailCommande

	public boolean isEnregistrerUneCommandeDisabled() {
		boolean isEnregistrerUneCommandeDisabled = true;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null && commande.isEnregistrable()) {
			isEnregistrerUneCommandeDisabled = false;
		}
		component.session.setMessageErreur(commande.messageErreur());

		return isEnregistrerUneCommandeDisabled;
	}

	public WOComponent enregistrerUneCommande() {
		//		BonDeCommande nextPage = null;
		BonDeCommande2 nextPage = null;
		EOCommande commande = component.laCommandeEnCours();
		if (commande == null) {
			return accueil();
		}
		else if (commande.isPrecommande() == false && commande.isPrestationInterne() &&
				component.utilisateur().isDroitCreerPrestation(commande.exercice())) {
			// Prestation Interne
			nextPage = NavigationCtrl.goToBonDeCommande(component.context(), commande, null, "Informations");
			nextPage.setOnloadJS(createOpenWinJS("pageCommandePrestationInterne", "Fin de commande"));
		}
		else if (commande.isPrecommande() == false && commande.isHorsMarche() && commande.isMAPA()) {
			nextPage = NavigationCtrl.goToBonDeCommande(component.context(), commande, null, "Articles");
			nextPage.setOnloadJS(createOpenWinJS("pageCommandeAttestationMAPA", "Fin de commande"));
		}
		else {
			try {
				EOEditingContext edc = commande.editingContext();
				nextPage = NavigationCtrl.goToBonDeCommande(component.context(), commande, null, "Informations");

				ProcessCommande.enregistrer(component.session.dataBus(), edc, commande, component.utilisateurInContext(edc));
				component.session.setAlertMessage(null);
				// On refetche la commande pour la rafraichir
				EOExercice exercice = commande.exercice();
				Number numero = commande.commNumero();

				commande = FinderCommande.getCommande(ERXEC.newEditingContext(), exercice, numero);
				component.session.setLaCommandeEnCours(commande);

				nextPage.setLaCommande(commande);
				nextPage.setOnloadJS(createOpenWinJS("pageCommandeApresEnregistrement", "Fin de commande"));

			} catch (FactoryException e) {

				String alertMessage = e.getMessageFormatte();
				if (e.isBloquant()) {
					nextPage.setLaCommande(commande);
					if (e.isInformatif()) {
						// Exception contenant un message d'information pour l'utilisateur
						component.session.setAlertMessage(alertMessage);
					}
					else {
						e.printStackTrace();
						throw e;
					}
				}
				else {
					component.session.setAlertMessage(alertMessage);
					// On refetche la commande pour la rafraichir
					EOEditingContext edc = commande.editingContext();
					EOExercice exercice = commande.exercice();
					Number numero = commande.commNumero();
					commande = FinderCommande.getCommande(edc, exercice, numero);
					component.session.setLaCommandeEnCours(commande);
					nextPage.setLaCommande(commande);
					nextPage.setOnloadJS(createOpenWinJS("pageCommandeApresEnregistrement", "Fin de commande"));
				}
			} catch (RuntimeException e1) {
				component.session.setAlertMessage(e1.getMessage());
				e1.printStackTrace();
				throw e1;
			}
		}
		return nextPage;
	}

	public WOComponent annulerUneCommande() {
		WOComponent nextPage = null;
		EOCommande commandeEnCours = component.laCommandeEnCours();
		component.session.setMessageErreur(null);
		if (commandeEnCours != null && commandeEnCours.commNumero() != null) {
			nextPage = detail();
		}
		else {
			nextPage = accueil();
		}
		return nextPage;
	}

	public WOComponent ajouterDesSources() {
		SourceListeSelection nextPage = (SourceListeSelection) component.pageWithName("SourceListeSelection");
		nextPage.reset();
		return nextPage;
	}

	public boolean isCreerUnNouvelArticleDisabled() {
		boolean isCreerUnNouvelArticleDisabled = true;
		EOCommande commande = (EOCommande) component.laCommandeEnCours();
		if (commande != null) {
			//			if (commande != null && commande.isSurUnMarcheCatalogue()==false) {
			isCreerUnNouvelArticleDisabled = false;

			if (commande.isCommandeB2b()) {
				isCreerUnNouvelArticleDisabled = true;
			}
		}

		return isCreerUnNouvelArticleDisabled;
	}

	public WOComponent creerUnNouvelArticle() {

		EOCommande commande = component.laCommandeEnCours();
		EOEditingContext edc = commande.editingContext();

		if (commande.isSurUnMarcheCatalogue()) {
			ArticleRechercheAvancee nextPage = (ArticleRechercheAvancee) component.pageWithName("ArticleRechercheAvancee");
			nextPage.initialiser();
			nextPage.setRechercheAvanceeDirecte(false);
			return nextPage;
		}
		else {
			ArticleCreation nextPage = (ArticleCreation) component.pageWithName("ArticleCreation");
			FactoryArticle fa = new FactoryArticle();
			EOArticle article = null;
			article = fa.creer(edc, "", "", BigDecimal.valueOf(1),
					BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0),
					null, null, null, null, null,
					null, null, false);
			nextPage.setNewArticle(article);
			nextPage.initialiser();
			return nextPage;
		}
	}

	public WOComponent rechercherDesArticles() {
		ArticleListeSelection nextPage = null;
		String strRecherche = component.getRechercheRapide();
		WODisplayGroup dg = component.dgArticles();
		if (dg != null) {
			EOCommande commande = component.laCommandeEnCours();
			@SuppressWarnings("unchecked")
			NSMutableDictionary<String, Object> bdgs = dg.queryBindings();
			// Initalisation des bindings en fonction de la commande en cours
			bdgs.removeAllObjects();
			bdgs.setObjectForKey(commande.exercice(), "exercice");
			EOFournisseur fournisseur = commande.fournisseur();
			EOAttribution attribution = commande.attribution();
			EOTypeAchat typeAchat = commande.typeAchat();

			if (fournisseur != null) {
				bdgs.setObjectForKey(fournisseur, "fournisseur");
			}
			if (attribution != null) {
				attribution.attValide();
				bdgs.setObjectForKey(attribution, "attribution");
			}
			else {
				bdgs.setObjectForKey(new NSTimestamp(), "date");
			}
			if (typeAchat != null) {
				bdgs.setObjectForKey(typeAchat, "typeAchat");
			}
			if (strRecherche != null) {
				bdgs.setObjectForKey(strRecherche, "rechercheGlobale");
			}

			NSArray<NSDictionary<String, Object>> lesArticles = FinderArticle.getRawRowArticles(commande.editingContext(), bdgs);
			dg.setObjectArray(lesArticles);
			dg.setSelectedObjects(null);
			//nextPage = (ArticleListeSelection) component.pageWithName("ArticleListeSelection");
			nextPage = (ArticleListeSelection) component.pageWithName(ArticleListeSelection.class.getName());
			// nextPage = (ArticleListeSelection)component.session.getSavedcomponent.pageWithName("ArticleListeSelection");
			nextPage.initialiser();
		}
		else {
			return accueil();
		}

		return nextPage;
	}

	// Menus de Liquidation

	/**

	 */
	public boolean isEnregistrerUneLiquidationDisabled() {
		boolean isEnregistrerUneLiquidationDisabled = true;
		EODepensePapier depensePapier = component.laDepensePapierEnCours();

		if (component.session.isServiceFacturierDisponible() && depensePapier.isPreLiquidation()) {
			if (depensePapier != null && depensePapier.isSFEnregistrable()) {
				isEnregistrerUneLiquidationDisabled = false;
			}
		}
		else {
			if (depensePapier != null && depensePapier.isComplete() &&
					depensePapier.isRepartitionComplete()) {
				isEnregistrerUneLiquidationDisabled = false;
			}
		}
		return isEnregistrerUneLiquidationDisabled;
	}

	public boolean isEnregistrerUneLiquidationSansEngagementDisabled() {
		boolean isEnregistrerUneLiquidationDisabled = true;
		EODepensePapier depensePapier = component.laDepensePapierEnCours();

		//		if (component.session.isServiceFacturierDisponible()) {
		//			if (depensePapier != null && depensePapier.isSFEnregistrable()) {
		//				isEnregistrerUneLiquidationDisabled = false;
		//			}
		//		}
		//		else {
		if (depensePapier != null && depensePapier.isComplete() &&
				depensePapier.isRepartitionComplete()) {
			isEnregistrerUneLiquidationDisabled = false;
		}
		//}
		return isEnregistrerUneLiquidationDisabled;
	}

	public boolean isEnregistrerUneLiquidationSurExtourneEnveloppeDisabled() {
		boolean isEnregistrerUneLiquidationDisabled = true;
		EODepensePapier depensePapier = component.laDepensePapierEnCours();
		if (depensePapier != null && depensePapier.isComplete() &&
				depensePapier.isRepartitionComplete()) {
			isEnregistrerUneLiquidationDisabled = false;
		}
		return isEnregistrerUneLiquidationDisabled;
	}

	public boolean isEnregistrerUneLiquidationSurExtourneMandatDisabled() {
		boolean isEnregistrerUneLiquidationDisabled = true;
		EODepensePapier depensePapier = component.laDepensePapierEnCours();
		if (depensePapier != null && depensePapier.isComplete() &&
				depensePapier.isRepartitionComplete()) {
			isEnregistrerUneLiquidationDisabled = false;
		}
		return isEnregistrerUneLiquidationDisabled;
	}

	public WOComponent enregistrerUneLiquidation() {
		EODepensePapier maDepensePapier = component.laDepensePapierEnCours();

		// controle extourne
		try {
			Process.controlerExtournePossible(maDepensePapier);
		} catch (FactoryException fe) {
			component.session.addSimpleErrorMessage("Action annulée", fe.getMessage());
			return null;
		}

		Liquidation2 nextPage = null;

		nextPage = (Liquidation2) component.pageWithName(Liquidation2.class.getName());
		nextPage.setLaDepensePapier(maDepensePapier);
		nextPage.setOnloadJS(createOpenWinJS("pageLiquidationConfirmation", "Confirmation"));

		return nextPage;
	}

	public WOComponent enregistrerUneLiquidationSansEngagement() {
		EODepensePapier maDepensePapier = component.laDepensePapierEnCours();
		// controle extourne
		try {
			Process.controlerExtournePossible(maDepensePapier);
		} catch (FactoryException fe) {
			component.session.addSimpleErrorMessage("Action annulée", fe.getMessage());
			return null;
		}

		LiquidationSansEngagement nextPage = null;

		nextPage = (LiquidationSansEngagement) component.pageWithName(LiquidationSansEngagement.class.getName());
		nextPage.setLaDepensePapier(maDepensePapier);
		nextPage.setOnloadJS(createOpenWinJS("pageLiquidationConfirmation", "Confirmation"));

		return nextPage;
	}

	public WOComponent enregistrerUneLiquidationSurExtourneEnveloppe() {
		LiquidationSurExtourneEnveloppe nextPage = null;

		nextPage = (LiquidationSurExtourneEnveloppe) component.pageWithName(LiquidationSurExtourneEnveloppe.class.getName());
		nextPage.setLaDepensePapier(component.laDepensePapierEnCours());
		//nextPage.setFournisseur(component.laDepensePapierEnCours().fournisseur());
		nextPage.setOnloadJS(createOpenWinJS("pageLiquidationConfirmation", "Confirmation"));

		return nextPage;
	}

	public WOComponent enregistrerUneLiquidationSurExtourneMandat() {
		LiquidationSurExtourneMandat nextPage = null;

		nextPage = (LiquidationSurExtourneMandat) component.pageWithName(LiquidationSurExtourneMandat.class.getName());
		nextPage.setLaLiquidationExtourne(component.session.getLiquidationExtourneEnCours());
		nextPage.setLaDepensePapier(component.laDepensePapierEnCours());
		nextPage.setOnloadJS(createOpenWinJS("pageLiquidationConfirmation", "Confirmation"));

		return nextPage;
	}

	public WOComponent annulerUneLiquidation() {
		WOComponent nextPage = null;
		component.session.setMessageErreur(null);
		EOCommande commande = component.laCommandeEnCours();
		if (commande == null) {
			nextPage = accueil();
		}
		else {
			EOEditingContext edc = commande.editingContext();
			edc.revert();
			nextPage = (DetailCommande) component.pageWithName(DetailCommande.class.getName());
			((DetailCommande) nextPage).setLaCommande(commande);
		}
		return nextPage;
	}

	public boolean isEnregistrerUneReimputationDisabled() {
		boolean isEnregistrerUneReimputationDisabled = true;
		EODepenseBudget depenseBudget = component.session.getLaDepenseBudgetReimputation();
		if (!StringCtrl.isEmpty(component.session.getReimpLibelle()) && depenseBudget != null &&
				(depenseBudget.isControleHorsMarcheGood() || depenseBudget.isControleMarcheGood()) &&
				depenseBudget.isControleActionGood() &&
				depenseBudget.isControlePlanComptableGood() &&
				depenseBudget.isControleConventionGood() &&
				depenseBudget.isControleAnalytiqueGood() &&
				depenseBudget.objectHasChangedForReimputation()) {
			isEnregistrerUneReimputationDisabled = false;
		}
		return isEnregistrerUneReimputationDisabled;
	}

	/**
	 * @return
	 * @see ProcessDepense#enregistrerReimputation(org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus, EOEditingContext, EODepenseBudget,
	 *      EOUtilisateur)
	 */
	public WOComponent enregistrerUneReimputation() {
		WOComponent nextPage = null;
		try {
			EODepenseBudget depenseBudget = component.session.getLaDepenseBudgetReimputation();
			EOEditingContext edc = depenseBudget.editingContext();
			//FIXME verifier que organ et type credit sont bien affectes a la source, voir si changer de fonctionnement pour affectation nouvelle organ et nvo type de credit
			ProcessDepense.enregistrerReimputation(component.session.dataBus(), edc, depenseBudget, component.utilisateurInContext(edc), component.session.getReimpLibelle(), depenseBudget.getSource().organ(), depenseBudget.getSource().typeCredit());
			edc.invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(new EOGlobalID[] {
					edc.globalIDForObject(depenseBudget.engagementBudget()), edc.globalIDForObject(depenseBudget)
			}));

			component.session.setReimpLibelle(null);

			nextPage = retourALaFacture();

		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				nextPage = null;
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					component.session.setAlertMessage(alertMessage);
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				nextPage = detail();
			}
		} catch (RuntimeException e1) {
			component.session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}
		return nextPage;

	}

	public WOComponent annulerUneReimputation() {
		//appeler un revert sur l'editingContext dans le cas de l'annulation d'une reimputation
		EODepenseBudget depenseBudget = component.session.getLaDepenseBudgetReimputation();
		EOEditingContext edc = depenseBudget.editingContext();
		edc.revert();
		component.session.setReimpLibelle(null);
		return retourALaFacture();
	}

	// Menus de Reversement

	public boolean isEnregistrerUnReversementDisabled() {
		boolean isEnregistrerUnReversementDisabled = true;
		EODepenseBudget depenseBudget = component.session.getLaDepenseBudgetReversement();
		if (depenseBudget != null) {
			EODepensePapier depensePapier = depenseBudget.depensePapier();
			if (depenseBudget != null &&
					depensePapier.isComplete() &&
					(depenseBudget.isControleHorsMarcheGood() || depenseBudget.isControleMarcheGood()) &&
					depenseBudget.isControleActionGood() &&
					depenseBudget.isControlePlanComptableGood() &&
					depenseBudget.isControleConventionGood() &&
					depenseBudget.isControleAnalytiqueGood()) {
				isEnregistrerUnReversementDisabled = false;
				NSDictionary<String, String> menu = component.lesItemsMenu().objectForKey("EnregistrerUnReversement");
				String onClickMsg = "return openConfirmPanel('Voulez-vous vraiment enregistrer ce reversement ?\\n";
				onClickMsg += "Montant HT : " + depensePapier.dppHtInitial().abs() + "\\n" + "Montant TTC : " + depensePapier.dppTtcInitial().abs();
				onClickMsg += "');";
				menu.takeValueForKey(onClickMsg, "onClick");
				component.lesItemsMenu().setObjectForKey(menu, "EnregistrerUnReversement");
			}
		}
		return isEnregistrerUnReversementDisabled;
	}

	public WOComponent enregistrerUnReversement() {
		WOComponent nextPage = null;
		try {
			EODepenseBudget depenseBudget = component.session.getLaDepenseBudgetReversement();
			EODepensePapier depensePapier = depenseBudget.depensePapier();
			EOEditingContext edc = depensePapier.editingContext();
			ProcessDepense.enregistrer(component.session.dataBus(), edc, depensePapier, component.utilisateurInContext(edc));
			if (component.laCommandeEnCours() != null) {
				nextPage = detail();
			}
			else {
				nextPage = retourALaFacture();
			}
		} catch (FactoryException e) {
			String alertMessage = e.getMessageFormatte();
			if (e.isBloquant()) {
				nextPage = null;
				if (e.isInformatif()) {
					// Exception contenant un message d'information pour l'utilisateur
					component.session.setAlertMessage(alertMessage);
				}
				else {
					e.printStackTrace();
					throw e;
				}
			}
			else {
				if (component.laCommandeEnCours() != null) {
					nextPage = detail();
				}
				else {
					nextPage = retourALaFacture();
				}

			}
		} catch (RuntimeException e1) {
			component.session.setAlertMessage(e1.getMessage());
			e1.printStackTrace();
			throw e1;
		}
		return nextPage;
	}

	public WOComponent annulerUnReversement() {
		DetailFacture nextPage = null;
		if (component.session.defaultEditingContext().hasChanges()) {
			component.session.defaultEditingContext().revert();
		}
		if (component.session.getLaDepenseBudgetReversement().editingContext().hasChanges()) {
			component.session.getLaDepenseBudgetReversement().editingContext().revert();
		}
		EODepensePapier dp = component.laDepensePapierEnCours();
		component.session.setAlertMessage(null);
		component.session.setMessageErreur(null);
		component.session.setLaDepenseBudgetReversement(null);
		nextPage = (DetailFacture) component.pageWithName("DetailFacture");
		nextPage.setLaFacture(dp);
		return nextPage;
	}

	public WOComponent rechercheAvanceeArticles() {
		ArticleRechercheAvancee nextPage = (ArticleRechercheAvancee) component.pageWithName("ArticleRechercheAvancee");
		nextPage.setRechercheAvanceeDirecte(true);
		component.dgArticles().queryBindings().removeAllObjects();
		nextPage.initialiser();

		return nextPage;
	}

	// Methodes privees utilisees dans la gestion des menus

	private WOComponent detail() {
		WOComponent nextPage = null;
		EOCommande commande = component.laCommandeEnCours();
		if (commande != null) {
			nextPage = (DetailCommande) component.pageWithName("DetailCommande");
			EOEditingContext edc = commande.editingContext();
			EOEditingContext newEdc = ERXEC.newEditingContext();
			EOExercice exercice = (EOExercice) newEdc.faultForGlobalID(edc.globalIDForObject(commande.exercice()), newEdc);
			Number numero = commande.commNumero();
			commande = FinderCommande.getCommande(newEdc, exercice, numero);
			if (commande != null) {
				((DetailCommande) nextPage).setLaCommande(commande);
				component.session.setLaCommandeEnCours(commande);
				component.session.setNestedEdc(newEdc);
				if (component.session.dgActif == component.dgFacture()) {
					((DetailCommande) nextPage).setRetourALaFacturePossible(true);
				}
			}
			else {
				component.session.setLaCommandeEnCours(null);
				nextPage = accueil();
			}
		}
		return nextPage;
	}

	/**
	 * @param action Le nom de l'action à exécuter
	 * @param titre Le titre de la fenêtre modale
	 * @return Le script de création de la fenêtre modale, en utilisant du https si activé (secureMode)
	 */
	private String createOpenWinJS(String action, String titre) {
		String url = component.context().directActionURLForActionNamed(action, null, component.context().secureMode(), false);
		return "openListeSelectionWin(null,'" + url + "',\'" + titre + "\',\'CommandeDetaillee\',\'Contenu\',true);";
	}

}
