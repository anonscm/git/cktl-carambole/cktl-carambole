/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface _ILiquidationCtrl {
	public void creerDepenseControleHorsMarche(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat,
			EOExercice exercice, _IDepenseBudget depenseBudget);

	public void creerDepenseControleMarche(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOAttribution attribution,
			EOExercice exercice, _IDepenseBudget depenseBudget);

	public void supprimerDepenseControleHorsMarche(EOEditingContext ed, _IDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException;

	public void supprimerDepenseControleHorsMarches(EOEditingContext ed, _IDepenseBudget db) throws DepenseControleHorsMarcheException;

	public void supprimerDepenseControleMarches(EOEditingContext ed, _IDepenseBudget db) throws DepenseControleHorsMarcheException;

	public void creerDepenseControlePlanComptable(EOEditingContext ed, BigDecimal dpcoHtSaisie, BigDecimal dpcoTvaSaisie,
			BigDecimal dpcoTtcSaisie, BigDecimal dpcoMontantBudgetaire, BigDecimal dpcoPourcentage, EOPlanComptable planComptable, EOExercice exercice,
			_IDepenseBudget depenseBudget);

	public void supprimerDepenseControlePlanComptable(EOEditingContext ed, _IDepenseControlePlanComptable depense);

	public void creerDepenseControleConvention(EOEditingContext ed, BigDecimal dconHtSaisie, BigDecimal dconTvaSaisie,
			BigDecimal dconTtcSaisie, BigDecimal dconMontantBudgetaire, BigDecimal dconPourcentage, EOConvention convention, EOExercice exercice,
			_IDepenseBudget depenseBudget);

	public void supprimerDepenseControleConvention(EOEditingContext ed, _IDepenseControleConvention depense);

	public void creerDepenseControleAnalytique(EOEditingContext ed, BigDecimal danaHtSaisie, BigDecimal danaTvaSaisie,
			BigDecimal danaTtcSaisie, BigDecimal danaMontantBudgetaire, BigDecimal danaPourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			_IDepenseBudget depenseBudget);

	public void supprimerDepenseControleAnalytique(EOEditingContext ed, _IDepenseControleAnalytique depense);

	public void creerDepenseControleAction(EOEditingContext ed, BigDecimal dactHtSaisie, BigDecimal dactTvaSaisie,
			BigDecimal dactTtcSaisie, BigDecimal dactMontantBudgetaire, BigDecimal dactPourcentage, EOTypeAction typeAction, EOExercice exercice,
			_IDepenseBudget depenseBudget);

	public void supprimerDepenseControleAction(EOEditingContext ed, _IDepenseControleAction depense);

	public void supprimerInventaire(_IDepenseControlePlanComptable depense, EOInventaire inventaire);

	public NSArray<? extends _IDepenseBudget> getLesDepenseBudgets();

	public NSArray<? extends _IDepenseBudget> getLesDepenseBudgetsHorsExtourne();

	public _IFactoryDepenseBudget getFdb();

	public _IFactoryDepenseControleAction getFdca();

	public _IFactoryDepenseControleHorsMarche getFdchm();

	public _IFactoryDepenseControleMarche getFdcm();

	public _IFactoryDepenseControlePlanComptable getFdcpc();

	public _IFactoryDepenseControleConvention getFdcc();

	public _IFactoryDepenseControleAnalytique getFdcca();

	public ArrayList<EOEngagementBudget> getEngagementLiquidables();

	public void resetEngagementLiquidables(EOEditingContext ed, EOUtilisateur utilisateur);

	public Boolean isEngagementLiquidable(EOEngagementBudget engagementBudget);
}
