package org.cocktail.carambole.server.controleurs;

import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepenseguiajax.serveur.ICktlDepenseGuiAjaxSession;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.CktlSourceSelectionCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SourceListSelectionForReimputationCtrl extends CktlSourceSelectionCtrl {
	private EODepenseBudget depenseBudget;

	public SourceListSelectionForReimputationCtrl(ICktlDepenseGuiAjaxSession session, EOEditingContext edc, EOExercice exercice, EODepenseBudget eoDepenseBudget) {
		super(session, edc, exercice);
		this.depenseBudget = eoDepenseBudget;
	}

	public EODepenseBudget getDepenseBudget() {
		return depenseBudget;
	}

	public void setDepenseBudget(EODepenseBudget depenseBudget) {
		this.depenseBudget = depenseBudget;
	}

	protected NSArray<NSDictionary<String, Object>> getRawRowBudgets(EOEditingContext ed, NSMutableDictionary<String, Object> bindings) {
		// FIXME n'afficher que les sources compatibles avec la depenseBudget
		//		changement de la ligne budgétaire
		//
		//	    dépense sans inventaire : si la dépense est mandatée, on choisit une ligne budgétaire de la même UB, sinon toutes les lignes, dans la limite que le disponible le permette.
		//	    dépense avec inventaire : suivant un paramètre a mettre en place, même limitation que la dépense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
		//	    le changement de ligne peut impliquer la modification des conventions et des codes analytiques éventuellement associés à cette dépense    cf. changement convention
		//	     changement de ligne peut impliquer une création d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs liés a celle qu'on ré-impute) par exemple
		//	    si la dépense ré-imputée possède un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..

		//si la dépense est mandatée, on choisit une ligne budgétaire de la même UB
		if (getDepenseBudget().isMandate()) {
			bindings.setObjectForKey(getDepenseBudget().getSource().organ().orgUb(), "organUb");
		}

		// ré-imputation possible par un type de crédit de la même section que le précédent, 
		//sous réserve que le disponible budgétaire soit suffisant et que l'imputation comptable 
		//utilisée par cette dépense soit affectée au type de crédit dans Maracuja.

		@SuppressWarnings("unused")
		//rod 24/04/2012 a priori ca sert à rien 
		EOTypeCredit tc = getDepenseBudget().getSource().typeCredit();
		bindings.setObjectForKey(EOTypeCredit.TCD_SECT_KEY, getDepenseBudget().getSource().typeCredit().tcdSect());

		//FIXME ajouter filtre sur taux de prorata si depense mandatee pour recherche des sources ?

		return super.getRawRowBudgets(ed, bindings);
	}

	public NSDictionary<String, Object> getLaSourceUtilisateurSelectionnee() {
		NSMutableArray<NSDictionary<String, Object>> res = getLesSourcesUtilisateurSelectionnees();
		if (res == null || res.count() == 0) {
			return null;
		}
		return res.objectAtIndex(0);
	}

	public void setLaSourceUtilisateurSelectionnee(NSDictionary<String, Object> source) {
		setLesSourcesUtilisateurSelectionnees(new NSMutableArray<NSDictionary<String, Object>>(source));
	}

}
