/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.math.BigDecimal;

import org.cocktail.fwkcktldepense.server.exception.DepenseControleHorsMarcheException;
import org.cocktail.fwkcktldepense.server.factory.FactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.factory.FactoryPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseBudget;
import org.cocktail.fwkcktldepense.server.factory._IFactoryDepenseControleMarche;
import org.cocktail.fwkcktldepense.server.metier.EOAttribution;
import org.cocktail.fwkcktldepense.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOCodeExer;
import org.cocktail.fwkcktldepense.server.metier.EOConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOInventaire;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier.EOPreDepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAchat;
import org.cocktail.fwkcktldepense.server.metier.EOTypeAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAction;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleAnalytique;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControleHorsMarche;
import org.cocktail.fwkcktldepense.server.metier._IDepenseControlePlanComptable;

import com.webobjects.eocontrol.EOEditingContext;

public class PreLiquidationCtrl extends AbstractLiquidationCtrl {
	private static FactoryPreDepenseControleAction fdca = new FactoryPreDepenseControleAction();
	private static FactoryPreDepenseControleHorsMarche fdchm = new FactoryPreDepenseControleHorsMarche();
	private static FactoryDepenseControleMarche fdcm = new FactoryDepenseControleMarche();
	private static FactoryPreDepenseControlePlanComptable fdcpc = new FactoryPreDepenseControlePlanComptable();
	private static FactoryPreDepenseControleConvention fdcc = new FactoryPreDepenseControleConvention();
	private static FactoryPreDepenseControleAnalytique fdcca = new FactoryPreDepenseControleAnalytique();
	private static FactoryPreDepenseBudget fdb = new FactoryPreDepenseBudget();

	public PreLiquidationCtrl(EODepensePapier dpp) {
		super(dpp);
	}

	public void creerDepenseControleHorsMarche(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOCodeExer codeExer, EOTypeAchat typeAchat,
			EOExercice exercice, _IDepenseBudget depenseBudget) {

		fdchm.creer(ed, dhomHtSaisie, dhomTvaSaisie,
				dhomTtcSaisie, dhomMontantBudgetaire, dhomPourcentage, codeExer, typeAchat,
				exercice, (EOPreDepenseBudget) depenseBudget);

	}

	public void creerDepenseControleMarche(EOEditingContext ed, BigDecimal dhomHtSaisie, BigDecimal dhomTvaSaisie,
			BigDecimal dhomTtcSaisie, BigDecimal dhomMontantBudgetaire, BigDecimal dhomPourcentage, EOAttribution attribution,
			EOExercice exercice, _IDepenseBudget depenseBudget) {

		fdcm.creer(ed, dhomHtSaisie, dhomTvaSaisie,
				dhomTtcSaisie, dhomMontantBudgetaire, dhomPourcentage, attribution,
				exercice, (EODepenseBudget) depenseBudget);
	}

	public void supprimerDepenseControleHorsMarche(EOEditingContext ed, _IDepenseControleHorsMarche depense) throws DepenseControleHorsMarcheException {
		fdchm.supprimer(ed, (EOPreDepenseControleHorsMarche) depense);

	}

	public void creerDepenseControlePlanComptable(EOEditingContext ed, BigDecimal dpcoHtSaisie, BigDecimal dpcoTvaSaisie,
			BigDecimal dpcoTtcSaisie, BigDecimal dpcoMontantBudgetaire, BigDecimal dpcoPourcentage, EOPlanComptable planComptable, EOExercice exercice,
			_IDepenseBudget depenseBudget) {

		fdcpc.creer(ed, dpcoHtSaisie, dpcoTvaSaisie,
				dpcoTtcSaisie, dpcoMontantBudgetaire, dpcoPourcentage, planComptable, exercice,
				(EOPreDepenseBudget) depenseBudget);
	}

	public void supprimerDepenseControlePlanComptable(EOEditingContext ed, _IDepenseControlePlanComptable depense) {

		fdcpc.supprimer(ed, (EOPreDepenseControlePlanComptable) depense);
	}

	public void creerDepenseControleConvention(EOEditingContext ed, BigDecimal dconHtSaisie, BigDecimal dconTvaSaisie,
			BigDecimal dconTtcSaisie, BigDecimal dconMontantBudgetaire, BigDecimal dconPourcentage, EOConvention convention, EOExercice exercice,
			_IDepenseBudget depenseBudget) {

		fdcc.creer(ed, dconHtSaisie, dconTvaSaisie,
				dconTtcSaisie, dconMontantBudgetaire, dconPourcentage, convention, exercice,
				(EOPreDepenseBudget) depenseBudget);
	}

	public void supprimerDepenseControleConvention(EOEditingContext ed, _IDepenseControleConvention depense) {

		fdcc.supprimer(ed, (EOPreDepenseControleConvention) depense);
	}

	public void creerDepenseControleAnalytique(EOEditingContext ed, BigDecimal danaHtSaisie, BigDecimal danaTvaSaisie,
			BigDecimal danaTtcSaisie, BigDecimal danaMontantBudgetaire, BigDecimal danaPourcentage, EOCodeAnalytique codeAnalytique, EOExercice exercice,
			_IDepenseBudget depenseBudget) {

		fdcca.creer(ed, danaHtSaisie, danaTvaSaisie,
				danaTtcSaisie, danaMontantBudgetaire, danaPourcentage, codeAnalytique, exercice,
				(EOPreDepenseBudget) depenseBudget);
	}

	public void supprimerDepenseControleAnalytique(EOEditingContext ed, _IDepenseControleAnalytique depense) {

		fdcca.supprimer(ed, (EOPreDepenseControleAnalytique) depense);
	}

	public void creerDepenseControleAction(EOEditingContext ed, BigDecimal dactHtSaisie, BigDecimal dactTvaSaisie,
			BigDecimal dactTtcSaisie, BigDecimal dactMontantBudgetaire, BigDecimal dactPourcentage, EOTypeAction typeAction, EOExercice exercice,
			_IDepenseBudget depenseBudget) {

		fdca.creer(ed, dactHtSaisie, dactTvaSaisie,
				dactTtcSaisie, dactMontantBudgetaire, dactPourcentage, typeAction, exercice,
				(EOPreDepenseBudget) depenseBudget);
	}

	public void supprimerDepenseControleAction(EOEditingContext ed, _IDepenseControleAction depense) {

		fdca.supprimer(ed, (EOPreDepenseControleAction) depense);
	}

	public void supprimerInventaire(_IDepenseControlePlanComptable depense, EOInventaire inventaire) {

		FactoryPreDepenseControlePlanComptable fdcpc = new FactoryPreDepenseControlePlanComptable();
		fdcpc.supprimerInventaire((EOPreDepenseControlePlanComptable) depense, inventaire);
	}

	public FactoryPreDepenseControleAction getFdca() {
		return fdca;
	}

	public FactoryPreDepenseControleHorsMarche getFdchm() {
		return fdchm;
	}

	public FactoryPreDepenseControlePlanComptable getFdcpc() {
		return fdcpc;
	}

	public FactoryPreDepenseControleConvention getFdcc() {
		return fdcc;
	}

	public FactoryPreDepenseControleAnalytique getFdcca() {
		return fdcca;
	}

	public _IFactoryDepenseControleMarche getFdcm() {
		return fdcm;
	}

	public void supprimerDepenseControleHorsMarches(EOEditingContext ed, _IDepenseBudget db) throws DepenseControleHorsMarcheException {
		//nettoyer les repart hors marche
		if (db.depenseControleHorsMarches() != null) {
			for (int i = db.depenseControleHorsMarches().count() - 1; i >= 0; i--) {
				getFdchm()._supprimer(ed, db.depenseControleHorsMarches().objectAtIndex(i));
			}
		}
	}

	public void supprimerDepenseControleMarches(EOEditingContext ed, _IDepenseBudget db) throws DepenseControleHorsMarcheException {
		if (db.depenseControleMarches() != null) {
			for (int i = db.depenseControleHorsMarches().count() - 1; i >= 0; i--) {
				getFdchm()._supprimer(ed, db.depenseControleHorsMarches().objectAtIndex(i));
			}
		}
	}

	public _IFactoryDepenseBudget getFdb() {
		return fdb;
	}
}
