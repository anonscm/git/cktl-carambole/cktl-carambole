/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.components.LiquidationExtourneListe;
import org.cocktail.fwkcktldepense.server.finder.FinderDepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOPlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit.ESection;
import org.cocktail.fwkcktldepense.server.service.ConfigurationExtourneService;
import org.cocktail.fwkcktldepenseguiajax.serveur.component.CktlLiquidationExtourneList;
import org.cocktail.fwkcktldepenseguiajax.serveur.component.CktlLiquidationListe;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;

public class LiquidationExtourneListeCtrl {
	public static Logger log = Logger.getLogger(LiquidationExtourneListeCtrl.class.getName());

	private LiquidationExtourneListe view;

	private Map<String, String> filtresMap;
	private List<EODepenseBudget> listeLiquidationsExtourne;
	private WODisplayGroup listeLiquidationsExtourneAsDG;
	private EODepenseBudget depenseBudgetSelectionnee;

	public LiquidationExtourneListeCtrl(LiquidationExtourneListe view) {
		this.view = view;
		this.listeLiquidationsExtourne = Collections.emptyList();
		this.listeLiquidationsExtourneAsDG = new WODisplayGroup();
		this.listeLiquidationsExtourneAsDG.setSelectsFirstObjectAfterFetch(false);
		this.filtresMap = new HashMap<String, String>();
	}

	public void initListeLiquidationsExtourne() {
		this.filtresMap.clear();
		this.filtresMap.put(CktlLiquidationExtourneList.FILTRE_RESTE_DISPO_SUPERIEUR_A, "0");
		chargerListeLiquidatonsExtourne(null);
	}

	public void onSelect() {
		this.depenseBudgetSelectionnee = (EODepenseBudget) listeLiquidationsExtourneAsDG.selectedObject();
	}

	public void appliquerFiltres() {
		// convertir filtres en restriction
		EOQualifier restrictionsAdditionnelles = convertFiltres(this.filtresMap);

		chargerListeLiquidatonsExtourne(restrictionsAdditionnelles);
	}

	private EOQualifier convertFiltres(Map<String, String> filtres) {
		NSArray<EOQualifier> restrictions = new NSMutableArray<EOQualifier>();
		if (filtres.get(CktlLiquidationListe.FILTRE_FOURNISSEUR) != null) {
			restrictions.add(convertFournisseurFiltre(filtres.get(CktlLiquidationListe.FILTRE_FOURNISSEUR)));
		}
		if (filtres.get(CktlLiquidationListe.FILTRE_UB) != null) {
			restrictions.add(convertFiltreUB(filtres.get(CktlLiquidationListe.FILTRE_UB)));
		}
		if (filtres.get(CktlLiquidationListe.FILTRE_PCO) != null) {
			restrictions.add(convertFiltrePCO(filtres.get(CktlLiquidationListe.FILTRE_PCO)));
		}
		if (filtres.get(CktlLiquidationListe.FILTRE_NO_FACTURE) != null) {
			restrictions.add(convertFiltreNoFacture(filtres.get(CktlLiquidationListe.FILTRE_NO_FACTURE)));
		}

		return ERXQ.and(restrictions);
	}

	private EOQualifier convertFournisseurFiltre(String filtreFournisseur) {
		String filtreFournisseurExpression = "*" + filtreFournisseur + "*";
		ERXKey<EOFournisseur> fournisseurKey = EODepenseBudget.DEPENSE_PAPIER.append(EODepensePapier.FOURNISSEUR);
		return ERXQ.or(
				ERXQ.likeInsensitive(fournisseurKey.append(EOFournisseur.FOU_NOM).key(), filtreFournisseurExpression),
				ERXQ.likeInsensitive(fournisseurKey.append(EOFournisseur.FOU_CODE).key(), filtreFournisseurExpression));
	}

	private EOQualifier convertFiltreUB(String filtreUB) {
		return ERXQ.equals(
				EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.ORGAN).append(EOOrgan.ORG_UB).key(),
				filtreUB);
	}

	private EOQualifier convertFiltrePCO(String filtrePCO) {
		String filtrePCOExpression = filtrePCO + "*";
		return ERXQ.like(
				EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES.append(EODepenseControlePlanComptable.PLAN_COMPTABLE).append(EOPlanComptable.PCO_NUM).key(),
				filtrePCOExpression);
	}

	private EOQualifier convertFiltreNoFacture(String filtre) {
		String filtreExpression = "*" + filtre + "*";
		return ERXQ.like(
				EODepenseBudget.DEPENSE_PAPIER.append(EODepensePapier.DPP_NUMERO_FACTURE).key(),
				filtreExpression);
	}

	private EOQualifier convertFiltreResteDispoSuperieurA(String filtre) {
		if (filtre == null) {
			return null;
		}
		BigDecimal resteDispo = null;
		try {
			resteDispo = new BigDecimal(filtre);
			return ERXQ.greaterThan(EODepenseBudget.RESTE_DISPONIBLE_BUDGETAIRE_POUR_EXTOURNE_KEY, resteDispo);
		} catch (Exception e) {
			return null;
		}
	}

	private EOQualifier buildRestrictionsSurMasseCredit() {
		EOQualifier res = null;

		ConfigurationExtourneService confExtourneService = ConfigurationExtourneService.instance();
		EOExercice exerciceCourant = view.exerciceCourant();
		boolean extourneFlecheeTypeCredit1Autorisee = confExtourneService.extourneFlecheeTypeCreditAutorise(
				view.edc(), exerciceCourant, ESection.SECTION_1);
		boolean extourneFlecheeTypeCredit2Autorisee = confExtourneService.extourneFlecheeTypeCreditAutorise(
				view.edc(), exerciceCourant, ESection.SECTION_2);
		boolean extourneFlecheeTypeCredit3Autorisee = confExtourneService.extourneFlecheeTypeCreditAutorise(
				view.edc(), exerciceCourant, ESection.SECTION_3);

		NSMutableArray<EOQualifier> qualsOr = new NSMutableArray<EOQualifier>();
		if (extourneFlecheeTypeCredit1Autorisee) {
			qualsOr.addObject(ERXQ.equals(
					EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.TYPE_CREDIT).append(EOTypeCredit.TCD_SECT).key(),
					ESection.SECTION_1.section()));
		}

		if (extourneFlecheeTypeCredit2Autorisee) {
			qualsOr.addObject(ERXQ.equals(
					EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.TYPE_CREDIT).append(EOTypeCredit.TCD_SECT).key(),
					ESection.SECTION_2.section()));

		}

		if (extourneFlecheeTypeCredit3Autorisee) {
			qualsOr.addObject(ERXQ.equals(
					EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.TYPE_CREDIT).append(EOTypeCredit.TCD_SECT).key(),
					ESection.SECTION_3.section()));
		}

		if (qualsOr.count() == 0) {
			log.warn("Les parametres org.cocktail.gfc.depense.extourne.flechee.autorisee_sur_tcd* ne sont pas définis ou bien sont tous à N.");
			//On filtre sur une valeur impossible pour que le select ne renvoie rien
			res = ERXQ.equals(EODepenseBudget.ENGAGEMENT_BUDGET.append(EOEngagementBudget.TYPE_CREDIT).append(EOTypeCredit.TCD_SECT).key(), null);
		}
		else {
			res = ERXQ.or(qualsOr);
		}
		return res;
	}

	private void chargerListeLiquidatonsExtourne(EOQualifier restrictionsAdditionnelles) {
		// on vide la selection actuelle
		this.depenseBudgetSelectionnee = null;
		this.listeLiquidationsExtourneAsDG.setSelectedObject(null);

		EOQualifier clauseExercice = ERXQ.is(EODepenseBudget.EXERCICE_KEY, view.exerciceCourant());
		EOQualifier restrictions = ERXQ.and(clauseExercice, buildRestrictionsSurMasseCredit(), restrictionsAdditionnelles);

		this.listeLiquidationsExtourne = FinderDepenseBudget.instance().fetchLiquidationsExtourne(
				view.edc(), view.utilisateur(), restrictions, defaultSortLiquidationExtourne());
		NSArray<EODepenseBudget> res = (NSArray<EODepenseBudget>) listeLiquidationsExtourne;
		for (EODepenseBudget obj : res) {
			ERXEOControlUtilities.refaultObject(obj.engagementBudget());
			ERXEOControlUtilities.refaultObject(obj);
		}

		//Appliquer les filtres post fetch
		EOQualifier qualPostFetch = convertFiltreResteDispoSuperieurA(this.filtresMap.get(CktlLiquidationExtourneList.FILTRE_RESTE_DISPO_SUPERIEUR_A));

		this.listeLiquidationsExtourne = EOQualifier.filteredArrayWithQualifier((NSArray) this.listeLiquidationsExtourne, qualPostFetch);

		NSArray<EODepenseBudget> res2 = (NSArray<EODepenseBudget>) listeLiquidationsExtourne;
		if (res2.count() > 100) {
			res2 = res2.subarrayWithRange(new NSRange(0, 100));
			view.session.addSimpleInfoMessage("Information", "Seuls les 100 dernières liquidations trouvées sont affichées. Affinez vos critères de recherche si vous voulez trouver une liquidation particulière.");
		}
		listeLiquidationsExtourne = res2;

		this.listeLiquidationsExtourneAsDG.setObjectArray(res2);
	}

	private static List<EOSortOrdering> defaultSortLiquidationExtourne() {
		List<EOSortOrdering> sortArray = new ArrayList<EOSortOrdering>();
		EOSortOrdering sortDateFacture = EOSortOrdering.sortOrderingWithKey(
				EODepenseBudget.DEPENSE_PAPIER.append(EODepensePapier.DPP_DATE_FACTURE).key(),
				EOSortOrdering.CompareDescending);
		EOSortOrdering sortNomFournisseur = EOSortOrdering.sortOrderingWithKey(
				EODepenseBudget.DEPENSE_PAPIER.append(EODepensePapier.FOURNISSEUR).append(EOFournisseur.FOU_NOM).key(),
				EOSortOrdering.CompareCaseInsensitiveAscending);
		sortArray.add(sortDateFacture);
		sortArray.add(sortNomFournisseur);
		return sortArray;
	}

	// ------------------------------------------
	// getter - setter
	// ------------------------------------------
	public Map<String, String> getFiltresMap() {
		return filtresMap;
	}

	public List<EODepenseBudget> getListeLiquidationsExtourne() {
		return listeLiquidationsExtourne;
	}

	public WODisplayGroup getListeLiquidationsExtourneAsDG() {
		return listeLiquidationsExtourneAsDG;
	}

	public EODepenseBudget getDepenseBudgetSelectionnee() {
		return depenseBudgetSelectionnee;
	}
}
