/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.util.Enumeration;

import org.cocktail.fwkcktldepense.server.finder.FinderCommande;
import org.cocktail.fwkcktldepense.server.finder.FinderExercice;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOTypeEtat;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.util.ZStringUtil;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;

public class AccueilCtrl {

	public static final String RefetchPreCommandesNeededNotification = "RefetchPreCommandesNeededNotification";
	public static final String RefetchPreLiquidationsNeededNotification = "RefetchPreLiquidationsNeededNotification";
	public static final String FetchCommandesNeededNotification = "FetchCommandesNeededNotification";
	public static final String FetchPreCommandesNeededNotification = "FetchPreCommandesNeededNotification";

	public WODisplayGroup dgPreCommandes = null;
	public WODisplayGroup dgCommandes = null;
	public WODisplayGroup dgFactures = null;
	public WODisplayGroup dgEngagements = null;
	public WODisplayGroup dgPreLiquidations = null;
	public WODisplayGroup dgLiquidationsAttente = null;

	private String sessionId = null;

	public NSArray<EOExercice> exercices = null;
	public NSArray<EOExercice> exercicesOuvertsPourDuplicationEnMasse = null;
	public EOExercice exerciceDuJour = null;
	private EOExercice exerciceCourant = null;

	// cache pour les droits
	private Boolean droitLiquidationSansEngagegement;
	private Boolean droitLiquidationSurMandatExtourne;
	private Boolean droitLiquidationSurEnveloppeExtourne;

	public AccueilCtrl() {
		super();
		dgPreCommandes = new WODisplayGroup();
		dgCommandes = new WODisplayGroup();
		dgEngagements = new WODisplayGroup();
		dgFactures = new WODisplayGroup();
		dgPreLiquidations = new WODisplayGroup();
		dgLiquidationsAttente = new WODisplayGroup();
		dgLiquidationsAttente.setSelectsFirstObjectAfterFetch(false);
	}

	public void reset() {

		dgPreCommandes.clearSelection();
		dgPreCommandes.setObjectArray(null);

		dgCommandes.clearSelection();
		dgCommandes.setObjectArray(null);
		dgCommandes.queryBindings().removeAllObjects();

		dgEngagements.clearSelection();
		dgEngagements.setObjectArray(null);
		dgEngagements.queryBindings().removeAllObjects();

		dgFactures.clearSelection();
		dgFactures.setObjectArray(null);
		dgFactures.queryBindings().removeAllObjects();

		dgPreLiquidations.clearSelection();
		dgPreLiquidations.setObjectArray(null);
		dgPreLiquidations.queryBindings().removeAllObjects();

		dgLiquidationsAttente.clearSelection();
		dgLiquidationsAttente.setObjectArray(null);
		dgLiquidationsAttente.queryBindings().removeAllObjects();

	}

	public void resetDgArrays() {
		dgPreCommandes.clearSelection();
		dgPreCommandes.setObjectArray(null);

		dgCommandes.clearSelection();
		dgCommandes.setObjectArray(null);

		dgEngagements.clearSelection();
		dgEngagements.setObjectArray(null);

		dgFactures.clearSelection();
		dgFactures.setObjectArray(null);

		dgPreLiquidations.clearSelection();
		dgPreLiquidations.setObjectArray(null);

		dgLiquidationsAttente.clearSelection();
		dgLiquidationsAttente.setObjectArray(null);
	}

	public void majDroits(EOUtilisateur utilisateur) {
		this.droitLiquidationSansEngagegement = Boolean.valueOf(
				isUtilisateurAvecDroitLiquidationSansEngagement(utilisateur, exerciceCourant));
		this.droitLiquidationSurEnveloppeExtourne = Boolean.valueOf(
				isUtilisateurAvecDroitLiquidationSurEnveloppeExtourne(utilisateur, exerciceCourant));
		this.droitLiquidationSurMandatExtourne = Boolean.valueOf(
				isUtilisateurAvecDroitLiquidationSurMandatExtourne(utilisateur, exerciceCourant));
	}

	public static AccueilCtrl createInstanceAndInitialize(EOUtilisateur utilisateur, String sessionId) {
		AccueilCtrl accueilCtrl = null;
		if (utilisateur != null) {
			accueilCtrl = new AccueilCtrl();
			EOEditingContext edc = utilisateur.editingContext();
			// Recuperation des exercices ouverts a cet utilisateur
			// et determination de l'exerciceCourant a la connexion
			accueilCtrl.exercices = FinderExercice.getExercicesOuvertsPourUtilisateur(edc, utilisateur);
			NSMutableArray<EOExercice> exercicesPourDuplication = new NSMutableArray<EOExercice>();
			Enumeration<EOExercice> enumExercices = accueilCtrl.exercices.objectEnumerator();
			while (enumExercices.hasMoreElements()) {
				EOExercice unExercice = (EOExercice) enumExercices.nextElement();
				if (utilisateur.isUtilisateurEnMasse(unExercice, null)) {
					exercicesPourDuplication.addObject(unExercice);
				}
			}
			accueilCtrl.exercicesOuvertsPourDuplicationEnMasse = exercicesPourDuplication;
			accueilCtrl.exerciceDuJour = FinderExercice.getExercicePourDate(edc, new NSTimestamp());
			accueilCtrl.exerciceCourant = accueilCtrl.exerciceDuJour;

			if (accueilCtrl.exercices != null && accueilCtrl.exercices.count() > 0 &&
					accueilCtrl.exercices.containsObject(accueilCtrl.exerciceCourant) == false) {
				// Si l'exerciceCourant n'est pas ouvert, on selectionne le premier de la liste
				accueilCtrl.exerciceCourant = (EOExercice) accueilCtrl.exercices.lastObject();
			}

			if (accueilCtrl.exercices == null || accueilCtrl.exercices.count() == 0) {
				accueilCtrl.exercices = new NSArray<EOExercice>(accueilCtrl.exerciceDuJour);
			}

			accueilCtrl.initialize(utilisateur, accueilCtrl.exerciceCourant, sessionId);

		}
		return accueilCtrl;
	}

	@SuppressWarnings({
			"unchecked", "rawtypes"
	})
	public void initialize(EOUtilisateur utilisateur, EOExercice exercice, String sessionId) {

		EOExercice oldExercice = (EOExercice) dgPreCommandes.queryBindings().objectForKey(EOCommande.EXERCICE_KEY);
		EOUtilisateur oldUtilisateur = (EOUtilisateur) dgPreCommandes.queryBindings().objectForKey(EOCommande.UTILISATEUR_KEY);

		if (oldExercice == null) {
			dgPreCommandes.queryBindings().setObjectForKey(exercice, EOCommande.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgPreCommandes.queryBindings().setObjectForKey(utilisateur, EOCommande.UTILISATEUR_KEY);
		}

		if (oldExercice == null) {
			dgPreLiquidations.queryBindings().setObjectForKey(exercice, EOCommande.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgPreLiquidations.queryBindings().setObjectForKey(utilisateur, EOCommande.UTILISATEUR_KEY);
		}
		if (oldExercice == null) {
			dgLiquidationsAttente.queryBindings().setObjectForKey(exercice, EOCommande.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgLiquidationsAttente.queryBindings().setObjectForKey(utilisateur, EOCommande.UTILISATEUR_KEY);
		}

		oldExercice = (EOExercice) dgCommandes.queryBindings().objectForKey(EOCommande.EXERCICE_KEY);
		oldUtilisateur = (EOUtilisateur) dgCommandes.queryBindings().objectForKey(EOCommande.UTILISATEUR_KEY);

		if (oldExercice == null) {
			dgCommandes.queryBindings().setObjectForKey(exercice, EOCommande.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgCommandes.queryBindings().setObjectForKey(utilisateur, EOCommande.UTILISATEUR_KEY);
		}

		NSArray oldTypesEtatLibelle = (NSArray) dgCommandes.queryBindings().objectForKey("lesTypesEtat");
		if (oldTypesEtatLibelle == null) {
			// Par defaut, on recupere toutes les commandes dont l'etat est autre que ETAT_ANNULEE
			NSMutableArray<String> typesEtatLibelle = new NSMutableArray<String>();
			typesEtatLibelle.addObject(EOCommande.ETAT_PRECOMMANDE);
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_ENGAGEE);
			typesEtatLibelle.addObject(EOCommande.ETAT_ENGAGEE);
			typesEtatLibelle.addObject(EOCommande.ETAT_PARTIELLEMENT_SOLDEE);
			typesEtatLibelle.addObject(EOCommande.ETAT_SOLDEE);
			// typesEtatLibelle.addObject(EOCommande.ETAT_ANNULEE);
			dgCommandes.queryBindings().setObjectForKey(typesEtatLibelle, "lesTypesEtat");
		}

		oldExercice = (EOExercice) dgEngagements.queryBindings().objectForKey(EOEngagementBudget.EXERCICE_KEY);
		oldUtilisateur = (EOUtilisateur) dgEngagements.queryBindings().objectForKey(EOEngagementBudget.UTILISATEUR_KEY);

		if (oldExercice == null) {
			dgEngagements.queryBindings().setObjectForKey(exercice, EOEngagementBudget.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgEngagements.queryBindings().setObjectForKey(utilisateur, EOEngagementBudget.UTILISATEUR_KEY);
		}

		oldTypesEtatLibelle = (NSArray) dgEngagements.queryBindings().objectForKey("lesTypesEtat");
		if (oldTypesEtatLibelle == null) {
			dgEngagements.queryBindings().setObjectForKey(exercice, EOEngagementBudget.EXERCICE_KEY);
			dgEngagements.queryBindings().setObjectForKey(utilisateur, EOEngagementBudget.UTILISATEUR_KEY);
			// Par defaut, on recupere tous les engagements
			NSMutableArray<String> typesEtatEngagementLibelle = new NSMutableArray<String>();
			typesEtatEngagementLibelle.addObject(EOTypeEtat.ENGAGE);
			typesEtatEngagementLibelle.addObject(EOTypeEtat.SOLDE);
			typesEtatEngagementLibelle.addObject(EOTypeEtat.PART_SOLDE);
			dgEngagements.queryBindings().setObjectForKey(typesEtatEngagementLibelle, "lesTypesEtat");
		}

		oldExercice = (EOExercice) dgFactures.queryBindings().objectForKey(EODepenseBudget.EXERCICE_KEY);
		oldUtilisateur = (EOUtilisateur) dgFactures.queryBindings().objectForKey(EODepenseBudget.UTILISATEUR_KEY);

		if (oldExercice == null) {
			dgFactures.queryBindings().setObjectForKey(exercice, EODepenseBudget.EXERCICE_KEY);
		}
		if (oldUtilisateur == null) {
			dgFactures.queryBindings().setObjectForKey(utilisateur, EODepenseBudget.UTILISATEUR_KEY);
		}

		majDroits(utilisateur);
	}

	@SuppressWarnings("unchecked")
	public void fetchCommandes(NSNotification notification) {
		if (notification != null && notification.object() != null) {
			WOComponent component = (WOComponent) notification.object();
			if (this.sessionId.equals(component.session().sessionID())) {
				NSArray<EOCommande> commandes = null;
				@SuppressWarnings("rawtypes")
				NSMutableDictionary<String, Object> bdgs = new NSMutableDictionary(notification.userInfo());
				if (bdgs == null || bdgs.count() == 0) {
					bdgs = dgCommandes.queryBindings();
					bdgs.removeObjectForKey(EOCommande.COMM_NUMERO_KEY);
				}
				EOUtilisateur utilisateur = (EOUtilisateur) bdgs.objectForKey("utilisateur");
				if (utilisateur != null && utilisateur.editingContext() != null) {
					EOEditingContext edc = utilisateur.editingContext();
					String rechercheGlobale = (String) bdgs.objectForKey("rechercheGlobale");
					if (ZStringUtil.isEmpty(rechercheGlobale)) {
						commandes = FinderCommande.getCommandes(edc, bdgs);
					}
					else {
						commandes = FinderCommande.getCommandesForRechercheGlobale(edc, bdgs, rechercheGlobale);
					}
					dgCommandes.setObjectArray(commandes);
					dgCommandes.queryBindings().setDictionary(bdgs);
					dgCommandes.setCurrentBatchIndex(1);
					dgCommandes.setSelectedObject(null);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void fetchPreCommandes(NSNotification notification) {
		if (notification != null && notification.object() != null) {
			WOComponent component = (WOComponent) notification.object();
			if (this.sessionId.equals(component.session().sessionID())) {
				NSArray<EOCommande> preCommandes = null;
				@SuppressWarnings("rawtypes")
				NSMutableDictionary bdgs = new NSMutableDictionary(notification.userInfo());
				if (bdgs == null || bdgs.count() == 0) {
					bdgs = dgPreCommandes.queryBindings();
					bdgs.removeObjectForKey(EOCommande.COMM_NUMERO_KEY);
				}
				EOUtilisateur utilisateur = (EOUtilisateur) bdgs.objectForKey("utilisateur");
				if (utilisateur != null && utilisateur.editingContext() != null) {
					EOEditingContext edc = utilisateur.editingContext();
					preCommandes = FinderCommande.getPreCommandes(edc, bdgs);
					dgPreCommandes.setObjectArray(preCommandes);
					dgPreCommandes.queryBindings().setDictionary(bdgs);
					dgPreCommandes.setCurrentBatchIndex(1);
					dgPreCommandes.setSelectedObject(null);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void refetchPreCommandes(NSNotification notification) {
		if (notification != null && notification.object() != null) {
			WOComponent component = (WOComponent) notification.object();
			if (this.sessionId.equals(component.session().sessionID())) {
				NSArray<NSDictionary<String, Object>> preCommandes = null;
				@SuppressWarnings("rawtypes")
				NSMutableDictionary bdgs = new NSMutableDictionary(notification.userInfo());
				if (bdgs == null || bdgs.count() == 0) {
					bdgs = dgPreCommandes.queryBindings();
					bdgs.removeObjectForKey(EOCommande.COMM_NUMERO_KEY);
				}
				EOUtilisateur utilisateur = (EOUtilisateur) bdgs.objectForKey("utilisateur");
				if (utilisateur != null && utilisateur.editingContext() != null) {
					EOEditingContext edc = utilisateur.editingContext();
					preCommandes = FinderCommande.getRawRowPreCommandes(edc, bdgs);
					dgPreCommandes.setObjectArray(preCommandes);
					dgPreCommandes.queryBindings().setDictionary(bdgs);
					dgPreCommandes.setCurrentBatchIndex(1);
					dgPreCommandes.setSelectedObject(null);
				}
			}
		}
	}

	private boolean isUtilisateurAvecDroitLiquidationSansEngagement(EOUtilisateur utilisateur, EOExercice exercice) {
		boolean isUtilisateurAvecDroitLiquidationSansEngagement = false;
		if (utilisateur != null && utilisateur.isDroitLiquiderSansEngagement(exercice)) {
			isUtilisateurAvecDroitLiquidationSansEngagement = true;
		}
		return isUtilisateurAvecDroitLiquidationSansEngagement;
	}

	private boolean isUtilisateurAvecDroitLiquidationSurMandatExtourne(EOUtilisateur utilisateur, EOExercice exercice) {
		boolean isUtilisateurAvecDroitLiquidationSurMandatExtourne = false;
		if (utilisateur != null && utilisateur.isDroitLiquiderSurMandatExtourne(exercice)) {
			isUtilisateurAvecDroitLiquidationSurMandatExtourne = true;
		}
		return isUtilisateurAvecDroitLiquidationSurMandatExtourne;
	}

	private boolean isUtilisateurAvecDroitLiquidationSurEnveloppeExtourne(EOUtilisateur utilisateur, EOExercice exercice) {
		boolean isUtilisateurAvecDroitLiquidationSurEnveloppeExtourne = false;
		if (utilisateur != null && utilisateur.isDroitLiquiderSurEnveloppeExtourne(exercice)) {
			isUtilisateurAvecDroitLiquidationSurEnveloppeExtourne = true;
		}
		return isUtilisateurAvecDroitLiquidationSurEnveloppeExtourne;
	}

	public Boolean getDroitLiquidationSansEngagegement() {
		return droitLiquidationSansEngagegement;
	}

	public Boolean getDroitLiquidationSurMandatExtourne() {
		return droitLiquidationSurMandatExtourne;
	}

	public Boolean getDroitLiquidationSurEnveloppeExtourne() {
		return droitLiquidationSurEnveloppeExtourne;
	}

	public WODisplayGroup getDgCommandes() {
		return dgCommandes;
	}

	public void setDgCommandes(WODisplayGroup dgCommandes) {
		this.dgCommandes = dgCommandes;
	}

	public WODisplayGroup getDgEngagements() {
		return dgEngagements;
	}

	public void setDgEngagements(WODisplayGroup dgEngagements) {
		this.dgEngagements = dgEngagements;
	}

	public WODisplayGroup getDgPreCommandes() {
		return dgPreCommandes;
	}

	public void setDgPreCommandes(WODisplayGroup dgPreCommandes) {
		this.dgPreCommandes = dgPreCommandes;
	}

	public WODisplayGroup getDgFactures() {
		return dgFactures;
	}

	public void setDgFactures(WODisplayGroup dgFactures) {
		this.dgFactures = dgFactures;
	}

	public NSArray<EOExercice> exercices() {
		return exercices;
	}

	public void setExercices(NSArray<EOExercice> exercices) {
		this.exercices = exercices;
	}

	public EOExercice getExerciceDuJour() {
		return exerciceDuJour;
	}

	public void setExerciceDuJour(EOExercice exerciceDuJour) {
		this.exerciceDuJour = exerciceDuJour;
	}

	public EOExercice getExerciceCourant() {
		return exerciceCourant;
	}

	public void setExerciceCourant(EOExercice exerciceCourant) {
		this.exerciceCourant = exerciceCourant;
	}

	public WODisplayGroup getDgPreLiquidations() {
		return dgPreLiquidations;
	}

	public void setDgPreLiquidations(WODisplayGroup dgPreLiquidations) {
		this.dgPreLiquidations = dgPreLiquidations;
	}

	public WODisplayGroup getDgLiquidationsAttente() {
		return dgLiquidationsAttente;
	}

	public void setDgLiquidationsAttente(WODisplayGroup dgLiquidationsAttente) {
		this.dgLiquidationsAttente = dgLiquidationsAttente;
	}

}
