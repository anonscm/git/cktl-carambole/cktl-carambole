/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.carambole.server.filtre.EngagementFiltreConverter;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltreBean;
import org.cocktail.fwkcktldepense.server.filtre.CktlDepenseFiltresManager;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOTypeApplication;
import org.cocktail.fwkcktldepenseguiajax.serveur.component.ACktlDepenseGuiAjaxComponent;
import org.cocktail.fwkcktldepenseguiajax.serveur.controllers.ACktlDepenseGuiAjaxComponentCtrl;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public class LiquidationEngagementsCtrl extends ACktlDepenseGuiAjaxComponentCtrl implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum FiltresEngagements {
		UB, PCO, COMMANDE
	}

	public static final String FILTRE_UB_LABEL = "UB : ";
	public static final String FILTRE_PCO_LABEL = "N° Compte : ";
	public static final String FILTRE_COMMANDE_LABEL = "N° Commande : ";

	private static final CktlDepenseFiltreBean FILTRE_UB =
			new CktlDepenseFiltreBean(FiltresEngagements.UB.toString(), FILTRE_UB_LABEL);
	private static final CktlDepenseFiltreBean FILTRE_PCO =
			new CktlDepenseFiltreBean(FiltresEngagements.PCO.toString(), FILTRE_PCO_LABEL);
	private static final CktlDepenseFiltreBean FILTRE_COMMANDE =
			new CktlDepenseFiltreBean(FiltresEngagements.COMMANDE.toString(), FILTRE_COMMANDE_LABEL);

	private CktlDepenseFiltresManager filtresManager;
	private EngagementFiltreConverter filtresConverter;
	private List<EOEngagementBudget> engagements;
	private WODisplayGroup engagementsAsDG;

	private EOEngagementBudget engagementSelectionne;

	/**
	 * Constructeur par défaut.
	 */
	public LiquidationEngagementsCtrl(ACktlDepenseGuiAjaxComponent componentView) {
		super(componentView);
		engagementsAsDG = new WODisplayGroup();
		this.filtresManager = new CktlDepenseFiltresManager();
		this.filtresConverter = new EngagementFiltreConverter();
		this.engagements = new ArrayList<EOEngagementBudget>();

		initFiltres();
	}

	public CktlDepenseFiltresManager getFiltresManager() {
		return filtresManager;
	}

	public List<EOEngagementBudget> getEngagements() {
		return engagements;
	}

	public WODisplayGroup getEngagementsAsDG() {
		return engagementsAsDG;
	}

	public void filtrer(EOEditingContext editingContext, EOExercice exerciceCourant, EOFournisseur fournisseur) throws Exception {
		if (exerciceCourant == null) {
			throw new Exception("L'exercice doit être renseigné");
		}
		if (fournisseur == null) {
			throw new Exception("Le fournisseur doit être renseigné");
		}

		loadData(editingContext, exerciceCourant, fournisseur);
	}

	public void loadData(EOEditingContext editingContext, EOExercice exerciceCourant, EOFournisseur fournisseur) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

		quals.add(buildFournisseurRestriction(fournisseur));
		quals.add(buildExerciceRestriction(exerciceCourant));
		quals.add(buildEngagementsCaramboleRestriction());
		quals.add(buildEngagementsNonSoldesRestriction());
		//TODO liquidables par l'utilisateur

		quals.addAll(getFiltresManager().convert(filtresConverter));

		engagements = EOEngagementBudget.fetchAll(editingContext, ERXQ.and(quals));
		engagementsAsDG = transformToDisplayGroup(engagements);
	}

	private EOQualifier buildExerciceRestriction(EOExercice exercice) {
		return ERXQ.equals(EOEngagementBudget.EXERCICE_KEY, exercice);
	}

	private EOQualifier buildFournisseurRestriction(EOFournisseur fournisseur) {
		return ERXQ.equals(EOEngagementBudget.FOURNISSEUR_KEY, fournisseur);
	}

	private EOQualifier buildEngagementsNonSoldesRestriction() {
		return ERXQ.greaterThan(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE_KEY, BigDecimal.ZERO);
	}

	private EOQualifier buildEngagementsCaramboleRestriction() {
		return ERXQ.equals(
				EOEngagementBudget.TYPE_APPLICATION.append(EOTypeApplication.TYAP_STRID).key(),
				EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
	}

	private WODisplayGroup transformToDisplayGroup(List<EOEngagementBudget> engagements) {
		//WODisplayGroup engagementsAsDG = new WODisplayGroup();
		engagementsAsDG.setSelectsFirstObjectAfterFetch(false);
		engagementsAsDG.setObjectArray(new NSArray<EOEngagementBudget>(engagements));
		return engagementsAsDG;
	}

	private void initFiltres() {
		filtresManager.add(FILTRE_UB);
		filtresManager.add(FILTRE_PCO);
		filtresManager.add(FILTRE_COMMANDE);
	}

	public void onSelect() {
		this.engagementSelectionne = (EOEngagementBudget) this.engagementsAsDG.selectedObject();
	}

	public EOEngagementBudget getEngagementSelectionne() {
		return engagementSelectionne;
	}

	public void setEngagementSelectionne(EOEngagementBudget engagementSelectionne) {
		this.engagementSelectionne = engagementSelectionne;
	}

	public EngagementFiltreConverter getFiltresConverter() {
		return filtresConverter;
	}

	public void setFiltresConverter(EngagementFiltreConverter filtresConverter) {
		this.filtresConverter = filtresConverter;
	}

	public void reset() {
		getEngagementsAsDG().setObjectArray(NSArray.emptyArray());
	}

	public Boolean isLiquidable() {
		return getEngagementSelectionne() != null;
	}
}
