/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import org.cocktail.carambole.server.MyAjaxComponent;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.process.ProcessDossierLiquidation;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class LiquidationDossierCreationCtrl {
	private MyAjaxComponent component;

	public LiquidationDossierCreationCtrl(MyAjaxComponent comp) {
		component = comp;
	}

	/**
	 * Cree les dossiers de liquidation et renvoie un NSArray rempli avec les borId.
	 * 
	 * @param edc
	 * @param dicos
	 * @param utilisateur
	 * @param exercice
	 * @return
	 */
	public NSArray<Integer> creerDossiers(EOEditingContext edc, NSArray<NSDictionary<String, Object>> dicos, EOUtilisateur utilisateur, EOExercice exercice) {
		NSMutableArray<Integer> borIds = new NSMutableArray<Integer>();
		@SuppressWarnings("unchecked")
		NSArray<String> gesCodes = (NSArray<String>) dicos.valueForKey("ORG_UB");
		gesCodes = NSArrayCtrl.getDistinctsOfNSArray(gesCodes);
		Integer borId;
		//extraire les gescodes pour determiner chaque dossier
		for (int i = 0; i < gesCodes.count(); i++) {
			String gesCode = gesCodes.objectAtIndex(i);
			//Filtrer 
			EOQualifier qual = new EOKeyValueQualifier("ORG_UB", EOQualifier.QualifierOperatorEqual, gesCode);
			NSArray<NSDictionary<String, Object>> dicosFiltrees = EOQualifier.filteredArrayWithQualifier(dicos, qual);
			NSArray<EODepenseControlePlanComptable> dpcos = convertRawDepensesToDpcos(edc, dicosFiltrees);

			borId = ProcessDossierLiquidation.enregistrer(component.session.dataBus(), edc, dpcos, utilisateur, exercice, gesCode);
			borIds.addObject(borId);
		}
		return borIds.immutableClone();
	}

	private NSArray<EODepenseControlePlanComptable> convertRawDepensesToDpcos(EOEditingContext edc, NSArray<NSDictionary<String, Object>> dicos) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (dicos.count() > 0) {
			for (int i = 0; i < dicos.count(); i++) {
				NSDictionary<String, Object> dico = dicos.objectAtIndex(i);
				Number dpcoId = (Number) dico.valueForKey(EODepenseControlePlanComptable.DPCO_ID_COLKEY);
				quals.addObject(new EOKeyValueQualifier(EODepenseControlePlanComptable.DPCO_ID_KEY, EOQualifier.QualifierOperatorEqual, dpcoId));
			}
			NSArray<EODepenseControlePlanComptable> res = EODepenseControlePlanComptable.fetchAll(edc, new EOOrQualifier(quals), null);
			return res;
		}
		return NSArray.emptyArray();

	}

}
