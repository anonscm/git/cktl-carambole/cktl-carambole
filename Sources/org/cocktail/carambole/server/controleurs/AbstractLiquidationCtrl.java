/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.util.ArrayList;
import java.util.Iterator;

import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOUtilisateur;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public abstract class AbstractLiquidationCtrl implements _ILiquidationCtrl {
	private ArrayList<EOEngagementBudget> engagementLiquidables = new ArrayList<EOEngagementBudget>();

	private EODepensePapier dpp;

	public AbstractLiquidationCtrl(EODepensePapier dpp) {
		this.dpp = dpp;
	}

	public ArrayList<EOEngagementBudget> getEngagementLiquidables() {
		return engagementLiquidables;
	}

	public void resetEngagementLiquidables(EOEditingContext ed, EOUtilisateur utilisateur) {
		Iterator<? extends _IDepenseBudget> it = getLesDepenseBudgets().iterator();
		while (it.hasNext()) {
			_IDepenseBudget iDepenseBudget = (_IDepenseBudget) it.next();
			EOEngagementBudget engagementBudget = iDepenseBudget.engagementBudget();
			if (engagementBudget != null && engagementBudget.isLiquidable(ed, utilisateur, null)) {
				getEngagementLiquidables().add(engagementBudget);
			}
		}
	}

	public Boolean isEngagementLiquidable(EOEngagementBudget engagementBudget) {
		return ((engagementBudget != null) && getEngagementLiquidables().contains(engagementBudget));
	}

	public NSArray<? extends _IDepenseBudget> getLesDepenseBudgets() {
		return dpp.currentDepenseBudgets();
	}

	public NSArray<? extends _IDepenseBudget> getLesDepenseBudgetsHorsExtourne() {
		return dpp.currentDepenseBudgetsHorsExtourne();
	}

}
