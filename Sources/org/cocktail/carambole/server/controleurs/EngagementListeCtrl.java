/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server.controleurs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOFournisseur;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepenseguiajax.serveur.beans.EngagementFiltre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public class EngagementListeCtrl {

	public static final String KEY_FILTRE_FOURNISSEUR = EOEngagementBudget.FOURNISSEUR.append(EOFournisseur.FOU_NOM).key();
	public static final String KEY_FILTRE_UB = EOEngagementBudget.ORGAN.append(EOOrgan.ORG_UB).key();
	public static final String FILTRE_UB = "ub";
	public static final String FILTRE_FOURNISSEUR = "fournisseur";
	public static final Map<String, String> FILTRES_PATH_MAP = new HashMap<String, String>();
	static {
		FILTRES_PATH_MAP.put(FILTRE_UB, KEY_FILTRE_UB);
		FILTRES_PATH_MAP.put(FILTRE_FOURNISSEUR, KEY_FILTRE_FOURNISSEUR);
	}

	private EOEditingContext edc;
	private EngagementService engagementService;
	private List<EngagementFiltre> filtres = new ArrayList<EngagementFiltre>();


	public EngagementListeCtrl(EOEditingContext ec) {
		this.edc = ec;
		this.engagementService = new EngagementService();
	}

    public void setEngagementService(EngagementService engagementService) {
        this.engagementService = engagementService;
    }

    public List<EOEngagementBudget> getEngagementsAsList() throws Exception {
		return fetchAll();
	}

	protected List<EOEngagementBudget> fetchAll() {
		 return EOEngagementBudget.fetchAll(getEditingContext(), getQualifier(), getSortOrderings());
	}

	public EOQualifier getQualifier() {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		for (EngagementFiltre filtreCourant : filtres) {
			quals.add(engagementService.getClause(filtreCourant.getKey(), filtreCourant.getValue()));
		}

		if (quals.count() > 0) {
			return ERXQ.and(quals);
		}
		return null;
	}

	protected NSArray<EOSortOrdering> getSortOrderings() {
		return null;
	}

	private EOEditingContext getEditingContext() {
		return edc;
	}

	public void addFiltre(EngagementFiltre filtre) {
		filtres.add(filtre);
	}
}
