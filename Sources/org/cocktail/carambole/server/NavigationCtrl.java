/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.carambole.server;

import org.apache.log4j.Logger;
import org.cocktail.carambole.server.components.Accueil;
import org.cocktail.carambole.server.components.BonDeCommande2;
import org.cocktail.carambole.server.components.Liquidation2;
import org.cocktail.carambole.server.components.LiquidationSansEngagement;
import org.cocktail.carambole.server.components.LiquidationSurExtourneEnveloppe;
import org.cocktail.carambole.server.components.LiquidationSurExtourneMandat;
import org.cocktail.fwkcktldepense.server.metier.EOCommande;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepensePapier;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;

public class NavigationCtrl {
	public final static Logger logger = Logger.getLogger(NavigationCtrl.class);

	public static BonDeCommande2 goToBonDeCommande(WOContext context, EOCommande commande, EOExercice exercice, String selectedTab) {
		BonDeCommande2 nextPage = (BonDeCommande2) WOApplication.application().pageWithName(BonDeCommande2.class.getName(), context);
		nextPage.setLaCommande(commande);
		nextPage.setExerciceSelectionne(exercice);
		nextPage.setSelectedTab(selectedTab);
		return nextPage;
	}

	public static BonDeCommande2 goToSavedBonDeCommande(CktlWebSession session, EOCommande commande, EOExercice exercice, String selectedTab) {
		BonDeCommande2 nextPage = (BonDeCommande2) session.getSavedPageWithName(BonDeCommande2.class.getName());
		nextPage.setLaCommande(commande);
		nextPage.setExerciceSelectionne(exercice);
		nextPage.setSelectedTab(selectedTab);
		return nextPage;
	}

	public static Liquidation2 goToLiquidation(WOContext context, EODepensePapier laDepensePapier) {
		Liquidation2 nextPage = (Liquidation2) WOApplication.application().pageWithName(Liquidation2.class.getName(), context);
		((Session) context.session()).setLaDepensePapierEnCours(laDepensePapier);
		nextPage.setLaDepensePapier(laDepensePapier);
		return nextPage;
	}

	public static LiquidationSansEngagement goToLiquidationSansEngagement(WOContext context, EODepensePapier laDepensePapier) {
		LiquidationSansEngagement nextPage = (LiquidationSansEngagement) WOApplication.application().pageWithName(LiquidationSansEngagement.class.getName(), context);
		((Session) context.session()).setLaDepensePapierEnCours(laDepensePapier);
		nextPage.setLaDepensePapier(laDepensePapier);
		return nextPage;
	}

	public static LiquidationSurExtourneEnveloppe goToLiquidationSurExtourneEnveloppe(WOContext context, EODepensePapier laDepensePapier) {
		LiquidationSurExtourneEnveloppe nextPage = (LiquidationSurExtourneEnveloppe) WOApplication.application().pageWithName(LiquidationSurExtourneEnveloppe.class.getName(), context);

		((Session) context.session()).setLaDepensePapierEnCours(laDepensePapier);
		nextPage.setLaDepensePapier(laDepensePapier);
		return nextPage;
	}

	/**
	 * @param context
	 * @param laDepensePapier La nouvelle liquidation
	 * @param dbExtourneInitiale la liquidation d'extourne
	 * @return
	 */
	public static LiquidationSurExtourneMandat goToLiquidationSurExtourneMandat(WOContext context, EODepensePapier laDepensePapier, EODepenseBudget dbExtourneInitiale) {
		LiquidationSurExtourneMandat nextPage = (LiquidationSurExtourneMandat) WOApplication.application().pageWithName(LiquidationSurExtourneMandat.class.getName(), context);

		((Session) context.session()).setLaDepensePapierEnCours(laDepensePapier);
		nextPage.setLaLiquidationExtourne(dbExtourneInitiale);
		nextPage.setLaDepensePapier(laDepensePapier);
		return nextPage;
	}

	public static Accueil goToAccueil(WOContext context) {
		if (logger.isDebugEnabled()) {
			logger.debug("Retour à l'accueil (debug)");
		}
		Accueil nextPage = (Accueil) WOApplication.application().pageWithName(Accueil.class.getName(), context);
		nextPage.reset();
		return nextPage;
	}

}
