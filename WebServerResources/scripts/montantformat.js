function formatterMontant(textfield) {
	var mmontant = textfield.value;
	if (mmontant.length >0) {
	  if (IsMontant(textfield.value)) {
		textfield.value=mmontant.replace('.',',');
		return (true);
	  } else {
		alert("Seuls les chiffres, le point decimal et la virgule sont autorises.");
		textfield.value="";
		return (false);
	  }
	}
	return (true);
}

function IsMontant(x) {
	var checkOK = "0123456789.,";
	var checkStr = x;
	var allValid = true;
	var decPoints = 0;
	var allNum = "";
	for (i = 0; i < checkStr.length; i++) {
		ch = checkStr.charAt(i);
		for (j = 0; j < checkOK.length; j++)
			if (ch == checkOK.charAt(j)) break;
			if (j == checkOK.length) {
				allValid = false;
				break;
			}
		allNum += ch;
	}
	if (!allValid) {
		return (false);
	}
	return (true);
}


function confirmIfMontant0(textfield, prefixMsg) {
    var chaineMontant = textfield.value;
    var res = false;
	if (chaineMontant.length >0 ) {
	    var currentMontant = parseFloat(chaineMontant);
	    if (currentMontant != null && currentMontant==0   ) {
		res = (confirm(prefixMsg + ' : Le montant saisi est nul, confirmez-vous ? ' )) ;
	    }
	    else {
		res = true;
	    }
	}
	else {
	    res = true;
	}
	if (!res) {
	    textfield.value = "";
	}
	return res;
}


