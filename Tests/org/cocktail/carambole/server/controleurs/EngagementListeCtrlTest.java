package org.cocktail.carambole.server.controleurs;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktldepenseguiajax.serveur.beans.EngagementFiltre;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.wounit.rules.MockEditingContext;

public class EngagementListeCtrlTest {
	@Rule
	public MockEditingContext ec = new MockEditingContext("carambole");

	@Before
	public void setUp() throws Exception {

		//		EOEngagementBudget obj = EOEngagementBudget.creerInstance(ec);
		//		ec.insertSavedObject(obj);
		//EOEngagementBudget obj = ec.createSavedObject(EOEngagementBudget.class);
		//ec.saveChanges()
		//		EOEngagementBudget obj = EOEngagementBudget.creerInstance(ec);
		//		obj.setEngMontantBudgetaireReste(BigDecimal.valueOf(0d));
		//		ec.saveChanges();
	}

	//TODO objet responsable de la récupération des filtres et leur transmission au moteur de fetch
	// et de récupération de la liste filtrée et de la transmission de cette liste à la vue.

	//TODO tester récupération des filtres

//	@Test
//	public void testGetFiltres() {
//		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
//		assertNotNull("Les filtres ne doivent jamais être nuls", ctrl.getFiltres());
//	}

	//TODO tester transmission des filtres au moteur de fetch
//	@Test
//	public void testEnvoiMoteurFetch() throws Exception {
//		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
//		assertNotNull("Le résultat du fetch ne doit jamais être nul", ctrl.getEngagementsAsList());
//
//		ctrl.getFiltres().clear();
//		ctrl.getFiltres().put("Toto", "Titi");
//
//	}

//	@Test(expected = EngagementFiltreException.class)
//	public void testEnvoiMoteurFetchException() throws Exception {
//		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
//		ctrl.getFiltres().clear();
//		ctrl.getFiltres().put("toto", "Titi");
//		ctrl.getEngagementsAsList();
//		fail("Une exception devrait etre lancée");
//	}

    @Test
    public void testQualifierNoFiltre() {
        EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);

        assertNull("Le qualifier doit etre nul quand il n'y à aucun filtres.", ctrl.getQualifier());

    }

    @Test
	public void testQualifierUnFiltre() {
		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
		EngagementService service = getMockService();
		ctrl.setEngagementService(service);

		EngagementFiltre filtre = new EngagementFiltre(EngagementListeCtrl.FILTRES_PATH_MAP.get(EngagementListeCtrl.FILTRE_UB), "UB");
		ctrl.addFiltre(filtre);

		EOQualifier qualifier = ctrl.getQualifier();
        assertNotNull("Le qualifier ne doit pas etre nul quand il y a au moins un filtre dont la clé est connue.",
                        qualifier);

		verify(service).getClause(EngagementListeCtrl.KEY_FILTRE_UB, "UB");
	}

	@Test
	public void testQualifierDeuxFiltres() {
		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
		EngagementService service = getMockService();
		ctrl.setEngagementService(service);

		EngagementFiltre filtre1 = 
		                new EngagementFiltre(
		                                EngagementListeCtrl.FILTRES_PATH_MAP.get(
		                                                EngagementListeCtrl.FILTRE_FOURNISSEUR), "Fournisseur");
        EngagementFiltre filtre2 = 
                        new EngagementFiltre(
                                        EngagementListeCtrl.FILTRES_PATH_MAP.get(EngagementListeCtrl.FILTRE_UB), "UB");
        ctrl.addFiltre(filtre1);
        ctrl.addFiltre(filtre2);
		
        EOQualifier qualifier = ctrl.getQualifier();
        assertNotNull("Le qualifier ne doit pas etre nul quand il y a au moins un filtre dont la clé est connue.",
                        qualifier);

		verify(service).getClause(EngagementListeCtrl.KEY_FILTRE_FOURNISSEUR, "Fournisseur");
        verify(service).getClause(EngagementListeCtrl.KEY_FILTRE_UB, "UB");
	}

	private EngagementService getMockService() {
		EOQualifier qualifier = mock(EOQualifier.class);
		EngagementService service = mock(EngagementService.class);
		when(service.getClause(anyString(), anyString())).thenReturn(qualifier);
		return service;
	}

	//TODO Tester le sortOrdering
	//@Test
	public void testSortOrderings() {
		EngagementListeCtrl ctrl = new EngagementListeCtrl(ec);
		assertNotNull("Le controleur doit retourner un tri", ctrl.getSortOrderings());
	}

	//TODO PB : Externalisr les "classes" propres a WebObjects (NSArray, EOQualifier,, SortOrderings)

	//TODO tester la récupération de la liste filtrée

	//TODO tester transmission à la vue

}
