package org.cocktail.carambole.server.utilitaires;

import static org.junit.Assert.*;

import java.util.Hashtable;
import java.util.Map;

import org.cocktail.carambole.server.utilitaires.UtilString;
import org.junit.Test;

public class UtilStringTest {

    @Test
    public void replaceStringByAnother() {
        
        assertEquals("jeremplacemonnomparcela", UtilString.replaceStringByAnother("jeremplacemonnomamoi", "amoi", "parcela"));
        assertEquals("jeremplacemonprenomamoi", UtilString.replaceStringByAnother("jeremplacemonnomamoi", "nom", "prenom"));
        assertEquals("rienaremplacer", UtilString.replaceStringByAnother("rienaremplacer", "toto", "titi"));
        assertEquals("plusieursplusieurs", UtilString.replaceStringByAnother("plusrienplusrien", "rien", "ieurs"));
        assertEquals("plusplus", UtilString.replaceStringByAnother("plusrienplusrien", "rien", ""));

        
    }
    
    @Test
    public void replaceWithValuesFromMap() {
        Map<String, Object> table = new Hashtable<String, Object>();
        table.put("nom", "XIV");
        table.put("prenom", "Louis");
        
        assertEquals("Bonjour XIV Louis!", UtilString.replaceWithValuesFromMap("Bonjour %nom% %prenom%!", table));
        assertEquals("Bonjour XIV!", UtilString.replaceWithValuesFromMap("Bonjour %nom%!", table));
        assertEquals("Bonjour Louis!", UtilString.replaceWithValuesFromMap("Bonjour %prenom%!", table));
        assertEquals("Bonjour %inconnu%!", UtilString.replaceWithValuesFromMap("Bonjour %inconnu%!", table));
    }

}
