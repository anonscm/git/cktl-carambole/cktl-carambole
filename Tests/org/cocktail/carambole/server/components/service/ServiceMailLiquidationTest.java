/**
 * 
 */
package org.cocktail.carambole.server.components.service;

import static org.junit.Assert.*;

import java.util.List;

import org.cocktail.carambole.server.components.service.ServiceMailLiquidation.AdresseMail;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDestinatairesListeSelector;
import org.junit.Test;

/**
 * @author Raymond NANEON <raymond.naneon at utt.fr> 5 nov. 2014
 *
 */
public class ServiceMailLiquidationTest {

	@Test
	public void getAdressesMailForDemandeSFAvecMailParametre() {
		ServiceMailLiquidation composant = new ServiceMailLiquidation() {
			@Override
			public boolean isMailServiceFacturierParametre() {
				return true;
			}
			@Override
			protected void setMailAServiceEnChargeServiceFacturier(List<AdresseMail> adresses) {
				adresses.add(new AdresseMail(CktlAjaxDestinatairesListeSelector.DestinataireListeElt.DESTINATAIRE_OPTION_TO,"destinataire@gmail.com"));
			}
		};
		
		List<AdresseMail> adresses = composant.getAdressesMailForDemandeSF();
		assertEquals(1, adresses.size());
		assertEquals("destinataire@gmail.com",adresses.get(0).getAdresse());
	}

}
