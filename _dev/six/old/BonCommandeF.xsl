<!DOCTYPE xsl[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>
<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version = "1.0" xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" xmlns:fo = "http://www.w3.org/1999/XSL/Format">
  <!-- TPL messagefournisseur -->
  <xsl:template name = "messagefournisseur" match = "messagefournisseur">
    <xsl:if test = ".!=''">
      <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
        <fo:table-column column-width = "proportional-column-width(1)" />
        <fo:table-column column-width = "160mm" />
        <fo:table-column column-width = "proportional-column-width(1)" />
        <fo:table-body font-size = "14pt" font-weight = "bold">
          <fo:table-row>
            <fo:table-cell />
            <fo:table-cell padding = "3pt" text-align = "center" background-color = "#EEEEEE" overflow = "hidden">
              <fo:block> <xsl:value-of select = "." /> </fo:block>
            </fo:table-cell>
            <fo:table-cell />
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </xsl:if>
  </xsl:template>
  <!-- TPL les articles -->
  <xsl:template name = "articlesF" match = "articles" mode = "boncommandeF">
    <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" border-width = "0.5pt" border-style = "solid">
      <fo:table-column column-width = "90mm" border = "0.5pt solid" />
      <fo:table-column column-width = "34mm" border = "0.5pt solid" />
      <fo:table-column column-width = "10mm" border = "0.5pt solid" />
      <fo:table-column column-width = "23mm" border = "0.5pt solid" />
      <fo:table-column column-width = "26mm" border = "0.5pt solid" />
      <fo:table-header font-weight = "bold">
        <fo:table-row>
          <fo:table-cell border = "0.5pt solid" padding = "3pt" text-align = "center">
            <fo:block>D&amp;eacute;signation des Travaux ou Fournitures</fo:block>
          </fo:table-cell>
          <fo:table-cell border = "0.5pt solid" padding = "3pt" text-align = "center">
            <fo:block>R&amp;eacute;f&amp;eacute;rence</fo:block>
          </fo:table-cell>
          <fo:table-cell border = "0.5pt solid" padding = "3pt" text-align = "center">
            <fo:block>Qt&amp;eacute;</fo:block>
          </fo:table-cell>
          <fo:table-cell border = "0.5pt solid" padding = "3pt" text-align = "center">
            <fo:block>Prix Unitaire</fo:block>
          </fo:table-cell>
          <fo:table-cell border = "0.5pt solid" padding = "3pt" text-align = "center">
            <fo:block>Montant</fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-header>
      <fo:table-body font-size = "8pt"> <xsl:apply-templates select = "article" mode = "boncommandeF" /> </fo:table-body>
    </fo:table>
  </xsl:template>
  <!-- les engagements -->
  <xsl:template name = "engagementsF" match = "engagements" mode = "boncommandeF"> <xsl:apply-templates /> </xsl:template>
  <!-- un engagement -->
  <xsl:template name = "engagementF" match = "engagement" mode = "boncommandeF"> <xsl:apply-templates /> </xsl:template>
  <!-- TPL Bon de commande -->
  <xsl:template name = "boncommandeF" match = "commande" mode = "boncommandeF">
    <fo:static-content flow-name = "xsl-region-end" />
    <fo:static-content flow-name = "xsl-region-after">
      <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" padding = "0pt">
        <fo:table-column column-width = "100%" />
        <fo:table-body>
          <fo:table-row>
            <fo:table-cell display-align = "after">
              <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                <fo:table-column column-width = "63mm" />
                <fo:table-column />
                <fo:table-column column-width = "119mm" />
                <fo:table-body>
                  <fo:table-row keep-with-next = "always">
                    <!-- cadre livraison + observations -->
                    <fo:table-cell>
                      <xsl:apply-templates select = "livraison" />
                      <xsl:if test = "marche != ' ' and marche != ''"> <xsl:apply-templates select = "marche" /> </xsl:if>
                      <xsl:apply-templates select = "observations" />
                    </fo:table-cell>
                    <!-- /cadre livraison + observations -->
                    <!-- colonne vide -->
                    <fo:table-cell />
                    <!-- /colonne vide -->
                    <fo:table-cell display-align = "before" border = "0pt solid">
                      <!-- totaux +blabla -->
                      <fo:table inline-progression-dimension = "119mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                        <fo:table-column />
                        <fo:table-body>
                          <fo:table-row keep-with-next = "always">
                            <fo:table-cell display-align = "before">
                              <fo:table>
                                <fo:table-column />
                                <fo:table-body>
                                  <fo:table-row keep-with-next = "always">
                                    <fo:table-cell padding = "5pt" display-align = "before" text-align = "right">
                                      <!-- blabla -->
                                      <xsl:call-template name = "lieudate">
                                        <xsl:with-param name = "lieuimpression" select = "lieuimpression" />
                                        <xsl:with-param name = "dateimpression" select = "dateimpression" />
                                      </xsl:call-template>
                                    </fo:table-cell>
                                  </fo:table-row>
                                  <fo:table-row keep-with-next = "always">
                                    <fo:table-cell padding = "5pt" display-align = "after">
                                      <fo:table table-layout = "fixed" space-before = "0pt">
                                        <fo:table-column />
                                        <fo:table-column column-width = "38mm" />
                                        <fo:table-body>
                                          <fo:table-row keep-with-next = "always">
                                            <fo:table-cell padding = "5pt" display-align = "before">
                                              <fo:block font-weight = "bold">L'ordonnateur ou ses d&amp;eacute;l&amp;eacute;gu&amp;eacute;s</fo:block>
                                              <xsl:apply-templates select = "signataires" />
                                            </fo:table-cell>
                                            <fo:table-cell display-align = "after" text-align = "center">
                                              <fo:block font-size = "7pt" space-before = "80pt">
                                                <xsl:call-template name = "write-icone-destination"><xsl:with-param name = "destination" select = "'F'" /></xsl:call-template>
                                              </fo:block>
                                              <fo:block font-size = "7pt" space-before = "2pt">Commande r&amp;eacute;alis&amp;eacute;e dans le cadre des march&amp;eacute;s publics&amp;br;Bon destin&amp;eacute;&amp;br;au Fournisseur</fo:block>
                                            </fo:table-cell>
                                          </fo:table-row>
                                        </fo:table-body>
                                      </fo:table>
                                    </fo:table-cell>
                                  </fo:table-row>
                                </fo:table-body>
                              </fo:table>
                              <!-- /blabla -->
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <!-- totaux+blabla -->
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
      <xsl:apply-templates select = "infossuivi" />
    </fo:static-content>
    <fo:static-content flow-name = "xsl-region-before">
      <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" padding = "0pt">
        <fo:table-column column-width = "100%" />
        <fo:table-body>
          <fo:table-row>
            <fo:table-cell>
              <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" padding = "0pt">
                <fo:table-column column-width = "64mm" />
                <fo:table-column column-width = "118mm" />
                <fo:table-body>
                  <fo:table-row height = "6.8cm">
                    <!-- Premiere colonne 1 : logo + etablissement -->
                    <fo:table-cell>
                      <fo:table inline-progression-dimension = "64mm" table-layout = "fixed" space-before = "0pt" border-width = "0.5pt" border-style = "solid">
                        <fo:table-column column-width = "100%" />
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell border-bottom = "0.5pt solid"> <xsl:apply-templates select = "etablissement" /> </fo:table-cell>
                          </fo:table-row>
                          <fo:table-row height = "40mm">
                            <fo:table-cell padding = "3pt" display-align = "after"> <xsl:apply-templates select = "service" /> </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                    </fo:table-cell>
                    <!-- / logo + etablissement -->
                    <fo:table-cell height = "100%">
                      <!-- deuxieme colonne, séparée en deux -->
                      <fo:table inline-progression-dimension = "118mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid" height = "100%">
                        <fo:table-column column-width = "100%" />
                        <fo:table-body>
                          <!-- titre + cadre référence -->
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:table inline-progression-dimension = "11.8cm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                                <fo:table-column column-width = "52mm" />
                                <fo:table-column column-width = "66mm" />
                                <fo:table-body>
                                  <fo:table-row height = "29mm">
                                    <!-- titre -->
                                    <fo:table-cell>
                                      <fo:block font-weight = "bold" font-family = "Arial, Helvetica, sans-serif" font-size = "20pt" text-align = "center"><xsl:value-of select = "entete" /></fo:block>
                                    </fo:table-cell>
                                    <!-- /titre -->
                                    <!-- Texte Référence  + n° commande -->
                                    <fo:table-cell>
                                      <fo:table inline-progression-dimension = "66mm" table-layout = "fixed" space-before = "0pt" border-width = "0.5pt" border-style = "solid">
                                        <fo:table-column column-width = "100%" />
                                        <fo:table-body>
                                          <fo:table-row>
                                            <fo:table-cell padding = "3pt 3pt 3pt 3pt">
                                              <fo:block text-align = "center" linefeed-treatment = "preserve">R&amp;eacute;f&amp;eacute;rence &amp;agrave; rappeler&amp;br;OBLIGATOIREMENT sur la facture et le bon de livraison</fo:block>
                                            </fo:table-cell>
                                          </fo:table-row>
                                          <!-- n° commande -->
                                          <fo:table-row>
                                            <fo:table-cell>
                                              <fo:block text-align = "center" font-weight = "bold">Commande : <xsl:value-of select = "numero" /> </fo:block>
                                              <fo:block text-align = "center">Reference : <xsl:value-of select = "reference" /> </fo:block>
                                            </fo:table-cell>
                                          </fo:table-row>
                                          <!-- /n° commande -->
                                        </fo:table-body>
                                      </fo:table>
                                    </fo:table-cell>
                                    <!-- /Texte Référence  + n° commande -->
                                  </fo:table-row>
                                </fo:table-body>
                              </fo:table>
                            </fo:table-cell>
                          </fo:table-row>
                          <!-- / titre + cadre référence -->
                          <fo:table-row height = "2mm" />
                          <fo:table-row height = "35mm">
                            <fo:table-cell>
                              <!-- Icone + coordonnées fournisseurs  -->
                              <fo:table inline-progression-dimension = "11.8cm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                                <fo:table-column column-width = "43mm" />
                                <fo:table-column column-width = "75mm" />
                                <fo:table-body>
                                  <fo:table-row>
                                    <!-- Icone -->
                                    <fo:table-cell display-align = "center" text-align = "center">
                                      <xsl:call-template name = "write-icone-destination"> <xsl:with-param name = "destination" select = "'F'" /> </xsl:call-template>
                                    </fo:table-cell>
                                    <!-- /Icone -->
                                    <!-- Coordonnees fournisseur -->
                                    <fo:table-cell>
                                      <fo:table inline-progression-dimension = "80mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                                        <fo:table-column column-width = "100%" />
                                        <fo:table-body>
                                          <fo:table-row>
                                            <fo:table-cell>
                                              <fo:block text-align = "left" font-size = "12pt" font-style = "italic" font-weight = "bold" text-decoration = "underline" space-after = "3pt">Nom du fournisseur</fo:block>
                                              <fo:block linefeed-treatment = "preserve"> <xsl:value-of select = "fournisseur/nom" /> </fo:block>
                                              <xsl:apply-templates select = "fournisseur/adresse" />
                                            </fo:table-cell>
                                          </fo:table-row>
                                        </fo:table-body>
                                      </fo:table>
                                    </fo:table-cell>
                                    <!-- /Coordonnees fournisseur -->
                                  </fo:table-row>
                                </fo:table-body>
                              </fo:table>
                            </fo:table-cell>
                            <!-- / Icone + coordonnées fournisseurs  -->
                          </fo:table-row>
                          <!--  telephone fax fournisseur -->
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:table inline-progression-dimension = "11.8cm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
                                <fo:table-column column-width = "59mm" />
                                <fo:table-column column-width = "59mm" />
                                <fo:table-body>
                                  <fo:table-row>
                                    <fo:table-cell><fo:block text-align = "left" font-size = "10pt" font-weight = "normal" space-after = "3pt"> Tel. : </fo:block><xsl:apply-templates select = "fournisseur/telephone" /> </fo:table-cell>
                                    <fo:table-cell> <fo:block text-align = "left" font-size = "10pt" font-weight = "normal" space-after = "3pt"> fax. : </fo:block> <xsl:apply-templates select = "fournisseur/fax" /> </fo:table-cell>
                                  </fo:table-row>
                                </fo:table-body>
                              </fo:table>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                    </fo:table-cell>
                    <!-- /  deuxieme colonne -->
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:static-content>
    <fo:static-content flow-name = "xsl-region-start" />
    <fo:flow flow-name = "xsl-region-body" font-family = "Arial, Helvetica, sans-serif" font-size = "9pt">
      <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" border-width = "0pt" border-style = "solid">
        <fo:table-column width = "100%" />
        <fo:table-body>
          <fo:table-row height = "5mm">
            <fo:table-cell>
              <fo:block> </fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
      <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "0pt" padding = "0pt">
        <fo:table-column column-width = "100%" />
        <fo:table-body>
          <!-- le corps de la page -->
          <!-- Message fournisseur -->
          <fo:table-row>
            <fo:table-cell>
              <fo:block font-size = "11pt" space-before = "2pt" space-after = "2pt"> <xsl:apply-templates select = "messagefournisseur" /> </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <!-- /Message fournisseur -->
          <!-- Lignes de commandes -->
          <fo:table-row>
            <fo:table-cell> <xsl:apply-templates select = "articles" mode = "boncommandeF" /> </fo:table-cell>
          </fo:table-row>
          <!-- /Lignes de commandes -->
          <fo:table-row>
            <fo:table-cell display-align = "after">
              <fo:table inline-progression-dimension = "182mm" table-layout = "fixed" space-before = "2pt" border-width = "0pt" border-style = "solid">
                <fo:table-column column-width = "63mm" />
                <fo:table-column />
                <fo:table-column column-width = "119mm" />
                <fo:table-body>
                  <fo:table-row keep-with-next = "always">
                    <!-- colonne vide -->
                    <fo:table-cell />
                    <fo:table-cell />
                    <!-- /colonne vide -->
                    <fo:table-cell display-align = "before" border = "0pt solid">
                      <!-- totaux +blabla -->
                      <xsl:apply-templates select = "totaux" mode = "boncommandeF" />
                      <!-- totaux+blabla -->
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:flow>
  </xsl:template>
</xsl:stylesheet>