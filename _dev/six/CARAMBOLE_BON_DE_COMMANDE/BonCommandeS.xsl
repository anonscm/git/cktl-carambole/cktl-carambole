<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">


	<!-- TPL Une commande -->
	<xsl:template name="articlesS" match="articles" mode="boncommandeS">

		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0pt" border-style="solid">
			<fo:table-column width="100%" />
			<fo:table-body>
				<fo:table-row height="5mm">
					<fo:table-cell>
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<!-- les lignes de commande -->
		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0.5pt" border-style="solid">

			<fo:table-column column-width="90mm" border="0.5pt solid" />
			<fo:table-column column-width="34mm" border="0.5pt solid" />
			<fo:table-column column-width="10mm" border="0.5pt solid" />
			<fo:table-column column-width="22mm" border="0.5pt solid" />
			<fo:table-column column-width="26mm" border="0.5pt solid" />
			<fo:table-header font-weight="bold">
				<fo:table-row>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>D�signation des Travaux ou Fournitures</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>R�f�rence</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Qt�</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Prix Unitaire</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Montant</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body font-size="8pt">
				<xsl:apply-templates select="article" />
			</fo:table-body>
		</fo:table>

		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0pt" border-style="solid">
			<fo:table-column width="100%" />
			<fo:table-body>
				<fo:table-row height="5mm">
					<fo:table-cell>
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<!-- /les lignes de commande -->
	</xsl:template>


	<xsl:template name="cnsS" match="cns" mode="boncommandeS">

		<!-- les cns de la commande -->
		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0.5pt" border-style="solid">

			<fo:table-column column-width="32mm" border="0.5pt solid" />
			<fo:table-column column-width="100mm" border="0.5pt solid" />
			<fo:table-column column-width="25mm" border="0.5pt solid" />
			<fo:table-column column-width="25mm" border="0.5pt solid" />
			<fo:table-header font-weight="bold">
				<fo:table-row>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Code</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Libell� de nomenclature</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Montant HT</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.5pt solid" padding="3pt"
						text-align="center">
						<fo:block>Montant TTC</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body font-size="8pt">
				<xsl:apply-templates select="cn" />
			</fo:table-body>
		</fo:table>

		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0pt" border-style="solid">
			<fo:table-column width="100%" />
			<fo:table-body>
				<fo:table-row height="5mm">
					<fo:table-cell>
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<!-- /les cns de la commande -->
	</xsl:template>


	<!-- TPL les engages -->
	<xsl:template name="engagementsBS" match="engagements"
		mode="boncommandeS">
		<xsl:apply-templates select="engagement" mode="boncommandeS" />
	</xsl:template>


	<!-- un engagement -->
	<xsl:template name="engagementBS" match="engagement" mode="boncommandeS">
		<fo:table inline-progression-dimension="182mm" padding="5pt"
			table-layout="fixed" space-before="0pt" border-width="0.5pt"
			border-style="solid">
			<fo:table-column column-width="42mm" />
			<fo:table-column column-width="140mm" />
			<fo:table-body>

				<fo:table-row keep-with-next="always">
					<fo:table-cell font-size="10pt" font-weight="bold">
						<fo:block>
							Engagement :
							<xsl:value-of select="numero" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="10pt" font-weight="bold"
						text-align="left" display-align="center">
						<fo:block>
							Ligne budg�taire :
							<xsl:value-of select="ligneBudgetaire" />
							- Type de cr�dit :
							<xsl:value-of select="typeCreditCode" />
							- Taux de prorata :
							<xsl:value-of select="format-number(prorata,'# ##0,00')" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row keep-with-next="always">
					<fo:table-cell font-size="10pt" font-weight="bold">
						<fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="10pt" font-weight="bold"
						text-align="left" display-align="center">
						<fo:block>
							Montant budg�taire :
							<xsl:value-of select="format-number(montantBudgetaire,'# ##0,00')" />
							- Montant TTC :
							<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<xsl:apply-templates select="engagementsActions"
					mode="boncommandeS" />
				<xsl:apply-templates select="engagementsAnalytiques"
					mode="boncommandeS" />
				<xsl:apply-templates select="engagementsConventions"
					mode="boncommandeS" />
				<xsl:apply-templates select="engagementsHorsMarches"
					mode="boncommandeS" />
				<xsl:apply-templates select="engagementsPlanComptables"
					mode="boncommandeS" />
			</fo:table-body>
		</fo:table>

		<fo:table inline-progression-dimension="182mm" table-layout="fixed"
			space-before="0pt" border-width="0pt" border-style="solid">
			<fo:table-column border="0pt" />
			<fo:table-body>
				<fo:table-row height="2mm" />
			</fo:table-body>
		</fo:table>

	</xsl:template>


	<!-- TPL actions -->
	<xsl:template name="engagementsActionsBS" match="engagementsActions"
		mode="boncommandeS">
		<fo:table-row keep-with-next="always" padding="5pt"
			background-color="#DDDDDD">
			<fo:table-cell font-size="8pt" font-weight="bold"
				text-align="right" display-align="center">
				<fo:block>Actions :</fo:block>
			</fo:table-cell>
			<fo:table-cell />
		</fo:table-row>
		<fo:table-row keep-with-next="always">
			<fo:table-cell />
			<fo:table-cell font-size="8pt" text-align="left"
				display-align="center">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					space-before="0pt" border-width="0pt" border-style="solid"
					font-size="8pt">
					<fo:table-column column-width="100mm" border="0pt solid" />
					<fo:table-column column-width="40mm" border="0pt solid" />
					<fo:table-body>
						<xsl:apply-templates select="engagementAction"
							mode="boncommandeS" />
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template name="engagementActionBS" match="engagementAction"
		mode="boncommandeS">
		<fo:table-row>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="left">
				<fo:block>
					<xsl:value-of select="actionCode" />
					-
					<xsl:value-of select="actionLibelle" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="right">
				<fo:block>
					<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<!-- TPL analytiques -->
	<xsl:template name="engagementsAnalytiquesBS" match="engagementsAnalytiques"
		mode="boncommandeS">
		<fo:table-row keep-with-next="always" background-color="#DDDDDD"
			border-end-color="#000000">
			<fo:table-cell font-size="8pt" font-weight="bold"
				text-align="right" display-align="center">
				<fo:block>Codes analytiques :</fo:block>
			</fo:table-cell>
			<fo:table-cell />
		</fo:table-row>
		<fo:table-row keep-with-next="always">
			<fo:table-cell />
			<fo:table-cell font-size="8pt" text-align="left"
				display-align="center">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					space-before="0pt" border-width="0pt" border-style="solid"
					font-size="8pt">
					<fo:table-column column-width="100mm" border="0pt solid" />
					<fo:table-column column-width="40mm" border="0pt solid" />
					<fo:table-body>
						<xsl:apply-templates select="engagementAnalytique"
							mode="boncommandeS" />
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template name="engagementAnalytiqueBS" match="engagementAnalytique"
		mode="boncommandeS">
		<fo:table-row>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="left">
				<fo:block>
					<xsl:value-of select="analytiqueCode" />
					-
					<xsl:value-of select="analytiqueLibelle" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="right">
				<fo:block>
					<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<!-- TPL conventions -->
	<xsl:template name="engagementsConventionsBS" match="engagementsConventions"
		mode="boncommandeS">
		<fo:table-row keep-with-next="always" background-color="#DDDDDD"
			border-end-color="#000000">
			<fo:table-cell font-size="8pt" font-weight="bold"
				text-align="right" display-align="center">
				<fo:block>Conventions :</fo:block>
			</fo:table-cell>
			<fo:table-cell />
		</fo:table-row>
		<fo:table-row keep-with-next="always">
			<fo:table-cell />
			<fo:table-cell font-size="8pt" text-align="left"
				display-align="center">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					space-before="0pt" border-width="0pt" border-style="solid"
					font-size="8pt">
					<fo:table-column column-width="100mm" border="0pt solid" />
					<fo:table-column column-width="40mm" border="0pt solid" />
					<fo:table-body>
						<xsl:apply-templates select="engagementConvention"
							mode="boncommandeS" />
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template name="engagementConventionBS" match="engagementConvention"
		mode="boncommandeS">
		<fo:table-row>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="left">
				<fo:block>
					<xsl:value-of select="convention" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="right">
				<fo:block>
					<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<!-- TPL hors march�s -->
	<xsl:template name="engagementsHorsMarchesBS" match="engagementsHorsMarches"
		mode="boncommandeS">
		<fo:table-row keep-with-next="always" background-color="#DDDDDD"
			border-end-color="#000000">
			<fo:table-cell font-size="8pt" font-weight="bold"
				text-align="right" display-align="center">
				<fo:block>Codes nomenclatures :</fo:block>
			</fo:table-cell>
			<fo:table-cell />
		</fo:table-row>
		<fo:table-row keep-with-next="always">
			<fo:table-cell />
			<fo:table-cell font-size="8pt" text-align="left"
				display-align="center">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					space-before="0pt" border-width="0pt" border-style="solid"
					font-size="8pt">
					<fo:table-column column-width="100mm" border="0pt solid" />
					<fo:table-column column-width="40mm" border="0pt solid" />
					<fo:table-body>
						<xsl:apply-templates select="engagementHorsMarche"
							mode="boncommandeS" />
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template name="engagementHorsMarcheBS" match="engagementHorsMarche"
		mode="boncommandeS">
		<fo:table-row>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="left">
				<fo:block>
					<xsl:value-of select="nomenclatureCode" />
					-
					<xsl:value-of select="nomenclatureLibelle" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="right">
				<fo:block>
					<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<!-- TPL imputations -->
	<xsl:template name="engagementsPlanComptablesBS" match="engagementsPlanComptables"
		mode="boncommandeS">
		<fo:table-row keep-with-next="always" background-color="#DDDDDD"
			border-end-color="#000000">
			<fo:table-cell font-size="8pt" font-weight="bold"
				text-align="right" display-align="center">
				<fo:block>Imputations :</fo:block>
			</fo:table-cell>
			<fo:table-cell />
		</fo:table-row>
		<fo:table-row keep-with-next="always">
			<fo:table-cell />
			<fo:table-cell font-size="8pt" text-align="left"
				display-align="center">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					space-before="0pt" border-width="0pt" border-style="solid"
					font-size="8pt">
					<fo:table-column column-width="100mm" border="0pt solid" />
					<fo:table-column column-width="40mm" border="0pt solid" />
					<fo:table-body>
						<xsl:apply-templates select="engagementPlanComptable"
							mode="boncommandeS" />
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template name="engagementPlanComptableBS" match="engagementPlanComptable"
		mode="boncommandeS">
		<fo:table-row>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="left">
				<fo:block>
					<xsl:value-of select="imputationCode" />
					-
					<xsl:value-of select="imputationLibelle" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="0pt solid" padding="0pt"
				text-align="right">
				<fo:block>
					<xsl:value-of select="format-number(montantTtc,'# ##0,00')" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<!-- TPL Bon de commandeC (service financier) -->
	<xsl:template name="boncommandeS" match="commande" mode="boncommandeS">

		<fo:static-content flow-name="xsl-region-end">
		</fo:static-content>

		<fo:static-content flow-name="xsl-region-after">
			<fo:table inline-progression-dimension="182mm" table-layout="fixed"
				space-before="0pt" padding="0pt">
				<fo:table-column column-width="100%" />
				<fo:table-body>


					<fo:table-row>
						<fo:table-cell>
							<fo:block font-size="11pt" space-before="2pt"
								space-after="2pt">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row keep-with-next="always">
						<fo:table-cell display-align="after">
							<fo:table inline-progression-dimension="182mm"
								table-layout="fixed" space-before="5pt" border-width="0pt"
								border-style="solid">
								<fo:table-column column-width="63mm" />
								<fo:table-column />
								<fo:table-column column-width="119mm" />
								<fo:table-body>
									<fo:table-row keep-with-next="always">
										<fo:table-cell border="0.5pt solid"
											display-align="before" padding="2pt" height="70mm">

											<!-- bloc exercice + devis +march�+remarques -->
											<fo:table display-align="before">
												<fo:table-column />
												<fo:table-body>
													<fo:table-row keep-with-next="always">
														<fo:table-cell>
															<fo:block>
																<fo:inline font-weight="bold">Exercice : </fo:inline>&nbsp;
																<xsl:value-of select="exercice" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row keep-with-next="always"
														padding-top="6pt">
														<fo:table-cell>
															<fo:block>
																<fo:inline font-weight="bold">March�</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="6pt">
															<fo:block>
																N� march� :
																<xsl:value-of select="marche" />
															</fo:block>
															<fo:block>
																N� lot :
																<xsl:value-of select="lot" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row keep-with-next="always"
														padding-top="6pt">
														<fo:table-cell>
															<fo:block linefeed-treatment="preserve">
																<fo:inline font-weight="bold">Remarques : </fo:inline>
																<xsl:value-of select="observations" />
															</fo:block>

														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											<!-- /bloc exercice + devis +march�+remarques -->
										</fo:table-cell>

										<!-- colonne vide -->
										<fo:table-cell />
										<!-- /colonne vide -->
										<fo:table-cell display-align="before" border="0pt solid">
											<!-- totaux +blabla -->
											<fo:table inline-progression-dimension="119mm"
												table-layout="fixed" space-before="0pt" border-width="0pt"
												border-style="solid">
												<fo:table-column />
												<fo:table-body>
													<fo:table-row keep-with-next="always">
														<fo:table-cell display-align="before">
															<fo:table>
																<fo:table-column />
																<fo:table-body>
																	<fo:table-row keep-with-next="always">
																		<fo:table-cell padding="5pt"
																			display-align="before" text-align="right">
																			<!-- blabla -->
																			<xsl:call-template name="lieudate">
																				<xsl:with-param name="lieuimpression"
																					select="lieuimpression" />
																				<xsl:with-param name="dateimpression"
																					select="dateimpression" />
																			</xsl:call-template>
																		</fo:table-cell>
																	</fo:table-row>
																	<fo:table-row keep-with-next="always">
																		<fo:table-cell padding="5pt"
																			display-align="after">
																			<!--<fo:block>Les responsables des centres de responsabilit� 
																				ou des lignes budg�taires (signature n'engageant pas la responsabilit� juridique 
																				de l'Universit�)</fo:block> -->

																			<fo:table table-layout="fixed" space-before="0pt">
																				<fo:table-column />
																				<fo:table-column column-width="38mm" />
																				<fo:table-body>
																					<fo:table-row keep-with-next="always">
																						<fo:table-cell padding="5pt"
																							display-align="before">
																							<fo:block font-weight="bold">L'ordonnateur ou
																								ses d�l�gu�s</fo:block>
																							<xsl:apply-templates select="signataires" />
																						</fo:table-cell>
																						<fo:table-cell display-align="after"
																							text-align="center">
																							<fo:block font-size="7pt" space-before="40pt">
																								<xsl:call-template name="write-icone-destination">
																									<xsl:with-param name="destination"
																										select="'S'" />
																								</xsl:call-template>
																							</fo:block>
																							<fo:block font-size="7pt" space-before="2pt">Souche
																							</fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																				</fo:table-body>
																			</fo:table>

																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
															<!-- /blabla -->
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											<!--totaux+blabla -->
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>


				</fo:table-body>
			</fo:table>
		</fo:static-content>

		<fo:static-content flow-name="xsl-region-before">
			<fo:table inline-progression-dimension="182mm" table-layout="fixed"
				space-before="0pt" padding="0pt">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>

							<fo:table inline-progression-dimension="182mm"
								table-layout="fixed" space-before="0pt" padding="0pt">
								<fo:table-column column-width="64mm" />
								<fo:table-column column-width="118mm" />

								<fo:table-body>
									<fo:table-row>

										<!-- Premiere colonne 1 : logo + etablissement -->
										<fo:table-cell>
											<fo:table inline-progression-dimension="64mm"
												table-layout="fixed" space-before="0pt" border-width="0.5pt"
												border-style="solid">
												<fo:table-column column-width="100%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell border-bottom="0.5pt solid">
															<xsl:apply-templates select="etablissement" />
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row height="40mm">
														<fo:table-cell padding="3pt" display-align="after">
															<xsl:apply-templates select="service" />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										<!-- / logo + etablissement -->

										<fo:table-cell>
											<!-- deuxieme colonne, s�par�e en deux -->
											<fo:table inline-progression-dimension="118mm"
												table-layout="fixed" space-before="0pt" border-width="0pt"
												border-style="solid" height="100%">
												<fo:table-column column-width="100%" />
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<!-- titre + cadre r�f�rence -->
															<fo:table inline-progression-dimension="11.8cm"
																table-layout="fixed" space-before="0pt" border-width="0pt"
																border-style="solid">
																<fo:table-column column-width="52mm" />
																<fo:table-column column-width="66mm" />
																<fo:table-body>
																	<fo:table-row height="33mm">
																		<fo:table-cell>
																			<!-- titre -->
																			<fo:block font-weight="bold" font-size="20pt"
																				text-align="center">
																				<xsl:value-of select="entete" />
																			</fo:block>
																			<!-- n� commande -->
																			<fo:block text-align="center" font-size="20pt"
																				font-weight="bold">
																				Cde :
																				<xsl:value-of select="numero" />
																			</fo:block>
																			<fo:block text-align="center" font-size="12pt">
																				Ref :
																				<xsl:value-of select="reference" />
																			</fo:block>
																			<!-- /n� commande -->
																			<!-- /titre -->
																		</fo:table-cell>

																		<fo:table-cell>
																			<!-- Texte R�f�rence + n� commande -->
																			<fo:table inline-progression-dimension="66mm"
																				table-layout="fixed" space-before="0pt"
																				border-width="0.5pt" border-style="solid">
																				<fo:table-column column-width="100%" />
																				<fo:table-body>
																					<fo:table-row height="6mm">
																						<fo:table-cell padding="3pt 3pt 3pt 3pt">
																							<fo:block text-align="left"
																								linefeed-treatment="preserve">Facture N�</fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																					<fo:table-row height="6mm">
																						<fo:table-cell padding="3pt 3pt 3pt 3pt"
																							border-bottom="0.5pt solid">
																							<fo:block text-align="left"
																								linefeed-treatment="preserve"></fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																					<fo:table-row height="6mm">
																						<fo:table-cell>
																							<fo:block text-align="center" padding="3pt 3pt 3pt 3pt">Pay�e
																								par mandat N�</fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																					<fo:table-row height="6mm">
																						<fo:table-cell padding="3pt 3pt 3pt 3pt"
																							border-bottom="0.5pt solid">
																							<fo:block text-align="left"
																								linefeed-treatment="preserve"></fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																					<fo:table-row height="5mm">
																						<fo:table-cell padding="1pt 1pt 1pt 1pt"
																							display-align="after">
																							<fo:block text-align="center" font-size="7pt">
																								Cadre r�serv� � la comptabilit�</fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																				</fo:table-body>
																			</fo:table>
																			<!-- /Texte R�f�rence + n� commande -->
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>

															<!-- / titre + cadre r�f�rence -->
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row height="2mm" />
													<fo:table-row>
														<fo:table-cell>
															<!-- Icone + coordonn�es fournisseurs -->
															<fo:table inline-progression-dimension="11.8cm"
																table-layout="fixed" space-before="0pt" border-width="0pt"
																border-style="solid">
																<fo:table-column column-width="43mm" />
																<fo:table-column column-width="75mm" />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell display-align="center"
																			text-align="center">
																			<!-- Icone -->
																			<xsl:call-template name="write-icone-destination">
																				<xsl:with-param name="destination"
																					select="'S'" />
																			</xsl:call-template>
																			<!-- /Icone -->
																		</fo:table-cell>
																		<fo:table-cell height="34mm">
																			<!-- Coordonnees fournisseur -->
																			<fo:table inline-progression-dimension="80mm"
																				table-layout="fixed" space-before="0pt"
																				border-width="0pt" border-style="solid">
																				<fo:table-column column-width="100%" />
																				<fo:table-body>
																					<fo:table-row>
																						<fo:table-cell>
																							<fo:block text-align="left" font-size="12pt"
																								font-style="italic" font-weight="bold"
																								text-decoration="underline" space-after="3pt">Nom du
																								fournisseur</fo:block>
																							<fo:block linefeed-treatment="preserve">
																								<xsl:value-of select="fournisseur/nom" />
																								(
																								<xsl:value-of select="fournisseur/code" />
																								)
																							</fo:block>
																							<xsl:apply-templates select="fournisseur/adresse" />
																						</fo:table-cell>
																					</fo:table-row>
																				</fo:table-body>
																			</fo:table>
																			<!-- /Coordonnees fournisseur -->
																		</fo:table-cell>
																	</fo:table-row>
																	<fo:table-row height="12mm">
																		<fo:table-cell display-align="center"
																			text-align="center">
																		</fo:table-cell>
																		<fo:table-cell>
																			<fo:table table-layout="fixed" space-before="0pt"
																				border-width="0pt" border-style="solid">
																				<fo:table-column column-width="10mm" />
																				<fo:table-column column-width="30mm" />
																				<fo:table-column column-width="10mm" />
																				<fo:table-column column-width="30mm" />
																				<fo:table-body>
																					<fo:table-row>
																						<fo:table-cell display-align="before"
																							text-align="center">
																							<fo:block text-align="right" font-size="8pt"
																								font-weight="normal" space-after="0pt">T�l. :
																							</fo:block>
																						</fo:table-cell>
																						<fo:table-cell>
																							<fo:block text-align="left" font-size="8pt"
																								font-weight="normal" space-after="0pt">
																								<xsl:apply-templates select="fournisseur/telephones" />
																							</fo:block>
																						</fo:table-cell>
																						<fo:table-cell display-align="before"
																							text-align="right">
																							<fo:block text-align="left" font-size="8pt"
																								font-weight="normal" space-after="0pt">Fax. :
																							</fo:block>
																						</fo:table-cell>
																						<fo:table-cell>
																							<fo:block text-align="left" font-size="8pt"
																								font-weight="normal" space-after="0pt">
																								<xsl:apply-templates select="fournisseur/faxes" />
																							</fo:block>
																						</fo:table-cell>
																					</fo:table-row>
																				</fo:table-body>
																			</fo:table>
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>
														</fo:table-cell>
														<!-- / Icone + coordonn�es fournisseurs -->
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										<!-- / deuxieme colonne -->
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>

				</fo:table-body>
			</fo:table>

		</fo:static-content>
		<fo:static-content flow-name="xsl-region-start">
		</fo:static-content>


		<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif"
			font-size="9pt">

			<!-- le corps de la page -->
			<fo:table inline-progression-dimension="182mm" table-layout="fixed"
				space-before="0pt" border-width="0pt" border-style="solid">
				<fo:table-column width="100%" />
				<fo:table-body>
					<fo:table-row height="5mm">
						<fo:table-cell>
							<fo:block>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>

			<fo:table inline-progression-dimension="182mm" table-layout="fixed"
				space-before="0pt" padding="0pt">
				<fo:table-column column-width="100%" />
				<fo:table-body>


					<fo:table-row>
						<fo:table-cell>
							<fo:block font-size="11pt" space-before="2pt"
								space-after="2pt">
								R�f�r�nce :
								<xsl:value-of select="reference" />
								- Libell� :
								<xsl:value-of select="libelle" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<!-- Lignes de commandes -->
					<fo:table-row>
						<fo:table-cell>
							<xsl:apply-templates select="articles" mode="boncommandeS" />
						</fo:table-cell>
					</fo:table-row>
					<!-- /Lignes de commandes -->

					<xsl:if test="marche != ' ' and marche != ''">
						<!-- Liste des cns -->
						<fo:table-row>
							<fo:table-cell>
								<xsl:apply-templates select="cns" mode="boncommandeS" />
							</fo:table-cell>
						</fo:table-row>
						<!-- /Liste des cns -->
					</xsl:if>

					<!-- engagements -->
					<fo:table-row>
						<fo:table-cell>
							<xsl:apply-templates select="engagements"
								mode="boncommandeS" />
						</fo:table-cell>
					</fo:table-row>
					<!-- /engagements -->

					<fo:table-row keep-with-next="always">
						<fo:table-cell display-align="after">
							<fo:table inline-progression-dimension="182mm"
								table-layout="fixed" space-before="5pt" border-width="0pt"
								border-style="solid">
								<fo:table-column column-width="63mm" />
								<fo:table-column />
								<fo:table-column column-width="119mm" />
								<fo:table-body>
									<fo:table-row keep-with-next="always">
										<fo:table-cell />
										<!-- colonne vide -->
										<fo:table-cell />
										<!-- /colonne vide -->
										<fo:table-cell display-align="before" border="0pt solid"
											text-align="right">
											<!-- totaux -->
											<xsl:apply-templates select="totaux" />
											<!-- /totaux -->
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>

				</fo:table-body>
			</fo:table>

			<fo:block id="last-page" />


		</fo:flow>



	</xsl:template>

</xsl:stylesheet>
