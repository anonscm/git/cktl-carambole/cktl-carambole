<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <!-- d�finition des patterns pour la fonction format-number -->
    <xsl:decimal-format decimal-separator="," grouping-separator=" "/>

    <!-- Le fichier qui contient le template de creation de logo -->
    <xsl:include href="logouniv.xsl"/>
    
    
    <!--le format doit toujours �tre YYYY-MM-DDTHH:MM:SS-->
    <xsl:template name="write-date"> <xsl:param name="date" />
        <!--<xsl:if test="$date!=''"> <xsl:value-of select="concat(substring($date,9,2),'/',substring($date,6,2),'/',substring($date,1,4))"/> </xsl:if>-->
        <xsl:if test="$date!=''"> <xsl:value-of select="$date"/> </xsl:if>
    </xsl:template>
    
    
    <!-- Cr�e un icone avec une lettre � l'int�rieur d'un cercle-->
    <xsl:template name="icone-lettre-rond"> <xsl:param name="lettre" />
        <fo:instream-foreign-object content-width="auto" content-height="auto" padding="0" width="20mm" height="20mm" border-width="1pt" border-style="solid">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="20mm" height="20mm" padding="0">
                <circle cy="11mm" cx="11mm" r="8mm" style="fill: #000000; stroke: #000000"/>
                <circle cy="10mm" cx="10mm" r="8mm" style="fill: #FFFFFF; stroke: #000000;"/>
                <text y="12mm" x="8mm" style="font-family: , Helvetica, sans-serif; font-weight: bold; font-size: 20pt"><xsl:value-of select="$lettre"/></text> 
            </svg>
        </fo:instream-foreign-object>
    </xsl:template>
    
    
    <!-- Cr�e l'icone indiquant � qui est destin� l'exemplaire papier-->
    <xsl:template name="write-icone-destination"> <xsl:param name="destination" />
        <fo:table inline-progression-dimension="100%">
            <fo:table-column column-width="proportional-column-width(1)"/>
            <fo:table-column column-width="20mm"/>
            <fo:table-column column-width="proportional-column-width(1)"/>					
            <fo:table-body>
                <fo:table-row height="20mm">
                    <fo:table-cell/>
                    <fo:table-cell border="0pt solid"  font-size="20pt" padding="3pt" text-align="center" display-align="center">
                        <xsl:call-template name="icone-lettre-rond"><xsl:with-param name="lettre" select="$destination"/></xsl:call-template>
                    </fo:table-cell>
                    <fo:table-cell/>
                </fo:table-row>
            </fo:table-body>
        </fo:table>	
    </xsl:template>


    <!-- TPL lieu , date -->
    <xsl:template name="lieudate"> <xsl:param name="lieuimpression" /> <xsl:param name="dateimpression" />
        <fo:block space-before="1mm" text-align="left"><xsl:value-of select="$lieuimpression"/>, le&nbsp;<xsl:call-template name="write-date"><xsl:with-param name="date" select="$dateimpression"/></xsl:call-template></fo:block>
    </xsl:template>


   
    <!--*******************************************************************************-->
    
    <xsl:include href="BonCommandeF.xsl"/> 
    <xsl:include href="BonCommandeC.xsl"/>
     <xsl:include href="BonCommandeS.xsl"/>
    
    <!--******************************************************************************* -->
    
    
    <!-- TPL un code de nomenclature -->
    <xsl:template match="cn">
        <fo:table-row border="0.5pt solid">
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"> <fo:block> <xsl:value-of select="code"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="left"> <fo:block> <xsl:value-of select="libelle"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(ht,'0,00')"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(ttc, '0,00')"/> </fo:block> </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <!-- TPL un article -->
    <xsl:template match="article">
        <fo:table-row border="0.5pt solid">
            <fo:table-cell border="0.5pt solid" padding="2pt"> <fo:block> <xsl:value-of select="libelle"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"> <fo:block> <xsl:value-of select="reference"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"> <fo:block> <xsl:value-of select="format-number(quantite,'0,00')"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(prixUnitaireHt,'0,00')"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(prixTotalHt, '0,00')"/> </fo:block> </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <!-- TPL un article pour bon F en tenant du booleen pour afficher ou non les totaux -->
    <xsl:template match="article" mode="boncommandeF">
        <fo:table-row border="0.5pt solid">
            <fo:table-cell border="0.5pt solid" padding="2pt"> <fo:block> <xsl:value-of select="libelle"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"> <fo:block> <xsl:value-of select="reference"/> </fo:block> </fo:table-cell>
            <fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"> <fo:block> <xsl:value-of select="format-number(quantite,'0,00')"/> </fo:block> </fo:table-cell>
            <xsl:if test="../../nePasAfficherLeMontant = 'false'">
                <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(prixUnitaireHt,'0,00')"/> </fo:block> </fo:table-cell>
                <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block> <xsl:value-of select="format-number(prixTotalHt, '0,00')"/> </fo:block> </fo:table-cell>
            </xsl:if>
            <xsl:if test="../../nePasAfficherLeMontant = 'true'">
                <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block>  </fo:block> </fo:table-cell>
                <fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"> <fo:block>  </fo:block> </fo:table-cell>
            </xsl:if>
        </fo:table-row>
    </xsl:template>
 
    
    <!-- TPL signataires -->
    <xsl:template name="signataires" match="signataires">
        <xsl:apply-templates select="signataire"/>
    </xsl:template>	
    
    <!-- TPL signataire -->
    <xsl:template name="signataire" match="signataire">
        <fo:block><xsl:if test="titre!=''"><xsl:value-of select="titre"/>,&nbsp;</xsl:if><xsl:value-of select="prenom"/>&nbsp;<xsl:value-of select="nom"/></fo:block>
    </xsl:template>
    
    
  <!-- TPL Adresse-->   
  <xsl:template name="adresse" match="adresse">	
	<fo:block linefeed-treatment="preserve"><xsl:value-of select="adresse1"/></fo:block>
	<xsl:if test="adresse2!=''"> <fo:block linefeed-treatment="preserve"><xsl:value-of select="adresse2"/></fo:block> </xsl:if>
	<xsl:if test="boitePostale!=''"> <fo:block linefeed-treatment="preserve"><xsl:value-of select="boitePostale"/></fo:block> </xsl:if>
	<fo:block><xsl:value-of select="codePostal"/>&nbsp;<xsl:value-of select="ville"/></fo:block>
	<fo:block><xsl:value-of select="pays"/></fo:block>
      
   </xsl:template>
  
   
    <!-- -TPL Livraison-->  
    <xsl:template name="livraison" match="livraison">
        <fo:table  inline-progression-dimension="63mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
            <fo:table-column width="100%"/>
            <fo:table-body>
                <fo:table-row height="5mm" >
                    <fo:table-cell font-size="10pt" font-weight="bold" text-align="center" border-bottom="0.5pt solid" padding="2pt">
                        <fo:block>Adresse de livraison</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row height="20mm">
                    <fo:table-cell  padding="2pt" font-size="8pt" font-weight="bold" display-align="before">
                        <fo:block><xsl:value-of select="nom"/></fo:block>
                        <xsl:apply-templates select="adresse"/>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row height="5mm">
                    <fo:table-cell font-size="10pt" border-bottom="0.5pt solid" padding="2pt">
                        <fo:block>  
                            <xsl:if test="tel">
                                T�l :&nbsp;<xsl:value-of select="tel"/>&nbsp;
                            </xsl:if>
                            <xsl:if test="fax">
                                Fax :&nbsp;<xsl:value-of select="fax"/>
                            </xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>								
            </fo:table-body>
        </fo:table>      
    </xsl:template>   
    
    
    <!-- -TPL Observations -->  
    <xsl:template name="observations" match="observations">
        <fo:table  inline-progression-dimension="63mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
            <fo:table-column width="100%"/>
            <fo:table-body>
                <fo:table-row height="5mm">
                    <fo:table-cell font-size="10pt" font-weight="bold" text-align="center"  border-bottom="0.5pt solid" padding="2pt"> <fo:block>Observations</fo:block> </fo:table-cell>
                </fo:table-row>
                <fo:table-row height="8mm" padding="2pt" display-align="before">
                    <fo:table-cell  padding="2pt"  font-weight="bold" font-size="8pt"> <fo:block linefeed-treatment="preserve">Merci de joindre un RIB avec toute Facture n'en portant pas la mention d�taill�e.</fo:block> </fo:table-cell>
                </fo:table-row>								
                <fo:table-row height="22mm" padding="2pt" display-align="before">
                    <fo:table-cell  padding="2pt"  font-weight="bold" font-size="8pt"> <fo:block linefeed-treatment="preserve"><xsl:value-of select="."/></fo:block> </fo:table-cell>
                </fo:table-row>								
            </fo:table-body>
        </fo:table>      
    </xsl:template> 
    
 
 
 <!-- TPL infossuivi -->
 <xsl:template name="infossuivi" match="infossuivi">
    <xsl:if test="urlsuivi!=''">
	<fo:block font-size="7.5pt" font-weight="bold" space-before="0pt">L'acceptation du pr�sent bon de commande vaut acceptation des conditions g�n�rales d'achat de l'Universit� que vous trouverez sur le site internet</fo:block>
	<fo:block font-size="7pt" space-before="0pt">Pour consulter la situation de vos remboursements/factures :&nbsp;<xsl:value-of select="urlsuivi"/></fo:block>
		<fo:block font-size="7pt">Login :&nbsp;<xsl:value-of select="login"/>&nbsp;Mot de passe :&nbsp;<xsl:value-of select="passwd"/></fo:block>
	</xsl:if>
 </xsl:template>
   
    <!-- -TPL Totaux-->  
    <xsl:template name ="totaux" match="totaux">
        <fo:table>
            <fo:table-column/>
            <fo:table-body>
                <fo:table-row keep-with-next="always">
                    <fo:table-cell>
                        <fo:table>
                            <fo:table-column/> <fo:table-column/> <fo:table-column/>
                            <fo:table-body>
                                <fo:table-row keep-with-next="always">
                                    <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block>TOTAL HT</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block><xsl:value-of select="format-number(totalHt,'# ##0,00')"/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                
                                <fo:table-row keep-with-next="always">
                                    <fo:table-cell font-size="10pt" padding="2pt"  text-align="right"> <fo:block>TVA</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block><xsl:value-of select="format-number(totalTva,'# ##0,00')"/></fo:block> </fo:table-cell>
                                </fo:table-row>
                                
                                <fo:table-row padding="2pt" text-align="right" keep-with-next="always">
                                    <fo:table-cell font-size="10pt"> <fo:block>TOTAL TTC</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <fo:table-cell font-size="10pt" padding="2pt" text-align="right" font-weight="bold"> <fo:block><xsl:value-of select="format-number(totalTtc,'# ##0,00')"/></fo:block> </fo:table-cell>
                                </fo:table-row>	
                            </fo:table-body>
                        </fo:table>	
                    </fo:table-cell>
                </fo:table-row>	
                <fo:table-row keep-with-next="always">
                    <fo:table-cell text-align="right"><fo:block padding-left="10mm" space-before="5mm">Vu la d�pense engag�e pour un montant de</fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                    <fo:table-cell text-align="right"><fo:block padding-left="10mm" font-weight="bold"><xsl:value-of select="totalLettres" /> </fo:block></fo:table-cell>
                </fo:table-row>	
            </fo:table-body>
        </fo:table>	
        
    </xsl:template>   
    
    <!-- -TPL Totaux pour bon F en tenant compte du booleen pour afficher ou non les totaux -->  
    <xsl:template match="totaux" mode="boncommandeF">
        <fo:table>
            <fo:table-column/>
            <fo:table-body>
                <fo:table-row keep-with-next="always">
                    <fo:table-cell>
                        <fo:table>
                            <fo:table-column/> <fo:table-column/> <fo:table-column/>
                            <fo:table-body>
                                <fo:table-row keep-with-next="always">
                                    <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block>TOTAL HT</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <xsl:if test="../nePasAfficherLeMontant = 'false'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block><xsl:value-of select="format-number(totalHt,'# ##0,00')"/></fo:block></fo:table-cell>
                                    </xsl:if>
                                    <xsl:if test="../nePasAfficherLeMontant = 'true'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block></fo:block></fo:table-cell>
                                    </xsl:if>                                   
                                </fo:table-row>
                                
                                <fo:table-row keep-with-next="always">
                                    <fo:table-cell font-size="10pt" padding="2pt"  text-align="right"> <fo:block>TVA</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <xsl:if test="../nePasAfficherLeMontant = 'false'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block><xsl:value-of select="format-number(totalTva,'# ##0,00')"/></fo:block> </fo:table-cell>
                                    </xsl:if>
                                    <xsl:if test="../nePasAfficherLeMontant = 'true'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block></fo:block></fo:table-cell>
                                    </xsl:if>
                                </fo:table-row>
                                
                                <fo:table-row padding="2pt" text-align="right" keep-with-next="always">
                                    <fo:table-cell font-size="10pt"> <fo:block>TOTAL TTC</fo:block> </fo:table-cell>
                                    <fo:table-cell padding="2pt" text-align="right"> <fo:block><fo:leader leader-pattern="rule" rule-style="solid" leader-length="100%"/></fo:block> </fo:table-cell>
                                    <xsl:if test="../nePasAfficherLeMontant = 'false'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right" font-weight="bold"> <fo:block><xsl:value-of select="format-number(totalTtc,'# ##0,00')"/></fo:block> </fo:table-cell>
                                    </xsl:if>
                                    <xsl:if test="../nePasAfficherLeMontant = 'true'">
                                        <fo:table-cell font-size="10pt" padding="2pt" text-align="right"> <fo:block></fo:block></fo:table-cell>
                                    </xsl:if>
                                </fo:table-row>	
                            </fo:table-body>
                        </fo:table>	
                    </fo:table-cell>
                </fo:table-row>	
                <fo:table-row keep-with-next="always">
                    <fo:table-cell text-align="right"><fo:block padding-left="10mm" space-before="5mm">Vu la d�pense engag�e pour un montant de</fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                    <xsl:if test="../nePasAfficherLeMontant = 'false'">
                        <fo:table-cell text-align="right"><fo:block padding-left="10mm" font-weight="bold"><xsl:value-of select="totalLettres" /> </fo:block></fo:table-cell>
                    </xsl:if>
                    <xsl:if test="../nePasAfficherLeMontant = 'true'">
                        <fo:table-cell text-align="right"><fo:block padding-left="10mm" font-weight="bold"></fo:block></fo:table-cell>
                    </xsl:if>
                </fo:table-row>	
            </fo:table-body>
        </fo:table>	
        
    </xsl:template>   

    <!-- TPL etablissement -->
    <xsl:template name="etablissement" match="etablissement">
        <fo:table>
            <fo:table-column column-width="28mm"/> <fo:table-column/>
            <fo:table-body>
                <fo:table-row>
                    <!--  logo -->
                    <fo:table-cell padding="2pt"> <xsl:call-template name="bs-logo"/> </fo:table-cell>
                    <!--  /logo -->
                    
                    <!-- coordonnees etablissement-->
                    <fo:table-cell padding="3pt" font-size="10pt">
                        <fo:block font-weight="bold"><xsl:value-of select="nom"/></fo:block>
                        <fo:block space-before="2mm" font-size="8pt">SIRET : </fo:block>
                        <fo:block font-weight="bold" font-size="8pt"><xsl:value-of select="numerosiret"/></fo:block>
                        <fo:block space-before="2mm" font-size="8pt">TVA Intracommunautaire : </fo:block>
                        <fo:block font-weight="bold" font-size="8pt"><xsl:value-of select="tvaintracom"/></fo:block>
                    </fo:table-cell>
                    <!-- /coordonnees etablissement-->										  
                </fo:table-row>
            </fo:table-body>
        </fo:table>		
    </xsl:template>
    
    
    <!-- -TPL service -->  
    <xsl:template name="service" match="service">
        <fo:table font-size="10pt">
            <fo:table-column width="100%"/>
            <fo:table-body>
                <fo:table-row height="28mm">
                    <fo:table-cell  padding="2pt" font-weight="bold" display-align="before">
                        <fo:block><xsl:value-of select="nom"/></fo:block>
                        <xsl:apply-templates select="adresse"/>
                    </fo:table-cell>
                </fo:table-row>
                
                <fo:table-row height="5mm">
                    <fo:table-cell font-size="10pt" padding="2pt">
                        <fo:block>   <xsl:if test="tel">T�l :&nbsp;<xsl:value-of select="tel"/>&nbsp; </xsl:if> <xsl:if test="fax"> Fax :&nbsp;<xsl:value-of select="fax"/> </xsl:if> </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                
                <fo:table-row height="3mm">
                    <fo:table-cell padding="2pt" text-align="center" font-size="7pt"> <fo:block>Cachet du service ou du laboratoire</fo:block> </fo:table-cell>
                </fo:table-row>						
            </fo:table-body>
        </fo:table>      
    </xsl:template>  
    
 
 
    <!-- TPL root -->
    <xsl:template name="racine" match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="pageF" margin-top="1cm" margin-bottom="1cm" page-width="21cm" page-height="29.7cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-before extent="77mm" margin="0pt" />
                    <fo:region-after extent="84mm" />
                    <fo:region-start />
                    <fo:region-end />
                    <fo:region-body margin-bottom="80mm" margin-top="70mm" margin-left="0cm" margin-right="0cm" />
                </fo:simple-page-master>
                
                <fo:simple-page-master master-name="pageC" margin-top="1cm" margin-bottom="1cm" page-width="21cm" page-height="29.7cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-before extent="77mm" margin="0pt" />
                    <fo:region-after extent="70mm" />
                    <fo:region-start />
                    <fo:region-end />
                    <fo:region-body margin-bottom="70mm" margin-top="70mm" margin-left="0cm" margin-right="0cm" />
                </fo:simple-page-master>
                
                <fo:simple-page-master master-name="pageS" margin-top="1cm" margin-bottom="1cm" page-width="21cm" page-height="29.7cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-before extent="77mm" margin="0pt" />
                    <fo:region-after extent="70mm" />
                    <fo:region-start />
                    <fo:region-end />
                    <fo:region-body margin-bottom="70mm" margin-top="70mm" margin-left="0cm" margin-right="0cm" />
                </fo:simple-page-master>
            </fo:layout-master-set>		 
            
            <fo:page-sequence master-reference="pageF" >
                <xsl:apply-templates select="commande" mode="boncommandeF"/>
            </fo:page-sequence>
            
            <fo:page-sequence master-reference="pageC" >
                <xsl:apply-templates select="commande" mode="boncommandeC"/>
            </fo:page-sequence>		 
 
            <fo:page-sequence master-reference="pageS" >
                <xsl:apply-templates select="commande" mode="boncommandeS"/>
            </fo:page-sequence>
            
            
        </fo:root>
    </xsl:template>
     <xsl:template name = "telephones" match = "telephones">
      <xsl:apply-templates select="telephone"/>
     </xsl:template>
     <xsl:template name = "faxes" match = "faxes">
      <xsl:apply-templates select="fax"/>
     </xsl:template>
   <xsl:template name = "telephone" match = "telephone">
     <fo:block text-align = "left" font-size = "8pt" font-weight = "normal" linefeed-treatment = "preserve"><xsl:value-of select = "." /></fo:block>
  </xsl:template>
   <xsl:template name = "fax" match = "fax">
     <fo:block text-align = "left" font-size = "8pt" font-weight = "normal" linefeed-treatment = "preserve"><xsl:value-of select = "." /></fo:block>
  </xsl:template>
    
</xsl:stylesheet>





