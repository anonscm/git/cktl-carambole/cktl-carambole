<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template name="annexeA" match="commande" mode="annexe">
        <fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt">
          <fo:block>TEST ANNEXE</fo:block>
        </fo:flow>
    </xsl:template>

</xsl:stylesheet>
